<?php
/**
 * Retrieves and creates the configuration.php file.
 *
 * The permissions for the base directory must allow for writing files in order
 * for the configuration.php to be created using this page.
 *
 * @internal This file must be parsable by PHP4.
 *
 * @package system
 * @subpackage Administration
 */

/**
 * We are installing.
 */
define('LJMC_INSTALLING', true);

/**
 * We are blissfully unaware of anything.
 */
define('LJMC_SETUP_CONFIG', true);

/**
 * Disable error reporting
 *
 * Set this to error_reporting( -1 ) for debugging
 */
error_reporting(0);

define( 'ABSPATH', dirname( dirname( __FILE__ ) ) . '/' );

require( ABSPATH . 'system-setup.php' );

/** Load system Administration Upgrade API */
require_once( ABSPATH . 'ljmc-admin/includes/upgrade.php' );

/** Load system Translation Install API */
require_once( ABSPATH . 'ljmc-admin/includes/translation-install.php' );

nocache_headers();

// Support configuration.sample.php one level up, for the develop repo.
if ( file_exists( ABSPATH . 'configuration.sample.php' ) )
	$config_file = file( ABSPATH . 'configuration.sample.php' );
elseif ( file_exists( dirname( ABSPATH ) . '/configuration.sample.php' ) )
	$config_file = file( dirname( ABSPATH ) . '/configuration.sample.php' );
else
	ljmc_die( __( 'Sorry, I need a configuration.sample.php file to work from. Please re-upload this file from your system installation.' ) );

// Check if configuration.php has been created
if ( file_exists( ABSPATH . 'configuration.php' ) )
	ljmc_die( '<p>' . sprintf( __( "The file 'configuration.php' already exists. If you need to reset any of the configuration items in this file, please delete it first. You may try <a href='%s'>installing now</a>." ), 'install.php' ) . '</p>' );

// Check if configuration.php exists above the root directory but is not part of another install
if ( file_exists(ABSPATH . '../configuration.php' ) && ! file_exists( ABSPATH . '../system-setup.php' ) )
	ljmc_die( '<p>' . sprintf( __( "The file 'configuration.php' already exists one level above your system installation. If you need to reset any of the configuration items in this file, please delete it first. You may try <a href='install.php'>installing now</a>."), 'install.php' ) . '</p>' );

$step = isset( $_GET['step'] ) ? (int) $_GET['step'] : -1;

/**
 * Display setup configuration.php file header.
 *
 * @ignore
 * @since 2.3.0
 */
function setup_config_display_header( $body_classes = array() ) {
	global $ljmc_version;
	$body_classes = (array) $body_classes;
	$body_classes[] = 'ljmc-core-ui';
	if ( is_rtl() ) {
		$body_classes[] = 'rtl';
	}

	header( 'Content-Type: text/html; charset=utf-8' );
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"<?php if ( is_rtl() ) echo ' dir="rtl"'; ?>>
<head>
	<meta name="vieljmcort" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php _e( 'system &rsaquo; Setup Configuration File' ); ?></title>
	<?php ljmc_admin_css( 'install', true ); ?>
</head>
<body class="<?php echo implode( ' ', $body_classes ); ?>">
<?php
} // end function setup_config_display_header();

$language = '';
if ( ! empty( $_REQUEST['language'] ) ) {
	$language = preg_replace( '/[^a-zA-Z_]/', '', $_REQUEST['language'] );
} elseif ( isset( $GLOBALS['ljmc_local_package'] ) ) {
	$language = $GLOBALS['ljmc_local_package'];
}

switch($step) {
	case -1:
		if ( ljmc_can_install_language_pack() && empty( $language ) && ( $languages = ljmc_get_available_translations() ) ) {
			setup_config_display_header( 'language-chooser' );
			echo '<form id="setup" method="post" action="?step=0">';
			ljmc_install_language_form( $languages );
			echo '</form>';
			break;
		}

		// Deliberately fall through if we can't reach the translations API.

	case 0:
		if ( ! empty( $language ) ) {
			$loaded_language = ljmc_download_language_pack( $language );
			if ( $loaded_language ) {
				load_default_textdomain( $loaded_language );
				$GLOBALS['ljmc_locale'] = new LJMC_Locale();
			}
		}

		setup_config_display_header();
		$step_1 = 'setup-config.php?step=1';
		if ( isset( $_REQUEST['noapi'] ) ) {
			$step_1 .= '&amp;noapi';
		}
		if ( ! empty( $loaded_language ) ) {
			$step_1 .= '&amp;language=' . $loaded_language;
		}
?>
<h3><?php _e( '<strong>configuration.php</strong> file is missing.' ) ?></h3>
<p><?php _e( 'Don\'t worry. Please provide following items in next step & we will create it for you.' ) ?></p>
<ol>
	<li><?php _e( 'Database name' ); ?></li>
	<li><?php _e( 'Database username' ); ?></li>
	<li><?php _e( 'Database password' ); ?></li>
	<li><?php _e( 'Database host' ); ?></li>
	<li><?php _e( 'Table prefix (if you want to run more than one system in a single database)' ); ?></li>
</ol>
<p class="step"><a href="<?php echo $step_1; ?>" class="button button-large"><?php _e( 'Continue' ); ?></a></p>
<?php
	break;

	case 1:
		load_default_textdomain( $language );
		$GLOBALS['ljmc_locale'] = new LJMC_Locale();

		setup_config_display_header();
	?>
<form method="post" action="setup-config.php?step=2">
	<p><?php _e( "Please enter database credentials below:" ); ?></p>
	<table class="form-table">
		<tr>
			<th scope="row"><label for="dbname"><?php _e( 'Database Name' ); ?></label></th>
			<td><input name="dbname" id="dbname" type="text" size="25" value="ljmc" /></td>
			<td><?php _e( 'The name of the database.' ); ?></td>
		</tr>
		<tr>
			<th scope="row"><label for="uname"><?php _e( 'User Name' ); ?></label></th>
			<td><input name="uname" id="uname" type="text" size="25" value="<?php echo htmlspecialchars( _x( 'username', 'example username' ), ENT_QUOTES ); ?>" /></td>
			<td><?php _e( 'MySQL username' ); ?></td>
		</tr>
		<tr>
			<th scope="row"><label for="pwd"><?php _e( 'Password' ); ?></label></th>
			<td><input name="pwd" id="pwd" type="text" size="25" value="<?php echo htmlspecialchars( _x( 'password', 'example password' ), ENT_QUOTES ); ?>" autocomplete="off" /></td>
			<td><?php _e( 'Your MySQL password.' ); ?></td>
		</tr>
		<tr>
			<th scope="row"><label for="dbhost"><?php _e( 'Database Host' ); ?></label></th>
			<td><input name="dbhost" id="dbhost" type="text" size="25" value="localhost" /></td>
			<td><?php _e( 'Your hostname. <br /> <i><small>For ex., <strong>http://hostname.com</strong> should be written as <strong>hostname.com</strong> OR <strong>localhost</strong>' ); ?></td>
		</tr>
		<tr>
			<th scope="row"><label for="prefix"><?php _e( 'Table Prefix' ); ?></label></th>
			<td><input name="prefix" id="prefix" type="text" value="ljmc_" size="25" /></td>
			<td><?php _e( 'Database table prefix to use for multiple systems in one database.' ); ?></td>
		</tr>
	</table>
	<?php if ( isset( $_GET['noapi'] ) ) { ?><input name="noapi" type="hidden" value="1" /><?php } ?>
	<input type="hidden" name="language" value="<?php echo esc_attr( $language ); ?>" />
	<p class="step"><input name="submit" type="submit" value="<?php echo htmlspecialchars( __( 'Create' ), ENT_QUOTES ); ?>" class="button button-large" /></p>
</form>
<?php
	break;

	case 2:
	load_default_textdomain( $language );
	$GLOBALS['ljmc_locale'] = new LJMC_Locale();

	$dbname = trim( ljmc_unslash( $_POST[ 'dbname' ] ) );
	$uname = trim( ljmc_unslash( $_POST[ 'uname' ] ) );
	$pwd = trim( ljmc_unslash( $_POST[ 'pwd' ] ) );
	$dbhost = trim( ljmc_unslash( $_POST[ 'dbhost' ] ) );
	$prefix = trim( ljmc_unslash( $_POST[ 'prefix' ] ) );

	$step_1 = 'setup-config.php?step=1';
	$install = 'install.php';
	if ( isset( $_REQUEST['noapi'] ) ) {
		$step_1 .= '&amp;noapi';
	}

	if ( ! empty( $language ) ) {
		$step_1 .= '&amp;language=' . $language;
		$install .= '?language=' . $language;
	} else {
		$install .= '?language=en_US';
	}

	$tryagain_link = '</p><p class="step"><a href="' . $step_1 . '" onclick="javascript:history.go(-1);return false;" class="button button-large">' . __( 'Try again' ) . '</a>';

	if ( empty( $prefix ) )
		ljmc_die( __( '<strong>ERROR</strong>: "Table Prefix" must not be empty.' . $tryagain_link ) );

	// Validate $prefix: it can only contain letters, numbers and underscores.
	if ( preg_match( '|[^a-z0-9_]|i', $prefix ) )
		ljmc_die( __( '<strong>ERROR</strong>: "Table Prefix" can only contain numbers, letters, and underscores.' . $tryagain_link ) );

	// Test the db connection.
	/**#@+
	 * @ignore
	 */
	define('DB_NAME', $dbname);
	define('DB_USER', $uname);
	define('DB_PASSWORD', $pwd);
	define('DB_HOST', $dbhost);
	/**#@-*/

	// Re-construct $ljmcdb with these new values.
	unset( $ljmcdb );
	require_ljmc_db();

	/*
	 * The ljmcdb constructor bails when LJMC_SETUP_CONFIG is set, so we must
	 * fire this manually. We'll fail here if the values are no good.
	 */
	$ljmcdb->db_connect();

	if ( ! empty( $ljmcdb->error ) )
		ljmc_die( $ljmcdb->error->get_error_message() . $tryagain_link );

	// Fetch or generate keys and salts.
	$no_api = isset( $_POST['noapi'] );
	if ( ! $no_api ) {
		$secret_keys = ljmc_remote_get( 'https://api.localhost/secret-key/1.1/salt/' );
	}

	if ( $no_api || is_ljmc_error( $secret_keys ) ) {
		$secret_keys = array();
		for ( $i = 0; $i < 8; $i++ ) {
			$secret_keys[] = ljmc_generate_password( 64, true, true );
		}
	} else {
		$secret_keys = explode( "\n", ljmc_remote_retrieve_body( $secret_keys ) );
		foreach ( $secret_keys as $k => $v ) {
			$secret_keys[$k] = substr( $v, 28, 64 );
		}
	}

	$key = 0;
	// Not a PHP5-style by-reference foreach, as this file must be parseable by PHP4.
	foreach ( $config_file as $line_num => $line ) {
		if ( '$table_prefix  =' == substr( $line, 0, 16 ) ) {
			$config_file[ $line_num ] = '$table_prefix  = \'' . addcslashes( $prefix, "\\'" ) . "';\r\n";
			continue;
		}

		if ( ! preg_match( '/^define\(\'([A-Z_]+)\',([ ]+)/', $line, $match ) )
			continue;

		$constant = $match[1];
		$padding  = $match[2];

		switch ( $constant ) {
			case 'DB_NAME'     :
			case 'DB_USER'     :
			case 'DB_PASSWORD' :
			case 'DB_HOST'     :
				$config_file[ $line_num ] = "define('" . $constant . "'," . $padding . "'" . addcslashes( constant( $constant ), "\\'" ) . "');\r\n";
				break;
			case 'DB_CHARSET'  :
				if ( 'utf8mb4' === $ljmcdb->charset || ( ! $ljmcdb->charset && $ljmcdb->has_cap( 'utf8mb4' ) ) ) {
					$config_file[ $line_num ] = "define('" . $constant . "'," . $padding . "'utf8mb4');\r\n";
				}
				break;
			case 'AUTH_KEY'         :
			case 'SECURE_AUTH_KEY'  :
			case 'LOGGED_IN_KEY'    :
			case 'NONCE_KEY'        :
			case 'AUTH_SALT'        :
			case 'SECURE_AUTH_SALT' :
			case 'LOGGED_IN_SALT'   :
			case 'NONCE_SALT'       :
				$config_file[ $line_num ] = "define('" . $constant . "'," . $padding . "'" . $secret_keys[$key++] . "');\r\n";
				break;
		}
	}
	unset( $line );

	if ( ! is_writable(ABSPATH) ) :
		setup_config_display_header();
?>
<p><?php _e( "Sorry, but I can&#8217;t write the <code>configuration.php</code> file." ); ?></p>
<p><?php _e( 'You can create the <code>configuration.php</code> manually and paste the following text into it.' ); ?></p>
<textarea id="configuration" cols="98" rows="15" class="code" readonly="readonly"><?php
		foreach( $config_file as $line ) {
			echo htmlentities($line, ENT_COMPAT, 'UTF-8');
		}
?></textarea>
<p><?php _e( 'After you&#8217;ve done that, click &#8220;Run the install.&#8221;' ); ?></p>
<p class="step"><a href="<?php echo $install; ?>" class="button button-large"><?php _e( 'Run the install' ); ?></a></p>
<script>
(function(){
if ( ! /iPad|iPod|iPhone/.test( navigator.userAgent ) ) {
	var el = document.getElementById('configuration');
	el.focus();
	el.select();
}
})();
</script>
<?php
	else :
		/*
		 * If this file doesn't exist, then we are using the configuration.sample.php
		 * file one level up, which is for the develop repo.
		 */
		if ( file_exists( ABSPATH . 'configuration.sample.php' ) )
			$path_to_ljmc_config = ABSPATH . 'configuration.php';
		else
			$path_to_ljmc_config = dirname( ABSPATH ) . '/configuration.php';

		$handle = fopen( $path_to_ljmc_config, 'w' );
		foreach( $config_file as $line ) {
			fwrite( $handle, $line );
		}
		fclose( $handle );
		chmod( $path_to_ljmc_config, 0666 );
		setup_config_display_header();
?>
<p><?php _e( "<strong>configuration.php</strong> was created. Click <i>Install</i> to start the system installation." ); ?></p>

<p class="step"><a href="<?php echo $install; ?>" class="button button-large"><?php _e( 'Run the install' ); ?></a></p>
<?php
	endif;
	break;
}
?>
<?php ljmc_print_scripts( 'language-chooser' ); ?>
</body>
</html>
