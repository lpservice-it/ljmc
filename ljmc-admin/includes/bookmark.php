<?php
/**
 * system Bookmark Administration API
 *
 * @package system
 * @subpackage Administration
 */

/**
 * Add a link to using values provided in $_POST.
 *
 * @since 2.0.0
 *
 * @return int|LJMC_Error Value 0 or LJMC_Error on failure. The link ID on success.
 */
function add_link() {
	return edit_link();
}

/**
 * Update or insert a link using values provided in $_POST.
 *
 * @since 2.0.0
 *
 * @param int $link_id Optional. ID of the link to edit.
 * @return int|LJMC_Error Value 0 or LJMC_Error on failure. The link ID on success.
 */
function edit_link( $link_id = 0 ) {
	if ( !current_user_can( 'manage_links' ) )
		ljmc_die( __( 'Cheatin&#8217; uh?' ), 403 );

	$_POST['link_url'] = esc_html( $_POST['link_url'] );
	$_POST['link_url'] = esc_url($_POST['link_url']);
	$_POST['link_name'] = esc_html( $_POST['link_name'] );
	$_POST['link_image'] = esc_html( $_POST['link_image'] );
	$_POST['link_rss'] = esc_url($_POST['link_rss']);
	if ( !isset($_POST['link_visible']) || 'N' != $_POST['link_visible'] )
		$_POST['link_visible'] = 'Y';

	if ( !empty( $link_id ) ) {
		$_POST['link_id'] = $link_id;
		return ljmc_update_link( $_POST );
	} else {
		return ljmc_insert_link( $_POST );
	}
}

/**
 * Retrieve the default link for editing.
 *
 * @since 2.0.0
 *
 * @return stdClass Default link
 */
function get_default_link_to_edit() {
	$link = new stdClass;
	if ( isset( $_GET['linkurl'] ) )
		$link->link_url = esc_url( ljmc_unslash( $_GET['linkurl'] ) );
	else
		$link->link_url = '';

	if ( isset( $_GET['name'] ) )
		$link->link_name = esc_attr( ljmc_unslash( $_GET['name'] ) );
	else
		$link->link_name = '';

	$link->link_visible = 'Y';

	return $link;
}

/**
 * Delete link specified from database.
 *
 * @since 2.0.0
 *
 * @param int $link_id ID of the link to delete
 * @return bool True
 */
function ljmc_delete_link( $link_id ) {
	global $ljmcdb;
	/**
	 * Fires before a link is deleted.
	 *
	 * @since 2.0.0
	 *
	 * @param int $link_id ID of the link to delete.
	 */
	do_action( 'delete_link', $link_id );

	ljmc_delete_object_term_relationships( $link_id, 'link_category' );

	$ljmcdb->delete( $ljmcdb->links, array( 'link_id' => $link_id ) );
	/**
	 * Fires after a link has been deleted.
	 *
	 * @since 2.2.0
	 *
	 * @param int $link_id ID of the deleted link.
	 */
	do_action( 'deleted_link', $link_id );

	clean_bookmark_cache( $link_id );

	return true;
}

/**
 * Retrieves the link categories associated with the link specified.
 *
 * @since 2.1.0
 *
 * @param int $link_id Link ID to look up
 * @return array The requested link's categories
 */
function ljmc_get_link_cats( $link_id = 0 ) {

	$cats = ljmc_get_object_terms( $link_id, 'link_category', array('fields' => 'ids') );

	return array_unique( $cats );
}

/**
 * Retrieve link data based on ID.
 *
 * @since 2.0.0
 *
 * @param int $link_id ID of link to retrieve
 * @return object Link for editing
 */
function get_link_to_edit( $link_id ) {
	return get_bookmark( $link_id, OBJECT, 'edit' );
}

/**
 * This function inserts/updates links into/in the database.
 *
 * @since 2.0.0
 *
 * @param array $linkdata Elements that make up the link to insert.
 * @param bool $ljmc_error Optional. If true return LJMC_Error object on failure.
 * @return int|LJMC_Error Value 0 or LJMC_Error on failure. The link ID on success.
 */
function ljmc_insert_link( $linkdata, $ljmc_error = false ) {
	global $ljmcdb;

	$defaults = array( 'link_id' => 0, 'link_name' => '', 'link_url' => '', 'link_rating' => 0 );

	$args = ljmc_parse_args( $linkdata, $defaults );
	$r = ljmc_unslash( sanitize_bookmark( $args, 'db' ) );

	$link_id   = $r['link_id'];
	$link_name = $r['link_name'];
	$link_url  = $r['link_url'];

	$update = false;
	if ( ! empty( $link_id ) ) {
		$update = true;
	}

	if ( trim( $link_name ) == '' ) {
		if ( trim( $link_url ) != '' ) {
			$link_name = $link_url;
		} else {
			return 0;
		}
	}

	if ( trim( $link_url ) == '' ) {
		return 0;
	}

	$link_rating      = ( ! empty( $r['link_rating'] ) ) ? $r['link_rating'] : 0;
	$link_image       = ( ! empty( $r['link_image'] ) ) ? $r['link_image'] : '';
	$link_target      = ( ! empty( $r['link_target'] ) ) ? $r['link_target'] : '';
	$link_visible     = ( ! empty( $r['link_visible'] ) ) ? $r['link_visible'] : 'Y';
	$link_owner       = ( ! empty( $r['link_owner'] ) ) ? $r['link_owner'] : get_current_user_id();
	$link_notes       = ( ! empty( $r['link_notes'] ) ) ? $r['link_notes'] : '';
	$link_description = ( ! empty( $r['link_description'] ) ) ? $r['link_description'] : '';
	$link_rss         = ( ! empty( $r['link_rss'] ) ) ? $r['link_rss'] : '';
	$link_rel         = ( ! empty( $r['link_rel'] ) ) ? $r['link_rel'] : '';
	$link_category    = ( ! empty( $r['link_category'] ) ) ? $r['link_category'] : array();

	// Make sure we set a valid category
	if ( ! is_array( $link_category ) || 0 == count( $link_category ) ) {
		$link_category = array( get_option( 'default_link_category' ) );
	}

	if ( $update ) {
		if ( false === $ljmcdb->update( $ljmcdb->links, compact( 'link_url', 'link_name', 'link_image', 'link_target', 'link_description', 'link_visible', 'link_rating', 'link_rel', 'link_notes', 'link_rss' ), compact( 'link_id' ) ) ) {
			if ( $ljmc_error ) {
				return new LJMC_Error( 'db_update_error', __( 'Could not update link in the database' ), $ljmcdb->last_error );
			} else {
				return 0;
			}
		}
	} else {
		if ( false === $ljmcdb->insert( $ljmcdb->links, compact( 'link_url', 'link_name', 'link_image', 'link_target', 'link_description', 'link_visible', 'link_owner', 'link_rating', 'link_rel', 'link_notes', 'link_rss' ) ) ) {
			if ( $ljmc_error ) {
				return new LJMC_Error( 'db_insert_error', __( 'Could not insert link into the database' ), $ljmcdb->last_error );
			} else {
				return 0;
			}
		}
		$link_id = (int) $ljmcdb->insert_id;
	}

	ljmc_set_link_cats( $link_id, $link_category );

	if ( $update ) {
		/**
		 * Fires after a link was updated in the database.
		 *
		 * @since 2.0.0
		 *
		 * @param int $link_id ID of the link that was updated.
		 */
		do_action( 'edit_link', $link_id );
	} else {
		/**
		 * Fires after a link was added to the database.
		 *
		 * @since 2.0.0
		 *
		 * @param int $link_id ID of the link that was added.
		 */
		do_action( 'add_link', $link_id );
	}
	clean_bookmark_cache( $link_id );

	return $link_id;
}

/**
 * Update link with the specified link categories.
 *
 * @since 2.1.0
 *
 * @param int $link_id ID of link to update
 * @param array $link_categories Array of categories to
 */
function ljmc_set_link_cats( $link_id = 0, $link_categories = array() ) {
	// If $link_categories isn't already an array, make it one:
	if ( !is_array( $link_categories ) || 0 == count( $link_categories ) )
		$link_categories = array( get_option( 'default_link_category' ) );

	$link_categories = array_map( 'intval', $link_categories );
	$link_categories = array_unique( $link_categories );

	ljmc_set_object_terms( $link_id, $link_categories, 'link_category' );

	clean_bookmark_cache( $link_id );
}

/**
 * Update a link in the database.
 *
 * @since 2.0.0
 *
 * @param array $linkdata Link data to update.
 * @return int|LJMC_Error Value 0 or LJMC_Error on failure. The updated link ID on success.
 */
function ljmc_update_link( $linkdata ) {
	$link_id = (int) $linkdata['link_id'];

	$link = get_bookmark( $link_id, ARRAY_A );

	// Escape data pulled from DB.
	$link = ljmc_slash( $link );

	// Passed link category list overwrites existing category list if not empty.
	if ( isset( $linkdata['link_category'] ) && is_array( $linkdata['link_category'] )
			 && 0 != count( $linkdata['link_category'] ) )
		$link_cats = $linkdata['link_category'];
	else
		$link_cats = $link['link_category'];

	// Merge old and new fields with new fields overwriting old ones.
	$linkdata = array_merge( $link, $linkdata );
	$linkdata['link_category'] = $link_cats;

	return ljmc_insert_link( $linkdata );
}

/**
 * @since 3.5.0
 * @access private
 */
function ljmc_link_manager_disabled_message() {
	global $pagenow;
	if ( 'link-manager.php' != $pagenow && 'link-add.php' != $pagenow && 'link.php' != $pagenow )
		return;

	add_filter( 'pre_option_link_manager_enabled', '__return_true', 100 );
	$really_can_manage_links = current_user_can( 'manage_links' );
	remove_filter( 'pre_option_link_manager_enabled', '__return_true', 100 );

	if ( $really_can_manage_links && current_user_can( 'install_plugins' ) ) {
		$link = network_admin_url( 'plugin-install.php?tab=search&amp;s=Link+Manager' );
		ljmc_die( sprintf( __( 'If you are looking to use the link manager, please install the <a href="%s">Link Manager</a> plugin.' ), $link ) );
	}

	ljmc_die( __( 'You do not have sufficient permissions to edit the links for this site.' ) );
}
add_action( 'admin_page_access_denied', 'ljmc_link_manager_disabled_message' );
