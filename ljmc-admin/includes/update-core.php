<?php
/**
 * system core upgrade functionality.
 *
 * @package system
 * @subpackage Administration
 * @since 2.7.0
 */

/**
 * Stores files to be deleted.
 *
 * @since 2.7.0
 * @global array $_old_files
 * @var array
 * @name $_old_files
 */
global $_old_files;

$_old_files = array(
// 2.0
'ljmc-admin/import-b2.php',
'ljmc-admin/import-blogger.php',
'ljmc-admin/import-greymatter.php',
'ljmc-admin/import-livejournal.php',
'ljmc-admin/import-mt.php',
'ljmc-admin/import-rss.php',
'ljmc-admin/import-textpattern.php',
'ljmc-admin/quicktags.js',
'ljmc-images/fade-butt.png',
'ljmc-images/get-firefox.png',
'ljmc-images/header-shadow.png',
'ljmc-images/smilies',
'ljmc-images/ljmc-small.png',
'ljmc-images/ljmcminilogo.png',
'ljmc.php',
// 2.0.8
'inc/js/tinymce/plugins/inlinepopups/readme.txt',
// 2.1
'ljmc-admin/edit-form-ajax-cat.php',
'ljmc-admin/execute-pings.php',
'ljmc-admin/inline-uploading.php',
'ljmc-admin/link-categories.php',
'ljmc-admin/list-manipulation.js',
'ljmc-admin/list-manipulation.php',
'inc/comment-functions.php',
'inc/feed-functions.php',
'inc/functions-compat.php',
'inc/functions-formatting.php',
'inc/functions-post.php',
'inc/js/dbx-key.js',
'inc/js/tinymce/plugins/autosave/langs/cs.js',
'inc/js/tinymce/plugins/autosave/langs/sv.js',
'inc/links.php',
'inc/pluggable-functions.php',
'inc/template-functions-author.php',
'inc/template-functions-category.php',
'inc/template-functions-general.php',
'inc/template-functions-links.php',
'inc/template-functions-post.php',
'inc/ljmc-l10n.php',
// 2.2
'ljmc-admin/cat-js.php',
'ljmc-admin/import/b2.php',
'inc/js/autosave-js.php',
'inc/js/list-manipulation-js.php',
'inc/js/ljmc-ajax-js.php',
// 2.3
'ljmc-admin/admin-db.php',
'ljmc-admin/cat.js',
'ljmc-admin/categories.js',
'ljmc-admin/custom-fields.js',
'ljmc-admin/dbx-admin-key.js',
'ljmc-admin/edit-comments.js',
'ljmc-admin/install-rtl.css',
'ljmc-admin/install.css',
'ljmc-admin/upgrade-schema.php',
'ljmc-admin/upload-functions.php',
'ljmc-admin/upload-rtl.css',
'ljmc-admin/upload.css',
'ljmc-admin/upload.js',
'ljmc-admin/users.js',
'ljmc-admin/widgets-rtl.css',
'ljmc-admin/widgets.css',
'ljmc-admin/xfn.js',
'inc/js/tinymce/license.html',
// 2.5
'ljmc-admin/css/upload.css',
'ljmc-admin/images/box-bg-left.gif',
'ljmc-admin/images/box-bg-right.gif',
'ljmc-admin/images/box-bg.gif',
'ljmc-admin/images/box-butt-left.gif',
'ljmc-admin/images/box-butt-right.gif',
'ljmc-admin/images/box-butt.gif',
'ljmc-admin/images/box-head-left.gif',
'ljmc-admin/images/box-head-right.gif',
'ljmc-admin/images/box-head.gif',
'ljmc-admin/images/heading-bg.gif',
'ljmc-admin/images/login-bkg-bottom.gif',
'ljmc-admin/images/login-bkg-tile.gif',
'ljmc-admin/images/notice.gif',
'ljmc-admin/images/toggle.gif',
'ljmc-admin/includes/upload.php',
'ljmc-admin/js/dbx-admin-key.js',
'ljmc-admin/js/link-cat.js',
'ljmc-admin/profile-update.php',
'ljmc-admin/templates.php',
'inc/images/wlw/WpComments.png',
'inc/images/wlw/WpIcon.png',
'inc/images/wlw/WpWatermark.png',
'inc/js/dbx.js',
'inc/js/fat.js',
'inc/js/list-manipulation.js',
'inc/js/tinymce/langs/en.js',
'inc/js/tinymce/plugins/autosave/editor_plugin_src.js',
'inc/js/tinymce/plugins/autosave/langs',
'inc/js/tinymce/plugins/directionality/images',
'inc/js/tinymce/plugins/directionality/langs',
'inc/js/tinymce/plugins/inlinepopups/css',
'inc/js/tinymce/plugins/inlinepopups/images',
'inc/js/tinymce/plugins/inlinepopups/jscripts',
'inc/js/tinymce/plugins/paste/images',
'inc/js/tinymce/plugins/paste/jscripts',
'inc/js/tinymce/plugins/paste/langs',
'inc/js/tinymce/plugins/spellchecker/classes/HttpClient.class.php',
'inc/js/tinymce/plugins/spellchecker/classes/TinyGoogleSpell.class.php',
'inc/js/tinymce/plugins/spellchecker/classes/TinyPspell.class.php',
'inc/js/tinymce/plugins/spellchecker/classes/TinyPspellShell.class.php',
'inc/js/tinymce/plugins/spellchecker/css/spellchecker.css',
'inc/js/tinymce/plugins/spellchecker/images',
'inc/js/tinymce/plugins/spellchecker/langs',
'inc/js/tinymce/plugins/spellchecker/tinyspell.php',
'inc/js/tinymce/plugins/ljmc/images',
'inc/js/tinymce/plugins/ljmc/langs',
'inc/js/tinymce/plugins/ljmc/ljmc.css',
'inc/js/tinymce/plugins/ljmchelp',
'inc/js/tinymce/themes/advanced/css',
'inc/js/tinymce/themes/advanced/images',
'inc/js/tinymce/themes/advanced/jscripts',
'inc/js/tinymce/themes/advanced/langs',
// 2.5.1
'inc/js/tinymce/tiny_mce_gzip.php',
// 2.6
'ljmc-admin/bookmarklet.php',
'inc/js/jquery/jquery.dimensions.min.js',
'inc/js/tinymce/plugins/ljmc/popups.css',
'inc/js/ljmc-ajax.js',
// 2.7
'ljmc-admin/css/press-this-ie-rtl.css',
'ljmc-admin/css/press-this-ie.css',
'ljmc-admin/css/upload-rtl.css',
'ljmc-admin/edit-form.php',
'ljmc-admin/images/comment-pill.gif',
'ljmc-admin/images/comment-stalk-classic.gif',
'ljmc-admin/images/comment-stalk-fresh.gif',
'ljmc-admin/images/comment-stalk-rtl.gif',
'ljmc-admin/images/del.png',
'ljmc-admin/images/gear.png',
'ljmc-admin/images/media-button-gallery.gif',
'ljmc-admin/images/media-buttons.gif',
'ljmc-admin/images/postbox-bg.gif',
'ljmc-admin/images/tab.png',
'ljmc-admin/images/tail.gif',
'ljmc-admin/js/forms.js',
'ljmc-admin/js/upload.js',
'ljmc-admin/link-import.php',
'inc/images/audio.png',
'inc/images/css.png',
'inc/images/default.png',
'inc/images/doc.png',
'inc/images/exe.png',
'inc/images/html.png',
'inc/images/js.png',
'inc/images/pdf.png',
'inc/images/swf.png',
'inc/images/tar.png',
'inc/images/text.png',
'inc/images/video.png',
'inc/images/zip.png',
'inc/js/tinymce/tiny_mce_config.php',
'inc/js/tinymce/tiny_mce_ext.js',
// 2.8
'ljmc-admin/js/users.js',
'inc/js/swfupload/plugins/swfupload.documentready.js',
'inc/js/swfupload/plugins/swfupload.graceful_degradation.js',
'inc/js/swfupload/swfupload_f9.swf',
'inc/js/tinymce/plugins/autosave',
'inc/js/tinymce/plugins/paste/css',
'inc/js/tinymce/utils/mclayer.js',
'inc/js/tinymce/ljmc.css',
// 2.8.5
'ljmc-admin/import/btt.php',
'ljmc-admin/import/jkw.php',
// 2.9
'ljmc-admin/js/page.dev.js',
'ljmc-admin/js/page.js',
'ljmc-admin/js/set-post-thumbnail-handler.dev.js',
'ljmc-admin/js/set-post-thumbnail-handler.js',
'ljmc-admin/js/slug.dev.js',
'ljmc-admin/js/slug.js',
'inc/gettext.php',
'inc/js/tinymce/plugins/ljmc/js',
'inc/streams.php',
// MU
'README.txt',
'htaccess.dist',
'index-install.php',
'ljmc-admin/css/mu-rtl.css',
'ljmc-admin/css/mu.css',
'ljmc-admin/images/site-admin.png',
'ljmc-admin/includes/mu.php',
'ljmc-admin/ljmcmu-admin.php',
'ljmc-admin/ljmcmu-blogs.php',
'ljmc-admin/ljmcmu-edit.php',
'ljmc-admin/ljmcmu-options.php',
'ljmc-admin/ljmcmu-themes.php',
'ljmc-admin/ljmcmu-upgrade-site.php',
'ljmc-admin/ljmcmu-users.php',
'inc/images/ljmc-mu.png',
'inc/ljmcmu-default-filters.php',
'inc/ljmcmu-functions.php',
'ljmcmu-settings.php',
// 3.0
'ljmc-admin/categories.php',
'ljmc-admin/edit-category-form.php',
'ljmc-admin/edit-page-form.php',
'ljmc-admin/edit-pages.php',
'ljmc-admin/images/admin-header-footer.png',
'ljmc-admin/images/browse-happy.gif',
'ljmc-admin/images/ico-add.png',
'ljmc-admin/images/ico-close.png',
'ljmc-admin/images/ico-edit.png',
'ljmc-admin/images/ico-vieljmcage.png',
'ljmc-admin/images/fav-top.png',
'ljmc-admin/images/screen-options-left.gif',
'ljmc-admin/images/ljmc-logo-vs.gif',
'ljmc-admin/images/ljmc-logo.gif',
'ljmc-admin/import',
'ljmc-admin/js/ljmc-gears.dev.js',
'ljmc-admin/js/ljmc-gears.js',
'ljmc-admin/options-misc.php',
'ljmc-admin/page-new.php',
'ljmc-admin/page.php',
'ljmc-admin/rtl.css',
'ljmc-admin/rtl.dev.css',
'ljmc-admin/update-links.php',
'ljmc-admin/ljmc-admin.css',
'ljmc-admin/ljmc-admin.dev.css',
'inc/js/codepress',
'inc/js/codepress/engines/khtml.js',
'inc/js/codepress/engines/older.js',
'inc/js/jquery/autocomplete.dev.js',
'inc/js/jquery/autocomplete.js',
'inc/js/jquery/interface.js',
'inc/js/scriptaculous/prototype.js',
'inc/js/tinymce/ljmc-tinymce.js',
// 3.1
'ljmc-admin/edit-attachment-rows.php',
'ljmc-admin/edit-link-categories.php',
'ljmc-admin/edit-link-category-form.php',
'ljmc-admin/edit-post-rows.php',
'ljmc-admin/images/button-grad-active-vs.png',
'ljmc-admin/images/button-grad-vs.png',
'ljmc-admin/images/fav-arrow-vs-rtl.gif',
'ljmc-admin/images/fav-arrow-vs.gif',
'ljmc-admin/images/fav-top-vs.gif',
'ljmc-admin/images/list-vs.png',
'ljmc-admin/images/screen-options-right-up.gif',
'ljmc-admin/images/screen-options-right.gif',
'ljmc-admin/images/visit-site-button-grad-vs.gif',
'ljmc-admin/images/visit-site-button-grad.gif',
'ljmc-admin/link-category.php',
'ljmc-admin/sidebar.php',
'inc/classes.php',
'inc/js/tinymce/blank.htm',
'inc/js/tinymce/plugins/media/css/content.css',
'inc/js/tinymce/plugins/media/img',
'inc/js/tinymce/plugins/safari',
// 3.2
'ljmc-admin/images/logo-login.gif',
'ljmc-admin/images/star.gif',
'ljmc-admin/js/list-table.dev.js',
'ljmc-admin/js/list-table.js',
'inc/default-embeds.php',
'inc/js/tinymce/plugins/ljmc/img/help.gif',
'inc/js/tinymce/plugins/ljmc/img/more.gif',
'inc/js/tinymce/plugins/ljmc/img/toolbars.gif',
'inc/js/tinymce/themes/advanced/img/fm.gif',
'inc/js/tinymce/themes/advanced/img/sflogo.png',
// 3.3
'ljmc-admin/css/colors-classic-rtl.css',
'ljmc-admin/css/colors-classic-rtl.dev.css',
'ljmc-admin/css/colors-fresh-rtl.css',
'ljmc-admin/css/colors-fresh-rtl.dev.css',
'ljmc-admin/css/dashboard-rtl.dev.css',
'ljmc-admin/css/dashboard.dev.css',
'ljmc-admin/css/global-rtl.css',
'ljmc-admin/css/global-rtl.dev.css',
'ljmc-admin/css/global.css',
'ljmc-admin/css/global.dev.css',
'ljmc-admin/css/install-rtl.dev.css',
'ljmc-admin/css/login-rtl.dev.css',
'ljmc-admin/css/login.dev.css',
'ljmc-admin/css/ms.css',
'ljmc-admin/css/ms.dev.css',
'ljmc-admin/css/nav-menu-rtl.css',
'ljmc-admin/css/nav-menu-rtl.dev.css',
'ljmc-admin/css/nav-menu.css',
'ljmc-admin/css/nav-menu.dev.css',
'ljmc-admin/css/plugin-install-rtl.css',
'ljmc-admin/css/plugin-install-rtl.dev.css',
'ljmc-admin/css/plugin-install.css',
'ljmc-admin/css/plugin-install.dev.css',
'ljmc-admin/css/press-this-rtl.dev.css',
'ljmc-admin/css/press-this.dev.css',
'ljmc-admin/css/theme-editor-rtl.css',
'ljmc-admin/css/theme-editor-rtl.dev.css',
'ljmc-admin/css/theme-editor.css',
'ljmc-admin/css/theme-editor.dev.css',
'ljmc-admin/css/theme-install-rtl.css',
'ljmc-admin/css/theme-install-rtl.dev.css',
'ljmc-admin/css/theme-install.css',
'ljmc-admin/css/theme-install.dev.css',
'ljmc-admin/css/widgets-rtl.dev.css',
'ljmc-admin/css/widgets.dev.css',
'ljmc-admin/includes/internal-linking.php',
'inc/images/admin-bar-sprite-rtl.png',
'inc/js/jquery/ui.button.js',
'inc/js/jquery/ui.core.js',
'inc/js/jquery/ui.dialog.js',
'inc/js/jquery/ui.draggable.js',
'inc/js/jquery/ui.droppable.js',
'inc/js/jquery/ui.mouse.js',
'inc/js/jquery/ui.position.js',
'inc/js/jquery/ui.resizable.js',
'inc/js/jquery/ui.selectable.js',
'inc/js/jquery/ui.sortable.js',
'inc/js/jquery/ui.tabs.js',
'inc/js/jquery/ui.widget.js',
'inc/js/l10n.dev.js',
'inc/js/l10n.js',
'inc/js/tinymce/plugins/ljmclink/css',
'inc/js/tinymce/plugins/ljmclink/img',
'inc/js/tinymce/plugins/ljmclink/js',
'inc/js/tinymce/themes/advanced/img/ljmcicons.png',
'inc/js/tinymce/themes/advanced/skins/ljmc_theme/img/butt2.png',
'inc/js/tinymce/themes/advanced/skins/ljmc_theme/img/button_bg.png',
'inc/js/tinymce/themes/advanced/skins/ljmc_theme/img/down_arrow.gif',
'inc/js/tinymce/themes/advanced/skins/ljmc_theme/img/fade-butt.png',
'inc/js/tinymce/themes/advanced/skins/ljmc_theme/img/separator.gif',
// Don't delete, yet: 'ljmc-rss.php',
// Don't delete, yet: 'ljmc-rdf.php',
// Don't delete, yet: 'ljmc-rss2.php',
// Don't delete, yet: 'ljmc-commentsrss2.php',
// Don't delete, yet: 'ljmc-atom.php',
// Don't delete, yet: 'ljmc-feed.php',
// 3.4
'ljmc-admin/images/gray-star.png',
'ljmc-admin/images/logo-login.png',
'ljmc-admin/images/star.png',
'ljmc-admin/index-extra.php',
'ljmc-admin/network/index-extra.php',
'ljmc-admin/user/index-extra.php',
'ljmc-admin/images/screenshots/admin-flyouts.png',
'ljmc-admin/images/screenshots/coediting.png',
'ljmc-admin/images/screenshots/drag-and-drop.png',
'ljmc-admin/images/screenshots/help-screen.png',
'ljmc-admin/images/screenshots/media-icon.png',
'ljmc-admin/images/screenshots/new-feature-pointer.png',
'ljmc-admin/images/screenshots/welcome-screen.png',
'inc/css/editor-buttons.css',
'inc/css/editor-buttons.dev.css',
'inc/js/tinymce/plugins/paste/blank.htm',
'inc/js/tinymce/plugins/ljmc/css',
'inc/js/tinymce/plugins/ljmc/editor_plugin.dev.js',
'inc/js/tinymce/plugins/ljmc/img/embedded.png',
'inc/js/tinymce/plugins/ljmc/img/more_bug.gif',
'inc/js/tinymce/plugins/ljmc/img/page_bug.gif',
'inc/js/tinymce/plugins/ljmcdialogs/editor_plugin.dev.js',
'inc/js/tinymce/plugins/ljmceditimage/css/editimage-rtl.css',
'inc/js/tinymce/plugins/ljmceditimage/editor_plugin.dev.js',
'inc/js/tinymce/plugins/ljmcfullscreen/editor_plugin.dev.js',
'inc/js/tinymce/plugins/ljmcgallery/editor_plugin.dev.js',
'inc/js/tinymce/plugins/ljmcgallery/img/gallery.png',
'inc/js/tinymce/plugins/ljmclink/editor_plugin.dev.js',
// Don't delete, yet: 'ljmc-pass.php',
// Don't delete, yet: 'ljmc-register.php',
// 3.5
'ljmc-admin/gears-manifest.php',
'ljmc-admin/includes/manifest.php',
'ljmc-admin/images/archive-link.png',
'ljmc-admin/images/blue-grad.png',
'ljmc-admin/images/button-grad-active.png',
'ljmc-admin/images/button-grad.png',
'ljmc-admin/images/ed-bg-vs.gif',
'ljmc-admin/images/ed-bg.gif',
'ljmc-admin/images/fade-butt.png',
'ljmc-admin/images/fav-arrow-rtl.gif',
'ljmc-admin/images/fav-arrow.gif',
'ljmc-admin/images/fav-vs.png',
'ljmc-admin/images/fav.png',
'ljmc-admin/images/gray-grad.png',
'ljmc-admin/images/loading-publish.gif',
'ljmc-admin/images/logo-ghost.png',
'ljmc-admin/images/logo.gif',
'ljmc-admin/images/menu-arrow-frame-rtl.png',
'ljmc-admin/images/menu-arrow-frame.png',
'ljmc-admin/images/menu-arrows.gif',
'ljmc-admin/images/menu-bits-rtl-vs.gif',
'ljmc-admin/images/menu-bits-rtl.gif',
'ljmc-admin/images/menu-bits-vs.gif',
'ljmc-admin/images/menu-bits.gif',
'ljmc-admin/images/menu-dark-rtl-vs.gif',
'ljmc-admin/images/menu-dark-rtl.gif',
'ljmc-admin/images/menu-dark-vs.gif',
'ljmc-admin/images/menu-dark.gif',
'ljmc-admin/images/required.gif',
'ljmc-admin/images/screen-options-toggle-vs.gif',
'ljmc-admin/images/screen-options-toggle.gif',
'ljmc-admin/images/toggle-arrow-rtl.gif',
'ljmc-admin/images/toggle-arrow.gif',
'ljmc-admin/images/upload-classic.png',
'ljmc-admin/images/upload-fresh.png',
'ljmc-admin/images/white-grad-active.png',
'ljmc-admin/images/white-grad.png',
'ljmc-admin/images/widgets-arrow-vs.gif',
'ljmc-admin/images/widgets-arrow.gif',
'ljmc-admin/images/ljmcspin_dark.gif',
'inc/images/upload.png',
'inc/js/prototype.js',
'inc/js/scriptaculous',
'ljmc-admin/css/ljmc-admin-rtl.dev.css',
'ljmc-admin/css/ljmc-admin.dev.css',
'ljmc-admin/css/media-rtl.dev.css',
'ljmc-admin/css/media.dev.css',
'ljmc-admin/css/colors-classic.dev.css',
'ljmc-admin/css/customize-controls-rtl.dev.css',
'ljmc-admin/css/customize-controls.dev.css',
'ljmc-admin/css/ie-rtl.dev.css',
'ljmc-admin/css/ie.dev.css',
'ljmc-admin/css/install.dev.css',
'ljmc-admin/css/colors-fresh.dev.css',
'inc/js/customize-base.dev.js',
'inc/js/json2.dev.js',
'inc/js/comment-reply.dev.js',
'inc/js/customize-preview.dev.js',
'inc/js/ljmclink.dev.js',
'inc/js/tw-sack.dev.js',
'inc/js/ljmc-list-revisions.dev.js',
'inc/js/autosave.dev.js',
'inc/js/admin-bar.dev.js',
'inc/js/quicktags.dev.js',
'inc/js/ljmc-ajax-response.dev.js',
'inc/js/ljmc-pointer.dev.js',
'inc/js/hoverIntent.dev.js',
'inc/js/colorpicker.dev.js',
'inc/js/ljmc-lists.dev.js',
'inc/js/customize-loader.dev.js',
'inc/js/jquery/jquery.table-hotkeys.dev.js',
'inc/js/jquery/jquery.color.dev.js',
'inc/js/jquery/jquery.color.js',
'inc/js/jquery/jquery.hotkeys.dev.js',
'inc/js/jquery/jquery.form.dev.js',
'inc/js/jquery/suggest.dev.js',
'ljmc-admin/js/xfn.dev.js',
'ljmc-admin/js/set-post-thumbnail.dev.js',
'ljmc-admin/js/comment.dev.js',
'ljmc-admin/js/theme.dev.js',
'ljmc-admin/js/cat.dev.js',
'ljmc-admin/js/password-strength-meter.dev.js',
'ljmc-admin/js/user-profile.dev.js',
'ljmc-admin/js/theme-preview.dev.js',
'ljmc-admin/js/post.dev.js',
'ljmc-admin/js/media-upload.dev.js',
'ljmc-admin/js/word-count.dev.js',
'ljmc-admin/js/plugin-install.dev.js',
'ljmc-admin/js/edit-comments.dev.js',
'ljmc-admin/js/media-gallery.dev.js',
'ljmc-admin/js/custom-fields.dev.js',
'ljmc-admin/js/custom-background.dev.js',
'ljmc-admin/js/common.dev.js',
'ljmc-admin/js/inline-edit-tax.dev.js',
'ljmc-admin/js/gallery.dev.js',
'ljmc-admin/js/utils.dev.js',
'ljmc-admin/js/widgets.dev.js',
'ljmc-admin/js/ljmc-fullscreen.dev.js',
'ljmc-admin/js/nav-menu.dev.js',
'ljmc-admin/js/dashboard.dev.js',
'ljmc-admin/js/link.dev.js',
'ljmc-admin/js/user-suggest.dev.js',
'ljmc-admin/js/postbox.dev.js',
'ljmc-admin/js/tags.dev.js',
'ljmc-admin/js/image-edit.dev.js',
'ljmc-admin/js/media.dev.js',
'ljmc-admin/js/customize-controls.dev.js',
'ljmc-admin/js/inline-edit-post.dev.js',
'ljmc-admin/js/categories.dev.js',
'ljmc-admin/js/editor.dev.js',
'inc/js/tinymce/plugins/ljmceditimage/js/editimage.dev.js',
'inc/js/tinymce/plugins/ljmcdialogs/js/popup.dev.js',
'inc/js/tinymce/plugins/ljmcdialogs/js/ljmcdialog.dev.js',
'inc/js/plupload/handlers.dev.js',
'inc/js/plupload/ljmc-plupload.dev.js',
'inc/js/swfupload/handlers.dev.js',
'inc/js/jcrop/jquery.Jcrop.dev.js',
'inc/js/jcrop/jquery.Jcrop.js',
'inc/js/jcrop/jquery.Jcrop.css',
'inc/js/imgareaselect/jquery.imgareaselect.dev.js',
'inc/css/ljmc-pointer.dev.css',
'inc/css/editor.dev.css',
'inc/css/jquery-ui-dialog.dev.css',
'inc/css/admin-bar-rtl.dev.css',
'inc/css/admin-bar.dev.css',
'inc/js/jquery/ui/jquery.effects.clip.min.js',
'inc/js/jquery/ui/jquery.effects.scale.min.js',
'inc/js/jquery/ui/jquery.effects.blind.min.js',
'inc/js/jquery/ui/jquery.effects.core.min.js',
'inc/js/jquery/ui/jquery.effects.shake.min.js',
'inc/js/jquery/ui/jquery.effects.fade.min.js',
'inc/js/jquery/ui/jquery.effects.explode.min.js',
'inc/js/jquery/ui/jquery.effects.slide.min.js',
'inc/js/jquery/ui/jquery.effects.drop.min.js',
'inc/js/jquery/ui/jquery.effects.highlight.min.js',
'inc/js/jquery/ui/jquery.effects.bounce.min.js',
'inc/js/jquery/ui/jquery.effects.pulsate.min.js',
'inc/js/jquery/ui/jquery.effects.transfer.min.js',
'inc/js/jquery/ui/jquery.effects.fold.min.js',
'ljmc-admin/images/screenshots/captions-1.png',
'ljmc-admin/images/screenshots/captions-2.png',
'ljmc-admin/images/screenshots/flex-header-1.png',
'ljmc-admin/images/screenshots/flex-header-2.png',
'ljmc-admin/images/screenshots/flex-header-3.png',
'ljmc-admin/images/screenshots/flex-header-media-library.png',
'ljmc-admin/images/screenshots/theme-customizer.png',
'ljmc-admin/images/screenshots/twitter-embed-1.png',
'ljmc-admin/images/screenshots/twitter-embed-2.png',
'ljmc-admin/js/utils.js',
'ljmc-admin/options-privacy.php',
'ljmc-app.php',
'inc/class-ljmc-atom-server.php',
'inc/js/tinymce/themes/advanced/skins/ljmc_theme/ui.css',
// 3.5.2
'inc/js/swfupload/swfupload-all.js',
// 3.6
'ljmc-admin/js/revisions-js.php',
'ljmc-admin/images/screenshots',
'ljmc-admin/js/categories.js',
'ljmc-admin/js/categories.min.js',
'ljmc-admin/js/custom-fields.js',
'ljmc-admin/js/custom-fields.min.js',
// 3.7
'ljmc-admin/js/cat.js',
'ljmc-admin/js/cat.min.js',
'inc/js/tinymce/plugins/ljmceditimage/js/editimage.min.js',
// 3.8
'inc/js/tinymce/themes/advanced/skins/ljmc_theme/img/page_bug.gif',
'inc/js/tinymce/themes/advanced/skins/ljmc_theme/img/more_bug.gif',
'inc/js/thickbox/tb-close-2x.png',
'inc/js/thickbox/tb-close.png',
'inc/images/ljmcmini-blue-2x.png',
'inc/images/ljmcmini-blue.png',
'ljmc-admin/css/colors-fresh.css',
'ljmc-admin/css/colors-classic.css',
'ljmc-admin/css/colors-fresh.min.css',
'ljmc-admin/css/colors-classic.min.css',
'ljmc-admin/js/about.min.js',
'ljmc-admin/js/about.js',
'ljmc-admin/images/arrows-dark-vs-2x.png',
'ljmc-admin/images/ljmc-logo-vs.png',
'ljmc-admin/images/arrows-dark-vs.png',
'ljmc-admin/images/ljmc-logo.png',
'ljmc-admin/images/arrows-pr.png',
'ljmc-admin/images/arrows-dark.png',
'ljmc-admin/images/press-this.png',
'ljmc-admin/images/press-this-2x.png',
'ljmc-admin/images/arrows-vs-2x.png',
'ljmc-admin/images/welcome-icons.png',
'ljmc-admin/images/ljmc-logo-2x.png',
'ljmc-admin/images/stars-rtl-2x.png',
'ljmc-admin/images/arrows-dark-2x.png',
'ljmc-admin/images/arrows-pr-2x.png',
'ljmc-admin/images/menu-shadow-rtl.png',
'ljmc-admin/images/arrows-vs.png',
'ljmc-admin/images/about-search-2x.png',
'ljmc-admin/images/bubble_bg-rtl-2x.gif',
'ljmc-admin/images/ljmc-badge-2x.png',
'ljmc-admin/images/ljmc-logo-2x.png',
'ljmc-admin/images/bubble_bg-rtl.gif',
'ljmc-admin/images/ljmc-badge.png',
'ljmc-admin/images/menu-shadow.png',
'ljmc-admin/images/about-globe-2x.png',
'ljmc-admin/images/welcome-icons-2x.png',
'ljmc-admin/images/stars-rtl.png',
'ljmc-admin/images/ljmc-logo-vs-2x.png',
'ljmc-admin/images/about-updates-2x.png',
// 3.9
'ljmc-admin/css/colors.css',
'ljmc-admin/css/colors.min.css',
'ljmc-admin/css/colors-rtl.css',
'ljmc-admin/css/colors-rtl.min.css',
'ljmc-admin/css/media-rtl.min.css',
'ljmc-admin/css/media.min.css',
'ljmc-admin/css/farbtastic-rtl.min.css',
'ljmc-admin/images/lock-2x.png',
'ljmc-admin/images/lock.png',
'ljmc-admin/js/theme-preview.js',
'ljmc-admin/js/theme-install.min.js',
'ljmc-admin/js/theme-install.js',
'ljmc-admin/js/theme-preview.min.js',
'inc/js/plupload/plupload.html4.js',
'inc/js/plupload/plupload.html5.js',
'inc/js/plupload/changelog.txt',
'inc/js/plupload/plupload.silverlight.js',
'inc/js/plupload/plupload.flash.js',
'inc/js/plupload/plupload.js',
'inc/js/tinymce/plugins/spellchecker',
'inc/js/tinymce/plugins/inlinepopups',
'inc/js/tinymce/plugins/media/js',
'inc/js/tinymce/plugins/media/css',
'inc/js/tinymce/plugins/ljmc/img',
'inc/js/tinymce/plugins/ljmcdialogs/js',
'inc/js/tinymce/plugins/ljmceditimage/img',
'inc/js/tinymce/plugins/ljmceditimage/js',
'inc/js/tinymce/plugins/ljmceditimage/css',
'inc/js/tinymce/plugins/ljmcgallery/img',
'inc/js/tinymce/plugins/ljmcfullscreen/css',
'inc/js/tinymce/plugins/paste/js',
'inc/js/tinymce/themes/advanced',
'inc/js/tinymce/tiny_mce.js',
'inc/js/tinymce/mark_loaded_src.js',
'inc/js/tinymce/ljmc-tinymce-schema.js',
'inc/js/tinymce/plugins/media/editor_plugin.js',
'inc/js/tinymce/plugins/media/editor_plugin_src.js',
'inc/js/tinymce/plugins/media/media.htm',
'inc/js/tinymce/plugins/ljmcview/editor_plugin_src.js',
'inc/js/tinymce/plugins/ljmcview/editor_plugin.js',
'inc/js/tinymce/plugins/directionality/editor_plugin.js',
'inc/js/tinymce/plugins/directionality/editor_plugin_src.js',
'inc/js/tinymce/plugins/ljmc/editor_plugin.js',
'inc/js/tinymce/plugins/ljmc/editor_plugin_src.js',
'inc/js/tinymce/plugins/ljmcdialogs/editor_plugin_src.js',
'inc/js/tinymce/plugins/ljmcdialogs/editor_plugin.js',
'inc/js/tinymce/plugins/ljmceditimage/editimage.html',
'inc/js/tinymce/plugins/ljmceditimage/editor_plugin.js',
'inc/js/tinymce/plugins/ljmceditimage/editor_plugin_src.js',
'inc/js/tinymce/plugins/fullscreen/editor_plugin_src.js',
'inc/js/tinymce/plugins/fullscreen/fullscreen.htm',
'inc/js/tinymce/plugins/fullscreen/editor_plugin.js',
'inc/js/tinymce/plugins/ljmclink/editor_plugin_src.js',
'inc/js/tinymce/plugins/ljmclink/editor_plugin.js',
'inc/js/tinymce/plugins/ljmcgallery/editor_plugin_src.js',
'inc/js/tinymce/plugins/ljmcgallery/editor_plugin.js',
'inc/js/tinymce/plugins/tabfocus/editor_plugin.js',
'inc/js/tinymce/plugins/tabfocus/editor_plugin_src.js',
'inc/js/tinymce/plugins/ljmcfullscreen/editor_plugin.js',
'inc/js/tinymce/plugins/ljmcfullscreen/editor_plugin_src.js',
'inc/js/tinymce/plugins/paste/editor_plugin.js',
'inc/js/tinymce/plugins/paste/pasteword.htm',
'inc/js/tinymce/plugins/paste/editor_plugin_src.js',
'inc/js/tinymce/plugins/paste/pastetext.htm',
'inc/js/tinymce/langs/ljmc-langs.php',
// 4.1
'inc/js/jquery/ui/jquery.ui.accordion.min.js',
'inc/js/jquery/ui/jquery.ui.autocomplete.min.js',
'inc/js/jquery/ui/jquery.ui.button.min.js',
'inc/js/jquery/ui/jquery.ui.core.min.js',
'inc/js/jquery/ui/jquery.ui.datepicker.min.js',
'inc/js/jquery/ui/jquery.ui.dialog.min.js',
'inc/js/jquery/ui/jquery.ui.draggable.min.js',
'inc/js/jquery/ui/jquery.ui.droppable.min.js',
'inc/js/jquery/ui/jquery.ui.effect-blind.min.js',
'inc/js/jquery/ui/jquery.ui.effect-bounce.min.js',
'inc/js/jquery/ui/jquery.ui.effect-clip.min.js',
'inc/js/jquery/ui/jquery.ui.effect-drop.min.js',
'inc/js/jquery/ui/jquery.ui.effect-explode.min.js',
'inc/js/jquery/ui/jquery.ui.effect-fade.min.js',
'inc/js/jquery/ui/jquery.ui.effect-fold.min.js',
'inc/js/jquery/ui/jquery.ui.effect-highlight.min.js',
'inc/js/jquery/ui/jquery.ui.effect-pulsate.min.js',
'inc/js/jquery/ui/jquery.ui.effect-scale.min.js',
'inc/js/jquery/ui/jquery.ui.effect-shake.min.js',
'inc/js/jquery/ui/jquery.ui.effect-slide.min.js',
'inc/js/jquery/ui/jquery.ui.effect-transfer.min.js',
'inc/js/jquery/ui/jquery.ui.effect.min.js',
'inc/js/jquery/ui/jquery.ui.menu.min.js',
'inc/js/jquery/ui/jquery.ui.mouse.min.js',
'inc/js/jquery/ui/jquery.ui.position.min.js',
'inc/js/jquery/ui/jquery.ui.progressbar.min.js',
'inc/js/jquery/ui/jquery.ui.resizable.min.js',
'inc/js/jquery/ui/jquery.ui.selectable.min.js',
'inc/js/jquery/ui/jquery.ui.slider.min.js',
'inc/js/jquery/ui/jquery.ui.sortable.min.js',
'inc/js/jquery/ui/jquery.ui.spinner.min.js',
'inc/js/jquery/ui/jquery.ui.tabs.min.js',
'inc/js/jquery/ui/jquery.ui.tooltip.min.js',
'inc/js/jquery/ui/jquery.ui.widget.min.js',
'inc/js/tinymce/skins/ljmc/images/dashicon-no-alt.png'
);

/**
 * Stores new files in data to copy
 *
 * The contents of this array indicate any new bundled plugins/themes which
 * should be installed with the system Upgrade. These items will not be
 * re-installed in future upgrades, this behaviour is controlled by the
 * introduced version present here being older than the current installed version.
 *
 * The content of this array should follow the following format:
 * Filename (relative to data) => Introduced version
 * Directories should be noted by suffixing it with a trailing slash (/)
 *
 * @since 3.2.0
 * @global array $_new_bundled_files
 * @var array
 * @name $_new_bundled_files
 */
global $_new_bundled_files;

$_new_bundled_files = array(
	'plugins/akismet/'       => '2.0',
	'themes/twentyten/'      => '3.0',
	'themes/twentyeleven/'   => '3.2',
	'themes/twentytwelve/'   => '3.5',
	'themes/twentythirteen/' => '3.6',
	'themes/twentyfourteen/' => '3.8',
	'themes/twentyfifteen/'  => '4.1',
);

/**
 * Upgrade the core of system.
 *
 * This will create a .maintenance file at the base of the system directory
 * to ensure that people can not access the web site, when the files are being
 * copied to their locations.
 *
 * The files in the {@link $_old_files} list will be removed and the new files
 * copied from the zip file after the database is upgraded.
 *
 * The files in the {@link $_new_bundled_files} list will be added to the installation
 * if the version is greater than or equal to the old version being upgraded.
 *
 * The steps for the upgrader for after the new release is downloaded and
 * unzipped is:
 *   1. Test unzipped location for select files to ensure that unzipped worked.
 *   2. Create the .maintenance file in current system base.
 *   3. Copy new system directory over old system files.
 *   4. Upgrade system to new version.
 *     4.1. Copy all files/folders other than data
 *     4.2. Copy any language files to LJMC_LANG_DIR (which may differ from LJMC_CONTENT_DIR
 *     4.3. Copy any new bundled themes/plugins to their respective locations
 *   5. Delete new system directory path.
 *   6. Delete .maintenance file.
 *   7. Remove old files.
 *   8. Delete 'update_core' option.
 *
 * There are several areas of failure. For instance if PHP times out before step
 * 6, then you will not be able to access any portion of your site. Also, since
 * the upgrade will not continue where it left off, you will not be able to
 * automatically remove old files and remove the 'update_core' option. This
 * isn't that bad.
 *
 * If the copy of the new system over the old fails, then the worse is that
 * the new system directory will remain.
 *
 * If it is assumed that every file will be copied over, including plugins and
 * themes, then if you edit the default theme, you should rename it, so that
 * your changes remain.
 *
 * @since 2.7.0
 *
 * @param string $from New release unzipped path.
 * @param string $to Path to old system installation.
 * @return LJMC_Error|null LJMC_Error on failure, null on success.
 */
function update_core($from, $to) {
	global $ljmc_filesystem, $_old_files, $_new_bundled_files, $ljmcdb;

	@set_time_limit( 300 );

	/**
	 * Filter feedback messages displayed during the core update process.
	 *
	 * The filter is first evaluated after the zip file for the latest version
	 * has been downloaded and unzipped. It is evaluated five more times during
	 * the process:
	 *
	 * 1. Before system begins the core upgrade process.
	 * 2. Before Maintenance Mode is enabled.
	 * 3. Before system begins copying over the necessary files.
	 * 4. Before Maintenance Mode is disabled.
	 * 5. Before the database is upgraded.
	 *
	 * @since 2.5.0
	 *
	 * @param string $feedback The core update feedback messages.
	 */
	apply_filters( 'update_feedback', __( 'Verifying the unpacked files&#8230;' ) );

	// Sanity check the unzipped distribution.
	$distro = '';
	$roots = array( '/ljmc/', '/ljmc-mu/' );
	foreach ( $roots as $root ) {
		if ( $ljmc_filesystem->exists( $from . $root . 'readme.html' ) && $ljmc_filesystem->exists( $from . $root . 'inc/version.php' ) ) {
			$distro = $root;
			break;
		}
	}
	if ( ! $distro ) {
		$ljmc_filesystem->delete( $from, true );
		return new LJMC_Error( 'insane_distro', __('The update could not be unpacked') );
	}

	// Import $ljmc_version, $required_php_version, and $required_mysql_version from the new version
	// $ljmc_filesystem->ljmc_content_dir() returned unslashed pre-2.8
	global $ljmc_version, $required_php_version, $required_mysql_version;

	$versions_file = trailingslashit( $ljmc_filesystem->ljmc_content_dir() ) . 'upgrade/version-current.php';
	if ( ! $ljmc_filesystem->copy( $from . $distro . 'inc/version.php', $versions_file ) ) {
		$ljmc_filesystem->delete( $from, true );
		return new LJMC_Error( 'copy_failed_for_version_file', __( 'The update cannot be installed because we will be unable to copy some files. This is usually due to inconsistent file permissions.' ), 'inc/version.php' );
	}

	$ljmc_filesystem->chmod( $versions_file, FS_CHMOD_FILE );
	require( LJMC_CONTENT_DIR . '/upgrade/version-current.php' );
	$ljmc_filesystem->delete( $versions_file );

	$php_version    = phpversion();
	$mysql_version  = $ljmcdb->db_version();
	$old_ljmc_version = $ljmc_version; // The version of system we're updating from
	$development_build = ( false !== strpos( $old_ljmc_version . $ljmc_version, '-' )  ); // a dash in the version indicates a Development release
	$php_compat     = version_compare( $php_version, $required_php_version, '>=' );
	if ( file_exists( LJMC_CONTENT_DIR . '/db.php' ) && empty( $ljmcdb->is_mysql ) )
		$mysql_compat = true;
	else
		$mysql_compat = version_compare( $mysql_version, $required_mysql_version, '>=' );

	if ( !$mysql_compat || !$php_compat )
		$ljmc_filesystem->delete($from, true);

	if ( !$mysql_compat && !$php_compat )
		return new LJMC_Error( 'php_mysql_not_compatible', sprintf( __('The update cannot be installed because system %1$s requires PHP version %2$s or higher and MySQL version %3$s or higher. You are running PHP version %4$s and MySQL version %5$s.'), $ljmc_version, $required_php_version, $required_mysql_version, $php_version, $mysql_version ) );
	elseif ( !$php_compat )
		return new LJMC_Error( 'php_not_compatible', sprintf( __('The update cannot be installed because system %1$s requires PHP version %2$s or higher. You are running version %3$s.'), $ljmc_version, $required_php_version, $php_version ) );
	elseif ( !$mysql_compat )
		return new LJMC_Error( 'mysql_not_compatible', sprintf( __('The update cannot be installed because system %1$s requires MySQL version %2$s or higher. You are running version %3$s.'), $ljmc_version, $required_mysql_version, $mysql_version ) );

	/** This filter is documented in ljmc-admin/includes/update-core.php */
	apply_filters( 'update_feedback', __( 'Preparing to install the latest version&#8230;' ) );

	// Don't copy data, we'll deal with that below
	// We also copy version.php last so failed updates report their old version
	$skip = array( 'data', 'inc/version.php' );
	$check_is_writable = array();

	// Check to see which files don't really need updating - only available for 3.7 and higher
	if ( function_exists( 'get_core_checksums' ) ) {
		// Find the local version of the working directory
		$working_dir_local = LJMC_CONTENT_DIR . '/upgrade/' . basename( $from ) . $distro;

		$checksums = get_core_checksums( $ljmc_version, isset( $ljmc_local_package ) ? $ljmc_local_package : 'en_US' );
		if ( is_array( $checksums ) && isset( $checksums[ $ljmc_version ] ) )
			$checksums = $checksums[ $ljmc_version ]; // Compat code for 3.7-beta2
		if ( is_array( $checksums ) ) {
			foreach( $checksums as $file => $checksum ) {
				if ( 'data' == substr( $file, 0, 10 ) )
					continue;
				if ( ! file_exists( ABSPATH . $file ) )
					continue;
				if ( ! file_exists( $working_dir_local . $file ) )
					continue;
				if ( md5_file( ABSPATH . $file ) === $checksum )
					$skip[] = $file;
				else
					$check_is_writable[ $file ] = ABSPATH . $file;
			}
		}
	}

	// If we're using the direct method, we can predict write failures that are due to permissions.
	if ( $check_is_writable && 'direct' === $ljmc_filesystem->method ) {
		$files_writable = array_filter( $check_is_writable, array( $ljmc_filesystem, 'is_writable' ) );
		if ( $files_writable !== $check_is_writable ) {
			$files_not_writable = array_diff_key( $check_is_writable, $files_writable );
			foreach ( $files_not_writable as $relative_file_not_writable => $file_not_writable ) {
				// If the writable check failed, chmod file to 0644 and try again, same as copy_dir().
				$ljmc_filesystem->chmod( $file_not_writable, FS_CHMOD_FILE );
				if ( $ljmc_filesystem->is_writable( $file_not_writable ) )
					unset( $files_not_writable[ $relative_file_not_writable ] );
			}

			// Store package-relative paths (the key) of non-writable files in the LJMC_Error object.
			$error_data = version_compare( $old_ljmc_version, '3.7-beta2', '>' ) ? array_keys( $files_not_writable ) : '';

			if ( $files_not_writable )
				return new LJMC_Error( 'files_not_writable', __( 'The update cannot be installed because we will be unable to copy some files. This is usually due to inconsistent file permissions.' ), implode( ', ', $error_data ) );
		}
	}

	/** This filter is documented in ljmc-admin/includes/update-core.php */
	apply_filters( 'update_feedback', __( 'Enabling Maintenance mode&#8230;' ) );
	// Create maintenance file to signal that we are upgrading
	$maintenance_string = '<?php $upgrading = ' . time() . '; ?>';
	$maintenance_file = $to . '.maintenance';
	$ljmc_filesystem->delete($maintenance_file);
	$ljmc_filesystem->put_contents($maintenance_file, $maintenance_string, FS_CHMOD_FILE);

	/** This filter is documented in ljmc-admin/includes/update-core.php */
	apply_filters( 'update_feedback', __( 'Copying the required files&#8230;' ) );
	// Copy new versions of LJMC files into place.
	$result = _copy_dir( $from . $distro, $to, $skip );
	if ( is_ljmc_error( $result ) )
		$result = new LJMC_Error( $result->get_error_code(), $result->get_error_message(), substr( $result->get_error_data(), strlen( $to ) ) );

	// Since we know the core files have copied over, we can now copy the version file
	if ( ! is_ljmc_error( $result ) ) {
		if ( ! $ljmc_filesystem->copy( $from . $distro . 'inc/version.php', $to . 'inc/version.php', true /* overwrite */ ) ) {
			$ljmc_filesystem->delete( $from, true );
			$result = new LJMC_Error( 'copy_failed_for_version_file', __( 'The update cannot be installed because we will be unable to copy some files. This is usually due to inconsistent file permissions.' ), 'inc/version.php' );
		}
		$ljmc_filesystem->chmod( $to . 'inc/version.php', FS_CHMOD_FILE );
	}

	// Check to make sure everything copied correctly, ignoring the contents of data
	$skip = array( 'data' );
	$failed = array();
	if ( isset( $checksums ) && is_array( $checksums ) ) {
		foreach ( $checksums as $file => $checksum ) {
			if ( 'data' == substr( $file, 0, 10 ) )
				continue;
			if ( ! file_exists( $working_dir_local . $file ) )
				continue;
			if ( file_exists( ABSPATH . $file ) && md5_file( ABSPATH . $file ) == $checksum )
				$skip[] = $file;
			else
				$failed[] = $file;
		}
	}

	// Some files didn't copy properly
	if ( ! empty( $failed ) ) {
		$total_size = 0;
		foreach ( $failed as $file ) {
			if ( file_exists( $working_dir_local . $file ) )
				$total_size += filesize( $working_dir_local . $file );
		}

		// If we don't have enough free space, it isn't worth trying again.
		// Unlikely to be hit due to the check in unzip_file().
		$available_space = @disk_free_space( ABSPATH );
		if ( $available_space && $total_size >= $available_space ) {
			$result = new LJMC_Error( 'disk_full', __( 'There is not enough free disk space to complete the update.' ) );
		} else {
			$result = _copy_dir( $from . $distro, $to, $skip );
			if ( is_ljmc_error( $result ) )
				$result = new LJMC_Error( $result->get_error_code() . '_retry', $result->get_error_message(), substr( $result->get_error_data(), strlen( $to ) ) );
		}
	}

	// Custom Content Directory needs updating now.
	// Copy Languages
	if ( !is_ljmc_error($result) && $ljmc_filesystem->is_dir($from . $distro . 'data/languages') ) {
		if ( LJMC_LANG_DIR != ABSPATH . LJMCINC . '/languages' || @is_dir(LJMC_LANG_DIR) )
			$lang_dir = LJMC_LANG_DIR;
		else
			$lang_dir = LJMC_CONTENT_DIR . '/languages';

		if ( !@is_dir($lang_dir) && 0 === strpos($lang_dir, ABSPATH) ) { // Check the language directory exists first
			$ljmc_filesystem->mkdir($to . str_replace(ABSPATH, '', $lang_dir), FS_CHMOD_DIR); // If it's within the ABSPATH we can handle it here, otherwise they're out of luck.
			clearstatcache(); // for FTP, Need to clear the stat cache
		}

		if ( @is_dir($lang_dir) ) {
			$ljmc_lang_dir = $ljmc_filesystem->find_folder($lang_dir);
			if ( $ljmc_lang_dir ) {
				$result = copy_dir($from . $distro . 'data/languages/', $ljmc_lang_dir);
				if ( is_ljmc_error( $result ) )
					$result = new LJMC_Error( $result->get_error_code() . '_languages', $result->get_error_message(), substr( $result->get_error_data(), strlen( $ljmc_lang_dir ) ) );
			}
		}
	}

	/** This filter is documented in ljmc-admin/includes/update-core.php */
	apply_filters( 'update_feedback', __( 'Disabling Maintenance mode&#8230;' ) );
	// Remove maintenance file, we're done with potential site-breaking changes
	$ljmc_filesystem->delete( $maintenance_file );

	// 3.5 -> 3.5+ - an empty twentytwelve directory was created upon upgrade to 3.5 for some users, preventing installation of Twenty Twelve.
	if ( '3.5' == $old_ljmc_version ) {
		if ( is_dir( LJMC_CONTENT_DIR . '/themes/twentytwelve' ) && ! file_exists( LJMC_CONTENT_DIR . '/themes/twentytwelve/style.css' )  ) {
			$ljmc_filesystem->delete( $ljmc_filesystem->ljmc_themes_dir() . 'twentytwelve/' );
		}
	}

	// Copy New bundled plugins & themes
	// This gives us the ability to install new plugins & themes bundled with future versions of system whilst avoiding the re-install upon upgrade issue.
	// $development_build controls us overwriting bundled themes and plugins when a non-stable release is being updated
	if ( !is_ljmc_error($result) && ( ! defined('CORE_UPGRADE_SKIP_NEW_BUNDLED') || ! CORE_UPGRADE_SKIP_NEW_BUNDLED ) ) {
		foreach ( (array) $_new_bundled_files as $file => $introduced_version ) {
			// If a $development_build or if $introduced version is greater than what the site was previously running
			if ( $development_build || version_compare( $introduced_version, $old_ljmc_version, '>' ) ) {
				$directory = ('/' == $file[ strlen($file)-1 ]);
				list($type, $filename) = explode('/', $file, 2);

				// Check to see if the bundled items exist before attempting to copy them
				if ( ! $ljmc_filesystem->exists( $from . $distro . 'data/' . $file ) )
					continue;

				if ( 'plugins' == $type )
					$dest = $ljmc_filesystem->ljmc_plugins_dir();
				elseif ( 'themes' == $type )
					$dest = trailingslashit($ljmc_filesystem->ljmc_themes_dir()); // Back-compat, ::ljmc_themes_dir() did not return trailingslash'd pre-3.2
				else
					continue;

				if ( ! $directory ) {
					if ( ! $development_build && $ljmc_filesystem->exists( $dest . $filename ) )
						continue;

					if ( ! $ljmc_filesystem->copy($from . $distro . 'data/' . $file, $dest . $filename, FS_CHMOD_FILE) )
						$result = new LJMC_Error( "copy_failed_for_new_bundled_$type", __( 'Could not copy file.' ), $dest . $filename );
				} else {
					if ( ! $development_build && $ljmc_filesystem->is_dir( $dest . $filename ) )
						continue;

					$ljmc_filesystem->mkdir($dest . $filename, FS_CHMOD_DIR);
					$_result = copy_dir( $from . $distro . 'data/' . $file, $dest . $filename);

					// If a error occurs partway through this final step, keep the error flowing through, but keep process going.
					if ( is_ljmc_error( $_result ) ) {
						if ( ! is_ljmc_error( $result ) )
							$result = new LJMC_Error;
						$result->add( $_result->get_error_code() . "_$type", $_result->get_error_message(), substr( $_result->get_error_data(), strlen( $dest ) ) );
					}
				}
			}
		} //end foreach
	}

	// Handle $result error from the above blocks
	if ( is_ljmc_error($result) ) {
		$ljmc_filesystem->delete($from, true);
		return $result;
	}

	// Remove old files
	foreach ( $_old_files as $old_file ) {
		$old_file = $to . $old_file;
		if ( !$ljmc_filesystem->exists($old_file) )
			continue;
		$ljmc_filesystem->delete($old_file, true);
	}

	// Remove any Genericons example.html's from the filesystem
	_upgrade_422_remove_genericons();

	// Upgrade DB with separate request
	/** This filter is documented in ljmc-admin/includes/update-core.php */
	apply_filters( 'update_feedback', __( 'Upgrading database&#8230;' ) );
	$db_upgrade_url = admin_url('upgrade.php?step=upgrade_db');
	ljmc_remote_post($db_upgrade_url, array('timeout' => 60));

	// Clear the cache to prevent an update_option() from saving a stale db_version to the cache
	ljmc_cache_flush();
	// (Not all cache backends listen to 'flush')
	ljmc_cache_delete( 'alloptions', 'options' );

	// Remove working directory
	$ljmc_filesystem->delete($from, true);

	// Force refresh of update information
	if ( function_exists('delete_site_transient') )
		delete_site_transient('update_core');
	else
		delete_option('update_core');

	/**
	 * Fires after system core has been successfully updated.
	 *
	 * @since 3.3.0
	 *
	 * @param string $ljmc_version The current system version.
	 */
	do_action( '_core_updated_successfully', $ljmc_version );

	// Clear the option that blocks auto updates after failures, now that we've been successful.
	if ( function_exists( 'delete_site_option' ) )
		delete_site_option( 'auto_core_update_failed' );

	return $ljmc_version;
}

/**
 * Copies a directory from one location to another via the system Filesystem Abstraction.
 * Assumes that LJMC_Filesystem() has already been called and setup.
 *
 * This is a temporary function for the 3.1 -> 3.2 upgrade, as well as for those upgrading to
 * 3.7+
 *
 * @ignore
 * @since 3.2.0
 * @since 3.7.0 Updated not to use a regular expression for the skip list
 * @see copy_dir()
 *
 * @param string $from source directory
 * @param string $to destination directory
 * @param array $skip_list a list of files/folders to skip copying
 * @return mixed LJMC_Error on failure, True on success.
 */
function _copy_dir($from, $to, $skip_list = array() ) {
	global $ljmc_filesystem;

	$dirlist = $ljmc_filesystem->dirlist($from);

	$from = trailingslashit($from);
	$to = trailingslashit($to);

	foreach ( (array) $dirlist as $filename => $fileinfo ) {
		if ( in_array( $filename, $skip_list ) )
			continue;

		if ( 'f' == $fileinfo['type'] ) {
			if ( ! $ljmc_filesystem->copy($from . $filename, $to . $filename, true, FS_CHMOD_FILE) ) {
				// If copy failed, chmod file to 0644 and try again.
				$ljmc_filesystem->chmod( $to . $filename, FS_CHMOD_FILE );
				if ( ! $ljmc_filesystem->copy($from . $filename, $to . $filename, true, FS_CHMOD_FILE) )
					return new LJMC_Error( 'copy_failed__copy_dir', __( 'Could not copy file.' ), $to . $filename );
			}
		} elseif ( 'd' == $fileinfo['type'] ) {
			if ( !$ljmc_filesystem->is_dir($to . $filename) ) {
				if ( !$ljmc_filesystem->mkdir($to . $filename, FS_CHMOD_DIR) )
					return new LJMC_Error( 'mkdir_failed__copy_dir', __( 'Could not create directory.' ), $to . $filename );
			}

			/*
			 * Generate the $sub_skip_list for the subdirectory as a sub-set
			 * of the existing $skip_list.
			 */
			$sub_skip_list = array();
			foreach ( $skip_list as $skip_item ) {
				if ( 0 === strpos( $skip_item, $filename . '/' ) )
					$sub_skip_list[] = preg_replace( '!^' . preg_quote( $filename, '!' ) . '/!i', '', $skip_item );
			}

			$result = _copy_dir($from . $filename, $to . $filename, $sub_skip_list);
			if ( is_ljmc_error($result) )
				return $result;
		}
	}
	return true;
}

/**
 * Redirect to the About system page after a successful upgrade.
 *
 * This function is only needed when the existing install is older than 3.4.0.
 *
 * @since 3.3.0
 *
 */
function _redirect_to_about_ljmc( $new_version ) {
	global $ljmc_version, $pagenow, $action;

	if ( version_compare( $ljmc_version, '3.4-RC1', '>=' ) )
		return;

	// Ensure we only run this on the update-core.php page. The Core_Upgrader may be used in other contexts.
	if ( 'update-core.php' != $pagenow )
		return;

 	if ( 'do-core-upgrade' != $action && 'do-core-reinstall' != $action )
 		return;

	// Load the updated default text localization domain for new strings.
	load_default_textdomain();

	// See do_core_upgrade()
	show_message( __('system updated successfully') );

	// self_admin_url() won't exist when upgrading from <= 3.0, so relative URLs are intentional.
	show_message( '<span class="hide-if-no-js">' . sprintf( __( 'Welcome to system %1$s. You will be redirected to the About system screen. If not, click <a href="%2$s">here</a>.' ), $new_version, 'about.php?updated' ) . '</span>' );
	show_message( '<span class="hide-if-js">' . sprintf( __( 'Welcome to system %1$s. <a href="%2$s">Learn more</a>.' ), $new_version, 'about.php?updated' ) . '</span>' );
	echo '</div>';
	?>
<script type="text/javascript">
window.location = 'about.php?updated';
</script>
	<?php

	// Include admin-footer.php and exit.
	include(ABSPATH . 'ljmc-admin/admin-footer.php');
	exit();
}
add_action( '_core_updated_successfully', '_redirect_to_about_ljmc' );

/**
 * Cleans up Genericons example files.
 *
 * @since 4.2.2
 */
function _upgrade_422_remove_genericons() {
	global $ljmc_theme_directories, $ljmc_filesystem;

	// A list of the affected files using the filesystem absolute paths.
	$affected_files = array();

	// Themes
	foreach ( $ljmc_theme_directories as $directory ) {
		$affected_theme_files = _upgrade_422_find_genericons_files_in_folder( $directory );
		$affected_files       = array_merge( $affected_files, $affected_theme_files );
	}

	// Plugins
	$affected_plugin_files = _upgrade_422_find_genericons_files_in_folder( LJMC_PLUGIN_DIR );
	$affected_files        = array_merge( $affected_files, $affected_plugin_files );

	foreach ( $affected_files as $file ) {
		$gen_dir = $ljmc_filesystem->find_folder( trailingslashit( dirname( $file ) ) );
		if ( empty( $gen_dir ) ) {
			continue;
		}

		// The path when the file is accessed via LJMC_Filesystem may differ in the case of FTP
		$remote_file = $gen_dir . basename( $file );

		if ( ! $ljmc_filesystem->exists( $remote_file ) ) {
			continue;
		}

		if ( ! $ljmc_filesystem->delete( $remote_file, false, 'f' ) ) {
			$ljmc_filesystem->put_contents( $remote_file, '' );
		}
	}
}

/**
 * Recursively find Genericons example files in a given folder.
 *
 * @ignore
 * @since 4.2.2
 *
 * @param string $directory Directory path. Expects trailingslashed.
 * @return array
 */
function _upgrade_422_find_genericons_files_in_folder( $directory ) {
	$directory = trailingslashit( $directory );
	$files     = array();

	if ( file_exists( "{$directory}example.html" ) && false !== strpos( file_get_contents( "{$directory}example.html" ), '<title>Genericons</title>' ) ) {
		$files[] = "{$directory}example.html";
	}

	$dirs = glob( $directory . '*', GLOB_ONLYDIR );
	if ( $dirs ) {
		foreach ( $dirs as $dir ) {
			$files = array_merge( $files, _upgrade_422_find_genericons_files_in_folder( $dir ) );
		}
	}

	return $files;
}
