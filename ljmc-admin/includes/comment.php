<?php
/**
 * system Comment Administration API.
 *
 * @package system
 * @subpackage Administration
 */

/**
 * Determine if a comment exists based on author and date.
 *
 * @since 2.0.0
 *
 * @global ljmcdb $ljmcdb system database abstraction object.
 *
 * @param string $comment_author Author of the comment
 * @param string $comment_date Date of the comment
 * @return mixed Comment post ID on success.
 */
function comment_exists($comment_author, $comment_date) {
	global $ljmcdb;

	$comment_author = stripslashes($comment_author);
	$comment_date = stripslashes($comment_date);

	return $ljmcdb->get_var( $ljmcdb->prepare("SELECT comment_post_ID FROM $ljmcdb->comments
			WHERE comment_author = %s AND comment_date = %s", $comment_author, $comment_date) );
}

/**
 * Update a comment with values provided in $_POST.
 *
 * @since 2.0.0
 */
function edit_comment() {

	if ( ! current_user_can( 'edit_comment', (int) $_POST['comment_ID'] ) )
		ljmc_die ( __( 'You are not allowed to edit comments on this post.' ) );

	if ( isset( $_POST['newcomment_author'] ) )
		$_POST['comment_author'] = $_POST['newcomment_author'];
	if ( isset( $_POST['newcomment_author_email'] ) )
		$_POST['comment_author_email'] = $_POST['newcomment_author_email'];
	if ( isset( $_POST['newcomment_author_url'] ) )
		$_POST['comment_author_url'] = $_POST['newcomment_author_url'];
	if ( isset( $_POST['comment_status'] ) )
		$_POST['comment_approved'] = $_POST['comment_status'];
	if ( isset( $_POST['content'] ) )
		$_POST['comment_content'] = $_POST['content'];
	if ( isset( $_POST['comment_ID'] ) )
		$_POST['comment_ID'] = (int) $_POST['comment_ID'];

	foreach ( array ('aa', 'mm', 'jj', 'hh', 'mn') as $timeunit ) {
		if ( !empty( $_POST['hidden_' . $timeunit] ) && $_POST['hidden_' . $timeunit] != $_POST[$timeunit] ) {
			$_POST['edit_date'] = '1';
			break;
		}
	}

	if ( !empty ( $_POST['edit_date'] ) ) {
		$aa = $_POST['aa'];
		$mm = $_POST['mm'];
		$jj = $_POST['jj'];
		$hh = $_POST['hh'];
		$mn = $_POST['mn'];
		$ss = $_POST['ss'];
		$jj = ($jj > 31 ) ? 31 : $jj;
		$hh = ($hh > 23 ) ? $hh -24 : $hh;
		$mn = ($mn > 59 ) ? $mn -60 : $mn;
		$ss = ($ss > 59 ) ? $ss -60 : $ss;
		$_POST['comment_date'] = "$aa-$mm-$jj $hh:$mn:$ss";
	}

	ljmc_update_comment( $_POST );
}

/**
 * Returns a comment object based on comment ID.
 *
 * @since 2.0.0
 *
 * @param int $id ID of comment to retrieve.
 * @return bool|object Comment if found. False on failure.
 */
function get_comment_to_edit( $id ) {
	if ( !$comment = get_comment($id) )
		return false;

	$comment->comment_ID = (int) $comment->comment_ID;
	$comment->comment_post_ID = (int) $comment->comment_post_ID;

	$comment->comment_content = format_to_edit( $comment->comment_content );
	/**
	 * Filter the comment content before editing.
	 *
	 * @since 2.0.0
	 *
	 * @param string $comment->comment_content Comment content.
	 */
	$comment->comment_content = apply_filters( 'comment_edit_pre', $comment->comment_content );

	$comment->comment_author = format_to_edit( $comment->comment_author );
	$comment->comment_author_email = format_to_edit( $comment->comment_author_email );
	$comment->comment_author_url = format_to_edit( $comment->comment_author_url );
	$comment->comment_author_url = esc_url($comment->comment_author_url);

	return $comment;
}

/**
 * Get the number of pending comments on a post or posts
 *
 * @since 2.3.0
 *
 * @global ljmcdb $ljmcdb system database abstraction object.
 *
 * @param int|array $post_id Either a single Post ID or an array of Post IDs
 * @return int|array Either a single Posts pending comments as an int or an array of ints keyed on the Post IDs
 */
function get_pending_comments_num( $post_id ) {
}

/**
 * Add avatars to relevant places in admin, or try to.
 *
 * @since 2.5.0
 * @uses $comment
 *
 * @param string $name User name.
 * @return string Avatar with Admin name.
 */
function floated_admin_avatar( $name ) {
	global $comment;
	$avatar = get_avatar( $comment, 32, 'mystery' );
	return "$avatar $name";
}

function enqueue_comment_hotkeys_js() {
	if ( 'true' == get_user_option( 'comment_shortcuts' ) )
		ljmc_enqueue_script( 'jquery-table-hotkeys' );
}
