<?php
/**
 * system Translation Install Administration API
 *
 * @package system
 * @subpackage Administration
 */


/**
 * Retrieve translations from system Translation API.
 *
 * @since 4.0.0
 *
 * @param string       $type Type of translations. Accepts 'plugins', 'themes', 'core'.
 * @param array|object $args Translation API arguments. Optional.
 * @return object|LJMC_Error On success an object of translations, LJMC_Error on failure.
 */
function translations_api( $type, $args = null ) {
	include( ABSPATH . LJMCINC . '/version.php' ); // include an unmodified $ljmc_version

	if ( ! in_array( $type, array( 'plugins', 'themes', 'core' ) ) ) {
		return	new LJMC_Error( 'invalid_type', __( 'Invalid translation type.' ) );
	}

	/**
	 * Allows a plugin to override the system.org Translation Install API entirely.
	 *
	 * @since 4.0.0
	 *
	 * @param bool|array  $result The result object. Default false.
	 * @param string      $type   The type of translations being requested.
	 * @param object      $args   Translation API arguments.
	 */
	$res = apply_filters( 'translations_api', false, $type, $args );

	if ( false === $res ) {
		$url = $http_url = 'http://api.localhost/translations/' . $type . '/1.0/';
		if ( $ssl = ljmc_http_supports( array( 'ssl' ) ) ) {
			$url = set_url_scheme( $url, 'https' );
		}

		$options = array(
			'timeout' => 3,
			'body' => array(
				'ljmc_version' => $ljmc_version,
				'locale'     => get_locale(),
				'version'    => $args['version'], // Version of plugin, theme or core
			),
		);

		if ( 'core' !== $type ) {
			$options['body']['slug'] = $args['slug']; // Plugin or theme slug
		}

		$request = ljmc_remote_post( $url, $options );

		if ( $ssl && is_ljmc_error( $request ) ) {

			$request = ljmc_remote_post( $http_url, $options );
		}

		if ( is_ljmc_error( $request ) ) {
			$res = new LJMC_Error( 'translations_api_failed', __( 'An unexpected error occurred. Something may be wrong with system.org or this server&#8217;s configuration. If you continue to have problems, please try the <a href="localhost/support/">support forums</a>.' ), $request->get_error_message() );
		} else {
			$res = json_decode( ljmc_remote_retrieve_body( $request ), true );
			if ( ! is_object( $res ) && ! is_array( $res ) ) {
				$res = new LJMC_Error( 'translations_api_failed', __( 'An unexpected error occurred. Something may be wrong with system.org or this server&#8217;s configuration. If you continue to have problems, please try the <a href="localhost/support/">support forums</a>.' ), ljmc_remote_retrieve_body( $request ) );
			}
		}
	}

	/**
	 * Filter the Translation Install API response results.
	 *
	 * @since 4.0.0
	 *
	 * @param object|LJMC_Error $res  Response object or LJMC_Error.
	 * @param string          $type The type of translations being requested.
	 * @param object          $args Translation API arguments.
	 */
	return apply_filters( 'translations_api_result', $res, $type, $args );
}

/**
 * Get available translations from the system.org API.
 *
 * @since 4.0.0
 *
 * @see translations_api()
 *
 * @return array Array of translations, each an array of data. If the API response results
 *               in an error, an empty array will be returned.
 */
function ljmc_get_available_translations() {
	if ( ! defined( 'LJMC_INSTALLING' ) && false !== ( $translations = get_site_transient( 'available_translations' ) ) ) {
		return $translations;
	}

	include( ABSPATH . LJMCINC . '/version.php' ); // include an unmodified $ljmc_version

	$api = translations_api( 'core', array( 'version' => $ljmc_version ) );

	if ( is_ljmc_error( $api ) || empty( $api['translations'] ) ) {
		return array();
	}

	$translations = array();
	// Key the array with the language code for now.
	foreach ( $api['translations'] as $translation ) {
		$translations[ $translation['language'] ] = $translation;
	}

	if ( ! defined( 'LJMC_INSTALLING' ) ) {
		set_site_transient( 'available_translations', $translations, 3 * HOUR_IN_SECONDS );
	}

	return $translations;
}

/**
 * Output the select form for the language selection on the installation screen.
 *
 * @since 4.0.0
 *
 * @param array $languages Array of available languages (populated via the Translation API).
 */
function ljmc_install_language_form( $languages ) {
	global $ljmc_local_package;

	$installed_languages = get_available_languages();

	echo "<label class='screen-reader-text' for='language'>Select a default language</label>\n";
	echo "<select size='14' name='language' id='language'>\n";
	echo '<option value="" lang="en" selected="selected" data-continue="Continue" data-installed="1">English (United States)</option>';
	echo "\n";

	if ( ! empty( $ljmc_local_package ) && isset( $languages[ $ljmc_local_package ] ) ) {
		if ( isset( $languages[ $ljmc_local_package ] ) ) {
			$language = $languages[ $ljmc_local_package ];
			printf( '<option value="%s" lang="%s" data-continue="%s"%s>%s</option>' . "\n",
				esc_attr( $language['language'] ),
				esc_attr( current( $language['iso'] ) ),
				esc_attr( $language['strings']['continue'] ),
				in_array( $language['language'], $installed_languages ) ? ' data-installed="1"' : '',
				esc_html( $language['native_name'] ) );

			unset( $languages[ $ljmc_local_package ] );
		}
	}

	foreach ( $languages as $language ) {
		printf( '<option value="%s" lang="%s" data-continue="%s"%s>%s</option>' . "\n",
			esc_attr( $language['language'] ),
			esc_attr( current( $language['iso'] ) ),
			esc_attr( $language['strings']['continue'] ),
			in_array( $language['language'], $installed_languages ) ? ' data-installed="1"' : '',
			esc_html( $language['native_name'] ) );
	}
	echo "</select>\n";
	echo '<p class="step"><span class="spinner"></span><input id="language-continue" type="submit" class="button button-primary button-large" value="Continue" /></p>';
}

/**
 * Download a language pack.
 *
 * @since 4.0.0
 *
 * @see ljmc_get_available_translations()
 *
 * @param string $download Language code to download.
 * @return string|bool Returns the language code if successfully downloaded
 *                     (or already installed), or false on failure.
 */
function ljmc_download_language_pack( $download ) {
	// Check if the translation is already installed.
	if ( in_array( $download, get_available_languages() ) ) {
		return $download;
	}

	if ( defined( 'DISALLOW_FILE_MODS' ) && DISALLOW_FILE_MODS ) {
		return false;
	}

	// Confirm the translation is one we can download.
	$translations = ljmc_get_available_translations();
	if ( ! $translations ) {
		return false;
	}
	foreach ( $translations as $translation ) {
		if ( $translation['language'] === $download ) {
			$translation_to_load = true;
			break;
		}
	}

	if ( empty( $translation_to_load ) ) {
		return false;
	}
	$translation = (object) $translation;

	require_once ABSPATH . 'ljmc-admin/includes/class-ljmc-upgrader.php';
	$skin = new Automatic_Upgrader_Skin;
	$upgrader = new Language_Pack_Upgrader( $skin );
	$translation->type = 'core';
	$result = $upgrader->upgrade( $translation, array( 'clear_update_cache' => false ) );

	if ( ! $result || is_ljmc_error( $result ) ) {
		return false;
	}

	return $translation->language;
}

/**
 * Check if system has access to the filesystem without asking for
 * credentials.
 *
 * @since 4.0.0
 *
 * @return bool Returns true on success, false on failure.
 */
function ljmc_can_install_language_pack() {
	if ( defined( 'DISALLOW_FILE_MODS' ) && DISALLOW_FILE_MODS ) {
		return false;
	}

	require_once ABSPATH . 'ljmc-admin/includes/class-ljmc-upgrader.php';
	$skin = new Automatic_Upgrader_Skin;
	$upgrader = new Language_Pack_Upgrader( $skin );
	$upgrader->init();

	$check = $upgrader->fs_connect( array( LJMC_CONTENT_DIR, LJMC_LANG_DIR ) );

	if ( ! $check || is_ljmc_error( $check ) ) {
		return false;
	}

	return true;
}
