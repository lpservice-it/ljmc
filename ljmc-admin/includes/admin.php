<?php
/**
 * Includes all of the system Administration API files.
 *
 * @package system
 * @subpackage Administration
 */

if ( ! defined('LJMC_ADMIN') ) {
	/*
	 * This file is being included from a file other than ljmc-admin/admin.php, so
	 * some setup was skipped. Make sure the admin message catalog is loaded since
	 * load_default_textdomain() will not have done so in this context.
	 */
	load_textdomain( 'default', LJMC_LANG_DIR . '/admin-' . get_locale() . '.mo' );
}

/** system Bookmark Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/bookmark.php');

/** system Comment Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/comment.php');

/** system Administration File API */
require_once(ABSPATH . 'ljmc-admin/includes/file.php');

/** system Image Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/image.php');

/** system Media Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/media.php');

/** system Import Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/import.php');

/** system Misc Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/misc.php');

/** system Plugin Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/plugin.php');

/** system Post Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/post.php');

/** system Administration Screen API */
require_once(ABSPATH . 'ljmc-admin/includes/screen.php');

/** system Taxonomy Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/taxonomy.php');

/** system Template Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/template.php');

/** system List Table Administration API and base class */
require_once(ABSPATH . 'ljmc-admin/includes/class-ljmc-list-table.php');
require_once(ABSPATH . 'ljmc-admin/includes/list-table.php');

/** system Theme Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/theme.php');

/** system User Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/user.php');

/** system Update Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/update.php');

/** system Deprecated Administration API */
require_once(ABSPATH . 'ljmc-admin/includes/deprecated.php');

/** system Multisite support API */
if ( is_multisite() ) {
	require_once(ABSPATH . 'ljmc-admin/includes/ms.php');
	require_once(ABSPATH . 'ljmc-admin/includes/ms-deprecated.php');
}
