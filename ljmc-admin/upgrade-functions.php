<?php
/**
 * system Upgrade Functions. Old file, must not be used. Include
 * ljmc-admin/includes/upgrade.php instead.
 *
 * @deprecated 2.5.0
 * @package system
 * @subpackage Administration
 */

_deprecated_file( basename(__FILE__), '2.5', 'ljmc-admin/includes/upgrade.php' );
require_once(ABSPATH . 'ljmc-admin/includes/upgrade.php');
