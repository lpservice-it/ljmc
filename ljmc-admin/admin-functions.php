<?php
/**
 * Administration Functions
 *
 * This file is deprecated, use 'ljmc-admin/includes/admin.php' instead.
 *
 * @deprecated 2.5.0
 * @package system
 * @subpackage Administration
 */

_deprecated_file( basename(__FILE__), '2.5', 'ljmc-admin/includes/admin.php' );

/** system Administration API: Includes all Administration functions. */
require_once(ABSPATH . 'ljmc-admin/includes/admin.php');
