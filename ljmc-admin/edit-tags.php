<?php
/**
 * Edit Tags Administration Screen.
 *
 * @package system
 * @subpackage Administration
 */

/** system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! $taxnow )
	ljmc_die( __( 'Invalid taxonomy' ) );

$tax = get_taxonomy( $taxnow );

if ( ! $tax )
	ljmc_die( __( 'Invalid taxonomy' ) );

if ( ! current_user_can( $tax->cap->manage_terms ) )
	ljmc_die( __( 'Cheatin&#8217; uh?' ), 403 );

// $post_type is set when the LJMC_Terms_List_Table instance is created
global $post_type;

$ljmc_list_table = _get_list_table('LJMC_Terms_List_Table');
$pagenum = $ljmc_list_table->get_pagenum();

$title = $tax->labels->name;

if ( 'post' != $post_type ) {
	$parent_file = ( 'attachment' == $post_type ) ? 'upload.php' : "edit.php?post_type=$post_type";
	$submenu_file = "edit-tags.php?taxonomy=$taxonomy&amp;post_type=$post_type";
} elseif ( 'link_category' == $tax->name ) {
	$parent_file = 'link-manager.php';
	$submenu_file = 'edit-tags.php?taxonomy=link_category';
} else {
	$parent_file = 'edit.php';
	$submenu_file = "edit-tags.php?taxonomy=$taxonomy";
}

add_screen_option( 'per_page', array( 'default' => 20, 'option' => 'edit_' . $tax->name . '_per_page' ) );

$location = false;

switch ( $ljmc_list_table->current_action() ) {

case 'add-tag':

	check_admin_referer( 'add-tag', '_ljmcnonce_add-tag' );

	if ( !current_user_can( $tax->cap->edit_terms ) )
		ljmc_die( __( 'Cheatin&#8217; uh?' ), 403 );

	$ret = ljmc_insert_term( $_POST['tag-name'], $taxonomy, $_POST );
	$location = 'edit-tags.php?taxonomy=' . $taxonomy;
	if ( 'post' != $post_type )
		$location .= '&post_type=' . $post_type;

	if ( $referer = ljmc_get_original_referer() ) {
		if ( false !== strpos( $referer, 'edit-tags.php' ) )
			$location = $referer;
	}

	if ( $ret && !is_ljmc_error( $ret ) )
		$location = add_query_arg( 'message', 1, $location );
	else
		$location = add_query_arg( array( 'error' => true, 'message' => 4 ), $location );

	break;

case 'delete':
	$location = 'edit-tags.php?taxonomy=' . $taxonomy;
	if ( 'post' != $post_type )
		$location .= '&post_type=' . $post_type;
	if ( $referer = ljmc_get_referer() ) {
		if ( false !== strpos( $referer, 'edit-tags.php' ) )
			$location = $referer;
	}

	if ( ! isset( $_REQUEST['tag_ID'] ) ) {
		break;
	}

	$tag_ID = (int) $_REQUEST['tag_ID'];
	check_admin_referer( 'delete-tag_' . $tag_ID );

	if ( !current_user_can( $tax->cap->delete_terms ) )
		ljmc_die( __( 'Cheatin&#8217; uh?' ), 403 );

	ljmc_delete_term( $tag_ID, $taxonomy );

	$location = add_query_arg( 'message', 2, $location );

	break;

case 'bulk-delete':
	check_admin_referer( 'bulk-tags' );

	if ( !current_user_can( $tax->cap->delete_terms ) )
		ljmc_die( __( 'Cheatin&#8217; uh?' ), 403 );

	$tags = (array) $_REQUEST['delete_tags'];
	foreach ( $tags as $tag_ID ) {
		ljmc_delete_term( $tag_ID, $taxonomy );
	}

	$location = 'edit-tags.php?taxonomy=' . $taxonomy;
	if ( 'post' != $post_type )
		$location .= '&post_type=' . $post_type;
	if ( $referer = ljmc_get_referer() ) {
		if ( false !== strpos( $referer, 'edit-tags.php' ) )
			$location = $referer;
	}

	$location = add_query_arg( 'message', 6, $location );

	break;

case 'edit':
	$title = $tax->labels->edit_item;

	$tag_ID = (int) $_REQUEST['tag_ID'];

	$tag = get_term( $tag_ID, $taxonomy, OBJECT, 'edit' );
	if ( ! $tag )
		ljmc_die( __( 'You attempted to edit an item that doesn&#8217;t exist. Perhaps it was deleted?' ) );
	require_once( ABSPATH . 'ljmc-admin/admin-header.php' );
	include( ABSPATH . 'ljmc-admin/edit-tag-form.php' );
	include( ABSPATH . 'ljmc-admin/admin-footer.php' );

	exit;

case 'editedtag':
	$tag_ID = (int) $_POST['tag_ID'];
	check_admin_referer( 'update-tag_' . $tag_ID );

	if ( !current_user_can( $tax->cap->edit_terms ) )
		ljmc_die( __( 'Cheatin&#8217; uh?' ), 403 );

	$tag = get_term( $tag_ID, $taxonomy );
	if ( ! $tag )
		ljmc_die( __( 'You attempted to edit an item that doesn&#8217;t exist. Perhaps it was deleted?' ) );

	$ret = ljmc_update_term( $tag_ID, $taxonomy, $_POST );

	$location = 'edit-tags.php?taxonomy=' . $taxonomy;
	if ( 'post' != $post_type )
		$location .= '&post_type=' . $post_type;

	if ( $referer = ljmc_get_original_referer() ) {
		if ( false !== strpos( $referer, 'edit-tags.php' ) )
			$location = $referer;
	}

	if ( $ret && !is_ljmc_error( $ret ) )
		$location = add_query_arg( 'message', 3, $location );
	else
		$location = add_query_arg( array( 'error' => true, 'message' => 5 ), $location );
	break;
}

if ( ! $location && ! empty( $_REQUEST['_ljmc_http_referer'] ) ) {
	$location = remove_query_arg( array('_ljmc_http_referer', '_ljmcnonce'), ljmc_unslash($_SERVER['REQUEST_URI']) );
}

if ( $location ) {
	if ( ! empty( $_REQUEST['paged'] ) ) {
		$location = add_query_arg( 'paged', (int) $_REQUEST['paged'], $location );
	}
	ljmc_redirect( $location );
	exit;
}

$ljmc_list_table->prepare_items();
$total_pages = $ljmc_list_table->get_pagination_arg( 'total_pages' );

if ( $pagenum > $total_pages && $total_pages > 0 ) {
	ljmc_redirect( add_query_arg( 'paged', $total_pages ) );
	exit;
}

ljmc_enqueue_script('admin-tags');
if ( current_user_can($tax->cap->edit_terms) )
	ljmc_enqueue_script('inline-edit-tax');

if ( 'category' == $taxonomy || 'link_category' == $taxonomy || 'post_tag' == $taxonomy  ) {
	$help ='';
	if ( 'category' == $taxonomy )
		$help = '<p>' . sprintf(__( 'You can use categories to define sections of your site and group related posts. The default category is &#8220;Uncategorized&#8221; until you change it in your <a href="%s">writing settings</a>.' ) , 'options-writing.php' ) . '</p>';
	elseif ( 'link_category' == $taxonomy )
		$help = '<p>' . __( 'You can create groups of links by using Link Categories. Link Category names must be unique and Link Categories are separate from the categories you use for posts.' ) . '</p>';
	else
		$help = '<p>' . __( 'You can assign keywords to your posts using <strong>tags</strong>. Unlike categories, tags have no hierarchy, meaning there&#8217;s no relationship from one tag to another.' ) . '</p>';

	if ( 'link_category' == $taxonomy )
		$help .= '<p>' . __( 'You can delete Link Categories in the Bulk Action pull-down, but that action does not delete the links within the category. Instead, it moves them to the default Link Category.' ) . '</p>';
	else
		$help .='<p>' . __( 'What&#8217;s the difference between categories and tags? Normally, tags are ad-hoc keywords that identify important information in your post (names, subjects, etc) that may or may not recur in other posts, while categories are pre-determined sections. If you think of your site like a book, the categories are like the Table of Contents and the tags are like the terms in the index.' ) . '</p>';

	get_current_screen()->add_help_tab( array(
		'id'      => 'overview',
		'title'   => __('Overview'),
		'content' => $help,
	) );

	if ( 'category' == $taxonomy || 'post_tag' == $taxonomy ) {
		if ( 'category' == $taxonomy )
			$help = '<p>' . __( 'When adding a new category on this screen, you&#8217;ll fill in the following fields:' ) . '</p>';
		else
			$help = '<p>' . __( 'When adding a new tag on this screen, you&#8217;ll fill in the following fields:' ) . '</p>';

		$help .= '<ul>' .
		'<li>' . __( '<strong>Name</strong> - The name is how it appears on your site.' ) . '</li>';

		if ( ! global_terms_enabled() )
			$help .= '<li>' . __( '<strong>Slug</strong> - The &#8220;slug&#8221; is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.' ) . '</li>';

		if ( 'category' == $taxonomy )
			$help .= '<li>' . __( '<strong>Parent</strong> - Categories, unlike tags, can have a hierarchy. You might have a Jazz category, and under that have child categories for Bebop and Big Band. Totally optional. To create a subcategory, just choose another category from the Parent dropdown.' ) . '</li>';

		$help .= '<li>' . __( '<strong>Description</strong> - The description is not prominent by default; however, some themes may display it.' ) . '</li>' .
		'</ul>' .
		'<p>' . __( 'You can change the display of this screen using the Screen Options tab to set how many items are displayed per screen and to display/hide columns in the table.' ) . '</p>';

		get_current_screen()->add_help_tab( array(
			'id'      => 'adding-terms',
			'title'   => 'category' == $taxonomy ? __( 'Adding Categories' ) : __( 'Adding Tags' ),
			'content' => $help,
		) );
	}

	$help = '<p><strong>' . __( 'For more information:' ) . '</strong></p>';

	if ( 'category' == $taxonomy )
		$help .= '<p>' . __( '<a href="localhost/Posts_Categories_Screen" target="_blank">Documentation on Categories</a>' ) . '</p>';
	elseif ( 'link_category' == $taxonomy )
		$help .= '<p>' . __( '<a href="localhost/Links_Link_Categories_Screen" target="_blank">Documentation on Link Categories</a>' ) . '</p>';
	else
		$help .= '<p>' . __( '<a href="localhost/Posts_Tags_Screen" target="_blank">Documentation on Tags</a>' ) . '</p>';

	$help .= '<p>' . __('<a href="localhost/support/" target="_blank">Support Forums</a>') . '</p>';

	get_current_screen()->set_help_sidebar( $help );

	unset( $help );
}

require_once( ABSPATH . 'ljmc-admin/admin-header.php' );

if ( !current_user_can($tax->cap->edit_terms) )
	ljmc_die( __('You are not allowed to edit this item.') );

$messages = array();
$messages['_item'] = array(
	0 => '', // Unused. Messages start at index 1.
	1 => __( 'Item added.' ),
	2 => __( 'Item deleted.' ),
	3 => __( 'Item updated.' ),
	4 => __( 'Item not added.' ),
	5 => __( 'Item not updated.' ),
	6 => __( 'Items deleted.' )
);
$messages['category'] = array(
	0 => '', // Unused. Messages start at index 1.
	1 => __( 'Category added.' ),
	2 => __( 'Category deleted.' ),
	3 => __( 'Category updated.' ),
	4 => __( 'Category not added.' ),
	5 => __( 'Category not updated.' ),
	6 => __( 'Categories deleted.' )
);
$messages['post_tag'] = array(
	0 => '', // Unused. Messages start at index 1.
	1 => __( 'Tag added.' ),
	2 => __( 'Tag deleted.' ),
	3 => __( 'Tag updated.' ),
	4 => __( 'Tag not added.' ),
	5 => __( 'Tag not updated.' ),
	6 => __( 'Tags deleted.' )
);

/**
 * Filter the messages displayed when a tag is updated.
 *
 * @since 3.7.0
 *
 * @param array $messages The messages to be displayed.
 */
$messages = apply_filters( 'term_updated_messages', $messages );

$message = false;
if ( isset( $_REQUEST['message'] ) && ( $msg = (int) $_REQUEST['message'] ) ) {
	if ( isset( $messages[ $taxonomy ][ $msg ] ) )
		$message = $messages[ $taxonomy ][ $msg ];
	elseif ( ! isset( $messages[ $taxonomy ] ) && isset( $messages['_item'][ $msg ] ) )
		$message = $messages['_item'][ $msg ];
}

$class = ( isset( $_REQUEST['error'] ) ) ? 'error' : 'updated';
?>

<div class="wrap nosubsub">

<?php if ( $message ) : ?>
<div id="message" class="<?php echo $class; ?> notice is-dismissible"><p><?php echo $message; ?></p></div>
<?php $_SERVER['REQUEST_URI'] = remove_query_arg( array( 'message', 'error' ), $_SERVER['REQUEST_URI'] );
endif; ?>
<div id="ajax-response"></div>

<form class="search-form" method="get">
<input type="hidden" name="taxonomy" value="<?php echo esc_attr($taxonomy); ?>" />
<input type="hidden" name="post_type" value="<?php echo esc_attr($post_type); ?>" />

<?php $ljmc_list_table->search_box( $tax->labels->search_items, 'tag' ); ?>

</form>
<br class="clear" />

<div id="col-container">

<div class="col-wrap">
<form id="posts-filter" method="post">
<input type="hidden" name="taxonomy" value="<?php echo esc_attr($taxonomy); ?>" />
<input type="hidden" name="post_type" value="<?php echo esc_attr($post_type); ?>" />

<?php $ljmc_list_table->display(); ?>

<br class="clear" />
</form>

<?php 

/**
 * Fires after the taxonomy list table.
 *
 * The dynamic portion of the hook name, `$taxonomy`, refers to the taxonomy slug.
 *
 * @since 3.0.0
 *
 * @param string $taxonomy The taxonomy name.
 */
do_action( "after-{$taxonomy}-table", $taxonomy );
?>

</div>

<div class="col-wrap">

<?php

if ( !is_null( $tax->labels->popular_items ) ) {
	if ( current_user_can( $tax->cap->edit_terms ) )
		$tag_cloud = ljmc_tag_cloud( array( 'taxonomy' => $taxonomy, 'post_type' => $post_type, 'echo' => false, 'link' => 'edit' ) );
	else
		$tag_cloud = ljmc_tag_cloud( array( 'taxonomy' => $taxonomy, 'echo' => false ) );

	if ( $tag_cloud ) :
	?>

<?php
endif;
}

if ( current_user_can($tax->cap->edit_terms) ) {
	if ( 'category' == $taxonomy ) {
		/**
 		 * Fires before the Add Category form.
		 *
		 * @since 2.1.0
		 * @deprecated 3.0.0 Use {$taxonomy}_pre_add_form instead.
		 *
		 * @param object $arg Optional arguments cast to an object.
		 */
		do_action( 'add_category_form_pre', (object) array( 'parent' => 0 ) );
	} elseif ( 'link_category' == $taxonomy ) {
		/**
		 * Fires before the link category form.
		 *
		 * @since 2.3.0
		 * @deprecated 3.0.0 Use {$taxonomy}_pre_add_form instead.
		 *
		 * @param object $arg Optional arguments cast to an object.
		 */
		do_action( 'add_link_category_form_pre', (object) array( 'parent' => 0 ) );
	} else {
		/**
		 * Fires before the Add Tag form.
		 *
		 * @since 2.5.0
		 * @deprecated 3.0.0 Use {$taxonomy}_pre_add_form instead.
		 *
		 * @param string $taxonomy The taxonomy slug.
		 */
		do_action( 'add_tag_form_pre', $taxonomy );
	}

	/**
	 * Fires before the Add Term form for all taxonomies.
	 *
	 * The dynamic portion of the hook name, `$taxonomy`, refers to the taxonomy slug.
	 *
	 * @since 3.0.0
	 *
	 * @param string $taxonomy The taxonomy slug.
	 */
	do_action( "{$taxonomy}_pre_add_form", $taxonomy );
?>


<?php } ?>

</div>

</div><!-- /col-container -->
</div><!-- /wrap -->

<?php if ( ! ljmc_is_mobile() ) : ?>
<script type="text/javascript">
try{document.forms.addtag['tag-name'].focus();}catch(e){}
</script>
<?php
endif;

$ljmc_list_table->inline_edit();

include( ABSPATH . 'ljmc-admin/admin-footer.php' );
