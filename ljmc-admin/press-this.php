<?php
/**
 * Press This Display and Handler.
 *
 * @package system
 * @subpackage Press_This
 */

define('IFRAME_REQUEST' , true);

/** system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! current_user_can( 'edit_posts' ) || ! current_user_can( get_post_type_object( 'post' )->cap->create_posts ) )
	ljmc_die( __( 'Cheatin&#8217; uh?' ), 403 );

if ( empty( $GLOBALS['ljmc_press_this'] ) ) {
	include( ABSPATH . 'ljmc-admin/includes/class-ljmc-press-this.php' );
}

$GLOBALS['ljmc_press_this']->html();
