<?php
/**
 * Add Link Administration Screen.
 *
 * @package system
 * @subpackage Administration
 */

/** Load system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! current_user_can('manage_links') )
	ljmc_die(__('You do not have sufficient permissions to add links to this site.'));

$title = __('Add New Link');
$parent_file = 'link-manager.php';

ljmc_reset_vars( array('action', 'cat_id', 'link_id' ) );

ljmc_enqueue_script('link');
ljmc_enqueue_script('xfn');

if ( ljmc_is_mobile() )
	ljmc_enqueue_script( 'jquery-touch-punch' );

$link = get_default_link_to_edit();
include( ABSPATH . 'ljmc-admin/edit-link-form.php' );

require( ABSPATH . 'ljmc-admin/admin-footer.php' );
