<?php
/**
 * Media settings administration panel.
 *
 * @package system
 * @subpackage Administration
 */

/** system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! current_user_can( 'manage_options' ) )
	ljmc_die( __( 'You do not have sufficient permissions to manage options for this site.' ) );

$title = __('Images');
$parent_file = 'options-general.php';

$media_options_help = '<p>' . __('You can set maximum sizes for images inserted into your written content; you can also insert an image as Full Size.') . '</p>';

if ( ! is_multisite() && ( get_option('upload_url_path') || ( get_option('upload_path') != 'data/uploads' && get_option('upload_path') ) ) ) {
	$media_options_help .= '<p>' . __('Uploading Files allows you to choose the folder and path for storing your uploaded files.') . '</p>';
}

$media_options_help .= '<p>' . __('You must click the Save Changes button at the bottom of the screen for new settings to take effect.') . '</p>';

get_current_screen()->add_help_tab( array(
	'id'      => 'overview',
	'title'   => __('Overview'),
	'content' => $media_options_help,
) );

get_current_screen()->set_help_sidebar(
	'<p><strong>' . __('For more information:') . '</strong></p>' .
	'<p>' . __('<a href="localhost/Settings_Media_Screen" target="_blank">Documentation on Media Settings</a>') . '</p>' .
	'<p>' . __('<a href="localhost/support/" target="_blank">Support Forums</a>') . '</p>'
);

include( ABSPATH . 'ljmc-admin/admin-header.php' );

?>

<div class="wrap">
<h2><?php echo esc_html( $title ); ?></h2>

<form action="options.php" method="post">
<?php settings_fields('media'); ?>


<table class="form-table">
<tr>
<th scope="row"><?php _e('Thumbnail') ?></th>
<td>
<input name="thumbnail_size_w" type="number" step="1" min="0" id="thumbnail_size_w" value="<?php form_option('thumbnail_size_w'); ?>" class="small-text" />
<label for="thumbnail_size_h">x</label>
<input name="thumbnail_size_h" type="number" step="1" min="0" id="thumbnail_size_h" value="<?php form_option('thumbnail_size_h'); ?>" class="small-text" /><br />
<input name="thumbnail_crop" type="checkbox" id="thumbnail_crop" value="1" <?php checked('1', get_option('thumbnail_crop')); ?>/>
<label for="thumbnail_crop"><?php _e('Crop to exact size'); ?></label>
</td>
</tr>

<tr>
<th scope="row"><?php _e('Medium') ?></th>
<td><fieldset><legend class="screen-reader-text"><span><?php _e('Medium'); ?></span></legend>
<input name="medium_size_w" type="number" step="1" min="0" id="medium_size_w" value="<?php form_option('medium_size_w'); ?>" class="small-text" />
<label for="medium_size_h">x</label>
<input name="medium_size_h" type="number" step="1" min="0" id="medium_size_h" value="<?php form_option('medium_size_h'); ?>" class="small-text" />
</fieldset></td>
</tr>

<tr>
<th scope="row"><?php _e('Large') ?></th>
<td><fieldset><legend class="screen-reader-text"><span><?php _e('Large'); ?></span></legend>
<input name="large_size_w" type="number" step="1" min="0" id="large_size_w" value="<?php form_option('large_size_w'); ?>" class="small-text" />
<label for="large_size_h">x</label>
<input name="large_size_h" type="number" step="1" min="0" id="large_size_h" value="<?php form_option('large_size_h'); ?>" class="small-text" />
</fieldset></td>
</tr>

<?php do_settings_fields('media', 'default'); ?>
</table>

<?php if ( isset( $GLOBALS['ljmc_settings']['media']['embeds'] ) ) : ?>
<h3 class="title"><?php _e('Embeds') ?></h3>
<table class="form-table">
<?php do_settings_fields( 'media', 'embeds' ); ?>
</table>
<?php endif; ?>



<?php do_settings_sections('media'); ?>

<?php submit_button(); ?>

</form>

</div>

<?php include( ABSPATH . 'ljmc-admin/admin-footer.php' ); ?>
