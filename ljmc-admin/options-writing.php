<?php
/**
 * Writing settings administration panel.
 *
 * @package system
 * @subpackage Administration
 */

/** system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! current_user_can( 'manage_options' ) )
	ljmc_die( __( 'You do not have sufficient permissions to manage options for this site.' ) );

$title = __('Post Settings');
$parent_file = 'options-general.php';

get_current_screen()->add_help_tab( array(
	'id'      => 'overview',
	'title'   => __('Overview'),
	'content' => '<p>' . __('You can submit content in several different ways; this screen holds the settings for all of them. The top section controls the editor within the dashboard, while the rest control external publishing methods. For more information on any of these methods, use the documentation links.') . '</p>' .
		'<p>' . __('You must click the Save Changes button at the bottom of the screen for new settings to take effect.') . '</p>',
) );

/** This filter is documented in ljmc-admin/options.php */
if ( apply_filters( 'enable_post_by_email_configuration', true ) ) {
	get_current_screen()->add_help_tab( array(
		'id'      => 'options-postemail',
		'title'   => __( 'Post Via Email' ),
		'content' => '<p>' . __( 'Post via email settings allow you to send your system install an email with the content of your post. You must set up a secret e-mail account with POP3 access to use this, and any mail received at this address will be posted, so it&#8217;s a good idea to keep this address very secret.' ) . '</p>',
	) );
}

/** This filter is documented in ljmc-admin/options-writing.php */
if ( apply_filters( 'enable_update_services_configuration', true ) ) {
	get_current_screen()->add_help_tab( array(
		'id'      => 'options-services',
		'title'   => __( 'Update Services' ),
		'content' => '<p>' . __( 'If desired, system will automatically alert various services of your new posts.' ) . '</p>',
	) );
}

get_current_screen()->set_help_sidebar(
	'<p><strong>' . __('For more information:') . '</strong></p>' .
	'<p>' . __('<a href="localhost/Settings_Writing_Screen" target="_blank">Documentation on Writing Settings</a>') . '</p>' .
	'<p>' . __('<a href="localhost/support/" target="_blank">Support Forums</a>') . '</p>'
);

include( ABSPATH . 'ljmc-admin/admin-header.php' );
?>

<div class="wrap">
<h2><?php echo esc_html( $title ); ?></h2>

<form method="post" action="options.php">
<?php settings_fields('writing'); ?>

<table class="form-table">

<tr>
<th scope="row"><label for="default_category"><?php _e('Noklusētā kategorija') ?></label></th>
<td>
<?php
ljmc_dropdown_categories(array('hide_empty' => 0, 'name' => 'default_category', 'orderby' => 'name', 'selected' => get_option('default_category'), 'hierarchical' => true));
?>
</td>
</tr>
<?php
$post_formats = get_post_format_strings();
unset( $post_formats['standard'] );
?>
<?php
if ( get_option( 'link_manager_enabled' ) ) :
?>
<tr>
<th scope="row"><label for="default_link_category"><?php _e('Default Link Category') ?></label></th>
<td>
<?php
ljmc_dropdown_categories(array('hide_empty' => 0, 'name' => 'default_link_category', 'orderby' => 'name', 'selected' => get_option('default_link_category'), 'hierarchical' => true, 'taxonomy' => 'link_category'));
?>
</td>
</tr>
<?php endif; ?>

<?php
do_settings_fields('writing', 'default');
do_settings_fields('writing', 'remote_publishing'); // A deprecated section.
?>
</table>




<?php do_settings_sections('writing'); ?>

<?php submit_button(); ?>
</form>
</div>

<?php include( ABSPATH . 'ljmc-admin/admin-footer.php' ); ?>
