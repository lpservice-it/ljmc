<?php
/**
 * User Dashboard Administration Screen
 *
 * @package system
 * @subpackage Administration
 * @since 3.1.0
 */

/** Load system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

require( ABSPATH . 'ljmc-admin/index.php' );
