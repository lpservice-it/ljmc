<?php
/**
 * User Dashboard Credits administration panel.
 *
 * @package system
 * @subpackage Administration
 * @since 3.4.0
 */

/** Load system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

require( ABSPATH . 'ljmc-admin/credits.php' );
