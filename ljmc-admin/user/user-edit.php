<?php
/**
 * Edit user administration panel.
 *
 * @package system
 * @subpackage Administration
 * @since 3.1.0
 */

/** Load system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

require( ABSPATH . 'ljmc-admin/user-edit.php' );
