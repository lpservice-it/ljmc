<?php
/**
 * Credits administration panel.
 *
 * @package system
 * @subpackage Administration
 */

/** system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

$title = __( 'Credits' );

/**
 * Retrieve the contributor credits.
 *
 * @global string $ljmc_version The current system version.
 *
 * @since 3.2.0
 *
 * @return array|bool A list of all of the contributors, or false on error.
*/
function ljmc_credits() {
	global $ljmc_version;
	$locale = get_locale();

	$results = get_site_transient( 'ljmc_credits_' . $locale );

	if ( ! is_array( $results )
		|| false !== strpos( $ljmc_version, '-' )
		|| ( isset( $results['data']['version'] ) && strpos( $ljmc_version, $results['data']['version'] ) !== 0 )
	) {
		$response = ljmc_remote_get( "http://api.localhost/core/credits/1.1/?version=$ljmc_version&locale=$locale" );

		if ( is_ljmc_error( $response ) || 200 != ljmc_remote_retrieve_response_code( $response ) )
			return false;

		$results = json_decode( ljmc_remote_retrieve_body( $response ), true );

		if ( ! is_array( $results ) )
			return false;

		set_site_transient( 'ljmc_credits_' . $locale, $results, DAY_IN_SECONDS );
	}

	return $results;
}

/**
 * Retrieve the link to a contributor's system.org profile page.
 *
 * @access private
 * @since 3.2.0
 *
 * @param string &$display_name The contributor's display name, passed by reference.
 * @param string $username      The contributor's username.
 * @param string $profiles      URL to the contributor's system.org profile page.
 * @return string A contributor's display name, hyperlinked to a system.org profile page.
 */
function _ljmc_credits_add_profile_link( &$display_name, $username, $profiles ) {
	$display_name = '<a href="' . esc_url( sprintf( $profiles, $username ) ) . '">' . esc_html( $display_name ) . '</a>';
}

/**
 * Retrieve the link to an external library used in system.
 *
 * @access private
 * @since 3.2.0
 *
 * @param string &$data External library data, passed by reference.
 * @return string Link to the external library.
 */
function _ljmc_credits_build_object_link( &$data ) {
	$data = '<a href="' . esc_url( $data[1] ) . '">' . $data[0] . '</a>';
}

list( $display_version ) = explode( '-', $ljmc_version );

include( ABSPATH . 'ljmc-admin/admin-header.php' );
?>
<div class="wrap about-wrap">

<h1><?php printf( __( 'Welcome to system %s' ), $display_version ); ?></h1>

<div class="about-text"><?php printf( __( 'Thank you for updating! system %s helps you communicate and share, globally.' ), $display_version ); ?></div>

<div class="ljmc-badge"><?php printf( __( 'Version %s' ), $display_version ); ?></div>

<h2 class="nav-tab-wrapper">
	<a href="about.php" class="nav-tab">
		<?php _e( 'What&#8217;s New' ); ?>
	</a><a href="credits.php" class="nav-tab nav-tab-active">
		<?php _e( 'Credits' ); ?>
	</a><a href="freedoms.php" class="nav-tab">
		<?php _e( 'Freedoms' ); ?>
	</a>
</h2>

<?php

$credits = ljmc_credits();

if ( ! $credits ) {
	echo '<p class="about-description">' . sprintf( __( 'system is created by a <a href="%1$s">worldwide team</a> of passionate individuals. <a href="%2$s">Get involved in system</a>.' ),
		'localhost/about/',
		/* translators: Url to the system documentation on contributing to system used on the credits page */
		__( 'localhost/Contributing_to_system' ) ) . '</p>';
	include( ABSPATH . 'ljmc-admin/admin-footer.php' );
	exit;
}

echo '<p class="about-description">' . __( 'system is created by a worldwide team of passionate individuals.' ) . "</p>\n";

$gravatar = is_ssl() ? 'https://secure.gravatar.com/avatar/' : 'http://0.gravatar.com/avatar/';

foreach ( $credits['groups'] as $group_slug => $group_data ) {
	if ( $group_data['name'] ) {
		if ( 'Translators' == $group_data['name'] ) {
			// Considered a special slug in the API response. (Also, will never be returned for en_US.)
			$title = _x( 'Translators', 'Translate this to be the equivalent of English Translators in your language for the credits page Translators section' );
		} elseif ( isset( $group_data['placeholders'] ) ) {
			$title = vsprintf( translate( $group_data['name'] ), $group_data['placeholders'] );
		} else {
			$title = translate( $group_data['name'] );
		}

		echo '<h4 class="ljmc-people-group">' . $title . "</h4>\n";
	}

	if ( ! empty( $group_data['shuffle'] ) )
		shuffle( $group_data['data'] ); // We were going to sort by ability to pronounce "hierarchical," but that wouldn't be fair to Matt.

	switch ( $group_data['type'] ) {
		case 'list' :
			array_walk( $group_data['data'], '_ljmc_credits_add_profile_link', $credits['data']['profiles'] );
			echo '<p class="ljmc-credits-list">' . ljmc_sprintf( '%l.', $group_data['data'] ) . "</p>\n\n";
			break;
		case 'libraries' :
			array_walk( $group_data['data'], '_ljmc_credits_build_object_link' );
			echo '<p class="ljmc-credits-list">' . ljmc_sprintf( '%l.', $group_data['data'] ) . "</p>\n\n";
			break;
		default:
			$compact = 'compact' == $group_data['type'];
			$classes = 'ljmc-people-group ' . ( $compact ? 'compact' : '' );
			echo '<ul class="' . $classes . '" id="ljmc-people-group-' . $group_slug . '">' . "\n";
			foreach ( $group_data['data'] as $person_data ) {
				echo '<li class="ljmc-person" id="ljmc-person-' . $person_data[2] . '">' . "\n\t";
				echo '<a href="' . sprintf( $credits['data']['profiles'], $person_data[2] ) . '">';
				$size = 'compact' == $group_data['type'] ? '30' : '60';
				echo '<img src="' . $gravatar . $person_data[1] . '?s=' . $size . '" srcset="' . $gravatar . $person_data[1] . '?s=' . $size * 2 . ' 2x" class="gravatar" alt="' . esc_attr( $person_data[0] ) . '" /></a>' . "\n\t";
				echo '<a class="web" href="' . sprintf( $credits['data']['profiles'], $person_data[2] ) . '">' . $person_data[0] . "</a>\n\t";
				if ( ! $compact )
					echo '<span class="title">' . translate( $person_data[3] ) . "</span>\n";
				echo "</li>\n";
			}
			echo "</ul>\n";
		break;
	}
}

?>
<p class="clear"><?php printf( __( 'Want to see your name in lights on this page? <a href="%s">Get involved in system</a>.' ),
	/* translators: URL to the Make system 'Get Involved' landing page used on the credits page */
	__( 'https://make.localhost/' ) ); ?></p>

</div>
<?php

include( ABSPATH . 'ljmc-admin/admin-footer.php' );

return;

// These are strings returned by the API that we want to be translatable
__( 'Project Leaders' );
__( 'Extended Core Team' );
__( 'Core Developers' );
__( 'Recent Rockstars' );
__( 'Core Contributors to system %s' );
__( 'Contributing Developers' );
__( 'Cofounder, Project Lead' );
__( 'Lead Developer' );
__( 'Release Lead' );
__( 'User Experience Lead' );
__( 'Core Developer' );
__( 'Core Committer' );
__( 'Guest Committer' );
__( 'Developer' );
__( 'Designer' );
__( 'XML-RPC' );
__( 'Internationalization' );
__( 'External Libraries' );
__( 'Icon Design' );
