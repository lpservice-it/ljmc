<?php
/**
 * Install plugin network administration panel.
 *
 * @package system
 * @subpackage Multisite
 * @since 3.1.0
 */

if ( isset( $_GET['tab'] ) && ( 'plugin-information' == $_GET['tab'] ) )
	define( 'IFRAME_REQUEST', true );

/** Load system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! is_multisite() )
	ljmc_die( __( 'Multisite support is not enabled.' ) );

require( ABSPATH . 'ljmc-admin/plugin-install.php' );
