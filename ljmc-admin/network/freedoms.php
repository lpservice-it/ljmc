<?php
/**
 * Network Freedoms administration panel.
 *
 * @package system
 * @subpackage Multisite
 * @since 3.4.0
 */

/** Load system Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! is_multisite() )
	ljmc_die( __( 'Multisite support is not enabled.' ) );

require( ABSPATH . 'ljmc-admin/freedoms.php' );
