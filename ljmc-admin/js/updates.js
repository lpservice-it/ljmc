/* global tb_remove */
window.ljmc = window.ljmc || {};

(function( $, ljmc, pagenow ) {
	ljmc.updates = {};

	/**
	 * User nonce for ajax calls.
	 *
	 * @since 4.2.0
	 *
	 * @var string
	 */
	ljmc.updates.ajaxNonce = window._ljmcUpdatesSettings.ajax_nonce;

	/**
	 * Localized strings.
	 *
	 * @since 4.2.0
	 *
	 * @var object
	 */
	ljmc.updates.l10n = window._ljmcUpdatesSettings.l10n;

	/**
	 * Whether filesystem credentials need to be requested from the user.
	 *
	 * @since 4.2.0
	 *
	 * @var bool
	 */
	ljmc.updates.shouldRequestFilesystemCredentials = null;

	/**
	 * Filesystem credentials to be packaged along with the request.
	 *
	 * @since 4.2.0
	 *
	 * @var object
	 */
	ljmc.updates.filesystemCredentials = {
		ftp: {
			host: null,
			username: null,
			password: null,
			connectionType: null
		},
		ssh: {
			publicKey: null,
			privateKey: null
		}
	};

	/**
	 * Flag if we're waiting for an update to complete.
	 *
	 * @since 4.2.0
	 *
	 * @var bool
	 */
	ljmc.updates.updateLock = false;

	/**
	 * * Flag if we've done an update successfully.
	 *
	 * @since 4.2.0
	 *
	 * @var bool
	 */
	ljmc.updates.updateDoneSuccessfully = false;

	/**
	 * If the user tries to update a plugin while an update is
	 * already happening, it can be placed in this queue to perform later.
	 *
	 * @since 4.2.0
	 *
	 * @var array
	 */
	ljmc.updates.updateQueue = [];

	/**
	 * Store a jQuery reference to return focus to when exiting the request credentials modal.
	 *
	 * @since 4.2.0
	 *
	 * @var jQuery object
	 */
	ljmc.updates.$elToReturnFocusToFromCredentialsModal = null;

	/**
	 * Decrement update counts throughout the various menus.
	 *
	 * @since 3.9.0
	 *
	 * @param {string} updateType
	 */
	ljmc.updates.decrementCount = function( upgradeType ) {
		var count,
			pluginCount,
			$adminBarUpdateCount = $( '#ljmc-admin-bar-updates .ab-label' ),
			$dashboardNavMenuUpdateCount = $( 'a[href="update-core.php"] .update-plugins' ),
			$pluginsMenuItem = $( '#menu-plugins' );


		count = $adminBarUpdateCount.text();
		count = parseInt( count, 10 ) - 1;
		if ( count < 0 || isNaN( count ) ) {
			return;
		}
		$( '#ljmc-admin-bar-updates .ab-item' ).removeAttr( 'title' );
		$adminBarUpdateCount.text( count );


		$dashboardNavMenuUpdateCount.each( function( index, elem ) {
			elem.className = elem.className.replace( /count-\d+/, 'count-' + count );
		} );
		$dashboardNavMenuUpdateCount.removeAttr( 'title' );
		$dashboardNavMenuUpdateCount.find( '.update-count' ).text( count );

		if ( 'plugin' === upgradeType ) {
			pluginCount = $pluginsMenuItem.find( '.plugin-count' ).eq(0).text();
			pluginCount = parseInt( pluginCount, 10 ) - 1;
			if ( pluginCount < 0 || isNaN( pluginCount ) ) {
				return;
			}
			$pluginsMenuItem.find( '.plugin-count' ).text( pluginCount );
			$pluginsMenuItem.find( '.update-plugins' ).each( function( index, elem ) {
				elem.className = elem.className.replace( /count-\d+/, 'count-' + pluginCount );
			} );

			if (pluginCount > 0 ) {
				$( '.subsubsub .upgrade .count' ).text( '(' + pluginCount + ')' );
			} else {
				$( '.subsubsub .upgrade' ).remove();
			}
		}
	};

	/**
	 * Send an Ajax request to the server to update a plugin.
	 *
	 * @since 4.2.0
	 *
	 * @param {string} plugin
	 * @param {string} slug
	 */
	ljmc.updates.updatePlugin = function( plugin, slug ) {
		var $message, name;
		if ( 'plugins' === pagenow || 'plugins-network' === pagenow ) {
			$message = $( '[data-slug="' + slug + '"]' ).next().find( '.update-message' );
		} else if ( 'plugin-install' === pagenow ) {
			$message = $( '.plugin-card-' + slug ).find( '.update-now' );
			name = $message.data( 'name' );
			$message.attr( 'aria-label', ljmc.updates.l10n.updatingLabel.replace( '%s', name ) );
		}

		$message.addClass( 'updating-message' );
		if ( $message.html() !== ljmc.updates.l10n.updating ){
			$message.data( 'originaltext', $message.html() );
		}

		$message.text( ljmc.updates.l10n.updating );
		ljmc.a11y.speak( ljmc.updates.l10n.updatingMsg );

		if ( ljmc.updates.updateLock ) {
			ljmc.updates.updateQueue.push( {
				type: 'update-plugin',
				data: {
					plugin: plugin,
					slug: slug
				}
			} );
			return;
		}

		ljmc.updates.updateLock = true;

		var data = {
			_ajax_nonce:     ljmc.updates.ajaxNonce,
			plugin:          plugin,
			slug:            slug,
			username:        ljmc.updates.filesystemCredentials.ftp.username,
			password:        ljmc.updates.filesystemCredentials.ftp.password,
			hostname:        ljmc.updates.filesystemCredentials.ftp.hostname,
			connection_type: ljmc.updates.filesystemCredentials.ftp.connectionType,
			public_key:      ljmc.updates.filesystemCredentials.ssh.publicKey,
			private_key:     ljmc.updates.filesystemCredentials.ssh.privateKey
		};

		ljmc.ajax.post( 'update-plugin', data )
			.done( ljmc.updates.updateSuccess )
			.fail( ljmc.updates.updateError );
	};

	/**
	 * On a successful plugin update, update the UI with the result.
	 *
	 * @since 4.2.0
	 *
	 * @param {object} response
	 */
	ljmc.updates.updateSuccess = function( response ) {
		var $updateMessage, name, $pluginRow, newText;
		if ( 'plugins' === pagenow || 'plugins-network' === pagenow ) {
			$pluginRow = $( '[data-slug="' + response.slug + '"]' ).first();
			$updateMessage = $pluginRow.next().find( '.update-message' );
			$pluginRow.addClass( 'updated' ).removeClass( 'update' );

			// Update the version number in the row.
			newText = $pluginRow.find('.plugin-version-author-uri').html().replace( response.oldVersion, response.newVersion );
			$pluginRow.find('.plugin-version-author-uri').html( newText );

			// Add updated class to update message parent tr
			$pluginRow.next().addClass( 'updated' );
		} else if ( 'plugin-install' === pagenow ) {
			$updateMessage = $( '.plugin-card-' + response.slug ).find( '.update-now' );
			$updateMessage.addClass( 'button-disabled' );
			name = $updateMessage.data( 'name' );
			$updateMessage.attr( 'aria-label', ljmc.updates.l10n.updatedLabel.replace( '%s', name ) );
		}

		$updateMessage.removeClass( 'updating-message' ).addClass( 'updated-message' );
		$updateMessage.text( ljmc.updates.l10n.updated );
		ljmc.a11y.speak( ljmc.updates.l10n.updatedMsg );

		ljmc.updates.decrementCount( 'plugin' );

		ljmc.updates.updateDoneSuccessfully = true;

		/*
		 * The lock can be released since the update was successful,
		 * and any other updates can commence.
		 */
		ljmc.updates.updateLock = false;

		$(document).trigger( 'ljmc-plugin-update-success', response );

		ljmc.updates.queueChecker();
	};


	/**
	 * On a plugin update error, update the UI appropriately.
	 *
	 * @since 4.2.0
	 *
	 * @param {object} response
	 */
	ljmc.updates.updateError = function( response ) {
		var $message, name;
		ljmc.updates.updateDoneSuccessfully = false;
		if ( response.errorCode && response.errorCode == 'unable_to_connect_to_filesystem' && ljmc.updates.shouldRequestFilesystemCredentials ) {
			ljmc.updates.credentialError( response, 'update-plugin' );
			return;
		}
		if ( 'plugins' === pagenow || 'plugins-network' === pagenow ) {
			$message = $( '[data-slug="' + response.slug + '"]' ).next().find( '.update-message' );
		} else if ( 'plugin-install' === pagenow ) {
			$message = $( '.plugin-card-' + response.slug ).find( '.update-now' );

			name = $message.data( 'name' );
			$message.attr( 'aria-label', ljmc.updates.l10n.updateFailedLabel.replace( '%s', name ) );
		}
		$message.removeClass( 'updating-message' );
		$message.html( ljmc.updates.l10n.updateFailed + ': ' + response.error );
		ljmc.a11y.speak( ljmc.updates.l10n.updateFailed );

		/*
		 * The lock can be released since this failure was
		 * after the credentials form.
		 */
		ljmc.updates.updateLock = false;

		$(document).trigger( 'ljmc-plugin-update-error', response );

		ljmc.updates.queueChecker();
	};

	/**
	 * Show an error message in the request for credentials form.
	 *
	 * @param {string} message
	 * @since 4.2.0
	 */
	ljmc.updates.showErrorInCredentialsForm = function( message ) {
		var $modal = $( '.notification-dialog' );

		// Remove any existing error.
		$modal.find( '.error' ).remove();

		$modal.find( 'h3' ).after( '<div class="error">' + message + '</div>' );
	};

	/**
	 * Events that need to happen when there is a credential error
	 *
	 * @since 4.2.0
	 */
	ljmc.updates.credentialError = function( response, type ) {
		ljmc.updates.updateQueue.push( {
			'type': type,
			'data': {
				// Not cool that we're depending on response for this data.
				// This would feel more whole in a view all tied together.
				plugin: response.plugin,
				slug: response.slug
			}
		} );
		ljmc.updates.showErrorInCredentialsForm( response.error );
		ljmc.updates.requestFilesystemCredentials();
	};

	/**
	 * If an update job has been placed in the queue, queueChecker pulls it out and runs it.
	 *
	 * @since 4.2.0
	 */
	ljmc.updates.queueChecker = function() {
		if ( ljmc.updates.updateLock || ljmc.updates.updateQueue.length <= 0 ) {
			return;
		}

		var job = ljmc.updates.updateQueue.shift();

		ljmc.updates.updatePlugin( job.data.plugin, job.data.slug );
	};


	/**
	 * Request the users filesystem credentials if we don't have them already.
	 *
	 * @since 4.2.0
	 */
	ljmc.updates.requestFilesystemCredentials = function( event ) {
		if ( ljmc.updates.updateDoneSuccessfully === false ) {
			/*
			 * For the plugin install screen, return the focus to the install button
			 * after exiting the credentials request modal.
			 */
			if ( 'plugin-install' === pagenow && event ) {
				ljmc.updates.$elToReturnFocusToFromCredentialsModal = $( event.target );
			}

			ljmc.updates.updateLock = true;

			ljmc.updates.requestForCredentialsModalOpen();
		}
	};

	/**
	 * Keydown handler for the request for credentials modal.
	 *
	 * Close the modal when the escape key is pressed.
	 * Constrain keyboard navigation to inside the modal.
	 *
	 * @since 4.2.0
	 */
	ljmc.updates.keydown = function( event ) {
		if ( 27 === event.keyCode ) {
			ljmc.updates.requestForCredentialsModalCancel();
		} else if ( 9 === event.keyCode ) {
			// #upgrade button must always be the last focusable element in the dialog.
			if ( event.target.id === 'upgrade' && ! event.shiftKey ) {
				$( '#hostname' ).focus();
				event.preventDefault();
			} else if ( event.target.id === 'hostname' && event.shiftKey ) {
				$( '#upgrade' ).focus();
				event.preventDefault();
			}
		}
	};

	/**
	 * Open the request for credentials modal.
	 *
	 * @since 4.2.0
	 */
	ljmc.updates.requestForCredentialsModalOpen = function() {
		var $modal = $( '#request-filesystem-credentials-dialog' );
		$( 'body' ).addClass( 'modal-open' );
		$modal.show();

		$modal.find( 'input:enabled:first' ).focus();
		$modal.keydown( ljmc.updates.keydown );
	};

	/**
	 * Close the request for credentials modal.
	 *
	 * @since 4.2.0
	 */
	ljmc.updates.requestForCredentialsModalClose = function() {
		$( '#request-filesystem-credentials-dialog' ).hide();
		$( 'body' ).removeClass( 'modal-open' );
		ljmc.updates.$elToReturnFocusToFromCredentialsModal.focus();
	};

	/**
	 * The steps that need to happen when the modal is canceled out
	 *
	 * @since 4.2.0
	 */
	ljmc.updates.requestForCredentialsModalCancel = function() {
		// no updateLock and no updateQueue means we already have cleared things up
		var slug, $message;

		if( ljmc.updates.updateLock === false && ljmc.updates.updateQueue.length === 0 ){
			return;
		}

		slug = ljmc.updates.updateQueue[0].data.slug,

		// remove the lock, and clear the queue
		ljmc.updates.updateLock = false;
		ljmc.updates.updateQueue = [];

		ljmc.updates.requestForCredentialsModalClose();
		if ( 'plugins' === pagenow || 'plugins-network' === pagenow ) {
			$message = $( '[data-slug="' + slug + '"]' ).next().find( '.update-message' );
		} else if ( 'plugin-install' === pagenow ) {
			$message = $( '.plugin-card-' + slug ).find( '.update-now' );
		}

		$message.removeClass( 'updating-message' );
		$message.html( $message.data( 'originaltext' ) );
		ljmc.a11y.speak( ljmc.updates.l10n.updateCancel );
	};
	/**
	 * Potentially add an AYS to a user attempting to leave the page
	 *
	 * If an update is on-going and a user attempts to leave the page,
	 * open an "Are you sure?" alert.
	 *
	 * @since 4.2.0
	 */

	ljmc.updates.beforeunload = function() {
		if ( ljmc.updates.updateLock ) {
			return ljmc.updates.l10n.beforeunload;
		}
	};


	$( document ).ready( function() {
		/*
		 * Check whether a user needs to submit filesystem credentials based on whether
		 * the form was output on the page server-side.
		 *
		 * @see {ljmc_print_request_filesystem_credentials_modal() in PHP}
		 */
		ljmc.updates.shouldRequestFilesystemCredentials = ( $( '#request-filesystem-credentials-dialog' ).length <= 0 ) ? false : true;

		// File system credentials form submit noop-er / handler.
		$( '#request-filesystem-credentials-dialog form' ).on( 'submit', function() {
			// Persist the credentials input by the user for the duration of the page load.
			ljmc.updates.filesystemCredentials.ftp.hostname = $('#hostname').val();
			ljmc.updates.filesystemCredentials.ftp.username = $('#username').val();
			ljmc.updates.filesystemCredentials.ftp.password = $('#password').val();
			ljmc.updates.filesystemCredentials.ftp.connectionType = $('input[name="connection_type"]:checked').val();
			ljmc.updates.filesystemCredentials.ssh.publicKey = $('#public_key').val();
			ljmc.updates.filesystemCredentials.ssh.privateKey = $('#private_key').val();

			ljmc.updates.requestForCredentialsModalClose();

			// Unlock and invoke the queue.
			ljmc.updates.updateLock = false;
			ljmc.updates.queueChecker();

			return false;
		});

		// Close the request credentials modal when
		$( '#request-filesystem-credentials-dialog [data-js-action="close"], .notification-dialog-background' ).on( 'click', function() {
			ljmc.updates.requestForCredentialsModalCancel();
		});

		// Hide SSH fields when not selected
		$( '#request-filesystem-credentials-dialog input[name="connection_type"]' ).on( 'change', function() {
			$( this ).parents( 'form' ).find( '#private_key, #public_key' ).parents( 'label' ).toggle( ( 'ssh' == $( this ).val() ) );
		}).change();

		// Click handler for plugin updates in List Table view.
		$( '.plugin-update-tr' ).on( 'click', '.update-link', function( e ) {
			e.preventDefault();
			if ( ljmc.updates.shouldRequestFilesystemCredentials && ! ljmc.updates.updateLock ) {
				ljmc.updates.requestFilesystemCredentials( e );
			}
			var updateRow = $( e.target ).parents( '.plugin-update-tr' );
			// Return the user to the input box of the plugin's table row after closing the modal.
			ljmc.updates.$elToReturnFocusToFromCredentialsModal = $( '#' + updateRow.data( 'slug' ) ).find( '.check-column input' );
			ljmc.updates.updatePlugin( updateRow.data( 'plugin' ), updateRow.data( 'slug' ) );
		} );

		$( '.plugin-card' ).on( 'click', '.update-now', function( e ) {
			e.preventDefault();
			var $button = $( e.target );

			if ( ljmc.updates.shouldRequestFilesystemCredentials && ! ljmc.updates.updateLock ) {
				ljmc.updates.requestFilesystemCredentials( e );
			}

			ljmc.updates.updatePlugin( $button.data( 'plugin' ), $button.data( 'slug' ) );
		} );

		$( '#plugin_update_from_iframe' ).on( 'click' , function( e ) {
			var target,	data;

			target = window.parent == window ? null : window.parent,
			$.support.postMessage = !! window.postMessage;

			if ( $.support.postMessage === false || target === null || window.parent.location.pathname.indexOf( 'update-core.php' ) !== -1 )
				return;

			e.preventDefault();

			data = {
				'action' : 'updatePlugin',
				'slug'	 : $(this).data('slug')
			};

			target.postMessage( JSON.stringify( data ), window.location.origin );
		});

	} );

	$( window ).on( 'message', function( e ) {
		var event = e.originalEvent,
			message,
			loc = document.location,
			expectedOrigin = loc.protocol + '//' + loc.hostname;

		if ( event.origin !== expectedOrigin ) {
			return;
		}

		message = $.parseJSON( event.data );

		if ( typeof message.action === 'undefined' ) {
			return;
		}

		switch (message.action){
			case 'decrementUpdateCount' :
				ljmc.updates.decrementCount( message.upgradeType );
				break;
			case 'updatePlugin' :
				tb_remove();
				if ( 'plugins' === pagenow || 'plugins-network' === pagenow ) {
					// Return the user to the input box of the plugin's table row after closing the modal.
					$( '#' + message.slug ).find( '.check-column input' ).focus();
					// trigger the update
					$( '.plugin-update-tr[data-slug="' + message.slug + '"]' ).find( '.update-link' ).trigger( 'click' );
				} else if ( 'plugin-install' === pagenow ) {
					$( '.plugin-card-' + message.slug ).find( 'h4 a' ).focus();
					$( '.plugin-card-' + message.slug ).find( '[data-slug="' + message.slug + '"]' ).trigger( 'click' );
				}
				break;
		}

	} );

	$( window ).on( 'beforeunload', ljmc.updates.beforeunload );

})( jQuery, window.ljmc, window.pagenow, window.ajaxurl );
