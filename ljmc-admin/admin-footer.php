<?php
/**
 * system Administration Template Footer
 *
 * @package system
 * @subpackage Administration
 */

// don't load directly
if ( !defined('ABSPATH') )
	die('-1');
?>

<div class="clear"></div></div><!-- ljmcbody-content -->
<div class="clear"></div></div><!-- ljmcbody -->
<div class="clear"></div></div><!-- ljmccontent -->

<div id="ljmcfooter" role="contentinfo">
	<?php
	/**
	 * Fires after the opening tag for the admin footer.
	 *
	 * @since 2.5.0
	 */
	do_action( 'in_admin_footer' );
	?>
	<div class="clear"></div>
</div>
<?php
/**
 * Print scripts or data before the default footer scripts.
 *
 * @since 1.2.0
 *
 * @param string $data The data to print.
 */
do_action( 'admin_footer', '' );

/**
 * Prints any scripts and data queued for the footer.
 *
 * @since 2.8.0
 */
do_action( 'admin_print_footer_scripts' );

/**
 * Print scripts or data after the default footer scripts.
 *
 * The dynamic portion of the hook name, `$GLOBALS['hook_suffix']`,
 * refers to the global hook suffix of the current page.
 *
 * @since 2.8.0
 *
 * @param string $hook_suffix The current admin page.
 */
do_action( "admin_footer-" . $GLOBALS['hook_suffix'] );

// get_site_option() won't exist when auto upgrading from <= 2.7
if ( function_exists('get_site_option') ) {
	if ( false === get_site_option('can_compress_scripts') )
		compression_test();
}

?>

<div class="clear"></div></div><!-- ljmcwrap -->
<script type="text/javascript">if(typeof ljmcOnload=='function')ljmcOnload();jQuery('#update-nav-menu').removeAttr('action');</script>
<script type="text/javascript">jQuery(document).ready(function(){
	if(jQuery('#update-nav-menu')){
		jQuery('#update-nav-menu').removeAttr('action');
	}
});
</script>
</body>
</html>
