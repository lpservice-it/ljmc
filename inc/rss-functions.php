<?php
/**
 * Deprecated. Use rss.php instead.
 *
 * @package system
 */

_deprecated_file( basename(__FILE__), '2.1', LJMCINC . '/rss.php' );
require_once( ABSPATH . LJMCINC . '/rss.php' );
