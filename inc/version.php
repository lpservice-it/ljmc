<?php
/**
 * The system version string
 *
 * @global string $ljmc_version
 */
$ljmc_version = '4.2.4';

/**
 * Holds the system DB revision, increments when changes are made to the system DB schema.
 *
 * @global int $ljmc_db_version
 */
$ljmc_db_version = 31536;

/**
 * Holds the TinyMCE version
 *
 * @global string $tinymce_version
 */
$tinymce_version = '4109-20150505';

/**
 * Holds the required PHP version
 *
 * @global string $required_php_version
 */
$required_php_version = '5.2.4';

/**
 * Holds the required MySQL version
 *
 * @global string $required_mysql_version
 */
$required_mysql_version = '5.0';
