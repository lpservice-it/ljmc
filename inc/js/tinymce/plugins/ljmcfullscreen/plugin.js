/* global tinymce */
/**
 * LJMC Fullscreen (Distraction-Free Writing) TinyMCE plugin
 */
tinymce.PluginManager.add( 'ljmcfullscreen', function( editor ) {
	var settings = editor.settings;

	function fullscreenOn() {
		settings.ljmc_fullscreen = true;
		editor.dom.addClass( editor.getDoc().documentElement, 'ljmc-fullscreen' );
		// Start auto-resizing
		editor.execCommand( 'ljmcAutoResizeOn' );
	}

	function fullscreenOff() {
		settings.ljmc_fullscreen = false;
		editor.dom.removeClass( editor.getDoc().documentElement, 'ljmc-fullscreen' );
		// Stop auto-resizing
		editor.execCommand( 'ljmcAutoResizeOff' );
	}

	// For use from outside the editor.
	editor.addCommand( 'ljmcFullScreenOn', fullscreenOn );
	editor.addCommand( 'ljmcFullScreenOff', fullscreenOff );

	function getExtAPI() {
		return ( typeof ljmc !== 'undefined' && ljmc.editor && ljmc.editor.fullscreen );
	}

	// Toggle DFW mode. For use from inside the editor.
	function toggleFullscreen() {
		var fullscreen = getExtAPI();

		if ( fullscreen ) {
			if ( editor.getParam('ljmc_fullscreen') ) {
				fullscreen.off();
			} else {
				fullscreen.on();
			}
		}
	}

	editor.addCommand( 'ljmcFullScreen', toggleFullscreen );

	editor.on( 'keydown', function( event ) {
		var fullscreen;

		// Turn fullscreen off when Esc is pressed.
		if ( event.keyCode === 27 && ( fullscreen = getExtAPI() ) && fullscreen.settings.visible ) {
			fullscreen.off();
		}
	});

	editor.on( 'init', function() {
		// Set the editor when initializing from whitin DFW
		if ( editor.getParam('ljmc_fullscreen') ) {
			fullscreenOn();
		}
	});

	// Register buttons
	editor.addButton( 'ljmc_fullscreen', {
		tooltip: 'Distraction-free writing mode',
		shortcut: 'Alt+Shift+W',
		onclick: toggleFullscreen,
		classes: 'ljmc-fullscreen btn widget' // This overwrites all classes on the container!
	});

	editor.addMenuItem( 'ljmc_fullscreen', {
		text: 'Distraction-free writing mode',
		icon: 'ljmc_fullscreen',
		shortcut: 'Alt+Shift+W',
		context: 'view',
		onclick: toggleFullscreen
	});
});
