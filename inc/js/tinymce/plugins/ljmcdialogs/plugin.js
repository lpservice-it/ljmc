/* global tinymce */
/**
 * Included for back-compat.
 * The default WindowManager in TinyMCE 4.0 supports three types of dialogs:
 *	- With HTML created from JS.
 *	- With inline HTML (like LJMCWindowManager).
 *	- Old type iframe based dialogs.
 * For examples see the default plugins: https://github.com/tinymce/tinymce/tree/master/js/tinymce/plugins
 */
tinymce.LJMCWindowManager = tinymce.InlineWindowManager = function( editor ) {
	if ( this.ljmc ) {
		return this;
	}

	this.ljmc = {};
	this.parent = editor.windowManager;
	this.editor = editor;

	tinymce.extend( this, this.parent );

	this.open = function( args, params ) {
		var $element,
			self = this,
			ljmc = this.ljmc;

		if ( ! args.ljmcDialog ) {
			return this.parent.open.apply( this, arguments );
		} else if ( ! args.id ) {
			return;
		}

		if ( typeof jQuery === 'undefined' || ! jQuery.ljmc || ! jQuery.ljmc.ljmcdialog ) {
			// ljmcdialog.js is not loaded
			if ( window.console && window.console.error ) {
				window.console.error('ljmcdialog.js is not loaded. Please set "ljmcdialogs" as dependency for your script when calling ljmc_enqueue_script(). You may also want to enqueue the "ljmc-jquery-ui-dialog" stylesheet.');
			}

			return;
		}

		ljmc.$element = $element = jQuery( '#' + args.id );

		if ( ! $element.length ) {
			return;
		}

		if ( window.console && window.console.log ) {
			window.console.log('tinymce.LJMCWindowManager is deprecated. Use the default editor.windowManager to open dialogs with inline HTML.');
		}

		ljmc.features = args;
		ljmc.params = params;

		// Store selection. Takes a snapshot in the FocusManager of the selection before focus is moved to the dialog.
		editor.nodeChanged();

		// Create the dialog if necessary
		if ( ! $element.data('ljmcdialog') ) {
			$element.ljmcdialog({
				title: args.title,
				width: args.width,
				height: args.height,
				modal: true,
				dialogClass: 'ljmc-dialog',
				zIndex: 300000
			});
		}

		$element.ljmcdialog('open');

		$element.on( 'ljmcdialogclose', function() {
			if ( self.ljmc.$element ) {
				self.ljmc = {};
			}
		});
	};

	this.close = function() {
		if ( ! this.ljmc.features || ! this.ljmc.features.ljmcDialog ) {
			return this.parent.close.apply( this, arguments );
		}

		this.ljmc.$element.ljmcdialog('close');
	};
};

tinymce.PluginManager.add( 'ljmcdialogs', function( editor ) {
	// Replace window manager
	editor.on( 'init', function() {
		editor.windowManager = new tinymce.LJMCWindowManager( editor );
	});
});
