<?php
/**
 * Admin Bar
 *
 * This code handles the building and rendering of the press bar.
 */

/**
 * Instantiate the admin bar object and set it up as a global for access elsewhere.
 *
 * UNHOOKING THIS FUNCTION WILL NOT PROPERLY REMOVE THE ADMIN BAR.
 * For that, use show_admin_bar(false) or the 'show_admin_bar' filter.
 *
 * @since 3.1.0
 * @access private
 * @return bool Whether the admin bar was successfully initialized.
 */
function _ljmc_admin_bar_init() {
	global $ljmc_admin_bar;

	if ( ! is_admin_bar_showing() )
		return false;

	/* Load the admin bar class code ready for instantiation */
	require_once( ABSPATH . LJMCINC . '/class-ljmc-admin-bar.php' );

	/* Instantiate the admin bar */

	/**
	 * Filter the admin bar class to instantiate.
	 *
	 * @since 3.1.0
	 *
	 * @param string $ljmc_admin_bar_class Admin bar class to use. Default 'LJMC_Admin_Bar'.
	 */
	$admin_bar_class = apply_filters( 'ljmc_admin_bar_class', 'LJMC_Admin_Bar' );
	if ( class_exists( $admin_bar_class ) )
		$ljmc_admin_bar = new $admin_bar_class;
	else
		return false;

	$ljmc_admin_bar->initialize();
	$ljmc_admin_bar->add_menus();

	return true;
}

/**
 * Render the admin bar to the page based on the $ljmc_admin_bar->menu member var.
 * This is called very late on the footer actions so that it will render after anything else being
 * added to the footer.
 *
 * It includes the action "admin_bar_menu" which should be used to hook in and
 * add new menus to the admin bar. That way you can be sure that you are adding at most optimal point,
 * right before the admin bar is rendered. This also gives you access to the $post global, among others.
 *
 * @since 3.1.0
 */
function ljmc_admin_bar_render() {
	global $ljmc_admin_bar;

	if ( ! is_admin_bar_showing() || ! is_object( $ljmc_admin_bar ) )
		return false;

	/**
	 * Load all necessary admin bar items.
	 *
	 * This is the hook used to add, remove, or manipulate admin bar items.
	 *
	 * @since 3.1.0
	 *
	 * @param LJMC_Admin_Bar $ljmc_admin_bar LJMC_Admin_Bar instance, passed by reference
	 */
	do_action_ref_array( 'admin_bar_menu', array( &$ljmc_admin_bar ) );

	/**
	 * Fires before the admin bar is rendered.
	 *
	 * @since 3.1.0
	 */
	do_action( 'ljmc_before_admin_bar_render' );

	$ljmc_admin_bar->render();

	/**
	 * Fires after the admin bar is rendered.
	 *
	 * @since 3.1.0
	 */
	do_action( 'ljmc_after_admin_bar_render' );
}

/**
 * Add the system logo menu.
 *
 * @since 3.3.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_ljmc_menu( $ljmc_admin_bar ) {


	if ( is_user_logged_in() ) {
		// Add "About system" link
		$ljmc_admin_bar->add_menu( array(
			'parent' => 'ljmc-logo',
			'id'     => 'about',
			'title'  => __('About system'),
			'href'   => self_admin_url( 'about.php' ),
		) );
	}

	// Add system.org link
	$ljmc_admin_bar->add_menu( array(
		'parent'    => 'ljmc-logo-external',
		'id'        => 'ljmcorg',
		'title'     => __('system.org'),
		'href'      => __('localhost/'),
	) );

	// Add system link
	$ljmc_admin_bar->add_menu( array(
		'parent'    => 'ljmc-logo-external',
		'id'        => 'documentation',
		'title'     => __('Documentation'),
		'href'      => __('localhost/'),
	) );

	// Add forums link
	$ljmc_admin_bar->add_menu( array(
		'parent'    => 'ljmc-logo-external',
		'id'        => 'support-forums',
		'title'     => __('Support Forums'),
		'href'      => __('localhost/support/'),
	) );

	// Add feedback link
	$ljmc_admin_bar->add_menu( array(
		'parent'    => 'ljmc-logo-external',
		'id'        => 'feedback',
		'title'     => __('Feedback'),
		'href'      => __('localhost/support/forum/requests-and-feedback'),
	) );
}

/**
 * Add the sidebar toggle button.
 *
 * @since 3.8.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_sidebar_toggle( $ljmc_admin_bar ) {
	if ( is_admin() ) {
		$ljmc_admin_bar->add_menu( array(
			'id'    => 'menu-toggle',
			'title' => '<span class="ab-icon"></span><span class="screen-reader-text">' . __( 'Menu' ) . '</span>',
			'href'  => '#',
		) );
	}
}

/**
 * Add the "My Account" item.
 *
 * @since 3.3.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_my_account_item( $ljmc_admin_bar ) {
	$user_id      = get_current_user_id();
	$current_user = ljmc_get_current_user();
	$profile_url  = get_edit_profile_url( $user_id );

	if ( ! $user_id )
		return;

	$style = 'style="' . 'padding-top:5px;padding-right:10px' . '"';
	$avatar = '<div ' . $style . ' class="dashicons dashicons-before dashicons-admin-users"><br></div>';
	$howdy  = sprintf( __('%1$s'), $current_user->display_name );
	$class  = empty( $avatar ) ? '' : 'with-avatar';

	$ljmc_admin_bar->add_menu( array(
		'id'        => 'my-account',
		'parent'    => 'top-secondary',
		'title'     => $avatar . $howdy,
		'href'      => $profile_url,
		'meta'      => array(
			'class'     => $class,
		),
	) );
}

/**
 * Add the "My Account" submenu items.
 *
 * @since 3.1.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_my_account_menu( $ljmc_admin_bar ) {
	$user_id      = get_current_user_id();
	$current_user = ljmc_get_current_user();
	$profile_url  = get_edit_profile_url( $user_id );

	if ( ! $user_id )
		return;

	$ljmc_admin_bar->add_group( array(
		'parent' => 'my-account',
		'id'     => 'user-actions',
	) );

	$user_info  = get_avatar( $user_id, 64 );
	$user_info .= "<span class='display-name'>{$current_user->display_name}</span>";

	if ( $current_user->display_name !== $current_user->user_login )
		$user_info .= "<span class='username'>{$current_user->user_login}</span>";

	$ljmc_admin_bar->add_menu( array(
		'parent' => 'user-actions',
		'id'     => 'edit-profile',
		'title'  => __( 'Labot profilu' ),
		'href' => $profile_url,
	) );
	$ljmc_admin_bar->add_menu( array(
		'parent' => 'user-actions',
		'id'     => 'logout',
		'title'  => __( 'Log Out' ),
		'href'   => ljmc_logout_url(),
	) );
}

/**
 * Add the "Site Name" menu.
 *
 * @since 3.3.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_site_menu( $ljmc_admin_bar ) {
	// Don't show for logged out users.
	if ( ! is_user_logged_in() )
		return;

	// Show only when the user is a member of this site, or they're a super admin.
	if ( ! is_user_member_of_blog() && ! is_super_admin() )
		return;

	$blogname = get_bloginfo('name');

	if ( ! $blogname ) {
		$blogname = preg_replace( '#^(https?://)?(www.)?#', '', get_home_url() );
	}

	if ( is_network_admin() ) {
		$blogname = sprintf( __('Network Admin: %s'), esc_html( get_current_site()->site_name ) );
	} elseif ( is_user_admin() ) {
		$blogname = sprintf( __('Global Dashboard: %s'), esc_html( get_current_site()->site_name ) );
	}

	$title = ljmc_html_excerpt( $blogname, 40, '&hellip;' );

	$ljmc_admin_bar->add_menu( array(
		'id'    => 'site-name',
		'title' => $title,
		'href'  => is_admin() ? home_url( '/' ) : admin_url(),
	) );

	// Create submenu items.

	if ( is_admin() ) {


		if ( is_blog_admin() && is_multisite() && current_user_can( 'manage_sites' ) ) {
			$ljmc_admin_bar->add_menu( array(
				'parent' => 'site-name',
				'id'     => 'edit-site',
				'title'  => __( 'Edit Site' ),
				'href'   => network_admin_url( 'site-info.php?id=' . get_current_blog_id() ),
			) );
		}

	} else {
		// We're on the front end, link to the Dashboard.
		$ljmc_admin_bar->add_menu( array(
			'parent' => 'site-name',
			'id'     => 'dashboard',
			'title'  => __( 'Dashboard' ),
			'href'   => admin_url(),
		) );

		// Add the appearance submenu items.
		ljmc_admin_bar_appearance_menu( $ljmc_admin_bar );
	}
}

/**
 * Add the "My Sites/[Site Name]" menu and all submenus.
 *
 * @since 3.1.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_my_sites_menu( $ljmc_admin_bar ) {
	// Don't show for logged out users or single site mode.
	if ( ! is_user_logged_in() || ! is_multisite() )
		return;

	// Show only when the user has at least one site, or they're a super admin.
	if ( count( $ljmc_admin_bar->user->blogs ) < 1 && ! is_super_admin() )
		return;

	if ( $ljmc_admin_bar->user->active_blog ) {
		$my_sites_url = get_admin_url( $ljmc_admin_bar->user->active_blog->blog_id, 'my-sites.php' );
	} else {
		$my_sites_url = admin_url( 'my-sites.php' );
	}

	$ljmc_admin_bar->add_menu( array(
		'id'    => 'my-sites',
		'title' => __( 'My Sites' ),
		'href'  => $my_sites_url,
	) );

	if ( is_super_admin() ) {
		$ljmc_admin_bar->add_group( array(
			'parent' => 'my-sites',
			'id'     => 'my-sites-super-admin',
		) );


		$ljmc_admin_bar->add_menu( array(
			'parent' => 'network-admin',
			'id'     => 'network-admin-d',
			'title'  => __( 'Dashboard' ),
			'href'   => network_admin_url(),
		) );
		$ljmc_admin_bar->add_menu( array(
			'parent' => 'network-admin',
			'id'     => 'network-admin-u',
			'title'  => __( 'Users' ),
			'href'   => network_admin_url( 'users.php' ),
		) );
		$ljmc_admin_bar->add_menu( array(
			'parent' => 'network-admin',
			'id'     => 'network-admin-p',
			'title'  => __( 'Plugins' ),
			'href'   => network_admin_url( 'plugins.php' ),
		) );
	}

	// Add site links
	$ljmc_admin_bar->add_group( array(
		'parent' => 'my-sites',
		'id'     => 'my-sites-list',
		'meta'   => array(
			'class' => is_super_admin() ? 'ab-sub-secondary' : '',
		),
	) );

	foreach ( (array) $ljmc_admin_bar->user->blogs as $blog ) {
		switch_to_blog( $blog->userblog_id );

		$blavatar = '<div class="blavatar"></div>';

		$blogname = $blog->blogname;

		if ( ! $blogname ) {
			$blogname = preg_replace( '#^(https?://)?(www.)?#', '', get_home_url() );
		}

		$menu_id  = 'blog-' . $blog->userblog_id;

		$ljmc_admin_bar->add_menu( array(
			'parent'    => 'my-sites-list',
			'id'        => $menu_id,
			'title'     => $blavatar . $blogname,
			'href'      => admin_url(),
		) );

		$ljmc_admin_bar->add_menu( array(
			'parent' => $menu_id,
			'id'     => $menu_id . '-d',
			'title'  => __( 'Dashboard' ),
			'href'   => admin_url(),
		) );

		if ( current_user_can( get_post_type_object( 'post' )->cap->create_posts ) ) {
			$ljmc_admin_bar->add_menu( array(
				'parent' => $menu_id,
				'id'     => $menu_id . '-n',
				'title'  => __( 'New Post' ),
				'href'   => admin_url( 'post-new.php' ),
			) );
		}

		if ( current_user_can( 'edit_posts' ) ) {
			$ljmc_admin_bar->add_menu( array(
				'parent' => $menu_id,
				'id'     => $menu_id . '-c',
				'title'  => __( 'Manage Comments' ),
				'href'   => admin_url( 'edit-comments.php' ),
			) );
		}

		$ljmc_admin_bar->add_menu( array(
			'parent' => $menu_id,
			'id'     => $menu_id . '-v',
			'title'  => __( 'Visit Site' ),
			'href'   => home_url( '/' ),
		) );

		restore_current_blog();
	}
}

/**
 * Provide a shortlink.
 *
 * @since 3.1.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_shortlink_menu( $ljmc_admin_bar ) {
	$short = ljmc_get_shortlink( 0, 'query' );
	$id = 'get-shortlink';

	if ( empty( $short ) )
		return;

	$html = '<input class="shortlink-input" type="text" readonly="readonly" value="' . esc_attr( $short ) . '" />';

	$ljmc_admin_bar->add_menu( array(
		'id' => $id,
		'title' => __( 'Shortlink' ),
		'href' => $short,
		'meta' => array( 'html' => $html ),
	) );
}

/**
 * Provide an edit link for posts and terms.
 *
 * @since 3.1.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_edit_menu( $ljmc_admin_bar ) {
	global $tag, $ljmc_the_query;

	if ( is_admin() ) {
		$current_screen = get_current_screen();
		$post = get_post();

		if ( 'post' == $current_screen->base
			&& 'add' != $current_screen->action
			&& ( $post_type_object = get_post_type_object( $post->post_type ) )
			&& current_user_can( 'read_post', $post->ID )
			&& ( $post_type_object->public )
			&& ( $post_type_object->show_in_admin_bar ) )
		{
			if( 'draft' == $post->post_status ) {
				$preview_link = set_url_scheme( get_permalink( $post->ID ) );
				/** This filter is documented in ljmc-admin/includes/meta-boxes.php */
				$preview_link = apply_filters( 'preview_post_link', add_query_arg( 'preview', 'true', $preview_link ), $post );
				$ljmc_admin_bar->add_menu( array(
					'id' => 'preview',
					'title' => $post_type_object->labels->view_item,
					'href' => esc_url( $preview_link ),
					'meta' => array( 'target' => 'ljmc-preview-' . $post->ID ),
				) );
			} else {
				$ljmc_admin_bar->add_menu( array(
					'id' => 'view',
					'title' => $post_type_object->labels->view_item,
					'href' => get_permalink( $post->ID )
				) );
			}
		} elseif ( 'edit-tags' == $current_screen->base
			&& isset( $tag ) && is_object( $tag )
			&& ( $tax = get_taxonomy( $tag->taxonomy ) )
			&& $tax->public )
		{
			$ljmc_admin_bar->add_menu( array(
				'id' => 'view',
				'title' => $tax->labels->view_item,
				'href' => get_term_link( $tag )
			) );
		}
	} else {
		$current_object = $ljmc_the_query->get_queried_object();

		if ( empty( $current_object ) )
			return;

		if ( ! empty( $current_object->post_type )
			&& ( $post_type_object = get_post_type_object( $current_object->post_type ) )
			&& current_user_can( 'edit_post', $current_object->ID )
			&& $post_type_object->show_ui && $post_type_object->show_in_admin_bar
			&& $edit_post_link = get_edit_post_link( $current_object->ID ) )
		{
			$ljmc_admin_bar->add_menu( array(
				'id' => 'edit',
				'title' => $post_type_object->labels->edit_item,
				'href' => $edit_post_link
			) );
		} elseif ( ! empty( $current_object->taxonomy )
			&& ( $tax = get_taxonomy( $current_object->taxonomy ) )
			&& current_user_can( $tax->cap->edit_terms )
			&& $tax->show_ui
			&& $edit_term_link = get_edit_term_link( $current_object->term_id, $current_object->taxonomy ) )
		{
			$ljmc_admin_bar->add_menu( array(
				'id' => 'edit',
				'title' => $tax->labels->edit_item,
				'href' => $edit_term_link
			) );
		}
	}
}

/**
 * Add "Add New" menu.
 *
 * @since 3.1.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_new_content_menu( $ljmc_admin_bar ) {
	$actions = array();

	$cpts = (array) get_post_types( array( 'show_in_admin_bar' => true ), 'objects' );

	if ( isset( $cpts['post'] ) && current_user_can( $cpts['post']->cap->create_posts ) )
		$actions[ 'post-new.php' ] = array( $cpts['post']->labels->name_admin_bar, 'new-post' );

	if ( isset( $cpts['attachment'] ) && current_user_can( 'upload_files' ) )
		$actions[ 'media-new.php' ] = array( $cpts['attachment']->labels->name_admin_bar, 'new-media' );

	if ( current_user_can( 'manage_links' ) )
		$actions[ 'link-add.php' ] = array( _x( 'Link', 'add new from admin bar' ), 'new-link' );

	if ( isset( $cpts['page'] ) && current_user_can( $cpts['page']->cap->create_posts ) )
		$actions[ 'post-new.php?post_type=page' ] = array( $cpts['page']->labels->name_admin_bar, 'new-page' );

	unset( $cpts['post'], $cpts['page'], $cpts['attachment'] );

	// Add any additional custom post types.
	foreach ( $cpts as $cpt ) {
		if ( ! current_user_can( $cpt->cap->create_posts ) )
			continue;

		$key = 'post-new.php?post_type=' . $cpt->name;
		$actions[ $key ] = array( $cpt->labels->name_admin_bar, 'new-' . $cpt->name );
	}
	// Avoid clash with parent node and a 'content' post type.
	if ( isset( $actions['post-new.php?post_type=content'] ) )
		$actions['post-new.php?post_type=content'][1] = 'add-new-content';

	if ( current_user_can( 'create_users' ) || current_user_can( 'promote_users' ) )
		$actions[ 'user-new.php' ] = array( _x( 'User', 'add new from admin bar' ), 'new-user' );

	if ( ! $actions )
		return;

	$title = '<span class="ab-icon"></span><span class="ab-label">' . _x( 'New', 'admin bar menu group label' ) . '</span>';

	$ljmc_admin_bar->add_menu( array(
		'id'    => 'new-content',
		'title' => $title,
		'href'  => admin_url( current( array_keys( $actions ) ) ),
	) );

	foreach ( $actions as $link => $action ) {
		list( $title, $id ) = $action;

		$ljmc_admin_bar->add_menu( array(
			'parent'    => 'new-content',
			'id'        => $id,
			'title'     => $title,
			'href'      => admin_url( $link )
		) );
	}
}

/**
 * Add edit comments link with awaiting moderation count bubble.
 *
 * @since 3.1.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_comments_menu( $ljmc_admin_bar ) {
}

/**
 * Add appearance submenu items to the "Site Name" menu.
 *
 * @since 3.1.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_appearance_menu( $ljmc_admin_bar ) {
	$ljmc_admin_bar->add_group( array( 'parent' => 'site-name', 'id' => 'appearance' ) );

	$current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$customize_url = add_query_arg( 'url', urlencode( $current_url ), ljmc_customize_url() );
	$ljmc_admin_bar->add_menu( array(
		'parent' => 'appearance',
		'id'     => 'posts',
		'title'  => __( 'Posts' ),
		'href'   => admin_url( 'edit.php' ),
	) );

	$ljmc_admin_bar->add_menu( array(
		'parent' => 'appearance',
		'id'     => 'pages',
		'title'  => __( 'Pages' ),
		'href'   => admin_url( 'edit.php?post_type=page' ),
	) );

	$ljmc_admin_bar->add_menu( array(
		'parent' => 'appearance',
		'id'     => 'media',
		'title'  => __( 'Media' ),
		'href'   => admin_url( 'upload.php' ),
	) );

	if (current_theme_supports( 'menus' )) :
		$ljmc_admin_bar->add_menu( array(
				'parent' => 'appearance',
				'id'     => 'menus',
				'title'  => __( 'Menus' ),
				'href'   => admin_url( 'nav-menus.php' ),
			) );
	endif;

	if ( current_theme_supports( 'widgets' ) ) :
		$ljmc_admin_bar->add_menu( array(
				'parent' => 'appearance',
				'id'     => 'widgets',
				'title'  => __( 'Widgets' ),
				'href'   => admin_url( 'widgets.php' ),
			) );
	endif;


	$ljmc_admin_bar->add_menu( array(
		'parent' => 'appearance',
		'id'     => 'users',
		'title'  => __( 'Users' ),
		'href'   => admin_url( 'users.php' ),
	) );

	$ljmc_admin_bar->add_menu( array(
		'parent' => 'appearance',
		'id'     => 'settings',
		'title'  => __( 'Settings' ),
		'href'   => admin_url( 'options-general.php' ),
	) );

	if ( ! current_user_can( 'edit_theme_options' ) ) {
		return;
	}



}

/**
 * Provide an update link if theme/plugin/core updates are available.
 *
 * @since 3.1.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_updates_menu( $ljmc_admin_bar ) {

	$update_data = ljmc_get_update_data();

	if ( !$update_data['counts']['total'] )
		return;

	$title = '<span class="ab-icon"></span><span class="ab-label">' . number_format_i18n( $update_data['counts']['total'] ) . '</span>';
	$title .= '<span class="screen-reader-text">' . $update_data['title'] . '</span>';

}

/**
 * Add search form.
 *
 * @since 3.3.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_search_menu( $ljmc_admin_bar ) {
	if ( is_admin() )
		return;

	$form  = '<form action="' . esc_url( home_url( '/' ) ) . '" method="get" id="adminbarsearch">';
	$form .= '<input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" />';
	$form .= '<label for="adminbar-search" class="screen-reader-text">' . __( 'Search' ) . '</label>';
	$form .= '<input type="submit" class="adminbar-button" value="' . __('Search') . '"/>';
	$form .= '</form>';

}

/**
 * Add secondary menus.
 *
 * @since 3.3.0
 *
 * @param LJMC_Admin_Bar $ljmc_admin_bar
 */
function ljmc_admin_bar_add_secondary_groups( $ljmc_admin_bar ) {
	$ljmc_admin_bar->add_group( array(
		'id'     => 'top-secondary',
		'meta'   => array(
			'class' => 'ab-top-secondary',
		),
	) );

	$ljmc_admin_bar->add_group( array(
		'parent' => 'ljmc-logo',
		'id'     => 'ljmc-logo-external',
		'meta'   => array(
			'class' => 'ab-sub-secondary',
		),
	) );
}

/**
 * Style and scripts for the admin bar.
 *
 * @since 3.1.0
 */
function ljmc_admin_bar_header() { ?>
<style type="text/css" media="print">#ljmcadminbar { display:none; }</style>
<?php
}

/**
 * Default admin bar callback.
 *
 * @since 3.1.0
 */
function _admin_bar_bump_cb() { ?>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
<?php
}

/**
 * Set the display status of the admin bar.
 *
 * This can be called immediately upon plugin load. It does not need to be called from a function hooked to the init action.
 *
 * @since 3.1.0
 *
 * @param bool $show Whether to allow the admin bar to show.
 * @return void
 */
function show_admin_bar( $show ) {
	global $show_admin_bar;
	$show_admin_bar = (bool) $show;
}

/**
 * Determine whether the admin bar should be showing.
 *
 * @since 3.1.0
 *
 * @return bool Whether the admin bar should be showing.
 */
function is_admin_bar_showing() {
	global $show_admin_bar, $pagenow;

	// For all these types of requests, we never want an admin bar.
	if ( defined('XMLRPC_REQUEST') || defined('DOING_AJAX') || defined('IFRAME_REQUEST') )
		return false;

	// Integrated into the admin.
	if ( is_admin() )
		return true;

	if ( ! isset( $show_admin_bar ) ) {
		if ( ! is_user_logged_in() || 'user-login.php' == $pagenow ) {
			$show_admin_bar = false;
		} else {
			$show_admin_bar = _get_admin_bar_pref();
		}
	}

	/**
	 * Filter whether to show the admin bar.
	 *
	 * Returning false to this hook is the recommended way to hide the admin bar.
	 * The user's display preference is used for logged in users.
	 *
	 * @since 3.1.0
	 *
	 * @param bool $show_admin_bar Whether the admin bar should be shown. Default false.
	 */
	$show_admin_bar = apply_filters( 'show_admin_bar', $show_admin_bar );

	return $show_admin_bar;
}

/**
 * Retrieve the admin bar display preference of a user.
 *
 * @since 3.1.0
 * @access private
 *
 * @param string $context Context of this preference check. Defaults to 'front'. The 'admin'
 * 	preference is no longer used.
 * @param int $user Optional. ID of the user to check, defaults to 0 for current user.
 * @return bool Whether the admin bar should be showing for this user.
 */
function _get_admin_bar_pref( $context = 'front', $user = 0 ) {
	$pref = get_user_option( "show_admin_bar_{$context}", $user );
	if ( false === $pref )
		return true;

	return 'true' === $pref;
}
