<?php



define('XMLRPC_REQUEST', true);

$_COOKIE = array();

if ( !isset( $HTTP_RAW_POST_DATA ) ) {
$HTTP_RAW_POST_DATA = file_get_contents( 'php://input' );
}

if ( isset($HTTP_RAW_POST_DATA) )
$HTTP_RAW_POST_DATA = trim($HTTP_RAW_POST_DATA);


include('./load.php');

if ( isset( $_GET['rsd'] ) ) { header('Content-Type: text/xml; charset=' . get_option('blog_charset'), true);
?>
<?php echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>
<rsd version="1.0" xmlns="http://archipelago.phrasewise.com/rsd">
  <service>
    <engineName>system</engineName>
    <engineLink>localhost/</engineLink>
    <homePageLink><?php bloginfo_rss('url') ?></homePageLink>
    <apis>
      <api name="system" blogID="1" preferred="true" apiLink="<?php echo site_url('support-xml.php', 'rpc') ?>" />
      <api name="Movable Type" blogID="1" preferred="false" apiLink="<?php echo site_url('support-xml.php', 'rpc') ?>" />
      <api name="MetaWeblog" blogID="1" preferred="false" apiLink="<?php echo site_url('support-xml.php', 'rpc') ?>" />
      <api name="Blogger" blogID="1" preferred="false" apiLink="<?php echo site_url('support-xml.php', 'rpc') ?>" />
      <?php
 
do_action( 'xmlrpc_rsd_apis' );
?>
    </apis>
  </service>
</rsd>
<?php
exit;
}

include_once(ABSPATH . 'ljmc-admin/includes/admin.php');
include_once(ABSPATH . LJMCINC . '/class-IXR.php');
include_once(ABSPATH . LJMCINC . '/class-ljmc-xmlrpc-server.php');


$post_default_title = "";


$ljmc_xmlrpc_server_class = apply_filters( 'ljmc_xmlrpc_server_class', 'ljmc_xmlrpc_server' );
$ljmc_xmlrpc_server = new $ljmc_xmlrpc_server_class;

$ljmc_xmlrpc_server->serve_request();

exit;


function logIO( $io, $msg ) {
_deprecated_function( __FUNCTION__, '3.4', 'error_log()' );
if ( ! empty( $GLOBALS['xmlrpc_logging'] ) )
error_log( $io . ' - ' . $msg );
}

?>