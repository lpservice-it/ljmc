<?php


if (empty($ljmc)) {
require_once( dirname( __FILE__ ) . '/load.php' );
ljmc( array( 'tb' => '1' ) );
}


function trackback_response($error = 0, $error_message = '') {
header('Content-Type: text/xml; charset=' . get_option('blog_charset') );
if ($error) {
echo '<?xml version="1.0" encoding="utf-8"?'.">\n";
echo "<response>\n";
echo "<error>1</error>\n";
echo "<message>$error_message</message>\n";
echo "</response>";
die();
} else {
echo '<?xml version="1.0" encoding="utf-8"?'.">\n";
echo "<response>\n";
echo "<error>0</error>\n";
echo "</response>";
}
}

$request_array = 'HTTP_POST_VARS';

if ( !isset($_GET['tb_id']) || !$_GET['tb_id'] ) {
$tb_id = explode('/', $_SERVER['REQUEST_URI']);
$tb_id = intval( $tb_id[ count($tb_id) - 1 ] );
}

$tb_url = isset($_POST['url']) ? $_POST['url'] : '';
$charset = isset($_POST['charset']) ? $_POST['charset'] : '';

$title = isset($_POST['title']) ? ljmc_unslash($_POST['title']) : '';
$excerpt = isset($_POST['excerpt']) ? ljmc_unslash($_POST['excerpt']) : '';
$blog_name = isset($_POST['blog_name']) ? ljmc_unslash($_POST['blog_name']) : '';

if ($charset)
$charset = str_replace( array(',', ' '), '', strtoupper( trim($charset) ) );
else
$charset = 'ASCII, UTF-8, ISO-8859-1, JIS, EUC-JP, SJIS';

if ( false !== strpos($charset, 'UTF-7') )
die;

if ( function_exists('mb_convert_encoding') ) {
$title = mb_convert_encoding($title, get_option('blog_charset'), $charset);
$excerpt = mb_convert_encoding($excerpt, get_option('blog_charset'), $charset);
$blog_name = mb_convert_encoding($blog_name, get_option('blog_charset'), $charset);
}

$title = ljmc_slash($title);
$excerpt = ljmc_slash($excerpt);
$blog_name = ljmc_slash($blog_name);

if ( is_single() || is_page() )
$tb_id = $posts[0]->ID;

if ( !isset($tb_id) || !intval( $tb_id ) )
trackback_response(1, 'I really need an ID for this to work.');

if (empty($title) && empty($tb_url) && empty($blog_name)) {
 ljmc_redirect(get_permalink($tb_id));
exit;
}

if ( !empty($tb_url) && !empty($title) ) {
header('Content-Type: text/xml; charset=' . get_option('blog_charset') );

if ( !pings_open($tb_id) )
trackback_response(1, 'Sorry, trackbacks are closed for this item.');

$title = ljmc_html_excerpt( $title, 250, '&#8230;' );
$excerpt = ljmc_html_excerpt( $excerpt, 252, '&#8230;' );

$comment_post_ID = (int) $tb_id;
$comment_author = $blog_name;
$comment_author_email = '';
$comment_author_url = $tb_url;
$comment_content = "<strong>$title</strong>\n\n$excerpt";
$comment_type = 'trackback';

$dupe = $ljmcdb->get_results( $ljmcdb->prepare("SELECT * FROM $ljmcdb->comments WHERE comment_post_ID = %d AND comment_author_url = %s", $comment_post_ID, $comment_author_url) );
if ( $dupe )
trackback_response(1, 'We already have a ping from that URL for this post.');

$commentdata = compact('comment_post_ID', 'comment_author', 'comment_author_email', 'comment_author_url', 'comment_content', 'comment_type');

ljmc_new_comment($commentdata);
$trackback_id = $ljmcdb->insert_id;


do_action( 'trackback_post', $trackback_id );
trackback_response( 0 );
}
?>