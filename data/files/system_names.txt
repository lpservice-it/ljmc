** SYSTEM / SYS / STEM**
1. Syre
2. Stematic

** CORE **
1. Coretic
2. Coredit
4. Edicore (EDIT + CORE)
5. Corsy (CORE + SYS)

** DEV (DEVELOPE) **
1. Devonic
3. Decore
4. Devmin

** ADMIN **
1. Adminimal
2. Adminal
3. Adminize
4. Adminimize
6. Adminity

** PLUGIN **
1. Plugineer