    
    <?php get_header(); ?>



	<?php
	global $query_string;

	$query_args = explode("&", $query_string);
	$search_query = array();

	foreach($query_args as $key => $string) {
		$query_split = explode("=", $string);
		$search_query[$query_split[0]] = urldecode($query_split[1]);
	} // foreach
	$search = new LJMC_Query($search_query);
	global $ljmc_query;
	$results = $ljmc_query->posts;
	?>


   	<section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">
	        <div class="row">
	          <div class="col-xs-12" style="text-align:center">
	          <h1><?php _e('Search','ljmc-theme'); ?></h1>
	          
	          <p style="margin-top:15px"><i><?php echo $ljmc_query->query['s']; ?> ...</i></p>
	          </div>
	        </div>
	      </div>
      </div>

      <div class="container" style="margin-top:80px; margin-bottom:80px">
          	<div class="row">

				<?php
				$posts = array();
				$pages = array();
				$services = array();
				$specialists = array();
				$hospitals = array();
				$prices = array();
				$timetables = array();

				foreach($results as $result){
					if($result->post_type == 'post'){
						$posts[] = $result;
					}elseif($result->post_type == 'page'){
						$pages[] = $result;
					}elseif($result->post_type == 'specialist'){
						$specialists[] = $result;
					}elseif($result->post_type == 'service'){
						$services[] = $result;
					}elseif($result->post_type == 'hospital'){
						$hospitals[] = $result;
					}else{
						
					}
				}

				?>

				<?php if(!empty($posts)) : ?>
      			<div class="col-xs-12">
	          		<div class="page-header" style="margin-top:0">
	                  <h1><span class="line"><?php _e('News','ljmc-theme'); ?></span></h1>
	                </div>
	          	<?php foreach ($posts as $post) : ?>
	          			
		                
	                <?php
	                $image = get_field('image',$post->ID);
	                ?>
	                <div class="col-xs-12 col-sm-6 col-md-4">
	                    <div class="thumbnail">
	                      <a href="<?php echo get_the_permalink($post->ID); ?>"><div style="width:100%;height:200px;overflow:hidden"><img style="width: 100%; min-height:100%; display: block;" src="<?php echo $image['url']; ?>"></div></a>
	                      <div class="caption">
	                        <h3 style="margin:5px 0" id="thumbnail-label"><a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></a></h3>
	                        <p><?php echo ljmc_get_the_excerpt($post->ID); ?></p>
	                        <p style="font-weight:400;color:#777;font-size:13px;margin-bottom:15px"><span class="fa fa-clock-o" style="margin-right:5px"></span> <?php echo get_post_time('d/m/Y', true, $post->ID); ?></p>
	                      </div>
	                    </div>
	                  </div>


	          	<?php endforeach; ?>
	          	</div>
	          	<?php endif; ?>




	          	<?php if(!empty($pages)) : ?>
      			<div class="col-xs-12">
	          		<div class="page-header" style="margin-top:0">
	                  <h1><span class="line"><?php _e('Pages','ljmc-theme'); ?></span></h1>
	                </div>
	          	<?php foreach ($pages as $page) : ?>
	          			

	                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
	                    <div class="thumbnail">
	                      <div class="caption">
	                        <h3 style="margin:5px 0" id="thumbnail-label"><a href="<?php echo get_permalink($page->ID); ?>"><?php echo get_the_title($page->ID); ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></a></h3>
	                        <p><?php echo ljmc_get_the_excerpt($page->ID); ?></p>
	                      </div>
	                    </div>
	                  </div>


	          	<?php endforeach; ?>
	          	</div>
	          	<?php endif; ?>






	          	<?php if(!empty($specialists)) : ?>
	          		<div class="col-xs-12">
		          		<div class="page-header" style="margin-top:0">
		                  <h1><span class="line"><?php _e('Specialists','ljmc-theme'); ?></span></h1>
		                </div>
	          	<?php foreach ($specialists as $specialist) : ?>
	          			


          			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
	                    <div class="thumbnail">
	                    <?php if(get_field('image',$specialist->ID)) : $image = get_field('image',$specialist->ID); ?>
	                      <a href="<?php echo get_the_permalink($specialist->ID); ?>"><div style="width:100%;height:200px;overflow:hidden;text-align:center"><img style="width:100%; display: block;" src="<?php echo $image; ?>"></div></a>
	                    <?php endif; ?>
	                      <div class="caption">
	                        <h4 id="thumbnail-label" style="text-align:center"><a href="<?php echo get_the_permalink($specialist->ID); ?>"><?php echo get_the_title($specialist->ID); ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></a></h4>
	                        <?php $postterms = get_the_terms($specialist->ID,'profession');?>
	                      </div>
	                    </div>
		                  </div>


	          	<?php endforeach; ?>
	          	</div>
	          	<?php endif; ?>





	          	<?php if(!empty($services)) : ?>
      			<div class="col-xs-12">
	          		<div class="page-header" style="margin-top:0">
	                  <h1><span class="line"><?php _e('Services','ljmc-theme'); ?></span></h1>
	                </div>
	          	<?php foreach ($services as $service) : ?>

	                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="position:relative;">
	                <div class="thumbnail" style="min-height:190px">
	                <?php if(get_field('icon',$service->ID)) : $image = get_field('icon',$service->ID); ?>
	                      <a href="<?php echo get_the_permalink($service->ID); ?>"><div style="width:100%;height:100px;overflow:hidden;text-align:center"><img style="text-align:center;width:100px;height:auto" src="<?php echo $image; ?>"></div></a>
	                    <?php endif; ?>
		                <div class="caption" style="text-align:center">
		                  <h4 style="text-align:center;font-weight:400"><a style="text-align:center" href="<?php echo get_the_permalink($service->ID); ?>"><?php echo get_the_title($service->ID); ?></a></h4>
		                </div>
		                </div>
             		</div>


	          	<?php endforeach; ?>
	          	</div>
	          	<?php endif; ?>




	          	<?php if(!empty($hospitals)) : ?>
      			<div class="col-xs-12">
	          		<div class="page-header" style="margin-top:0">
	                  <h1><span class="line"><?php _e('Hospitals','ljmc-theme'); ?></span></h1>
	                </div>
	          	<?php foreach ($hospitals as $hospital) : ?>
	          			

	                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
	                    <div class="thumbnail">
	                      <div class="caption">
	                        <h3  style="margin:5px 0" id="thumbnail-label"><a href="<?php echo get_the_permalink($hospital->ID); ?>"><?php echo get_the_title($hospital->ID); ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></a></h3>
	                        <p><?php $address = get_field('address',$hospital->ID); echo $address['address']; ?></p>
	                      </div>
	                    </div>
	                  </div>


	          	<?php endforeach; ?>
	          	</div>
	          	<?php endif; ?>


	          	<?php if(!empty($prices)) : ?>
      			<div class="col-xs-12">
	          		<div class="page-header" style="margin-top:0">
	                  <h1><span class="line"><?php _e('Cenas','ljmc-theme'); ?></span></h1>
	                </div>
	          	<?php foreach ($prices as $price) : ?>
	          			

	                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
	                    <div class="thumbnail">
	                      <div class="caption">
	                        <h3 style="margin:5px 0" id="thumbnail-label"><a href="<?php echo get_permalink($price->ID); ?>"><?php echo get_the_title($price->ID); ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></a></h3>
	                        <p><?php echo ljmc_get_the_excerpt($price->ID); ?></p>
	                      </div>
	                    </div>
	                  </div>


	          	<?php endforeach; ?>
	          	</div>
	          	<?php endif; ?>


      		</div>   
      </div>  
    </section>

    <?php get_footer(); ?>

    