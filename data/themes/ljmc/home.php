    
    <?php get_header(); ?>

    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden">
      <div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">
        <div class="row">
          <div class="col-xs-12" style="text-align:center">
          <h1><?php _e('News','ljmc-theme'); ?></h1>
          
          <ol class="breadcrumb" style="background:none;margin-bottom:80px">
            <li><a href="<?php echo home_url(); ?>"><?php _e('Home','ljmc-theme'); ?></a></li>
            <li><a style="text-decoration:none;color:#3b3b3b;cursor:default"><?php _e('Archive','ljmc-theme'); ?></a></li>
          </ol>
          </div>
        </div>
      </div>
      </div>

      <div id="newspage" class="container" style="margin-top:80px; margin-bottom:80px">
          <div class="row">


              <div class="col-xs-12 col-md-9">
                <div class="page-header" style="margin-top:0">
                  <h1><span class="line"><?php _e('News','ljmc-theme'); ?></span></h1>
                </div>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                	<?php if(get_field('image')) $image = get_field('image'); ?>
                  <?php
                  $image = get_field('image');

                  if(!isset($image['url']) || empty($image)){
                    $image['url'] = ljmc_base_url(false) . 'img/jmc.jpg';
                  }
                  ?>
                <div class="col-xs-12 col-sm-6">
                    <div class="thumbnail">
                      <a href="<?php the_permalink(); ?>"><div style="width:100%;height:200px;overflow:hidden"><img style="width: 100%; min-height:100%; display: block;" src="<?php echo $image['url']; ?>"></div></a>
                      <div class="caption">
                        <a href="<?php the_permalink(); ?>"><p style="font-weight:500;margin-top:15px"><?php the_title(); ?></p><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></a>
                        <?php the_excerpt(); ?>
                        <p style="font-weight:400;color:#777;font-size:13px;margin-bottom:15px"><span class="fa fa-clock-o" style="margin-right:5px"></span> <?php echo get_post_time('d/m/Y', true); ?></p>
                      </div>
                    </div>
                  </div>
              <?php endwhile; endif; ?>
              <div class="col-xs-12" style="text-align:center">
              <?php
              $pagination = get_the_posts_pagination( array(
                  'mid_size' => 2,
                  'prev_text' => __( '<i class="fa fa-angle-left"></i>','ljmc' ),
                  'next_text' => __( '<i class="fa fa-angle-right"></i>','ljmc' ),
              ) );
              echo $pagination;
              ?>
              </div>
              <script type="text/javascript">
              jQuery('h2.screen-reader-text').remove();
              </script>
              </div>

            <div class="col-md-3 hidden-xs hidden-sm">
                <div style="margin-top:0" class="page-header">
                  <h1><span class="line"><?php _e('Archive', 'ljmc-theme'); ?></span></h1>
                </div>

              <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                <option value=""><?php echo esc_attr( __( 'Select Month','ljmc-theme' ) ); ?></option> 
                <?php ljmc_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
              </select>

              <div style="margin-top:50px" class="page-header">
                  <h1><span class="line"><?php _e('Categories', 'ljmc-theme'); ?></span></h1>
                </div>

              <?php
                $args = array(
                  'menu'              => 'news',
                  'theme_location'    => 'news',
                  'depth'             => 2,
                  'container_id'      => 'bs-example-navbar-collapse-1',
                  'menu_class'        => 'nav nav-pills nav-stacked nav-pills-sidebar'
                );
                ljmc_nav_menu($args);
                ?>
          </div>
      </div>   
      </div>
    </section>

    <?php get_footer(); ?>

    