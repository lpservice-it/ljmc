  <?php /* Template Name: Partneri */ ?>    
    
    <?php get_header(); ?>


    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="row">
            <div class="col-xs-12" style="text-align:center">
            <h1><?php the_title(); ?></h1>
            
            <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

        </div>
      </div>


      <div class="container partners" style="margin-top:80px">
        <div class="row">
          <?php
          $partners = get_field('partners','option');
          foreach ($partners as $partner) {
            ?>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
              <a target="_blank" title="<?php echo $partner['name']; ?>" href="<?php echo $partner['url']; ?>"><img class="img-responsive" src="<?php echo $partner['logo']; ?>" class="img-responsive img-thumbnail" alt="<?php echo $partner['name']; ?>" style="margin-top: 20px"></a>
            </div>
            <?php
          }
          ?>
        </div>
      </div>


      <div class="container" style="margin-bottom:80px">
        <div class="row">

            <div class="col-xs-12">

              <?php the_content(); ?>
              <?php if(get_field('gallery')) : ?>
              <div class="page-header">
                <h1><span class="line"><?php _e('Gallery','ljmc-theme'); ?></span></h1>
              </div>

              <div id="lightgallery<?php echo get_the_ID();?>">
                <?php
                    $gallery_field = get_field('gallery');
                    foreach($gallery_field as $image){
                      ?>
                            <a href="<?php echo $image['url']; ?>">
                                <div class="col-xs-2" style="padding:0;margin:0;"><img class="img-responsive" src="<?php echo $image['url']; ?>" /></div>
                            </a>
                      <?php
                    }
                    ?>
              </div>
              <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery("#lightgallery"+<?php echo get_the_ID();?>).lightGallery(); 
                });
            </script>
            <?php endif; ?>
            </div>
          </div>
      </div>
    </section>

    <?php get_footer(); ?>

    