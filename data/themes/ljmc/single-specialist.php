    
    <?php get_header(); ?>


    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="row">
            <div class="col-xs-12" style="text-align:center">
            <h1><?php the_title(); ?></h1>
            
            <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

        </div>
      </div>


<nav class="navbar search hidden-xs">
        <div class="container">
            <div class="row">
                  <?php
                  $args = array(
                      'post_type' => 'page',
                      'fields' => 'ids',
                      'nopaging' => true,
                      'meta_key' => '_ljmc_page_template',
                      'meta_value' => 'page-specialists.php'
                  );
                  $pages = get_posts( $args );
                  foreach ( $pages as $page ) 
                      $servicesid = $page;

                  $url = get_permalink($servicesid);
                  $lang = ICL_LANGUAGE_CODE;
                  ?>
              <form id="search-doctor-form" action="<?php echo get_url_for_language($url,$lang); ?>" method="post">
                <div class="col-xs-3" style="position:relative">
                  <h3 style="margin:0;padding:0;line-height:1.1;vertical-align:middle;text-align:right"><span class="label label-default"><i class="fa fa-user-md" style="font-size:21px; margin-right:10px"></i><span style="font-weight:300"><?php _e('Search doctor','ljmc-theme'); ?></span></span></h3>
                </div>


                <div id="search-hospital-wrap" class="col-xs-3" style="position:relative">
                  <select id="search-hospital" class="search-doctor hospital selectpicker show-tick" title="<?php _e('Hospital...','ljmc-theme'); ?>" name="search-hospital[]" multiple>
                    <option value="0" disabled="disabled" style="text-align:left;font-size:90%"><?php _e('Choose hospital...','ljmc-theme'); ?></option>
                    <?php
                    $hospital_list = ljmc_get_hospitals();

                    $html = '';
                    foreach($hospital_list as $value){
                      $html .= '<option value="' . $value["id"] . '">' . $value["name"] . '</option>';
                    }
                    echo $html;
                    ?>
                  </select>
                </div>


                <div id="search-profession-wrap" class="col-xs-3" style="position:relative">
                  <select id="search-profession" class="search-doctor profession selectpicker show-tick" title="<?php _e('Profession...','ljmc-theme'); ?>" name="search-profession[]" multiple data-live-search="true">
                    <option value="0" disabled="disabled" style="text-align:left;font-size:90%"><?php _e('Choose profession...','ljmc-theme'); ?></option>
                  </select>
                </div>

                <div id="search-submit-wrap" class="col-xs-3">
                  <button id="search-submit-result" class="btn btn-red search"><?php _e('View','ljmc-theme'); ?></button>
                </div>
              </form>
            </div>
        </div>
      </nav>
      
      <div class="container" style="margin-top:80px;margin-bottom:80px">
        <div class="row">

          <div class="col-sm-4 col-xs-12">
            <div class="thumbnail">
            <?php if(get_field('image')) : $image = get_field('image'); ?>
              <div style="text-align:center"><a href="<?php the_permalink(); ?>"><div class="text-align:center"><img style="max-width:100%" src="<?php echo $image; ?>"></div></a></div>
            <?php endif; ?>
              <div class="caption">
                <h3 id="thumbnail-label" style="text-align:center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></a></h3>
                <div style="text-align:center"><?php $postterms = get_the_terms($post->ID,'profession'); foreach($postterms as $term){echo '<p><b>' . $term->name . '</b></p>';}?></div>
              </div>
            </div>
          </div>

          <div class="col-sm-8 col-xs-12">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <?php the_content(); ?>

           <?php endwhile; endif; ?>
           <?php if(get_field('gallery')) : ?>
              <div class="page-header">
                <h1><span class="line"><?php _e('Gallery','ljmc-theme'); ?></span></h1>
              </div>

              <div id="lightgallery<?php echo get_the_ID();?>">
                <?php
                    $gallery_field = get_field('gallery');
                    foreach($gallery_field as $image){
                      ?>
                            <a href="<?php echo $image['url']; ?>">
                                <div class="col-xs-2" style="padding:0;margin:0;"><img class="img-responsive" src="<?php echo $image['url']; ?>" /></div>
                            </a>
                      <?php
                    }
                    ?>
              </div>
              <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery("#lightgallery"+<?php echo get_the_ID();?>).lightGallery(); 
                });
            </script>
            <?php endif; ?>
            <?php if(get_field('contacts')) : ?>
            <div class="page-header" style="margin-top:0">
              <h1 style="margin-top:0;margin-bottom:20px"><span class="line"><?php _e('Contacts','ljmc-theme'); ?></span></h1>

              <?php
              $contacts = get_field('contacts');
              foreach ($contacts as $contact) {
                ?>
                <div class="thumbnail">
                  <div class="caption">
                    <?php
                    $icon = $contact['icon'];
                    switch($icon){
                      case 'E-mail':
                        $fa = 'fa-envelope';
                        break;
                      case 'Fax':
                        $fa = 'fa-fax';
                        break;
                      default:
                        $fa = 'fa-phone';
                        break;
                    }
                    ?>
                    <p style="margin:0"><i style="color:#cf4944; padding-right:5px" class="fa <?php echo $fa; ?>"></i><b><?php echo $contact['name'] . ':</b> ' . $contact['value']; ?></p>
                  </div>
                </div>
                <?php
              }
              ?>
            </div>

          <?php else : ?>
            <?php
            $telephone = get_field('telephone','option');
            $email = get_field('e-mail','option');
            ?>
            <div class="page-header" style="margin-top:0">
              <h1 style="margin-top:0;margin-bottom:20px"><span class="line"><?php _e('Contacts','ljmc-theme'); ?></span></h1>
                <div class="thumbnail">
                  <div class="caption">
                    <p style="margin:0"><i style="color:#cf4944; padding-right:5px" class="fa fa-envelope"></i><b><?php echo __('E-mail','ljmc-theme') . ':</b> ' . $email; ?></p>
                  </div>
                </div>
            </div>
          <?php endif; ?>
          </div>
            
        </div>
      </div>

      
      <div class="quote-wrap" style="position:relative;width:100%;height:auto;overflow:hidden;background:rgba(0, 0, 0, 0) url('<?php ljmc_base_url(); ?>img/174283720.jpg') no-repeat fixed center center / cover !important">
      <div style="position:absolute;top:0;left:0;background:rgba(0,0,0,0.7); width:100%;height:100%;"></div>
        <div class="container">
          <div class="row">
            <div class="col-xs-12" style="text-align:center;margin:80px 0 25px">
              <i class="fa fa-quote-right" style="font-size:30px;color:#faf9f1"></i>
            </div>

            <div class="col-xs-12" style="text-align:center;margin:0 0 80px">
              <p style="font-weight:300;font-size:18px;color:#faf9f1">
              <?php
              if(get_field('quote')) :
                the_field('quote');
              else:
                _e('A healthy outside starts from the inside.','ljmc-theme');
              endif;
              ?>
              </p>
            </div>
          </div>
        </div>
      </div>


      <div class="container" style="margin-top:80px;margin-bottom:80px">
              <?php $timetable = get_field('timetable'); $i = 1;?>
              <?php foreach ($timetable as $table) : ?>
                <?php $timetable = $table['timetable']; $hospital = $table['hospital'];?>
                <?php if($i<=1){$in = ' in';}else{$in='';} ?>
                        <div class="col-md-6 col-xs-12">
                          <div class="panel panel-red">
                            <div class="panel-heading">
                              <i class="fa fa-clock-o fa-fw"></i> <?php echo get_the_title($hospital->ID); ?>
                            </div>

                            <div class="panel-body">
                              <?php $time = get_field('timetable',$timetable[0]->ID);?>

                              <?php if(!empty($time)) : ?>

                              <?php
                                foreach ($time as $day) {
                                  switch ($day['day']) {
                        case 'Monday':
                          $day['day'] = __('Monday','ljmc-theme');
                          break;
                        
                        case 'Tuesday':
                          $day['day'] = __('Tuesday','ljmc-theme');
                          break;

                        case 'Wednesday':
                          $day['day'] = __('Wednesday','ljmc-theme');
                          break;

                        case 'Thursday':
                          $day['day'] = __('Thursday','ljmc-theme');
                          break;

                        case 'Friday':
                          $day['day'] = __('Friday','ljmc-theme');
                          break;

                        case 'Saturday':
                          $day['day'] = __('Saturday','ljmc-theme');
                          break;

                        case 'Sunday':
                          $day['day'] = __('Sunday','ljmc-theme');
                          break;

                        default:
                          break;
                      }
                                  ?>
                                  <div class="col-xs-5"><?php echo $day['day']; ?></div>
                                  <div class="col-xs-7">
                                  <?php

                                  foreach ($day['timetable'] as $hours) {
                                    ?>
                                    <p><?php echo $hours['from-hour'].':'.$hours['from-minute'].' - '.$hours['to-hour'].':'.$hours['to-minute']; ?></p>
                                    <?php
                                  }
                                  ?>
                                  </div>

                                  <?php
                                }
                              ?>

                            <?php else: ?>

                              <p><i class="fa fa-pencil" style="color:#cf4240; margin-right:5px"></i><?php _e('By appointment','ljmc-theme'); ?></p>

                            <?php endif; ?>

                            </div>
                            <!-- /.panel-body -->
                          </div>
                        </div>
<?php $i++; endforeach; ?>
<?php if(get_field('pricelists')) : ?>
                        <div class="col-xs-12" style="margin-top:25px">
                          <div class="panel panel-red">
                            <div class="panel-heading">
                              <i class="fa fa-eur fa-fw"></i> <?php _e('Prices','ljmc-theme'); ?>
                            </div>
                            
                            <div class="panel-body">
                              <?php $pricelists = get_field('pricelists'); ?>
                              <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th><?php _e('#','ljmc-theme'); ?></th>
                                    <th><?php _e('Name','ljmc-theme'); ?></th>
                                    <th><?php _e('Code','ljmc-theme'); ?></th>
                                    <th><?php _e('Price','ljmc-theme'); ?></th>
                                  </tr>
                                </thead>
                                <tbody>
                              <?php
                              $i = 1;
                                foreach ($pricelists as $pricelist) {
                                  $pricelist = get_field('pricelist',$pricelist['pricelist']->ID);
                                  foreach ($pricelist as $price) :
                                  ?>
                                  <tr>
                                    <th scope="row"><?php echo $i; ?></th>
                                    <td><?php echo $price['name']; ?></td>
                                    <td><?php echo $price['code']; ?></td>
                                    <td>&euro; <?php echo $price['price']; ?></td>
                                  </tr>
                                  <?php
                                  $i++;
                                  endforeach;
                                }
                              ?>
                             </tbody>
                             </table>
                            </div>
                          
                            <!-- /.panel-body -->
                          </div>
                        </div>
                        <?php endif; ?>
              
          </div>
        </div>
      </div>
    </section>

    <?php get_footer(); ?>

    