<?php
define('LJMC_ROOTDIR', dirname(dirname(__FILE__)) . '/');
define('LJMC_INCDIR', LJMC_ROOTDIR . 'inc/');

define('LJMC_THEMEDIR', LJMC_ROOTDIR . 'data/themes/ljmc' . '/');

define('LJMC_THEME_URI',get_template_directory_uri());
?>