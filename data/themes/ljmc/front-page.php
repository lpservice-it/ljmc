    
    <?php get_header(); ?>


    <section id="content" style="margin-bottom:80px;padding-top:40px">
      <div class="container services" style="margin-top:40px">
        <div class="row">
          <div class="col-xs-12">
            <div class="page-header">
              <h1><span class="line"><?php _e('Services','ljmc-theme'); ?></span></h1>
            </div>
          </div>

        <?php
        $services = get_field('services');

        foreach ($services as $service) {
          $icon = get_field('icon',$service->ID);
          ?>
          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div class="icon"><a href="<?php echo get_the_permalink($service->ID); ?>"><img style="max-height:100px;max-width:100px;margin-top:20px" src="<?php echo $icon; ?>" /></a></div>
            <div class="caption">
              <h4 style="text-align:center;font-weight:400;margin:20px 0"><a href="<?php echo get_the_permalink($service->ID); ?>"><?php echo get_the_title($service->ID); ?></a></h4>
              <p style="text-align:center"><?php echo ljmc_get_the_excerpt($service->ID); ?></p>
            </div>
          </div>
          <?php
        }
        ?>

          
        </div>
      </div>


      <div class="container news" style="margin-top:40px">
        <div class="row">
          <div class="col-xs-12">
            <div class="page-header">
              <h1><span class="line"><?php _e('News','ljmc-theme'); ?></span></h1>
            </div>
          </div>
        </div>

        <div class="row">


          <?php
          query_posts('post_type=post&posts_per_page=4');
          // The Query
          if ( have_posts() ) : while ( have_posts() ) : the_post();
          $image = get_field('image');

          if(!isset($image['url']) || empty($image)){
            $image['url'] = ljmc_base_url(false) . 'img/jmc.jpg';
          }
          ?>


          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div class="thumb" style="width:100%; height:200px; overflow:hidden"><a href="<?php the_permalink(); ?>"><img src="<?php echo $image['url']; ?>" class="img-news" alt="Responsive image" style="margin-top: 20px"></div>
              <div class="caption">
                <a href="<?php the_permalink(); ?>"><p style="font-weight:500;margin-top:15px"><?php the_title(); ?></p></a>
               <p style="font-weight:400;color:#777;font-size:13px"><span class="fa fa-clock-o" style="margin-right:5px"></span> <?php echo get_post_time('d/m/Y', true); ?></p>
              </div>
          </div>


          <?php
          endwhile; endif;

          // Reset Query
          ljmc_reset_query();
          ?>
        </div>
      </div>


      <div class="container partners" style="margin-top:40px">
        <div class="row">
          <div class="col-xs-12">
            <div class="page-header">
              <h1><span class="line"><?php _e('Partners','ljmc-theme'); ?></span></h1>
            </div>
          </div>
        </div>

        <div class="row">
          <?php
          $partners = get_field('partners','option');
          foreach ($partners as $partner) {
            ?>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
              <a target="_blank" title="<?php echo $partner['name']; ?>" href="<?php echo $partner['url']; ?>"><img class="img-responsive" src="<?php echo $partner['logo']; ?>" class="img-responsive img-thumbnail" alt="<?php echo $partner['name']; ?>" style="margin-top: 20px"></a>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </section>


    <?php get_footer(); ?>

    