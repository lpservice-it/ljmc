    <?php /* Template Name: Valde */ ?>
        
    <?php get_header(); ?>


    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="row">
            <div class="col-xs-12" style="text-align:center">
            <h1><?php the_title(); ?></h1>
            
            <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

        </div>
      </div>




      <div class="container" style="margin-top:80px;margin-bottom:80px">

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <div class="row">
            <div class="col-xs-3">
              <?php
                $args = array(
                  'menu'              => 'about',
                  'theme_location'    => 'about',
                  'depth'             => 2,
                  'container_id'      => 'bs-example-navbar-collapse-1',
                  'menu_class'        => 'nav nav-pills nav-stacked nav-pills-sidebar'
                );
                ljmc_nav_menu($args);
                ?>
            </div>

            <div class="col-xs-9">
              <div class="col-xs-12">

                <?php the_content(); ?>
                <?php if(get_field('gallery')) : ?>
              <div class="page-header">
                <h1><span class="line"><?php _e('Gallery','ljmc-theme'); ?></span></h1>
              </div>

              <div id="lightgallery<?php echo get_the_ID();?>">
                <?php
                    $gallery_field = get_field('gallery');
                    foreach($gallery_field as $image){
                      ?>
                            <a href="<?php echo $image['url']; ?>">
                                <div class="col-xs-2" style="padding:0;margin:0;"><img class="img-responsive" src="<?php echo $image['url']; ?>" /></div>
                            </a>
                      <?php
                    }
                    ?>
              </div>
              <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery("#lightgallery"+<?php echo get_the_ID();?>).lightGallery(); 
                });
            </script>
            <?php endif; ?>
              </div>

              


<?php if(get_field('cauncil')) : ?>

              <div class="col-xs-12">
                <div class="page-header" style="margin-top:0">
                  <h1><span class="line"><?php _e('Cauncil','ljmc-theme'); ?></span></h1>
                </div>

                <?php

                $documents = get_field('cauncil');

                foreach ($documents as $key => $document) {
                  ?>

                  <div class="col-xs-12" style="padding-left:0;padding-right:0">
                  <div class="panel panel-red">
            <div class="panel-heading">
              <h3 class="panel-title"><?php echo $document['name']; ?></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div align="center" class="col-md-3 col-lg-3 "> <img class="img-rounded img-responsive" src="<?php echo $document['image']; ?>"> </div>
                <div class="col-md-9 col-lg-9 "> 
                  <table class="table">
                    <tbody>
                      <tr>
                        <td><?php _e('Position:','ljmc-theme'); ?></td>
                        <td><?php echo $document['position']; ?></td>
                      </tr>
                      <tr>
                        <td><?php _e('Year of Birth:','ljmc-theme'); ?></td>
                        <td><?php echo $document['birthday']; ?></td>
                      </tr>
                      <tr>
                        <td><?php _e('Education:','ljmc-theme'); ?></td>
                        <td><?php echo $document['education']; ?></td>
                      </tr>
                   
                         <tr>
                             </tr><tr>
                        <td><?php _e('Elected period:','ljmc-theme'); ?></td>
                        <td><?php echo $document['period']; ?></td>
                      </tr>
                        <tr>
                        <td><?php _e('Experience:','ljmc-theme'); ?></td>
                        <td><?php echo $document['experience']; ?></td>
                      </tr>
                      <tr>
                        <td><?php _e('Auctions held:','ljmc-theme'); ?></td>
                        <td><?php echo $document['auctions']; ?></td>
                      </tr>
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            
          </div>
                    <!-- <div class="thumbnail" style="min-height:100px">
                      <div class="caption">
                        <div style="padding:0;width:100%:height:300px;overflow:hidden" class="col-xs-3"><img style="width:100%;max-height:100%" src="<?php echo $document['image']; ?>" /></div>


                        <div class="col-xs-9">
                          <h4 id="thumbnail-label" style="margin-top:0;font-weight:400"><?php echo $document['name']; ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></h4>
                          <p style="padding-bottom:15px;border-bottom:1px solid #ddd"><?php echo $document['position']; ?></p>
                          <p><span style="color:#cf4240"><?php _e('Year of Birth:','ljmc-theme'); ?></span> <?php echo $document['birthday']; ?></p>
                          <p><span style="color:#cf4240"><?php _e('Education:','ljmc-theme'); ?></span> <?php echo $document['education']; ?></p>
                          <p><span style="color:#cf4240"><?php _e('Elected period:','ljmc-theme'); ?></span> <?php echo $document['period']; ?></p>
                          <p><span style="color:#cf4240"><?php _e('Experience:','ljmc-theme'); ?></span> <?php echo $document['experience']; ?></p>
                          <p><span style="color:#cf4240"><?php _e('Auctions held:','ljmc-theme'); ?></span> <?php echo $document['auctions']; ?></p>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div> -->
                  </div>

                  <?php
                }

                ?>

              </div>

            <?php endif;?>
            </div>
          </div>

        <?php endwhile; endif; ?>

      </div>
    </section>

    <?php get_footer(); ?>

    