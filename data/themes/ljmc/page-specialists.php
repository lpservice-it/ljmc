  <?php /* Template Name: Speciālisti */ ?>
    
    <?php get_header(); ?>

<?php  ?>
    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"s><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="row">
            <div class="col-xs-12" style="text-align:center">
            <h1><?php the_title(); ?></h1>
            
            <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

        </div>
      </div>

<nav class="navbar search hidden-xs">
        <div class="container">
            <div class="row">
                  <?php
                  $args = array(
                      'post_type' => 'page',
                      'fields' => 'ids',
                      'nopaging' => true,
                      'meta_key' => '_ljmc_page_template',
                      'meta_value' => 'page-specialists.php'
                  );
                  $pages = get_posts( $args );
                  foreach ( $pages as $page ) 
                      $servicesid = $page;

                  $url = get_permalink($servicesid);
                  $lang = ICL_LANGUAGE_CODE;
                  ?>
              <form id="search-doctor-form" action="<?php echo get_url_for_language($url,$lang); ?>" method="post">
                <div class="col-xs-3" style="position:relative">
                  <h3 style="margin:0;padding:0;line-height:1.1;vertical-align:middle;text-align:right"><span class="label label-default"><i class="fa fa-user-md" style="font-size:21px; margin-right:10px"></i><span style="font-weight:300"><?php _e('Search doctor','ljmc-theme'); ?></span></span></h3>
                </div>


                <div id="search-hospital-wrap" class="col-xs-3" style="position:relative">
                  <select id="search-hospital" class="search-doctor hospital selectpicker show-tick" title="<?php _e('Hospital...','ljmc-theme'); ?>" name="search-hospital[]" multiple>
                    <option value="0" disabled="disabled" style="text-align:left;font-size:90%"><?php _e('Choose hospital...','ljmc-theme'); ?></option>
                    <?php
                    $hospital_list = ljmc_get_hospitals();

                    $html = '';
                    foreach($hospital_list as $value){
                      $html .= '<option value="' . $value["id"] . '">' . $value["name"] . '</option>';
                    }
                    echo $html;
                    ?>
                  </select>
                </div>


                <div id="search-profession-wrap" class="col-xs-3" style="position:relative">
                  <select id="search-profession" class="search-doctor profession selectpicker show-tick" title="<?php _e('Profession...','ljmc-theme'); ?>" name="search-profession[]" multiple data-live-search="true">
                    <option value="0" disabled="disabled" style="text-align:left;font-size:90%"><?php _e('Choose profession...','ljmc-theme'); ?></option>
                  </select>
                </div>

                <div id="search-submit-wrap" class="col-xs-3">
                  <button id="search-submit-result" class="btn btn-red search"><?php _e('View','ljmc-theme'); ?></button>
                </div>
              </form>
            </div>
        </div>
      </nav>

      <div class="container" style="margin-top:80px;margin-bottom:80px">
        <div class="row">

            <?php
            if(isset($_POST['search-hospital'])){
              $search_hospital = $_POST['search-hospital'];
              $search_list = ljmc_get_hospitals();
              $selected_hospitals = array();
              foreach ($search_list as $value) {
                if(in_array($value["id"], $search_hospital)){
                  $selected_hospitals[] = $value['id'];
                }
              }
            }else{
              $selected_hospitals = ljmc_get_hospitals();
            }

            if(isset($_POST['search-profession'])){
              $seach_profession = $_POST['search-profession'];
              $search_list = ljmc_get_professions();
              $selected_professions = array();
              foreach ($search_list as $value) {
                if(in_array($value["id"], $seach_profession)){
                  $selected_professions[] = $value['id'];
                }
              }
            }else{
              $selected_professions = ljmc_get_professions();
            }

            ?>



            <?php if(isset($_POST['search-hospital']) || isset($_POST['search-profession'])) : ?>

              <?php
              $all_specialists = ljmc_get_specialists();
              $hospital_specialists = array();
              $profession_specialists = array();
              $available_specialists = array();

              foreach ($all_specialists as $specialist) {
                $get_hospitals = get_field('hospitals',$specialist["id"]);
                foreach ($get_hospitals as $get_hospital) {
                  if(in_array($get_hospital->ID, $selected_hospitals)){
                    if(!in_array($specialist["id"], $hospital_specialists)){
                      $hospital_specialists[] = $specialist["id"];
                    }
                  }
                }

                foreach ($hospital_specialists as $hospital_specialist) {
                  $terms = get_the_terms($hospital_specialist,'profession');
                  foreach ($terms as $term) {
                    if(in_array($term->term_id, $selected_professions) && !in_array($hospital_specialist,$available_specialists)){
                      $available_specialists[] = $hospital_specialist;
                    }
                  }
                }
              }

              $exclude = array();
              foreach ($all_specialists as $specialist) {
                if(in_array($specialist["id"], $available_specialists) == false) $exclude[] = $specialist["id"];
              }

              if(empty($available_specialists)) $empty = true;

              ?>

            <div class="col-xs-12">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <?php the_content(); ?>

           <?php endwhile; endif; ?>
                <div class="page-header" style="margin-top:0">
                  <h1><span class="line"><?php _e('Specialists','ljmc-theme'); ?></span></h1>
                </div>
                <?php if($empty == true) : ?>
                  <h4 style="font-weight:300;text-align:center"><?php _e("Specialists not found","ljmc-theme"); ?></h4>
                <?php endif; ?>
                <?php $terms = get_the_terms( $post->ID , 'profession' ); ?>
                <?php $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1; ?>
                <?php $qargs = array(
                                    'post_type'   => 'specialist',
                                    'order_by'    => 'title',
                                    'order'       => 'ASC',
                                    'posts_per_page'  => '16',
                                    'post__not_in'     => $exclude,
                                    'paged'       => $paged

                ); ?>
                <?php query_posts($qargs); ?>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="thumbnail">
                    <?php if(get_field('image')) : $image = get_field('image'); ?>
                      <a href="<?php the_permalink(); ?>"><div class="avatar-img"><img src="<?php echo $image; ?>"></div></a>
                    <?php endif; ?>
                      <div class="caption">
                        <h4 id="thumbnail-label" style="text-align:center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></a></h4>
                        <?php $postterms = get_the_terms($post->ID,'profession');?>
                      </div>
                    </div>
                  </div>
              <?php endwhile; endif; ?>
              <?php ljmc_reset_query(); ?>
              <?php $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1; ?>
              <?php $qargs = array(
                                    'post_type'   => 'specialist',
                                    'order_by'    => 'title',
                                    'order'       => 'ASC',
                                    'posts_per_page'  => '16',
                                    'post__not_in'     => $exclude,
                                    'paged'       => $paged

                ); ?>
                <?php $i=0;query_posts($qargs); ?>
                <?php if ( have_posts() ) : while ( have_posts() && $i==0) : the_post(); ?>
              <div class="col-xs-12" style="text-align:center">
              <?php
              $pagination = get_the_posts_pagination( array(
                  'mid_size' => 2,
                  'prev_text' => __( '<i class="fa fa-angle-left"></i>','ljmc' ),
                  'next_text' => __( '<i class="fa fa-angle-right"></i>','ljmc' ),
              ) );
              echo $pagination;
              ?>
              </div>
              <script type="text/javascript">
              jQuery('h2.screen-reader-text').remove();
              jQuery('.page-numbers.dots').addClass('disabled');
              </script>
              <?php $i++; endwhile; endif; ?>
              <?php ljmc_reset_query(); ?>
              </div>
      </div>   


            <?php else : ?>
            <div class="col-xs-12">
                <div class="page-header" style="margin-top:0">
                  <h1><span class="line"><?php _e('Specialists','ljmc-theme'); ?></span></h1>
                </div>
                <?php $terms = get_the_terms( $post->ID , 'profession' ); ?>
                <?php $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1; ?>
                <?php query_posts('post_type=specialist&orderby=title&order=ASC&posts_per_page=16&paged='.$paged); ?>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="thumbnail">
                    <?php if(get_field('image')) : $image = get_field('image'); ?>
                      <a href="<?php the_permalink(); ?>"><div class="avatar-img"><img src="<?php echo $image; ?>"></div></a>
                    <?php endif; ?>
                      <div class="caption">
                        <h4 id="thumbnail-label" style="text-align:center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></a></h4>
                        <?php $postterms = get_the_terms($post->ID,'profession');?>
                      </div>
                    </div>
                  </div>
              <?php endwhile; endif; ?>
              <?php ljmc_reset_query(); ?>
              <?php $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1; ?>
                <?php $i=0;query_posts('post_type=specialist&posts_per_page=16&paged='.$paged); ?>
                <?php if ( have_posts() ) : while ( have_posts() && $i==0) : the_post(); ?>
              <div class="col-xs-12" style="text-align:center">
              <?php
              $pagination = get_the_posts_pagination( array(
                  'mid_size' => 2,
                  'prev_text' => __( '<i class="fa fa-angle-left"></i>','ljmc' ),
                  'next_text' => __( '<i class="fa fa-angle-right"></i>','ljmc' ),
              ) );
              echo $pagination;
              ?>
              </div>
              <script type="text/javascript">
              jQuery('h2.screen-reader-text').remove();
              jQuery('.page-numbers.dots').addClass('disabled');


              jQuery(".avatar-img img").css("width",jQuery(".avatar-img").width());
              jQuery(".avatar-img img").css("height","auto");
              jQuery(".avatar-img img").css("margin-top",0);
              jQuery(".avatar-img img").css("top",0);

              jQuery(".avatar-img img").css("margin-top",(300 - jQuery(".avatar-img img").height()) / 2);

              </script>
              <?php $i++; endwhile; endif; ?>
              <?php ljmc_reset_query(); ?>
              </div>
      </div> 
            <?php endif; ?>


          </div>
      </div>
    </section>

    <?php get_footer(); ?>

    