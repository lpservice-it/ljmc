
    <?php get_header(); ?>

<?php  ?>
    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="row">
            <div class="col-xs-12" style="text-align:center">
            <h1><?php the_title(); ?></h1>
            
            <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

        </div>
      </div>

<nav class="navbar search hidden-xs">
        <div class="container">
            <div class="row">
                  <?php
                  $args = array(
                      'post_type' => 'page',
                      'fields' => 'ids',
                      'nopaging' => true,
                      'meta_key' => '_ljmc_page_template',
                      'meta_value' => 'page-specialists.php'
                  );
                  $pages = get_posts( $args );
                  foreach ( $pages as $page ) 
                      $servicesid = $page;

                  $url = get_permalink($servicesid);
                  $lang = ICL_LANGUAGE_CODE;
                  ?>
              <form id="search-doctor-form" action="<?php echo get_url_for_language($url,$lang); ?>" method="post">
                <div class="col-xs-3" style="position:relative">
                  <h3 style="margin:0;padding:0;line-height:1.1;vertical-align:middle;text-align:right"><span class="label label-default"><i class="fa fa-user-md" style="font-size:21px; margin-right:10px"></i><span style="font-weight:300"><?php _e('Search doctor','ljmc-theme'); ?></span></span></h3>
                </div>


                <div id="search-hospital-wrap" class="col-xs-3" style="position:relative">
                  <select id="search-hospital" class="search-doctor hospital selectpicker show-tick" title="<?php _e('Hospital...','ljmc-theme'); ?>" name="search-hospital[]" multiple>
                    <option value="0" disabled="disabled" style="text-align:left;font-size:90%"><?php _e('Choose hospital...','ljmc-theme'); ?></option>
                    <?php
                    $hospital_list = ljmc_get_hospitals();

                    $html = '';
                    foreach($hospital_list as $value){
                      $html .= '<option value="' . $value["id"] . '">' . $value["name"] . '</option>';
                    }
                    echo $html;
                    ?>
                  </select>
                </div>


                <div id="search-profession-wrap" class="col-xs-3" style="position:relative">
                  <select id="search-profession" class="search-doctor profession selectpicker show-tick" title="<?php _e('Profession...','ljmc-theme'); ?>" name="search-profession[]" multiple data-live-search="true">
                    <option value="0" disabled="disabled" style="text-align:left;font-size:90%"><?php _e('Choose profession...','ljmc-theme'); ?></option>
                  </select>
                </div>

                <div id="search-submit-wrap" class="col-xs-3">
                  <button id="search-submit-result" class="btn btn-red search"><?php _e('View','ljmc-theme'); ?></button>
                </div>
              </form>
            </div>
        </div>
      </nav>

      <div class="container" style="margin-top:80px;margin-bottom:80px">
        <div class="row">


            <?php if (get_field('service_list')) : $services = get_field('service_list'); ?>
              <?php $has_col3 = true; ?>
            <div class="col-xs-3">
              <div id="bs-example-navbar-collapse-1">
                <ul class="nav nav-pills nav-stacked nav-pills-sidebar">
                <?php
                foreach ($services as $service) {
                  $curent = '';
                  if($service->ID == get_the_ID()){
                    $curent = ' class="current_page_item"';
                  }
                  ?><li<?php echo $curent; ?>><a href="<?php echo get_the_permalink($service->ID); ?>"><?php echo $service->post_title; ?></a></li><?php
                }
                ?>
                </ul>
              </div>
            </div>
            <?php
            $image = get_field('image');
            ?>
            <div class="col-xs-9">
          <?php else : ?>


            <?php
              $args = array(
                'post_type' => 'service',
                'posts_per_page' => -1
              );
              $services = get_posts($args);
              $has_parent = false;
              $parent_id = false;
              foreach ($services as $service) {
                $field = get_field('service_list',$service->ID);
                if($field){
                  foreach($field as $f){
                    if($f->ID == get_the_ID()){
                      if($service->ID !== get_the_ID()){
                        $has_parent = true;
                        $parent_id = $service->ID;
                      }
                    }
                  }
                }
              }
            ?>

            <?php if (get_field('service_list',$parent_id)) : $services = get_field('service_list',$parent_id); ?>
              <?php $has_col3 = true; ?>
              <div class="col-xs-3">
              <div id="bs-example-navbar-collapse-1">
                <ul class="nav nav-pills nav-stacked nav-pills-sidebar">
                <li class="parent"><a href="<?php echo get_the_permalink($parent_id); ?>"><?php echo get_the_title($parent_id); ?></a></li>
                <?php
                foreach ($services as $service) {
                  $curent = '';
                  if($service->ID == get_the_ID()){
                    $curent = ' class="current_page_item"';
                  }
                  ?><li<?php echo $curent; ?>><a href="<?php echo get_the_permalink($service->ID); ?>"><?php echo $service->post_title; ?></a></li><?php
                }
                ?>
                </ul>
              </div>
            </div>
            <?php endif; ?>
              <?php if($has_col3 == true) : ?>
              <div class="col-xs-9">
            <?php else : ?>
              <div class="col-xs-12">
            <?php endif; ?>
            <?php endif; ?>
            <?php
            $image = get_field('image');
            ?>
            <?php if(!empty($image)) : ?>
              <div style="width:100%;height:300px;background:rgba(0, 0, 0, 0) url('<?php echo $image; ?>') no-repeat center center / cover " ></div>
            <?php endif; ?>
              
              <div>
              	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          		<?php the_content(); ?>

   				 <?php endwhile; endif; ?>
              </div>
              <?php if(get_field('gallery')) : ?>
              <div class="page-header">
                <h1><span class="line"><?php _e('Gallery','ljmc-theme'); ?></span></h1>
              </div>

              <div id="lightgallery<?php echo get_the_ID();?>">
                <?php
                    $gallery_field = get_field('gallery');
                    foreach($gallery_field as $image){
                      ?>
                            <a href="<?php echo $image['url']; ?>">
                                <div class="col-xs-2" style="padding:0;margin:0;"><img class="img-responsive" src="<?php echo $image['url']; ?>" /></div>
                            </a>
                      <?php
                    }
                    ?>
              </div>
              <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery("#lightgallery"+<?php echo get_the_ID();?>).lightGallery(); 
                });
            </script>
            <?php endif; ?>
              <div class="full-width">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <?php if(get_field('specialists')) : ?><li role="presentation" class="active"><a href="#specialists" aria-controls="specialists" role="tab" data-toggle="tab"><?php _e('Specialists','ljmc-theme'); ?></a></li><?php endif; ?>
    <?php if(get_field('pricelist')) : ?><li role="presentation"><a href="#pricelists" aria-controls="pricelists" role="tab" data-toggle="tab"><?php _e('Pricelist','ljmc-theme'); ?></a></li><?php endif; ?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
  <?php
  if(!get_field('specialists')){
     $inn = ' active';
  }else{
     $inn = '';
  }
  ?>
  <?php if(get_field('specialists')) : ?>
    <div role="tabpanel" class="tab-pane fade in active" id="specialists" style="margin-top:25px">
          <?php
          $specialist_list = get_field('specialists');
          foreach ($specialist_list as $specialist) {
            $terms = get_the_terms( $specialist->ID , 'profession' );
            if(get_field('image',$specialist->ID)) $image = get_field('image',$specialist->ID);
            ?>
    <div class="col-xs-4">
        <div class="thumbnail">
          <a href="<?php echo get_permalink($specialist->ID); ?>"><div style="width:100%;max-height:200px;overflow:hidden;text-align:center"><img style="width:100%; display: block; text-align:center" src="<?php echo $image; ?>"></div></a>
          <div class="caption">
            <h3 id="thumbnail-label" style="text-align: center"><a href="<?php echo get_permalink($specialist->ID); ?>"><?php echo get_the_title($specialist->ID); ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></a></h3>
            <p style="text-align: center"><?php $postterms = get_the_terms($specialist->ID,'profession'); $pid = 1; foreach($postterms as $term){ if($pid > 1){echo ', ';} echo $term->name; $pid++;}?></p>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  <?php endif; ?>
    <?php if(get_field('pricelist')) : ?>
      <div role="tabpanel" class="tab-pane fade in<?php echo $inn; ?>" id="pricelists">
        <?php $pricelists = get_field('pricelist'); ?>
        <?php
        foreach($pricelists as $pricelist){
        ?>
      <table class="table table-hover">
      <thead>
        <tr>
          <th><?php _e('#','ljmc-theme'); ?></th>
          <th><?php _e('Code','ljmc-theme'); ?></th>
          <th><?php _e('Name','ljmc-theme'); ?></th>
          <th><?php _e('Price','ljmc-theme'); ?></th>
        </tr>
      </thead>
      <div class="page-header"><h1><span class="line"><strong><?php echo $pricelist->post_title; ?></strong></span></h1></div>
      <tbody>
        <?php
          $prices = get_field('pricelist',$pricelist->ID);
          $i = 1;
          foreach ($prices as $price) {
            ?><tr>
            <th scope="row"><?php echo $i; ?></th>
            <td><?php echo $price['code']; ?></td>
            <td><?php echo $price['name']; ?></td>
            <td><?php echo $price['price']; ?> &euro;</td>
            </tr><?php
            $i++;
          }
        }
        ?>
      </tbody>
    </table>
    </div>  
    </div><?php endif; ?>
  </div>

</div>

            </div>

          </div>
      </div>
    </section>

    <?php get_footer(); ?>