<?php

add_filter( 'wp_image_editors', 'change_graphic_lib' );

function change_graphic_lib($array) {
  return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );
// Default constants
require_once(dirname(__FILE__) . '/inc/constants.php');

add_action( 'after_setup_theme', 'ljmc_theme_setup' );
function ljmc_theme_setup(){
  load_theme_textdomain('ljmc-theme', get_template_directory() . '/languages');
}


add_filter('acf/settings/default_language', 'my_acf_settings_default_language');

function my_acf_settings_default_language( $language ) {
    global $sitepress;
    return $sitepress->get_default_language();
}

add_filter('acf/settings/current_language', 'my_acf_settings_current_language');

function my_acf_settings_current_language( $language ) {
    global $sitepress;
    return $sitepress->get_current_language();
}

add_image_size('small', 90);
add_image_size('medium', 350);
add_image_size('large', 1200);

/**
 * Return base directory url
 */
function ljmc_base_url($format=true){
  if(isset($_SERVER['HTTPS'])){
    $protocol = 'https://';
  }else{
    $protocol = 'http://';
  }

  $url = $protocol;
  $url .= $_SERVER['SERVER_NAME'];
  $url .= dirname($_SERVER['PHP_SELF']) . '/';
  $url .= 'data/themes/ljmc/';

  if($format){
    echo $url;
  }else{
    return $url;
  }
}

if( function_exists('acf_add_options_sub_page') )
{
    acf_add_options_sub_page(array(
        'title' => __('Slīdrāde','ljmc-theme'),
        'parent' => 'options-general.php',
        'capability' => 'manage_options'
    ));

    acf_add_options_sub_page(array(
        'title' => __('Partneri','ljmc-theme'),
        'parent' => 'options-general.php',
        'capability' => 'manage_options'
    ));
}

/* Hospitals */
add_action( 'init', 'ljmc_register_hospitals' );

function ljmc_register_hospitals() {
  $labels = array(
    'name'               => __( 'Hospitals', 'ljmc-theme' ),
    'singular_name'      => __( 'Hospital', 'ljmc-theme' ),
    'menu_name'          => __( 'Hospitals', 'ljmc-theme' ),
    'name_admin_bar'     => __( 'Hospital', 'ljmc-theme' ),
    'add_new'            => __( 'Add New', 'ljmc-theme' ),
    'add_new_item'       => __( 'Add New Hospital', 'ljmc-theme' ),
    'new_item'           => __( 'New Hospital', 'ljmc-theme' ),
    'edit_item'          => __( 'Edit Hospital', 'ljmc-theme' ),
    'view_item'          => __( 'View Hospital', 'ljmc-theme' ),
    'all_items'          => __( 'All Hospitals', 'ljmc-theme' ),
    'search_items'       => __( 'Search Hospitals', 'ljmc-theme' ),
    'parent_item_colon'  => __( 'Parent Hospitals:', 'ljmc-theme' ),
    'not_found'          => __( 'No hospitals found.', 'ljmc-theme' ),
    'not_found_in_trash' => __( 'No hospitals found in Trash.', 'ljmc-theme' )
  );

  $args = array(
    'labels'             => $labels,
                'description'        => __( 'Description.', 'ljmc-theme' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'hospital' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 32,
    'menu_icon'          => 'fa-hospital-o',
    'supports'           => array( 'title' )
  );

  register_post_type( 'hospital', $args );
}



/** Header Contact info **/
function get_header_contacts(){

  $args = array(
      'post_type' => 'page',
      'fields' => 'ids',
      'nopaging' => true,
      'meta_key' => '_ljmc_page_template',
      'meta_value' => 'page-contacts.php'
  );
  $pages = get_posts( $args );
  foreach ( $pages as $page )
      $contactid = $page;

  $hospital = get_field('main',$contactid);



  $telephone = get_field('telephone',$hospital->ID);
  if(strpos($telephone, '371')) $telephone = substr_replace($telephone, ' ' . substr($telephone, 4), 4);
  $email = get_field('e-mail',$hospital->ID);
  $address = get_field('address',$hospital->ID);
  $facebook = get_field('facebook',$contactid);
  $youtube = get_field('youtube',$contactid);


  ?>
    <a href="callto://<?php echo $telephone; ?>"><span class="fa fa-phone"></span><?php echo $telephone; ?></a>
    <a href="mailto://<?php echo $email; ?>"><span class="fa fa-envelope-o"></span><?php echo $email; ?></a>
    <a><span class="fa fa-map-marker"></span><?php echo $address['address']; ?></a>
    <div class="socials" style="float:right;text-align:right">
      <?php if($facebook) : ?><a href="<?php echo $facebook; ?>"><i class="fa fa-facebook"></i></a><?php endif; ?>
      <?php if($youtube) : ?><a href="<?php echo $youtube; ?>"><i class="fa fa-youtube"></i></a><?php endif; ?>
    </div>
  <?php
}



/* Register Services Posts */
add_action( 'init', 'ljmc_register_services' );
/**
 * Register a book post type.
 *
 * @link http://codex.ljmc.org/Function_Reference/register_post_type
 */
function ljmc_register_services() {
  $labels = array(
    'name'               => __( 'Services', 'ljmc-theme' ),
    'singular_name'      => __( 'Service', 'ljmc-theme' ),
    'menu_name'          => __( 'Services', 'ljmc-theme' ),
    'name_admin_bar'     => __( 'Service', 'ljmc-theme' ),
    'add_new'            => __( 'Add New', 'ljmc-theme' ),
    'add_new_item'       => __( 'Add New Service', 'ljmc-theme' ),
    'new_item'           => __( 'New Service', 'ljmc-theme' ),
    'edit_item'          => __( 'Edit Services', 'ljmc-theme' ),
    'view_item'          => __( 'View Services', 'ljmc-theme' ),
    'all_items'          => __( 'All Services', 'ljmc-theme' ),
    'search_items'       => __( 'Search Services', 'ljmc-theme' ),
    'parent_item_colon'  => __( 'Parent Services:', 'ljmc-theme' ),
    'not_found'          => __( 'No services found.', 'ljmc-theme' ),
    'not_found_in_trash' => __( 'No services found in Trash.', 'ljmc-theme' )
  );

  $args = array(
    'labels'             => $labels,
                'description'        => __( 'Description.', 'ljmc-theme' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'service' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 28,
    'menu_icon'          => 'fa-briefcase',
    'supports'           => array( 'title', 'editor' )
  );

  register_post_type( 'service', $args );
}



/* Register Specialists Posts */
add_action( 'init', 'ljmc_register_specialists' );
/**
 * Register a book post type.
 *
 * @link http://codex.ljmc.org/Function_Reference/register_post_type
 */
function ljmc_register_specialists() {
  $labels = array(
    'name'               => __( 'Specialists', 'ljmc-theme' ),
    'singular_name'      => __( 'Specialist', 'ljmc-theme' ),
    'menu_name'          => __( 'Specialists', 'ljmc-theme' ),
    'name_admin_bar'     => __( 'Specialist', 'ljmc-theme' ),
    'add_new'            => __( 'Add New', 'ljmc-theme' ),
    'add_new_item'       => __( 'Add New Specialist', 'ljmc-theme' ),
    'new_item'           => __( 'New Specialist', 'ljmc-theme' ),
    'edit_item'          => __( 'Edit Specialist', 'ljmc-theme' ),
    'view_item'          => __( 'View Specialist', 'ljmc-theme' ),
    'all_items'          => __( 'All Specialists', 'ljmc-theme' ),
    'search_items'       => __( 'Search Specialists', 'ljmc-theme' ),
    'parent_item_colon'  => __( 'Parent Specialists:', 'ljmc-theme' ),
    'not_found'          => __( 'No specialists found.', 'ljmc-theme' ),
    'not_found_in_trash' => __( 'No specialists found in Trash.', 'ljmc-theme' )
  );

  $args = array(
    'labels'             => $labels,
                'description'        => __( 'Description.', 'ljmc-theme' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'specialist' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 29,
    'menu_icon'          => 'fa-user-md',
    'supports'           => array( 'title', 'editor' )
  );

  register_post_type( 'specialist', $args );
}

/* Register Timetables Posts */
add_action( 'init', 'ljmc_register_timetables' );
/**
 * Register a book post type.
 *
 * @link http://codex.ljmc.org/Function_Reference/register_post_type
 */
function ljmc_register_timetables() {
  $labels = array(
    'name'               => __( 'Timetables', 'ljmc-theme' ),
    'singular_name'      => __( 'Timetable', 'ljmc-theme' ),
    'menu_name'          => __( 'Timetables', 'ljmc-theme' ),
    'name_admin_bar'     => __( 'Timetable', 'ljmc-theme' ),
    'add_new'            => __( 'Add New', 'ljmc-theme' ),
    'add_new_item'       => __( 'Add New Timetable', 'ljmc-theme' ),
    'new_item'           => __( 'New Timetable', 'ljmc-theme' ),
    'edit_item'          => __( 'Edit Timetable', 'ljmc-theme' ),
    'view_item'          => __( 'View Timetable', 'ljmc-theme' ),
    'all_items'          => __( 'All Timetables', 'ljmc-theme' ),
    'search_items'       => __( 'Search Timetables', 'ljmc-theme' ),
    'parent_item_colon'  => __( 'Parent Timetables:', 'ljmc-theme' ),
    'not_found'          => __( 'No timetables found.', 'ljmc-theme' ),
    'not_found_in_trash' => __( 'No timetables found in Trash.', 'ljmc-theme' )
  );

  $args = array(
    'labels'             => $labels,
                'description'        => __( 'Description.', 'ljmc-theme' ),
    'public'             => false,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'timetable' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 30,
    'menu_icon'          => 'fa-calendar',
    'supports'           => array( 'title' )
  );

  register_post_type( 'timetable', $args );
}


/* Register Pricelists Posts */
add_action( 'init', 'ljmc_register_pricelists' );
/**
 * Register a book post type.
 *
 * @link http://codex.ljmc.org/Function_Reference/register_post_type
 */
function ljmc_register_pricelists() {
  $labels = array(
    'name'               => __( 'Pricelists', 'ljmc-theme' ),
    'singular_name'      => __( 'Pricelist', 'ljmc-theme' ),
    'menu_name'          => __( 'Pricelists', 'ljmc-theme' ),
    'name_admin_bar'     => __( 'Pricelist', 'ljmc-theme' ),
    'add_new'            => __( 'Add New', 'ljmc-theme' ),
    'add_new_item'       => __( 'Add New Pricelist', 'ljmc-theme' ),
    'new_item'           => __( 'New Pricelist', 'ljmc-theme' ),
    'edit_item'          => __( 'Edit Pricelist', 'ljmc-theme' ),
    'view_item'          => __( 'View Pricelist', 'ljmc-theme' ),
    'all_items'          => __( 'All Pricelists', 'ljmc-theme' ),
    'search_items'       => __( 'Search Pricelists', 'ljmc-theme' ),
    'parent_item_colon'  => __( 'Parent Pricelists:', 'ljmc-theme' ),
    'not_found'          => __( 'No pricelists found.', 'ljmc-theme' ),
    'not_found_in_trash' => __( 'No pricelists found in Trash.', 'ljmc-theme' )
  );

  $args = array(
    'labels'             => $labels,
                'description'        => __( 'Description.', 'ljmc-theme' ),
    'public'             => false,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'pricelist' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 31,
    'menu_icon'          => 'fa-usd',
    'supports'           => array( 'title' )
  );

  register_post_type( 'pricelist', $args );
}



/** SPECIALISTS TAX **/
// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'ljmc_register_specialists_tax', 0 );

// create two taxonomies, genres and writers for the post type "book"
function ljmc_register_specialists_tax() {
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name'              => __( 'Professions', 'ljmc-theme' ),
    'singular_name'     => __( 'Profession', 'ljmc-theme' ),
    'search_items'      => __( 'Search Professions', 'ljmc-theme' ),
    'all_items'         => __( 'All Professions', 'ljmc-theme' ),
    'parent_item'       => __( 'Parent Profession', 'ljmc-theme' ),
    'parent_item_colon' => __( 'Parent Profession:', 'ljmc-theme' ),
    'edit_item'         => __( 'Edit Profession', 'ljmc-theme' ),
    'update_item'       => __( 'Update Profession', 'ljmc-theme' ),
    'add_new_item'      => __( 'Add New Profession', 'ljmc-theme' ),
    'new_item_name'     => __( 'New Profession Name', 'ljmc-theme' ),
    'menu_name'         => __( 'Professions', 'ljmc-theme' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'profession' ),
  );

  register_taxonomy( 'profession', 'specialist', $args );
}


/**
 * Register Main Menu
 */
add_action( 'after_setup_theme', 'ljmc_register_main_menu' );
function ljmc_register_main_menu() {
  register_nav_menu( 'primary', __( 'Main Menu', 'ljmc-theme' ) );
}

/**
 * Register About Menu
 */
add_action( 'after_setup_theme', 'ljmc_register_about_menu' );
function ljmc_register_about_menu() {
  register_nav_menu( 'about', __( 'About us', 'ljmc-theme' ) );
}


/**
 * Register News Menu
 */
add_action( 'after_setup_theme', 'ljmc_register_news_menu' );
function ljmc_register_news_menu() {
  register_nav_menu( 'news', __( 'News', 'ljmc-theme' ) );
}

/**
 * Add bootstrap support for navs
 */
require_once(dirname(__FILE__) . '/inc/bootstrap_navwalker.php');

function ljmc_page_breadcrumb(){
  global $post;     // if outside the loop
  $lang = ICL_LANGUAGE_CODE;

  if(is_single()) :
    if(get_post_type(get_the_ID()) == 'post') : ?>
	<?php
	$url = get_permalink( get_option( 'page_for_posts' ) );

	?>
    <ol class="breadcrumb" style="background:none;margin-bottom:80px">
      <?php
        ?><li><a href="<?php echo bloginfo('url'); ?>"><?php _e('Home','ljmc-theme'); ?></a></li>
        <li><a href="<?php echo get_url_for_language($url,$lang); ?>"><?php _e('News','ljmc-theme'); ?></a></li><?php
      ?>
    </ol><?php
    elseif(get_post_type(get_the_ID()) == 'service') : ?>

    <?php
    $args = array(
      'post_type' => 'service',
      'posts_per_page' => -1
    );
    $services = get_posts($args);
    $has_parent = false;
    $parent_id = false;
    foreach ($services as $service) {
      $field = get_field('service_list',$service->ID);
      if($field){
        foreach($field as $f){
          if($f->ID == get_the_ID()){
            if($service->ID !== get_the_ID()){
              $has_parent = true;
              $parent_id = $service->ID;
            }
          }
        }
      }
    }
    if($has_parent && $parent_id){
      $url = get_permalink( $parent_id );
      $parent_link = '<li><a href="' . get_url_for_language($url, $lang) . '">' . get_the_title($parent_id) . '</a></li>';
    }else{
      $parent_link = '';
    }
    ?>

    <ol class="breadcrumb" style="background:none;margin-bottom:80px">
      <?php
        ?><li><a href="<?php echo bloginfo('url'); ?>"><?php _e('Home','ljmc-theme'); ?></a></li>
        <?php
        $args = array(
            'post_type' => 'page',
            'fields' => 'ids',
            'nopaging' => true,
            'meta_key' => '_ljmc_page_template',
            'meta_value' => 'page-services.php'
        );
        $pages = get_posts( $args );
        foreach ( $pages as $page )
            $servicesid = $page;

        $url = get_permalink( $servicesid );
        ?>
        <li><a href="<?php echo get_url_for_language($url,$lang); ?>"><?php _e('Services','ljmc-theme'); ?></a></li>
        <?php echo $parent_link; ?>
        <li><a style="text-decoration:none;color:#3b3b3b;cursor:default"><?php the_title(); ?></a></li><?php
      ?>
    </ol>
  <?php elseif(is_home()) : ?>
    <?php else : ?>
      <ol class="breadcrumb" style="background:none;margin-bottom:80px">
      <?php
        ?><li><a href="<?php echo bloginfo('url'); ?>"><?php _e('Home','ljmc-theme'); ?></a></li>
        <?php
      ?>
    </ol>
    <?php endif;
  else: ?>

    <ol class="breadcrumb" style="background:none;margin-bottom:80px">
      <?php
      if ( is_page() && $post->post_parent ) {
      	$url = get_permalink($post->post_parent);
        ?><li><a href="<?php echo bloginfo('url'); ?>"><?php _e('Home','ljmc-theme'); ?></a></li>
        <li><a href="<?php echo get_url_for_language($url,$lang); ?>"><?php echo get_the_title($post->post_parent); ?></a></li><?php
      } else {
        ?><li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e('Home','ljmc-theme'); ?></a></li><?php
      }

      ?>
    </ol><?php

  endif;

}

function get_url_for_language( $original_url, $language )
{
    $post_id = url_to_postid( $original_url );
    $lang_post_id = icl_object_id( $post_id , 'page', true, $language );

    $url = "";
    if($lang_post_id != 0) {
        $url = get_permalink( $lang_post_id );
    }else {
        // No page found, it's most likely the homepage
        global $sitepress;
        $url = $sitepress->language_url( $language );
    }

    return $url;
}

/** FILTER h1 tags **/
add_filter( 'the_content', 'ljmc_filter_content_h1' );
function ljmc_filter_content_h1( $content )
{

    $content = str_ireplace( '<h1>', '<div class="page-header"><h1><span class="line">', $content );
    $content = str_ireplace( '</h1>', '</span></h1></div>', $content );

    return $content;
}



function ljmc_load_slider_style(){
    ?>
    <link rel="stylesheet" type="text/css" href="<?php ljmc_base_url(); ?>css/slider-pro.css" media="screen"/>
    <?php
}

function ljmc_load_slider_script(){
    ?>
    <script type="text/javascript" src="<?php ljmc_base_url(); ?>js/jquery.sliderPro.js"></script>
    <?php   if(is_front_page()) : ?>
    <script type="text/javascript">

      jQuery( document ).ready(function( jQuery ) {
        var slidesCount = jQuery('#slider .sp-slide').length;
        if(slidesCount <= 3){
          slidesCount = jQuery('#slider .sp-slide').length;
        }
        if(slidesCount >= 4){
          slidesCount = 3;
        }

       var adminBar = 0;
        if(jQuery('#ljmcadminbar')){
          adminBar = jQuery('#ljmcadminbar').height();
        }

        sliderHeight = jQuery(window).height() - (140 + jQuery('nav .navbar-red').height() + jQuery('nav#main').height()) - adminBar;

        jQuery( '#slider' ).sliderPro({
          width: jQuery(window).width(),
          height: sliderHeight,
          arrows: false,
          buttons: false,
          waitForLayers: true,
          thumbnailWidth: Math.round(jQuery(window).width() / slidesCount),
          thumbnailHeight: 100,
          thumbnailPointer: true,
          autoplay: true,
          loop: true,
          autoScaleLayers: false
        });
        if(jQuery('#ljmcadminbar')){
          var navbarHeight = jQuery('#ljmcadminbar').height() + jQuery('#header .navbar-red').height();
        }else{
          var navbarHeight = jQuery('#header .navbar-red').height();
        }
        jQuery(window).resize(function(){
          jQuery('#slider .sp-thumbnail-container').css('width', jQuery(window).width() / slidesCount);
          jQuery('#slider .sp-thumbnails-container').css('width', jQuery(window).width());
          if(jQuery(window).height() > jQuery(window).width()){
            jQuery('#slider').css('max-height', jQuery(window).height() / 2);
          }else{
            jQuery('#slider').css('max-height', '100%');
          }
          jQuery('#slider').sliderPro('resize');
        });
        jQuery('#slider .sp-thumbnails-container').css('width', jQuery(window).width());
        jQuery('#slider .sp-thumbnail-container').css('width', jQuery('#slider .sp-thumbnail-container').width() - 1);
        if(jQuery(window).height() > jQuery(window).width()){
          jQuery('#slider').css('max-height', jQuery(window).height() / 2);
        }else{
          jQuery('#slider').css('max-height', '100%');
        }
        jQuery('#slider').sliderPro('resize');
      });
    </script>
    <?php
  endif;
}

function ljmc_init_slider(){
  global $sitepress;
  if(is_front_page()) : ?>
    <div id="slider" class="slider-pro">
      <div class="sp-slides">
        <?php
        $slider = get_field('slides_' . $sitepress->get_current_language(),'option');
        foreach ($slider as $key => $slide) {
          $position = $slide['position'];
          $url = '';
          if(!empty($slide['url'])) $url = '<a class="btn btn-red" href="' . $slide['url'] . '" style="padding:10px 20px;margin-top:5px;float:left;border:none">' . __('Read more','ljmc-theme') . '</a>';
          switch ($position) {
            case 'Top':
              $position = 'topCenter';
              $padding = 'data-vertical="50"';
              $trans = 'top';
              break;

            case 'Right':
              $position = 'rightCenter';
              $padding = 'data-horizontal="70"';
              $trans = 'right';
              break;

            case 'Left':
              $position = 'leftCenter';
              $padding = 'data-horizontal="70" style="left:50px"';
              $trans = 'left';
              break;

            case 'Bottom':
              $position = 'bottomCenter';
              $padding = 'data-vertical="50"';
              $trans = 'bottom';
              break;

            case 'Top Right':
              $position = 'topRight';
              $padding = 'data-vertical="50" data-horizontal="70"';
              $trans = 'top';
              break;

            case 'Top Left':
              $position = 'topLeft';
              $padding = 'data-vertical="50" data-horizontal="70"';
              $trans = 'top';
              break;

            case 'Bottom Right':
              $position = 'bottomRight';
              $padding = 'data-vertical="50" data-horizontal="70"';
              $trans = 'bottom';
              break;

            case 'Bottom Left':
              $position = 'bottomLeft';
              $padding = 'data-vertical="50" data-horizontal="70"';
              $trans = 'bottom';
              break;

            default:
              $position = 'centerCenter';
              $padding = '';
              $trans = 'top';
              break;
          }
          ?>
          <div class="sp-slide">
            <img class="sp-image" src="<?php ljmc_base_url(); ?>img/blank.gif"
              data-src="<?php echo $slide['image']; ?>"
              data-retina="<?php echo $slide['image']; ?>"/>
            <div class="sp-layer sp-padding col-lg-4 col-md-5 col-sm-6 col-xs-6" data-show-delay="400" data-show-transition="$<?php echo $trans; ?>" <?php echo $padding; ?>  data-position="<?php echo $position; ?>">
              <h3 style="line-height:2;font-weight:300;text-transform:uppercase;margin-bottom:25px"><span style="color:#faf9f1;"><?php echo $slide['title']; ?></span></h3>
              <p class="slide-excerpt"><span><?php echo $slide['description']; ?></span></p>
              <?php echo $url; ?>
            </div>
          </div>
          <?php
        }?>
        </div>
        <div class="sp-thumbnails">
        <?php
        foreach ($slider as $key => $slide) {
          ?>
            <div class="sp-thumbnail">
              <div class="sp-thumbnail-title"><?php echo $slide['title']; ?></div>
            </div>
          <?php
        }
        ?>
      </div>
    </div>
  <?php endif;
}


/**
 * Hide editor for specific page templates.
 *
 */
add_action( 'admin_init', 'hide_editor' );

function hide_editor() {
  // Get the Post ID.
  if(isset($_GET['post']) && !empty($_POST['post'])) :
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    if( !isset( $post_id ) ) return;

    // Get the name of the Page Template file.
    $template_file = get_post_meta($post_id, '_ljmc_page_template', true);

      if($template_file == 'page-contacts.php'){ // edit the template name
        remove_post_type_support('page', 'editor');
      }
      endif;
}

/** REMOVE CONTACTS PAGE **/
function custom_menu_page_removing() {
  $user = get_current_user_id();

  if($user !== 1) :
    remove_menu_page( 'ljmccf7' );
    remove_submenu_page( 'nav-menus.php', 'widgets.php' );
    remove_menu_page( 'plugins.php' );
    remove_menu_page( 'edit.php?post_type=acf-field-group' );
    remove_menu_page( 'ljmcseo_dashboard' );
    remove_submenu_page('options-general.php','sljmcsmtp_settings');
    remove_menu_page( 'translations/menu/languages.php' );
    remove_submenu_page( 'statistics/ljmc-statistics.php', 'statistics/settings' );
    remove_submenu_page( 'statistics/ljmc-statistics.php', 'statistics/optimization' );
  endif;
}

/** UNSET TAGS **/
function unregister_taxonomy(){
    register_taxonomy('post_tag', array());
}
add_action('init', 'unregister_taxonomy');

add_action( 'admin_menu', 'custom_menu_page_removing' );

/*** EXCERPT ***/
function custom_excerpt_length( $length ) {
  return 14;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
  return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function ljmc_get_the_excerpt($post_id) {
  global $post;
  $save_post = $post;
  $post = get_post($post_id);
  setup_postdata( $post ); // hello
  $output = get_the_excerpt();
  $post = $save_post;
  return $output;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


function ljmc_get_specialists(){
  $args = array(
  'posts_per_page' => -1,
  'post_type' => 'specialist',
  'orderby'=> 'title',
  'order' => 'ASC'
  );

  $specialist_list = get_posts($args);
  $specialists = array();

  foreach ($specialist_list as $specialist) {
    $specialists[$specialist->ID] = array('id' => $specialist->ID, 'name' => $specialist->post_title);
  }

  return $specialists;
}

function ljmc_get_professions(){
  $specialists = ljmc_get_specialists();
  $professions = array();

  foreach ($specialists as $specialist) {
    $terms = get_the_terms($specialist['id'],'profession');

    if(!empty($terms)){
      foreach ($terms as $term) {
          if(!isset($professions[$term->term_id]) && !empty($term)){
              $professions[$term->term_id] = array('id' => $term->term_id, 'name' => $term->name);
          }
      }
    }
  }

  return $professions;
}


function ljmc_get_hospitals(){
  $specialists = ljmc_get_specialists();
  $hospitals = array();

  foreach ($specialists as $specialist) {
    $hospital_field = get_field('hospitals',$specialist['id']);

    if(!empty($hospital_field)){
      foreach ($hospital_field as $hospital) {
          if(!isset($hospitals[$hospital->ID]) && !empty($hospital)){
            $hospitals[$hospital->ID] = array('id' => $hospital->ID, 'name' => $hospital->post_title);
          }
      }
    }
  }

  return $hospitals;
}







function alx_embed_html( $html ) {
  return '<div class="video-container">' . $html . '</div>';
}

add_filter( 'embed_oembed_html', 'alx_embed_html', 10, 3 );
add_filter( 'video_embed_html', 'alx_embed_html' );

function manage_dash_setup($arg1) {
  global $ljmc_meta_boxes, $ljmcdb;
  if (current_user_can('administrator') && is_array($ljmc_meta_boxes['dashboard'])) {
          update_option($ljmcdb->prefix.'dash_widget_manager_registered_widgets', $ljmc_meta_boxes['dashboard']);
  }
}

add_action('ljmc_dashboard_setup', 'manage_dash_setup', 100);












add_action( 'ljmc_footer', 'ljmc_ajax_search_js' );
add_action( 'ljmc_ajax_nopriv_my_action', 'my_action_callback' );
function ljmc_ajax_search_js() {
  ?>
  <script type="text/javascript">
	jQuery(document).ready(function($) {
	  	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	    var hospital = jQuery('#search-hospital');
	    var profession = jQuery('#search-profession');
		var data = {
			'action': 'my_action',
			'hospital': hospital.val()
		};

		jQuery.ajax({
				url: ajaxurl,
				type: "POST",
				data: data,
				success: function(results) {
				  var result = JSON.parse(results);
				  profession.html(result);
	        profession.selectpicker('refresh');
				}
			});


		hospital.change(function(){
			data = {
				'action': 'my_action',
				'hospital': hospital.val()
			};

			jQuery.ajax({
				url: ajaxurl,
				type: "POST",
				data: data,
				success: function(results) {
				  var result = JSON.parse(results);
				  profession.html(result);
	              profession.selectpicker('refresh');
				}
			});
		});
	});
  </script>
<?php
}

add_action( 'ljmc_ajax_my_action', 'my_action_callback' );
function my_action_callback() {
  $result = array();

  if(isset($_POST['hospital'])) :
  	$post_hospitals = $_POST['hospital'];
    $specialists = ljmc_get_specialists();
    $selected_specialists = array();
    $professions = array();
    $html = '';

    foreach ($specialists as $specialist) {
      $hospitals_field = get_field('hospitals',$specialist['id']);

      foreach ($hospitals_field as $hospital_field) {
        if(in_array($hospital_field->ID, $post_hospitals) || $hospital_field->ID == $post_hospitals){
            $specialist_hospitals[] = $hospital_field->ID;
            if(!in_array($specialist["id"], $selected_specialists)){
              $selected_specialists[] = $specialist["id"];
            }
        }
      }
    }

    if(empty($selected_specialists)){
      foreach ($specialists as $specialist) {
        if(!in_array($specialist['id'], $selected_specialists)) $selected_specialists[] = $specialist['id'];
      }
    }

    foreach($selected_specialists as $selected_specialist){
      $terms = get_the_terms($selected_specialist,'profession');

        if(!empty($terms)){
          foreach ($terms as $term) {
              if(!in_array($term->term_id,$professions) && !empty($term)){
                  $professions[] = array('id' => $term->term_id, 'name' => $term->name);
              }
          }
        }
    }

    foreach ($professions as $key => $row) {
        $professionsid[$key]  = $row['id'];
        $professionsname[$key] = $row['name'];
    }

    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    array_multisort($professionsname, SORT_ASC, $professions);
    $professions = array_map("unserialize", array_unique(array_map("serialize", $professions)));

    foreach ($professions as $profession) {
      $html .= '<option value="' . $profession['id'] . '">' . $profession['name'] . '</option>';
    }
  endif;

  echo json_encode($html);
  ljmc_die();
}

function sort_member_by_name ($a, $b) {
         return strcmp ($a->name, $b->name);
}

?>
