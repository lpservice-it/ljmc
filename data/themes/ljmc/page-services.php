  <?php /* Template Name: Pakalpojumi */ ?>
    
    <?php get_header(); ?>

    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="row">
            <div class="col-xs-12" style="text-align:center">
            <h1><?php the_title(); ?></h1>
            
            <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

        </div>
      </div>

<nav class="navbar search hidden-xs">
        <div class="container">
            <div class="row">
                  <?php
                  $args = array(
                      'post_type' => 'page',
                      'fields' => 'ids',
                      'nopaging' => true,
                      'meta_key' => '_ljmc_page_template',
                      'meta_value' => 'page-specialists.php'
                  );
                  $pages = get_posts( $args );
                  foreach ( $pages as $page ) 
                      $servicesid = $page;

                  $url = get_permalink($servicesid);
                  $lang = ICL_LANGUAGE_CODE;
                  ?>
              <form id="search-doctor-form" action="<?php echo get_url_for_language($url,$lang); ?>" method="post">
                <div class="col-xs-3" style="position:relative">
                  <h3 style="margin:0;padding:0;line-height:1.1;vertical-align:middle;text-align:right"><span class="label label-default"><i class="fa fa-user-md" style="font-size:21px; margin-right:10px"></i><span style="font-weight:300"><?php _e('Search doctor','ljmc-theme'); ?></span></span></h3>
                </div>


                <div id="search-hospital-wrap" class="col-xs-3" style="position:relative">
                  <select id="search-hospital" class="search-doctor hospital selectpicker show-tick" title="<?php _e('Hospital...','ljmc-theme'); ?>" name="search-hospital[]" multiple>
                    <option value="0" disabled="disabled" style="text-align:left;font-size:90%"><?php _e('Choose hospital...','ljmc-theme'); ?></option>
                    <?php
                    $hospital_list = ljmc_get_hospitals();

                    $html = '';
                    foreach($hospital_list as $value){
                      $html .= '<option value="' . $value["id"] . '">' . $value["name"] . '</option>';
                    }
                    echo $html;
                    ?>
                  </select>
                </div>


                <div id="search-profession-wrap" class="col-xs-3" style="position:relative">
                  <select id="search-profession" class="search-doctor profession selectpicker show-tick" title="<?php _e('Profession...','ljmc-theme'); ?>" name="search-profession[]" multiple data-live-search="true">
                    <option value="0" disabled="disabled" style="text-align:left;font-size:90%"><?php _e('Choose profession...','ljmc-theme'); ?></option>
                  </select>
                </div>

                <div id="search-submit-wrap" class="col-xs-3">
                  <button id="search-submit-result" class="btn btn-red search"><?php _e('View','ljmc-theme'); ?></button>
                </div>
              </form>
            </div>
        </div>
      </nav>

      <div class="container services" style="margin-top:80px;margin-bottom:80px">
        <div class="row">

          <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <?php the_content(); ?>

           <?php endwhile; endif; ?>
            <?php
            $services = get_field('services');

            foreach ($services as $service) {
              $icon = get_field('icon',$service->ID);
              ?>
              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="position:relative;height:300px">
                <div class="icon"><a href="<?php echo get_the_permalink($service->ID); ?>"><img src="<?php echo $icon; ?>" style="max-height:100px;max-width:100px;margin-top:20px"></a></div>
                <div class="caption">
                  <h4 style="text-align:center;font-weight:400;margin:20px 0"><a style="text-align:center" href="<?php echo get_the_permalink($service->ID); ?>"><?php echo get_the_title($service->ID); ?></a></h4>
                  <p style="text-align:center"><?php echo ljmc_get_the_excerpt($service->ID); ?></p>
                </div>
              </div>
              <?php
            }
            ?>


          </div>
      </div>
    </section>
    <?php get_footer(); ?>

    