    
    <?php get_header(); ?>

    <?php if(get_post_type(get_the_ID()) == 'post') : ?>

      <?php
      $bgimg = get_field('image');
      if(isset($bgimg['url'])){
        $bgimg = "background:rgba(0, 0, 0, 0) url('". $bgimg['url'] . "') no-repeat fixed center center / cover !important";
      }
      ?>
    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden;<?php echo $bgimg; ?>">
      <div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="row">
            <div class="col-xs-12" style="text-align:center">
            <h1><?php the_title(); ?></h1>
            
            <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

          <div class="row" style="padding-bottom:100px">
            <div class="col-xs-2 hidden-md hidden-sm hidden-xs">
            <img src="<?php ljmc_base_url(); ?>img/logo.svg" style="width:100%;height:auto;padding-right:40px;margin-top:-20px">
            </div>

            <div class="col-xs-12 col-lg-10" style="padding-top:10px;margin-top:5px;padding-left:40px;border-left:2px solid #cf4240;">
            <?php the_excerpt(); ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

        </div>
      </div>



      <div class="container" style="margin-top:80px;margin-bottom:80px">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <div class="row">

            <div class="col-xs-12 col-md-9 news">

              <?php the_content(); ?>

              <?php if(get_field('gallery')) : ?>
              <div class="page-header">
                <h1><span class="line"><?php _e('Gallery','ljmc-theme'); ?></span></h1>
              </div>

              <div id="lightgallery<?php echo get_the_ID();?>">
                <?php
                    $gallery_field = get_field('gallery');
                    foreach($gallery_field as $image){
                      ?>
                            <a href="<?php echo $image['url']; ?>">
                                <div class="col-xs-2" style="padding:0;margin:0;"><img class="img-responsive" src="<?php echo $image['url']; ?>" /></div>
                            </a>
                      <?php
                    }
                    ?>
              </div>
              <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery("#lightgallery"+<?php echo get_the_ID();?>).lightGallery(); 
                });
            </script>
            <?php endif; ?>

            </div>

            <div class="col-md-3 hidden-xs hidden-sm">
                <div style="margin-top:0" class="page-header">
                  <h1><span class="line"><?php _e('Archive', 'ljmc-theme'); ?></span></h1>
                </div>

              <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                <option value=""><?php echo esc_attr( __( 'Select Month','ljmc-theme' ) ); ?></option> 
                <?php ljmc_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
              </select>

              <div style="margin-top:50px" class="page-header">
                  <h1><span class="line"><?php _e('Categories', 'ljmc-theme'); ?></span></h1>
                </div>

              <?php
                $args = array(
                  'menu'              => 'news',
                  'theme_location'    => 'news',
                  'depth'             => 2,
                  'container_id'      => 'bs-example-navbar-collapse-1',
                  'menu_class'        => 'nav nav-pills nav-stacked nav-pills-sidebar'
                );
                ljmc_nav_menu($args);
                ?>
          </div>
          </div>

          <?php endwhile; endif; ?>

      </div>
    </section>



  <?php else: ?>


        <section id="content">
      <div class="header">
        <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="row">
            <div class="col-xs-12" style="text-align:center">
            <h1><?php the_title(); ?></h1>
            
            <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

          <div class="row" style="padding-bottom:100px">
            <div class="col-xs-2 hidden-md hidden-sm hidden-xs">
            <img src="<?php ljmc_base_url(); ?>img/logo.svg" style="width:100%;height:auto;padding-right:40px;margin-top:-20px">
            </div>

            <div class="col-xs-12 col-lg-10" style="padding-top:10px;margin-top:5px;padding-left:40px;border-left:2px solid #cf4240;">
            <?php the_excerpt(); ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

        </div>
      </div>



      <div class="container" style="margin-top:80px;margin-bottom:80px">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <div class="row">

            <div class="col-xs-12">

              <?php the_content(); ?>
            </div>
          </div>

          <?php endwhile; endif; ?>

      </div>
    </section>

<?php endif; ?>

    <?php get_footer(); ?>

    