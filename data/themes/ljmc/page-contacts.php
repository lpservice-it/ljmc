    <?php /* Template Name: Kontakti */ ?>

    <?php get_header(); ?>


    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="row">
            <div class="col-xs-12" style="text-align:center">
            <h1><?php the_title(); ?></h1>

            <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

        </div>
      </div>




      <div class="container" style="margin-top:80px;margin-bottom:80px">
      <div class="row">
      <div class="col-xs-12">
      <?php $hospital = get_field('main'); $hospitalid=$hospital->ID; ?>
      <div class="page-header" style="margin-top:0">
              <h1><span class="line"><?php _e('Choose Hospital','ljmc-theme'); ?></span></h1>
            </div>
        <div class="dropdown">
            <button class="btn btn-red dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true" style="min-width:200px">
              <?php echo get_the_title($hospitalid); ?>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
              <?php query_posts('post_type=hospital&posts_per_page=-1'); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <?php if(get_the_ID() !== $hospitalid) : ?>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php endif; ?>
              <?php endwhile; endif; ?>
        <?php ljmc_reset_query(); ?>
            </ul>
          </div>
      </div>
      <div class="col-xs-12" style="margin-top:25px">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <?php the_content(); ?>

           <?php endwhile; endif; ?>
      </div>

              <div class="col-xs-12" style="margin-top:25px">
          <div class="page-header" style="margin-top:0">
              <h1><span class="line"><?php _e('Time & Contacts','ljmc-theme'); ?></span></h1>
            </div>
        </div>
        <div class="col-xs-12">
            <?php
            $main = get_field('main');
            ?>

            <div class="col-md-3 col-xs-12">
              <div class="panel panel-red">
                <div class="panel-heading">
                    <i class="fa fa-clock-o fa-fw"></i> <?php _e('Working Hours','ljmc-theme'); ?>
                </div>
                <div class="panel-body">
                      <?php
                    $timetableo = get_fields($main->ID);
                    $timetable = get_field('timetable',$timetableo['timetable']->ID);

                    foreach ($timetable as $table) {
                      switch ($table['day']) {
                        case 'Monday':
                          $table['day'] = __('Monday','ljmc-theme');
                          break;

                        case 'Tuesday':
                          $table['day'] = __('Tuesday','ljmc-theme');
                          break;

                        case 'Wednesday':
                          $table['day'] = __('Wednesday','ljmc-theme');
                          break;

                        case 'Thursday':
                          $table['day'] = __('Thursday','ljmc-theme');
                          break;

                        case 'Friday':
                          $table['day'] = __('Friday','ljmc-theme');
                          break;

                        case 'Saturday':
                          $table['day'] = __('Saturday','ljmc-theme');
                          break;

                        case 'Sunday':
                          $table['day'] = __('Sunday','ljmc-theme');
                          break;

                        default:
                          break;
                      }
                      ?>
                      <div class="col-xs-5">
                        <p><?php echo $table['day']; ?></p>
                      </div>

                      <?php
                      if(!empty($table['timetable'])) :
                        foreach ($table['timetable'] as $time) {
                            ?>
                            <p><?php echo $time['from-hour'] . ':' . $time['from-minute'] . ' - ' . $time['to-hour'] . ':' . $time['to-minute']; ?></p>
                            <?php
                          }
                          ?>
                          <?php
                      else:
                        ?><p><?php _e('Closed','ljmc-theme'); ?></p><?php
                      endif;
                                              }
                    ?>
                </div>
                <!-- /.panel-body -->
              </div>
            </div>


            <div class="col-md-6 col-xs-12" style="padding-left:0;padding-right:0">
              <div class="col-xs-12">
                <div class="panel panel-red">
                  <div class="panel-heading">
                      <i class="fa fa-map-marker fa-fw"></i> <?php _e('Address','ljmc-theme'); ?>
                  </div>
                  <div class="panel-body">
                      <?php $address = get_field('address',$main->ID); echo $address['address']; ?>
                  </div>
                  <!-- /.panel-body -->
                </div>
              </div>

              <div class="col-xs-12">
                <div class="panel panel-red">
                  <div class="panel-heading">
                      <i class="fa fa-phone fa-fw"></i> <?php _e('Phone','ljmc-theme'); ?>
                  </div>
                  <div class="panel-body">
                    <?php
                        $phone2 = get_field('telephone2');
                        if($phone2) :
                          foreach($phone2 as $nr){
                            echo $nr['name'] . ': ' . $nr['nr'] . '</br>';
                          }
                        endif;
                    ?>
                  </div>
                  <!-- /.panel-body -->
                </div>
              </div>

              <div class="col-xs-12">
                <div class="panel panel-red">
                  <div class="panel-heading">
                      <i class="fa fa-envelope-o fa-fw"></i> <?php _e('E-mail','ljmc-theme'); ?>
                  </div>
                  <div class="panel-body">
                       <?php the_field('e-mail',$main->ID); ?>
                  </div>
                </div>
              </div>

              <div class="col-xs-12">
                <div class="panel panel-red">
                  <div class="panel-heading">
                      <i class="fa fa-fax fa-fw"></i> <?php _e('Fax','ljmc-theme'); ?>
                  </div>
                  <div class="panel-body">
                       <?php the_field('fax',$main->ID); ?>
                  </div>
                  <!-- /.panel-body -->
                </div>
              </div>
            </div>

            <div class="col-md-3 col-xs-12">
              <div class="panel panel-red">
                <div class="panel-heading">
                    <i class="fa fa-briefcase fa-fw"></i> <?php _e('Properties','ljmc-theme'); ?>
                </div>
                <div class="panel-body">
                    <?php the_field('properties',$main->ID); ?>
                    <br /><br />
                    <?php the_field('account',$main->ID); ?>
                </div>
                <!-- /.panel-body -->
              </div>
            </div>



        </div>


<div class="col-xs-12"  style="margin-top:25px">
          <div class="page-header" style="margin-top:0">
              <h1><span class="line"><?php _e('Map','ljmc-theme'); ?></span></h1>
            </div>
        </div>
        <div class="col-xs-12">
          <?php

          $location = get_field('address',$main->ID);

          if( !empty($location) ):
          ?>
          <div class="acf-map">
            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
          </div>
          <?php endif; ?>
        </div>

                <div class="col-xs-12" style="margin-top:25px">
          <div class="page-header" style="margin-top:0">
              <h1><span class="line"><?php _e('Transport','ljmc-theme'); ?></span></h1>
            </div>
        </div>

        <div class="col-xs-12">
          <?php
          $transport = get_field('transport',$main->ID);

          foreach ($transport as $trans) {
            ?>
            <p><span style="margin-right:5px"><?php echo $trans['name']; ?>:</span>
            <?php foreach ($trans['number'] as $number) {
              ?><a style="margin-right:10px" href="<?php echo $number['url']; ?>" class="btn btn-red"><?php echo $number['number']; ?></a><?php
            }
            ?>
            </p>
            <?php
          }
          ?>
        </div>
        </div>
      </div>
    </section>

    <?php get_footer(); ?>
