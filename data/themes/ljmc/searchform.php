<form method="get" id="ljmc-search" class="navbar-form search pull-right hidden-xs hidden-sm" role="search" action="<?php echo LJMC_SITEURL; ?>">
  <div class="input-group">
    <input type="text" class="form-control" placeholder="<?php _e('Search..','ljmc-theme'); ?>" name="s" id="search-top" value="<?php echo get_search_query(); ?>">
    <div class="input-group-btn">
      <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
    </div>
  </div>
</form>