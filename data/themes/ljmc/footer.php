    <section id="footer" style="padding:20px 0">
      <div class="container">
      <div class="row">
        <div class="col-md-7 col-xs-12" style="padding:0;margin:0">
        <div class="heading">
              <h3><span class="line"><?php _e('Write Us','ljmc-theme'); ?></span></h3>
            </div>
        <?php
        global $sitepress;
        if($sitepress->get_current_language() == 'en'){
          echo do_shortcode('[contact-form-7 id="412" title="Contact form" html_class="use-floating-validation-tip"]');
        }elseif($sitepress->get_current_language() == 'ru'){
          echo do_shortcode('[contact-form-7 id="1294" title="Контактная форма" html_class="use-floating-validation-tip"]');
        }else{
          echo do_shortcode('[contact-form-7 id="1059" title="Kontaktu Forma" html_class="use-floating-validation-tip"]');
        }
        ?>
        <div class="contactformerrors"></div>
        </div>

<?php
$contact_page = get_pages(array(
    'meta_key' => '_ljmc_page_template',
    'meta_value' => 'page-contacts.php'
));
$main = get_field('main',$contact_page[0]->ID);
$main = $main->ID;
$hospitals = get_field('hospitals',$contact_page[0]->ID);
?>

        <div class="col-md-5 col-xs-12" style="padding:0;margin:0">
            <div class="heading">
              <h3><span class="line"><?php _e('Contact Us','ljmc-theme'); ?></span></h3>
            </div>

            <?php
            foreach ($hospitals as $hospital) {
                $address = get_field('address',$hospital->ID);
                $phone = get_field('telephone2',$hospital->ID);
                ?>
                <div class="col-xs-12">
                  <p class="title"><a href="<?php echo get_permalink($hospital->ID); ?>"><?php echo get_the_title($hospital->ID); ?></a></p>
                </div>
                <div class="col-xs-12">
                    <p class="address"><span class="fa fa-map-marker"></span> <?php echo $address['address']; ?></p>
                </div>
                <?php
                  foreach($phone as $nr){
                  ?>
                  <div class="col-xs-12">
                      <p class="number"><span class="fa fa-phone"></span><?php echo $nr['name'] . ': ' . $nr['nr']; ?></p>
                  </div>
                  <?php
                }
            }
            ?>


            <div class="col-xs-6">
              <address>
                <span style="color:#999"><?php _e('Properties','ljmc-theme'); ?>:</span><br/>
                <?php the_field('properties',$main); ?>
              </address>
            </div>

            <div class="col-xs-6">
              <address>
                <span style="color:#999"><?php _e('Bank Account','ljmc-theme'); ?></span><br/>
                <?php the_field('account',$main); ?>
              </address>
            </div>
          </div>
        </div>
        <div class="row" style="padding-top:20px;margin-top:20px">
          	<div class="col-xs-4 socials" style="text-align:left">
          		<a href="https://www.facebook.com/LJmedicinascentrs" class="fa fa-facebook"></a>
            	<a href="http://www.youtube.com/user/LJmedicinascentrs" class="fa fa-youtube"></a>
            </div>

            <div class="col-xs-4" style="text-align:center">
            	<p style="color:#999">© A/S "Latvijas Jūras medicīnas centrs"</p>
            </div>

            <div class="col-xs-4" style="text-align:right">
            	<p style="color:#999"><?php _e('Developed by:','ljmc-theme'); ?> <a style="color:#999;text-transform:none" href="http://dewogroup.com" target="_blank">DEWO</a></p>
            </div>
        </div>
        </div>

      </div>
    </section>

    <!-- JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php ljmc_base_url(); ?>js/bootstrap.js"></script>
    <!-- IE10 vieljmcort hack for Surface/desktop Windows 8 bug -->
    <script src="<?php ljmc_base_url(); ?>js/ie10-viewport-bug-workaround.js"></script>
    <script src="<?php ljmc_base_url(); ?>js/bootstrap-select.min.js"></script>
    <?php if(get_post_meta( $ljmc_query->post->ID, '_ljmc_page_template', true ) == 'page-contacts.php' || get_post_type() == 'hospital') : ?>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php ljmc_base_url(); ?>js/google-map.js" type="text/javascript"></script>
    <?php endif; ?>
    <script src="<?php ljmc_base_url(); ?>js/lightgallery.min.js" type="text/javascript"></script>
    <?php ljmc_load_slider_script(); ?>
    <script type="text/javascript">
      (function($) {

          $.fn.attach = function(options) {
             // Remove elements (without id) on which the plugin was invoked
              var valid = this.not('[id]');

             // Loop each valid element (with id attribute) and process
             valid.each(function() {
                if(!$(this).parents('#menu-galvena').length){
                  $(this).remove();
                }
             });

             return this; // Maintain chainability
          };

      })(jQuery);

      $('.menu-item').attach();

      jQuery( document ).ready(function( jQuery ) {

        $('#ljmc-loader').fadeOut('fast');
        if(jQuery('#ljmcadminbar')){
          var navbarHeight = jQuery('#ljmcadminbar').height() + jQuery('#header .navbar-red').height();
          var navbarContactHeight = jQuery('#ljmcadminbar').height() + jQuery('#main').height();
        }else{
          var navbarHeight = jQuery('#header .navbar-red').height();
          var navbarContactHeight = jQuery('#ljmcadminbar').height() + jQuery('#main').height();
        }

        jQuery('.modal').css('margin-top',navbarContactHeight);

        if(jQuery(window).scrollTop() > navbarHeight){
            jQuery('#header #main').fadeIn('fast', function(){
              jQuery(this).addClass('navbar-fixed-top');
              jQuery(this).css('margin-top',jQuery('#ljmcadminbar').height());
            });
          }else{
            jQuery('#header #main').fadeIn('fast', function(){
              jQuery(this).removeClass('navbar-fixed-top');
              jQuery(this).css('margin-top',0);
            });
          }
        jQuery(window).scroll(function(){
          if(jQuery(window).scrollTop() > jQuery('#header .navbar-red').height()){
            jQuery('#header #main').fadeIn('fast', function(){
              jQuery(this).addClass('navbar-fixed-top');
              jQuery(this).css('margin-top',jQuery('#ljmcadminbar').height());
            });
          }else{
            jQuery('#header #main').fadeIn('fast', function(){
              jQuery(this).removeClass('navbar-fixed-top');
              jQuery(this).css('margin-top',0);
            });
          }
        });

        jQuery(window).scroll(function () {
            var fromTop = jQuery(window).scrollTop() / 3;
            jQuery("#slider").css('top', fromTop + 'px');
        });

        var searchProfessionSelect = jQuery('#search-profession').selectpicker({dropupAuto:false});
        var searchHospitalSelect = jQuery('#search-hospital').selectpicker();


        if(jQuery('#content-gallery')){
          jQuery("#content-gallery").sliderPro({
            width: jQuery('#content-gallery').width(),
            height: 300,
            arrows: false,
            buttons: false,
            waitForLayers: true,
            thumbnailWidth: jQuery("#content-gallery").width() / 3,
            thumbnailHeight: 100,
            thumbnailPointer: true,
            autoplay: true,
            loop: true,
            autoScaleLayers: false
          });
        }

        jQuery('#ljmc-search').attr('action','/');
        var htmlString='<?php _e('Enter keywords..','ljmc-theme'); ?>';
        jQuery('#doctor-search-text-input').attr('placeholder',htmlString);

        jQuery('.search-doctor.hospital .bs.dropdown-menu').removeClass('open');
        jQuery('.search-doctor.profession .bs.dropdown-menu').removeClass('open');

        jQuery('.search-doctor.hospital').click(function(){
          jQuery('.search-doctor.hospital .bs.dropdown-menu').toggleClass('open',true);
          jQuery('.search-doctor.profession .bs.dropdown-menu').toggleClass('open',false);
        });

        jQuery('.search-doctor.profession').click(function(){
          jQuery('.search-doctor.hospital .bs.dropdown-menu').toggleClass('open',false);
          jQuery('.search-doctor.profession .bs.dropdown-menu').toggleClass('open',true);
        });

      });
    </script>

    <?php ljmc_footer(); ?>
</body>
</html>
