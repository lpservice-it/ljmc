    
    <?php get_header(); ?>


    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">
          <div class="row">
            <div class="col-xs-12" style="text-align:left;margin-left:222px">
              <h1><?php _e('404! Not found.','ljmc-theme'); ?></h1>
            </div>
          </div>

          <div class="row" style="padding-bottom:100px">
            <div class="col-xs-2 hidden-md hidden-sm hidden-xs">
            <img src="<?php ljmc_base_url(); ?>img/logo.svg" style="width:100%;height:auto;padding-right:40px;margin-top:-65px">
            </div>

            <div class="col-xs-12 col-lg-10" style="padding-left:40px">
              <p><?php _e('Sorry, but page you were searching for is not here.','ljmc-theme'); ?></p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php get_footer(); ?>

    