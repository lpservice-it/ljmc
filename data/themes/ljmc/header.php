<!DOCTYPE html>
<html lang="<?php bloginfo('language'); ?>">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="vieljmcort">

    <link rel="shortcut icon" type="image/x-icon" href="<?php ljmc_base_url(); ?>favicon.ico" />
    <title>
    <?php
    if(is_home()) :
      echo get_bloginfo('name') . ' | ' . __('News','ljmc-theme');
    elseif(is_category()) :
      $categories = get_the_category();
      $category_id = $categories[0]->cat_ID;
      echo get_bloginfo('name') . ' | ' . get_cat_name($category_id);
    elseif(is_archive()) :
      echo get_bloginfo('name') . ' | ' . __('Archive','ljmc-theme');
     elseif(is_search()) :
      global $ljmc_query;
      echo get_bloginfo('name') . ' | ' . $ljmc_query->query['s'];
    elseif(!is_front_page()) :
      echo get_bloginfo('name') . ' | ' . get_the_title(get_the_ID());
    else:
      bloginfo('name');
    endif;
    ?>
    </title>

    <!-- CSS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700&subset=latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php ljmc_base_url(); ?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php ljmc_base_url(); ?>css/style.css">
    <link rel="stylesheet" href="<?php ljmc_base_url(); ?>css/lightgallery.css">
    <?php ljmc_load_slider_style(); ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">

    .acf-map {
      width: 100%;
      height: 400px;
      border: #ccc solid 1px;
      margin: 20px 0;
    }
    .dropdown-toggle { cursor: pointer }
    .doctor.hospital.show-tick div.dropdown-menu.open { display: none !important }
    .doctor.hospital.show-tick.open div.dropdown-menu.open { display: inline-block !important }
    </style>
    <?php require_once(dirname(__FILE__) . '/' . 'fb-pixel.php'); ?>
    <?php ljmc_head(); ?>
  </head>

  <body>
    <?php require_once(dirname(__FILE__) . '/' . 'analytics.php'); ?>
    <div id="ljmc-loader"><img src="<?php ljmc_base_url(); ?>img/loader.gif" /></div>
    <!-- RED INFO NAV-->
    <section id="header">
      <nav class="navbar navbar-red hidden-xs">
        <div class="container">
          <div class="row">
            <div class="contacts col-xs-12">
              <?php get_header_contacts(); ?>
            </div>
          </div>
        </div>
      </nav><!--/ RED INFO NAV -->

      <!-- MAIN NAV -->
      <nav id="main" class="navbar navbar-default">
        <div class="container" style="padding-left:0;padding-right:0">
          <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#main-navigation-menu" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
              <span class="sr-only"><?php _e('Toggle Navigation','ljmc-theme'); ?></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="<?php bloginfo('url'); ?>" class="logo hidden-xs"><img src="<?php ljmc_base_url(); ?>img/logo.svg" /></a>
            <a href="<?php bloginfo('url'); ?>" class="logo hidden-sm hidden-md hidden-lg" style="margin-left:10px"><img src="<?php ljmc_base_url(); ?>img/logo.svg" /></a>
          </div>
                <?php
                $args = array(
                  'menu'              => 'primary',
                  'theme_location'    => 'primary',
                  'depth'             => 2,
                  'container'         => 'div',
                  'container_class'   => 'collapse navbar-collapse',
                  'container_id'      => 'main-navigation-menu',
                  'menu_class'        => 'nav navbar-nav',
                  'fallback_cb'       => 'ljmc_bootstrap_navwalker::fallback',
                  'walker'            => new ljmc_bootstrap_navwalker()
                );
                ljmc_nav_menu($args);
                ?>

                <form method="get" id="ljmc-search" class="navbar-form search pull-right hidden-xs hidden-sm" role="search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="<?php _e('Search..','ljmc-theme'); ?>" name="s" id="search-top">
                    <div class="input-group-btn">
                      <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
              </form>
              <script type="text/javascript">jQuery('#ljmc-search').appendTo('#main-navigation-menu');</script>
        </div>
      </nav><!--/ MAIN NAV -->


      <?php ljmc_init_slider(); ?>

      <?php if(is_front_page()) : ?>

      <nav class="navbar search hidden-xs">
        <div class="container">
            <div class="row">
                  <?php
                  $args = array(
                      'post_type' => 'page',
                      'fields' => 'ids',
                      'nopaging' => true,
                      'meta_key' => '_ljmc_page_template',
                      'meta_value' => 'page-specialists.php'
                  );
                  $pages = get_posts( $args );
                  foreach ( $pages as $page )
                      $servicesid = $page;

                  $url = get_permalink($servicesid);
                  $lang = ICL_LANGUAGE_CODE;
                  ?>
              <form id="search-doctor-form" action="<?php echo get_url_for_language($url,$lang); ?>" method="post">
                <div class="col-xs-3" style="position:relative">
                  <h3 style="margin:0;padding:0;line-height:1.1;vertical-align:middle;text-align:right"><span class="label label-default"><i class="fa fa-user-md" style="font-size:21px; margin-right:10px"></i><span style="font-weight:300"><?php _e('Search doctor','ljmc-theme'); ?></span></span></h3>
                </div>


                <div id="search-hospital-wrap" class="col-xs-3" style="position:relative">
                  <select id="search-hospital" class="search-doctor hospital selectpicker show-tick" title="<?php _e('Hospital...','ljmc-theme'); ?>" name="search-hospital[]" multiple>
                    <option value="0" disabled="disabled" style="text-align:left;font-size:90%"><?php _e('Choose hospital...','ljmc-theme'); ?></option>
                    <?php
                    $hospital_list = ljmc_get_hospitals();

                    $html = '';
                    foreach($hospital_list as $value){
                      $html .= '<option value="' . $value["id"] . '">' . $value["name"] . '</option>';
                    }
                    echo $html;
                    ?>
                  </select>
                </div>


                <div id="search-profession-wrap" class="col-xs-3" style="position:relative">
                  <select id="search-profession" class="search-doctor profession selectpicker show-tick" title="<?php _e('Profession...','ljmc-theme'); ?>" name="search-profession[]" multiple data-live-search="true">
                    <option value="0" disabled="disabled" style="text-align:left;font-size:90%"><?php _e('Choose profession...','ljmc-theme'); ?></option>
                  </select>
                </div>

                <div id="search-submit-wrap" class="col-xs-3">
                  <button id="search-submit-result" class="btn btn-red search"><?php _e('View','ljmc-theme'); ?></button>
                </div>
              </form>
            </div>
        </div>
      </nav>
    <?php endif; ?>
    </section>

    <?php
    $post_hospitals = array(739,907);


    ?>
