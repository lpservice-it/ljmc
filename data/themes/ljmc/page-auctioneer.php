    <?php /* Template Name: Akcionāri */ ?>
        
    <?php get_header(); ?>


    <section id="content">
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="row">
            <div class="col-xs-12" style="text-align:center">
            <h1><?php the_title(); ?></h1>
            
            <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

        </div>
      </div>




      <div class="container" style="margin-top:80px;margin-bottom:80px">

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <div class="row">
            <div class="col-xs-3">
              <?php
                $args = array(
                  'menu'              => 'about',
                  'theme_location'    => 'about',
                  'depth'             => 2,
                  'container_id'      => 'bs-example-navbar-collapse-1',
                  'menu_class'        => 'nav nav-pills nav-stacked nav-pills-sidebar'
                );
                ljmc_nav_menu($args);
                ?>
            </div>

            <div class="col-xs-9">
              <div class="col-xs-12">

                <?php the_content(); ?>
                <?php if(get_field('gallery')) : ?>
              <div class="page-header">
                <h1><span class="line"><?php _e('Gallery','ljmc-theme'); ?></span></h1>
              </div>

              <div id="lightgallery<?php echo get_the_ID();?>">
                <?php
                    $gallery_field = get_field('gallery');
                    foreach($gallery_field as $image){
                      ?>
                            <a href="<?php echo $image['url']; ?>">
                                <div class="col-xs-2" style="padding:0;margin:0;"><img class="img-responsive" src="<?php echo $image['url']; ?>" /></div>
                            </a>
                      <?php
                    }
                    ?>
              </div>
              <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery("#lightgallery"+<?php echo get_the_ID();?>).lightGallery(); 
                });
            </script>
            <?php endif; ?>
              </div>

              


              <div class="col-xs-12">
                <div class="page-header" style="margin-top:0">
                  <h1><span class="line"><?php _e('Auctioneers with >5%','ljmc-theme'); ?></span></h1>
                </div>

                <?php

                $auctioneers = get_field('auctioneer');

                foreach ($auctioneers as $key => $auctioneer) {
                  ?>

                  <div class="col-xs-4">
                  <p><?php  echo $auctioneer['name']; ?></p>
                  </div>

                  <div class="col-xs-8">
                    <div class="progress">
                      <div class="progress-bar progress-bar-red" role="progressbar" aria-valuenow="<?php echo $auctioneer['percentage']; ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: <?php echo $auctioneer['percentage'] . '%'; ?>">
                        <?php  echo $auctioneer['percentage']; ?>%
                      </div>
                    </div>
                  </div>

                  <?php
                }
                ?>
              </div>





              <?php if(get_field('documents')) : ?>
              <div class="col-xs-12">
                <div class="page-header" style="margin-top:0">
                  <h1><span class="line"><?php _e('Documents','ljmc-theme'); ?></span></h1>
                </div>

                <?php

                $documents = get_field('documents');

                foreach ($documents as $key => $document) {
                  ?>

                  <div class="col-xs-4">
                    <div class="thumbnail" style="min-height:100px">
                      <div class="caption">
                        <h4 id="thumbnail-label" style="margin-top:0;font-weight:400"><?php echo $document['name']; ?><a href="#thumbnail-label" class="anchorjs-link"><span class="anchorjs-icon"></span></a></h4>
                        <p><a role="button" class="btn btn-red" href="<?php echo $document['file']; ?>"><?php _e('View','ljmc-theme'); ?></a></p>
                      </div>
                    </div>
                  </div>

                  <?php
                }

                ?>

              </div>
            <?php endif; ?>
            </div>
          </div>

        <?php endwhile; endif; ?>

      </div>
    </section>

    <?php get_footer(); ?>

    