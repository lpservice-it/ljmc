    
    <?php get_header(); ?>


    <section id="content">

      <?php if(is_single()) : ?>
      <div class="header" style="position:relative;width:100%;height:auto;overflow:hidden"><div style="position:absolute;top:0;left:0;background:rgba(255,255,255,0.8); width:100%;height:100%;"></div>
        <div class="container">
          <div class="row">
            <div class="col-xs-12" style="text-align:center">
              <h1><?php the_title(get_the_ID()); ?></h1>
              <?php ljmc_page_breadcrumb(); ?>
            </div>
          </div>

          <div class="row" style="padding-bottom:100px">
            <div class="col-xs-2 hidden-md hidden-sm hidden-xs">
            <img src="<?php ljmc_base_url(); ?>img/logo.svg" style="width:100%;height:auto;padding-right:40px;margin-top:-20px">
            </div>

            <div class="col-xs-12 col-lg-10" style="padding-left:40px;border-left:2px solid #cf4240;">
              <?php the_excerpt(get_the_ID); ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>


      <div class="container" style="margin-top:80px">
      <div class="row">
        <div class="col-xs-9">
          <div class="col-xs-12">
            <?php the_content(); ?>
            <?php if(get_field('gallery')) : ?>
              <div class="page-header">
                <h1><span class="line"><?php _e('Gallery','ljmc-theme'); ?></span></h1>
              </div>

              <div id="lightgallery<?php echo get_the_ID();?>">
                <?php
                    $gallery_field = get_field('gallery');
                    foreach($gallery_field as $image){
                      ?>
                            <a href="<?php echo $image['url']; ?>">
                                <div class="col-xs-2" style="padding:0;margin:0;"><img class="img-responsive" src="<?php echo $image['url']; ?>" /></div>
                            </a>
                      <?php
                    }
                    ?>
              </div>
              <script type="text/javascript">
                jQuery(document).ready(function() {
                    jQuery("#lightgallery"+<?php echo get_the_ID();?>).lightGallery(); 
                });
            </script>
            <?php endif; ?>
          </div>
        </div>
      </div>
      </div>
    </section>

    <?php get_footer(); ?>

    