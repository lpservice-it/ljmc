<?php
// Default constants
require_once(dirname(__FILE__) . '/inc/constants.php');

add_action( 'after_setup_theme', 'ljmc_theme_setup' );
function ljmc_theme_setup(){
  load_theme_textdomain('ljmc-theme', get_template_directory() . '/languages');
}


add_filter('acf/settings/default_language', 'my_acf_settings_default_language');
 
function my_acf_settings_default_language( $language ) {
    global $sitepress;
    return $sitepress->get_default_language();
}

add_filter('acf/settings/current_language', 'my_acf_settings_current_language');
 
function my_acf_settings_current_language( $language ) {
    global $sitepress;
    return $sitepress->get_current_language();
}

add_image_size('small', 90);
add_image_size('medium', 350);
add_image_size('large', 1200);

/**
 * Return base directory url
 */
function ljmc_base_url($format=true){
  if(isset($_SERVER['HTTPS'])){
    $protocol = 'https://';
  }else{
    $protocol = 'http://';
  }

  $url = $protocol;
  $url .= $_SERVER['SERVER_NAME'];
  $url .= dirname($_SERVER['PHP_SELF']) . '/';
  $url .= 'data/themes/ljmc/';

  if($format){
    echo $url;
  }else{
    return $url;
  }
}

if( function_exists('acf_add_options_sub_page') )
{
    acf_add_options_sub_page(array(
        'title' => __('Slīdrāde','ljmc-theme'),
        'parent' => 'options-general.php',
        'capability' => 'manage_options'
    ));

    acf_add_options_sub_page(array(
        'title' => __('Partneri','ljmc-theme'),
        'parent' => 'options-general.php',
        'capability' => 'manage_options'
    ));
}

/* Hospitals */
add_action( 'init', 'ljmc_register_hospitals' );

function ljmc_register_hospitals() {
  $labels = array(
    'name'               => __( 'Hospitals', 'ljmc-theme' ),
    'singular_name'      => __( 'Hospital', 'ljmc-theme' ),
    'menu_name'          => __( 'Hospitals', 'ljmc-theme' ),
    'name_admin_bar'     => __( 'Hospital', 'ljmc-theme' ),
    'add_new'            => __( 'Add New', 'ljmc-theme' ),
    'add_new_item'       => __( 'Add New Hospital', 'ljmc-theme' ),
    'new_item'           => __( 'New Hospital', 'ljmc-theme' ),
    'edit_item'          => __( 'Edit Hospital', 'ljmc-theme' ),
    'view_item'          => __( 'View Hospital', 'ljmc-theme' ),
    'all_items'          => __( 'All Hospitals', 'ljmc-theme' ),
    'search_items'       => __( 'Search Hospitals', 'ljmc-theme' ),
    'parent_item_colon'  => __( 'Parent Hospitals:', 'ljmc-theme' ),
    'not_found'          => __( 'No hospitals found.', 'ljmc-theme' ),
    'not_found_in_trash' => __( 'No hospitals found in Trash.', 'ljmc-theme' )
  );

  $args = array(
    'labels'             => $labels,
                'description'        => __( 'Description.', 'ljmc-theme' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'hospital' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 32,
    'menu_icon'          => 'fa-hospital-o',
    'supports'           => array( 'title' )
  );

  register_post_type( 'hospital', $args );
}



/** Header Contact info **/
function get_header_contacts(){
  $telephone = get_field('telephone','option');
  $email = get_field('e-mail','option');
  $address = get_field('address','option');
  ?>
    <a href="callto://<?php echo $telephone; ?>"><span class="fa fa-phone"></span><?php echo $telephone; ?></a>
    <a href="mailto://<?php echo $email; ?>"><span class="fa fa-envelope-o"></span><?php echo $email; ?></a>
    <a><span class="fa fa-map-marker"></span><?php echo $address; ?></a>
  <?php
}



/* Register Services Posts */
add_action( 'init', 'ljmc_register_services' );
/**
 * Register a book post type.
 *
 * @link http://codex.ljmc.org/Function_Reference/register_post_type
 */
function ljmc_register_services() {
  $labels = array(
    'name'               => __( 'Services', 'ljmc-theme' ),
    'singular_name'      => __( 'Service', 'ljmc-theme' ),
    'menu_name'          => __( 'Services', 'ljmc-theme' ),
    'name_admin_bar'     => __( 'Service', 'ljmc-theme' ),
    'add_new'            => __( 'Add New', 'ljmc-theme' ),
    'add_new_item'       => __( 'Add New Service', 'ljmc-theme' ),
    'new_item'           => __( 'New Service', 'ljmc-theme' ),
    'edit_item'          => __( 'Edit Services', 'ljmc-theme' ),
    'view_item'          => __( 'View Services', 'ljmc-theme' ),
    'all_items'          => __( 'All Services', 'ljmc-theme' ),
    'search_items'       => __( 'Search Services', 'ljmc-theme' ),
    'parent_item_colon'  => __( 'Parent Services:', 'ljmc-theme' ),
    'not_found'          => __( 'No services found.', 'ljmc-theme' ),
    'not_found_in_trash' => __( 'No services found in Trash.', 'ljmc-theme' )
  );

  $args = array(
    'labels'             => $labels,
                'description'        => __( 'Description.', 'ljmc-theme' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'service' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 28,
    'menu_icon'          => 'fa-briefcase',
    'supports'           => array( 'title', 'editor' )
  );

  register_post_type( 'service', $args );
}



/* Register Specialists Posts */
add_action( 'init', 'ljmc_register_specialists' );
/**
 * Register a book post type.
 *
 * @link http://codex.ljmc.org/Function_Reference/register_post_type
 */
function ljmc_register_specialists() {
  $labels = array(
    'name'               => __( 'Specialists', 'ljmc-theme' ),
    'singular_name'      => __( 'Specialist', 'ljmc-theme' ),
    'menu_name'          => __( 'Specialists', 'ljmc-theme' ),
    'name_admin_bar'     => __( 'Specialist', 'ljmc-theme' ),
    'add_new'            => __( 'Add New', 'ljmc-theme' ),
    'add_new_item'       => __( 'Add New Specialist', 'ljmc-theme' ),
    'new_item'           => __( 'New Specialist', 'ljmc-theme' ),
    'edit_item'          => __( 'Edit Specialist', 'ljmc-theme' ),
    'view_item'          => __( 'View Specialist', 'ljmc-theme' ),
    'all_items'          => __( 'All Specialists', 'ljmc-theme' ),
    'search_items'       => __( 'Search Specialists', 'ljmc-theme' ),
    'parent_item_colon'  => __( 'Parent Specialists:', 'ljmc-theme' ),
    'not_found'          => __( 'No specialists found.', 'ljmc-theme' ),
    'not_found_in_trash' => __( 'No specialists found in Trash.', 'ljmc-theme' )
  );

  $args = array(
    'labels'             => $labels,
                'description'        => __( 'Description.', 'ljmc-theme' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'specialist' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 29,
    'menu_icon'          => 'fa-user-md',
    'supports'           => array( 'title', 'editor' )
  );

  register_post_type( 'specialist', $args );
}

/* Register Timetables Posts */
add_action( 'init', 'ljmc_register_timetables' );
/**
 * Register a book post type.
 *
 * @link http://codex.ljmc.org/Function_Reference/register_post_type
 */
function ljmc_register_timetables() {
  $labels = array(
    'name'               => __( 'Timetables', 'ljmc-theme' ),
    'singular_name'      => __( 'Timetable', 'ljmc-theme' ),
    'menu_name'          => __( 'Timetables', 'ljmc-theme' ),
    'name_admin_bar'     => __( 'Timetable', 'ljmc-theme' ),
    'add_new'            => __( 'Add New', 'ljmc-theme' ),
    'add_new_item'       => __( 'Add New Timetable', 'ljmc-theme' ),
    'new_item'           => __( 'New Timetable', 'ljmc-theme' ),
    'edit_item'          => __( 'Edit Timetable', 'ljmc-theme' ),
    'view_item'          => __( 'View Timetable', 'ljmc-theme' ),
    'all_items'          => __( 'All Timetables', 'ljmc-theme' ),
    'search_items'       => __( 'Search Timetables', 'ljmc-theme' ),
    'parent_item_colon'  => __( 'Parent Timetables:', 'ljmc-theme' ),
    'not_found'          => __( 'No timetables found.', 'ljmc-theme' ),
    'not_found_in_trash' => __( 'No timetables found in Trash.', 'ljmc-theme' )
  );

  $args = array(
    'labels'             => $labels,
                'description'        => __( 'Description.', 'ljmc-theme' ),
    'public'             => false,
    'publicly_queryable' => false,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'timetable' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 30,
    'menu_icon'          => 'fa-calendar',
    'supports'           => array( 'title' )
  );

  register_post_type( 'timetable', $args );
}


/* Register Pricelists Posts */
add_action( 'init', 'ljmc_register_pricelists' );
/**
 * Register a book post type.
 *
 * @link http://codex.ljmc.org/Function_Reference/register_post_type
 */
function ljmc_register_pricelists() {
  $labels = array(
    'name'               => __( 'Pricelists', 'ljmc-theme' ),
    'singular_name'      => __( 'Pricelist', 'ljmc-theme' ),
    'menu_name'          => __( 'Pricelists', 'ljmc-theme' ),
    'name_admin_bar'     => __( 'Pricelist', 'ljmc-theme' ),
    'add_new'            => __( 'Add New', 'ljmc-theme' ),
    'add_new_item'       => __( 'Add New Pricelist', 'ljmc-theme' ),
    'new_item'           => __( 'New Pricelist', 'ljmc-theme' ),
    'edit_item'          => __( 'Edit Pricelist', 'ljmc-theme' ),
    'view_item'          => __( 'View Pricelist', 'ljmc-theme' ),
    'all_items'          => __( 'All Pricelists', 'ljmc-theme' ),
    'search_items'       => __( 'Search Pricelists', 'ljmc-theme' ),
    'parent_item_colon'  => __( 'Parent Pricelists:', 'ljmc-theme' ),
    'not_found'          => __( 'No pricelists found.', 'ljmc-theme' ),
    'not_found_in_trash' => __( 'No pricelists found in Trash.', 'ljmc-theme' )
  );

  $args = array(
    'labels'             => $labels,
                'description'        => __( 'Description.', 'ljmc-theme' ),
    'public'             => false,
    'publicly_queryable' => false,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'pricelist' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 31,
    'menu_icon'          => 'fa-usd',
    'supports'           => array( 'title' )
  );

  register_post_type( 'pricelist', $args );
}



/** SPECIALISTS TAX **/
// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'ljmc_register_specialists_tax', 0 );

// create two taxonomies, genres and writers for the post type "book"
function ljmc_register_specialists_tax() {
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name'              => __( 'Professions', 'ljmc-theme' ),
    'singular_name'     => __( 'Profession', 'ljmc-theme' ),
    'search_items'      => __( 'Search Professions', 'ljmc-theme' ),
    'all_items'         => __( 'All Professions', 'ljmc-theme' ),
    'parent_item'       => __( 'Parent Profession', 'ljmc-theme' ),
    'parent_item_colon' => __( 'Parent Profession:', 'ljmc-theme' ),
    'edit_item'         => __( 'Edit Profession', 'ljmc-theme' ),
    'update_item'       => __( 'Update Profession', 'ljmc-theme' ),
    'add_new_item'      => __( 'Add New Profession', 'ljmc-theme' ),
    'new_item_name'     => __( 'New Profession Name', 'ljmc-theme' ),
    'menu_name'         => __( 'Professions', 'ljmc-theme' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'profession' ),
  );

  register_taxonomy( 'profession', 'specialist', $args );
}


/**
 * Register Main Menu
 */
add_action( 'after_setup_theme', 'ljmc_register_main_menu' );
function ljmc_register_main_menu() {
  register_nav_menu( 'primary', __( 'Main Menu', 'ljmc-theme' ) );
}

/**
 * Register About Menu
 */
add_action( 'after_setup_theme', 'ljmc_register_about_menu' );
function ljmc_register_about_menu() {
  register_nav_menu( 'about', __( 'About us', 'ljmc-theme' ) );
}


/**
 * Register News Menu
 */
add_action( 'after_setup_theme', 'ljmc_register_news_menu' );
function ljmc_register_news_menu() {
  register_nav_menu( 'news', __( 'News', 'ljmc-theme' ) );
}

/**
 * Add bootstrap support for navs
 */
require_once(dirname(__FILE__) . '/inc/bootstrap_navwalker.php');

function ljmc_page_breadcrumb(){
  global $post;     // if outside the loop

  if(is_single()) :
    if(get_post_type(get_the_ID()) == 'post') : ?>
    <ol class="breadcrumb" style="background:none;margin-bottom:80px">
      <?php
        ?><li><a href="<?php echo bloginfo('url'); ?>"><?php _e('Home','ljmc-theme'); ?></a></li>
        <li><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><?php _e('News','ljmc-theme'); ?></a></li><?php
      ?>
    </ol><?php
    elseif(get_post_type(get_the_ID()) == 'service') : ?>
    <ol class="breadcrumb" style="background:none;margin-bottom:80px">
      <?php
        ?><li><a href="<?php echo bloginfo('url'); ?>"><?php _e('Home','ljmc-theme'); ?></a></li>
        <li><a href="<?php echo get_permalink( 292 ); ?>"><?php _e('Services','ljmc-theme'); ?></a></li>
        <li><a style="text-decoration:none;color:#3b3b3b;cursor:default"><?php the_title(); ?></a></li><?php
      ?>
    </ol>
  <?php elseif(is_home()) : ?>
    <?php else : ?>
      <ol class="breadcrumb" style="background:none;margin-bottom:80px">
      <?php
        ?><li><a href="<?php echo bloginfo('url'); ?>"><?php _e('Home','ljmc-theme'); ?></a></li>
        <?php
      ?>
    </ol>
    <?php endif;
  else: ?>

    <ol class="breadcrumb" style="background:none;margin-bottom:80px">
      <?php
      if ( is_page() && $post->post_parent ) {
        ?><li><a href="<?php echo bloginfo('url'); ?>"><?php _e('Home','ljmc-theme'); ?></a></li>
        <li><a href="<?php echo get_the_permalink($post->post_parent); ?>"><?php echo get_the_title($post->post_parent); ?></a></li><?php
      } else {
        ?><li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e('Home','ljmc-theme'); ?></a></li><?php
      }

      ?>
    </ol><?php

  endif;

}



/** FILTER h1 tags **/
add_filter( 'the_content', 'ljmc_filter_content_h1' );
function ljmc_filter_content_h1( $content )
{

    $content = str_ireplace( '<h1>', '<div class="page-header" style="margin-top:0"><h1><span class="line">', $content );
    $content = str_ireplace( '</h1>', '</span></h1></div>', $content );

    return $content;
}



function ljmc_load_slider_style(){
  if(is_front_page()) :
    ?>
    <link rel="stylesheet" type="text/css" href="<?php ljmc_base_url(); ?>css/slider-pro.css" media="screen"/>
    <?php
  endif;
}

function ljmc_load_slider_script(){
  if(is_front_page()) :
    ?>
    <script type="text/javascript" src="<?php ljmc_base_url(); ?>js/jquery.sliderPro.js"></script>
    <script type="text/javascript">
      jQuery( document ).ready(function( jQuery ) {
        var slidesCount = jQuery('#slider .sp-slide').length;
        if(slidesCount < 4){
          slidesCount = 4;
        }
        if(slidesCount > 5){
          slidesCount = 5;
        }

        var adminBar = 0;
        if(jQuery('#ljmcadminbar')){
          adminBar = jQuery('#ljmcadminbar').height();
        }

        sliderHeight = jQuery(window).height() - (140 + jQuery('nav .navbar-red').height() + jQuery('nav#main').height()) - adminBar;

        jQuery( '#slider' ).sliderPro({
          width: jQuery(window).width(),
          height: sliderHeight,
          arrows: false,
          buttons: false,
          waitForLayers: true,
          thumbnailWidth: jQuery(window).width() / slidesCount - slidesCount,
          thumbnailHeight: 100,
          thumbnailPointer: true,
          autoplay: true,
          loop: true,
          autoScaleLayers: false
        });
        if(jQuery('#ljmcadminbar')){
          var navbarHeight = jQuery('#ljmcadminbar').height() + jQuery('#header .navbar-red').height();
        }else{
          var navbarHeight = jQuery('#header .navbar-red').height();
        }
        jQuery(window).resize(function(){
          jQuery('#slider .sp-thumbnail-container').css('width', jQuery(window).width() / slidesCount);
          jQuery('#slider .sp-thumbnails-container').css('width', jQuery(window).width());
          if(jQuery(window).height() > jQuery(window).width()){
            jQuery('#slider').css('max-height', jQuery(window).height() / 2);
          }else{
            jQuery('#slider').css('max-height', '100%');
          }
        });
        if(jQuery(window).scrollTop() > navbarHeight){
            jQuery('#slider .sp-thumbnails-container').fadeOut('fast');
          }else{
            jQuery('#slider .sp-thumbnails-container').fadeIn('fast');
          }
        jQuery(window).scroll(function(){
          if(jQuery(window).scrollTop() > jQuery('#header .navbar-red').height()){
            jQuery('#slider .sp-thumbnails-container').fadeOut('fast');
          }else{
            jQuery('#slider .sp-thumbnails-container').fadeIn('fast');
          }
        });
        jQuery('#slider .sp-thumbnails-container').css('width', jQuery(window).width());
        jQuery('#slider .sp-thumbnail-container').css('width', jQuery('#slider .sp-thumbnail-container').width() - 1);
        if(jQuery(window).height() > jQuery(window).width()){
          jQuery('#slider').css('max-height', jQuery(window).height() / 2);
        }else{
          jQuery('#slider').css('max-height', '100%');
        }
      });
    </script>
    <?php
  endif;
}

function ljmc_init_slider(){
  if(is_front_page()) : ?>
    <div id="slider" class="slider-pro">
      <div class="sp-slides">
        <?php
        $slider = get_field('slides','option');
        foreach ($slider as $key => $slide) {
          $position = $slide['position'];
          $url = '';
          if(!empty($slide['url'])) $url = '<a class="btn btn-red" href="' . $slide['url'] . '" style="padding:10px 20px;margin-top:5px;float:left;border:none">' . __('Read more','ljmc-theme') . '</a>';
          switch ($position) {
            case 'Top':
              $position = 'topCenter';
              $padding = 'data-vertical="50"';
              $trans = 'top';
              break;

            case 'Right':
              $position = 'rightCenter';
              $padding = 'data-horizontal="70"';
              $trans = 'right';
              break;

            case 'Left':
              $position = 'leftCenter';
              $padding = 'data-horizontal="70" style="left:50px"';
              $trans = 'left';
              break;

            case 'Bottom':
              $position = 'bottomCenter';
              $padding = 'data-vertical="50"';
              $trans = 'bottom';
              break;

            case 'Top Right':
              $position = 'topRight';
              $padding = 'data-vertical="50" data-horizontal="70"';
              $trans = 'top';
              break;

            case 'Top Left':
              $position = 'topLeft';
              $padding = 'data-vertical="50" data-horizontal="70"';
              $trans = 'top';
              break;

            case 'Bottom Right':
              $position = 'bottomRight';
              $padding = 'data-vertical="50" data-horizontal="70"';
              $trans = 'bottom';
              break;

            case 'Bottom Left':
              $position = 'bottomLeft';
              $padding = 'data-vertical="50" data-horizontal="70"';
              $trans = 'bottom';
              break;
            
            default:
              $position = 'centerCenter';
              $padding = '';
              $trans = 'top';
              break;
          }
          ?>
          <div class="sp-slide">
            <img class="sp-image" src="<?php ljmc_base_url(); ?>img/blank.gif"
              data-src="<?php echo $slide['image']; ?>"
              data-retina="<?php echo $slide['image']; ?>"/>
            <div class="sp-layer sp-padding col-lg-4 col-md-5 col-sm-6 col-xs-6" data-show-delay="400" data-show-transition="$<?php echo $trans; ?>" <?php echo $padding; ?>  data-position="<?php echo $position; ?>">
              <h2 style="color:#cf4944;font-weight:300;text-transform:uppercase"><?php echo $slide['title']; ?></h2>
              <p style="line-height:1.5"><?php echo $slide['description']; ?></p>
              <?php echo $url; ?>
            </div>
          </div>
          <?php
        }?>
        </div>
        <div class="sp-thumbnails">
        <?php
        foreach ($slider as $key => $slide) {
          ?>
            <div class="sp-thumbnail">
              <div class="sp-thumbnail-title"><?php echo $slide['title']; ?></div>
            </div>
          <?php
        }
        ?>
      </div>
    </div>
  <?php endif;
}


/**
 * Hide editor for specific page templates.
 *
 */
add_action( 'admin_init', 'hide_editor' );

function hide_editor() {
  // Get the Post ID.
  if(isset($_GET['post']) && !empty($_POST['post'])) :
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    if( !isset( $post_id ) ) return;

    // Get the name of the Page Template file.
    $template_file = get_post_meta($post_id, '_ljmc_page_template', true);
      
      if($template_file == 'page-contacts.php'){ // edit the template name
        remove_post_type_support('page', 'editor');
      }
      endif;
}

/** REMOVE CONTACTS PAGE **/
function custom_menu_page_removing() {
    // remove_menu_page( 'ljmccf7' );
    // remove_submenu_page( 'nav-menus.php', 'widgets.php' );
    // remove_menu_page( 'plugins.php' );
    // remove_menu_page( 'edit.php?post_type=acf-field-group' );
    // remove_menu_page( 'ljmcseo_dashboard' );
    // remove_menu_page( 'ajaxy-page' );
    // remove_menu_page( 'translations/menu/languages.php' );
}

add_action( 'admin_menu', 'custom_menu_page_removing' );

/*** EXCERPT ***/
function custom_excerpt_length( $length ) {
  return 14;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
  return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function ljmc_get_the_excerpt($post_id) {
  global $post;  
  $save_post = $post;
  $post = get_post($post_id);
  setup_postdata( $post ); // hello
  $output = get_the_excerpt();
  $post = $save_post;
  return $output;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

?>