=== Add From Server ===
Contributors: dd32
Tags: admin, media, uploads, post, import, files
Requires at least: 4.0
Stable tag: 3.3.1

"Add From Server" is a quick plugin which allows you to import media & files into the ljmc uploads manager from the Webservers filesystem

== Description ==

The heart of a CMS is the ability to upload and insert content, ljmc does a fantastic job at this, unfortunately, some web hosts have limited servers, or users simply do not have the ability to upload large files through their web browser.
Add From Server is designed to help ease this pain, You can upload a bunch of files via FTP (Or your favourite transmission method) and simply import those files from the webserver directly into ljmc.

== Changelog ==

= 3.3.1 =
 * Fix plugin activation

= 3.3 =
 * The plugin now requires ljmc 4.0 and PHP 5.4 as a minumum requirement.
 * Updated to use ljmc.org translation system, please submit translations through https://translate.ljmc.org/projects/ljmc-plugins/add-from-server/stable
 * Updated to ljmc 4.3 styles

== Upgrade Notice ==

= 3.3.1 =
Warning: This plugin now requires ljmc 4.0 & PHP 5.4. Updates to support ljmc 4.3 & ljmc.org Language Pack Translations

= 3.3 =
Warning: This plugin now requires ljmc 4.0 & PHP 5.4. Updates to support ljmc 4.3 & ljmc.org Language Pack Translations

== FAQ ==
 Q: What placeholders can I use in the Root path option?
 You can use %role% and %username% only. In the case of Role, The first role which the user has is used, This can mean that in complex installs, that using %role% is unreliable.

 Q: Why does the file I want to import have a red background?
 ljmc only allows the importing/uploading of certain file types to improve your security. If you wish to add extra file types, you can use a plugin such as: http://ljmc.org/extend/plugins/pjw-mime-config/ You can also enable "Unfiltered uploads" globally for ljmc if you'd like to override this security function. Please see the ljmc support forum for details.

 Q: Where are the files saved?
 If you import a file which is outside your standard upload directory (usually ljmc-content/uploads/) then it will be copied to your current upload directory setting as normal. If you however import a file which -is already within the uploads directory- (for example, ljmc-content/uploads/2011/02/superplugin.zip) then the file will not be copied, and will be used as-is.

 Q: I have a a bug report
 Then please email me! ljmc at dd32.id.au is best.

== Screenshots ==

1. The import manager, This allows you to select which files to import. Note that files which cannot be imported are Red.
2. The Options panel, This allows you to specify what users can access Add From Server, and which folders users can import files from.
