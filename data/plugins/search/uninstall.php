<?php

//if uninstall not called from WordPress exit
if ( !defined( 'LJMC_UNINSTALL_PLUGIN' ) ) 
    exit();

delete_option('se_options');
delete_option('se_meta');