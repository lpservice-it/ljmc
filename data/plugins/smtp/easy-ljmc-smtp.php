<?php
/*
Plugin Name: SMTP
Version: 1.2.0
*/

/**
* Add menu and submenu.
* @return void
*/

if ( ! function_exists( 'sljmcsmtp_admin_default_setup' ) ) {
	function sljmcsmtp_admin_default_setup() {		
		//add_submenu_page( 'options-general.php', __( 'Easy LJMC SMTP', 'ljmc_smtp' ), __( 'Easy LJMC SMTP', 'ljmc_smtp' ), $capabilities, 'sljmcsmtp_settings', 'sljmcsmtp_settings' );
                add_options_page(__('SMTP', 'ljmc_smtp'), __('SMTP', 'ljmc_smtp'), 'manage_options', 'sljmcsmtp_settings', 'sljmcsmtp_settings');
	}
}

/**
 * Plugin functions for init
 * @return void
 */
if ( ! function_exists ( 'sljmcsmtp_admin_init' ) ) {
	function sljmcsmtp_admin_init() {
		/* Internationalization, first(!) */
		load_plugin_textdomain( 'ljmc_smtp', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

		if ( isset( $_REQUEST['page'] ) && 'sljmcsmtp_settings' == $_REQUEST['page'] ) {
			/* register plugin settings */
			sljmcsmtp_register_settings();
		}
	}
}

/**
 * Register settings function
 * @return void
 */
if ( ! function_exists( 'sljmcsmtp_register_settings' ) ) {
	function sljmcsmtp_register_settings() {
		$sljmcsmtp_options_default = array(
			'from_email_field' 		=> '',
			'from_name_field'   		=> '',
			'smtp_settings'     		=> array( 
				'host'               	=> 'smtp.example.com',
				'type_encryption'	=> 'none',
				'port'              	=> 25,
				'autentication'		=> 'yes',
				'username'		=> 'yourusername',
				'password'          	=> 'yourpassword'
			)
		);

		/* install the default plugin options */
                if ( ! get_option( 'sljmcsmtp_options' ) ){
                    add_option( 'sljmcsmtp_options', $sljmcsmtp_options_default, '', 'yes' );
                }
	}
}


/**
 * Add action links on plugin page in to Plugin Name block
 * @param $links array() action links
 * @param $file  string  relative path to pugin "easy-ljmc-smtp/easy-ljmc-smtp.php"
 * @return $links array() action links
 */
if ( ! function_exists ( 'sljmcsmtp_plugin_action_links' ) ) {
	function sljmcsmtp_plugin_action_links( $links, $file ) {
		/* Static so we don't call plugin_basename on every plugin row. */
		static $this_plugin;
		if ( ! $this_plugin ) {
			$this_plugin = plugin_basename( __FILE__ );
		}
		if ( $file == $this_plugin ) {
			$settings_link = '<a href="options-general.php?page=sljmcsmtp_settings">' . __( 'Settings', 'ljmc_smtp' ) . '</a>';
			array_unshift( $links, $settings_link );
		}
		return $links;
	}
}

/**
 * Add action links on plugin page in to Plugin Description block
 * @param $links array() action links
 * @param $file  string  relative path to pugin "easy-ljmc-smtp/easy-ljmc-smtp.php"
 * @return $links array() action links
 */
if ( ! function_exists ( 'sljmcsmtp_register_plugin_links' ) ) {
	function sljmcsmtp_register_plugin_links( $links, $file ) {
		$base = plugin_basename( __FILE__ );
		if ( $file == $base ) {
			$links[] = '<a href="options-general.php?page=sljmcsmtp_settings">' . __( 'Settings', 'ljmc_smtp' ) . '</a>';
		}
		return $links;
	}
}


/**
 * Function to add plugin scripts
 * @return void
 */
if ( ! function_exists ( 'sljmcsmtp_admin_head' ) ) {
	function sljmcsmtp_admin_head() {
		ljmc_enqueue_style( 'sljmcsmtp_stylesheet', plugins_url( 'css/style.css', __FILE__ ) );

		if ( isset( $_REQUEST['page'] ) && 'sljmcsmtp_settings' == $_REQUEST['page'] ) {
			ljmc_enqueue_script( 'sljmcsmtp_script', plugins_url( 'js/script.js', __FILE__ ), array( 'jquery' ) );
		}
	}
}

/**
 * Function to add smtp options in the phpmailer_init
 * @return void
 */
if ( ! function_exists ( 'sljmcsmtp_init_smtp' ) ) {
	function sljmcsmtp_init_smtp( $phpmailer ) {              
		$sljmcsmtp_options = get_option( 'sljmcsmtp_options' );
		/* Set the mailer type as per config above, this overrides the already called isMail method */
		$phpmailer->IsSMTP();
		$from_email = $sljmcsmtp_options['from_email_field'];
                $phpmailer->From = $from_email;
                $from_name  = $sljmcsmtp_options['from_name_field'];
                $phpmailer->FromName = $from_name;
                $phpmailer->SetFrom($phpmailer->From, $phpmailer->FromName);
		/* Set the SMTPSecure value */
		if ( $sljmcsmtp_options['smtp_settings']['type_encryption'] !== 'none' ) {
			$phpmailer->SMTPSecure = $sljmcsmtp_options['smtp_settings']['type_encryption'];
		}
		
		/* Set the other options */
		$phpmailer->Host = $sljmcsmtp_options['smtp_settings']['host'];
		$phpmailer->Port = $sljmcsmtp_options['smtp_settings']['port']; 

		/* If we're using smtp auth, set the username & password */
		if( 'yes' == $sljmcsmtp_options['smtp_settings']['autentication'] ){
			$phpmailer->SMTPAuth = true;
			$phpmailer->Username = $sljmcsmtp_options['smtp_settings']['username'];
			$phpmailer->Password = sljmcsmtp_get_password();
		}
	}
}

/**
 * View function the settings to send messages.
 * @return void
 */
if ( ! function_exists( 'sljmcsmtp_settings' ) ) {
	function sljmcsmtp_settings() {
		$display_add_options = $message = $error = $result = '';

		$sljmcsmtp_options = get_option( 'sljmcsmtp_options' );
                
		if ( isset( $_POST['sljmcsmtp_form_submit'] ) && check_admin_referer( plugin_basename( __FILE__ ), 'sljmcsmtp_nonce_name' ) ) {	
			/* Update settings */
			$sljmcsmtp_options['from_name_field'] = isset( $_POST['sljmcsmtp_from_name'] ) ? sanitize_text_field(ljmc_unslash($_POST['sljmcsmtp_from_name'])) : '';
			if( isset( $_POST['sljmcsmtp_from_email'] ) ){
				if( is_email( $_POST['sljmcsmtp_from_email'] ) ){
					$sljmcsmtp_options['from_email_field'] = $_POST['sljmcsmtp_from_email'];
				}
				else{
					$error .= " " . __( "Please enter a valid email address in the 'FROM' field.", 'ljmc_smtp' );
				}
			}
					
			$sljmcsmtp_options['smtp_settings']['host']     				= sanitize_text_field($_POST['sljmcsmtp_smtp_host']);
			$sljmcsmtp_options['smtp_settings']['type_encryption'] = ( isset( $_POST['sljmcsmtp_smtp_type_encryption'] ) ) ? $_POST['sljmcsmtp_smtp_type_encryption'] : 'none' ;
			$sljmcsmtp_options['smtp_settings']['autentication']   = ( isset( $_POST['sljmcsmtp_smtp_autentication'] ) ) ? $_POST['sljmcsmtp_smtp_autentication'] : 'yes' ;
			$sljmcsmtp_options['smtp_settings']['username']  			= sanitize_text_field($_POST['sljmcsmtp_smtp_username']);
                        $smtp_password = trim($_POST['sljmcsmtp_smtp_password']);
			$sljmcsmtp_options['smtp_settings']['password'] 				= base64_encode($smtp_password);

			/* Check value from "SMTP port" option */
			if ( isset( $_POST['sljmcsmtp_smtp_port'] ) ) {
				if ( empty( $_POST['sljmcsmtp_smtp_port'] ) || 1 > intval( $_POST['sljmcsmtp_smtp_port'] ) || ( ! preg_match( '/^\d+$/', $_POST['sljmcsmtp_smtp_port'] ) ) ) {
					$sljmcsmtp_options['smtp_settings']['port'] = '25';
					$error .= " " . __( "Please enter a valid port in the 'SMTP Port' field.", 'ljmc_smtp' );
				} else {
					$sljmcsmtp_options['smtp_settings']['port'] = $_POST['sljmcsmtp_smtp_port'];
				}
			}

			/* Update settings in the database */
			if ( empty( $error ) ) {
				update_option( 'sljmcsmtp_options', $sljmcsmtp_options );
				$message .= __( "Settings saved.", 'ljmc_smtp' );	
			}
			else{
				$error .= " " . __( "Settings are not saved.", 'ljmc_smtp' );
			}
		} 
		
		/* Send test letter */
		if ( isset( $_POST['sljmcsmtp_test_submit'] ) && check_admin_referer( plugin_basename( __FILE__ ), 'sljmcsmtp_nonce_name' ) ) {	
			if( isset( $_POST['sljmcsmtp_to'] ) ){
				if( is_email( $_POST['sljmcsmtp_to'] ) ){
					$sljmcsmtp_to =$_POST['sljmcsmtp_to'];
				}
				else{
					$error .= " " . __( "Please enter a valid email address in the 'FROM' field.", 'ljmc_smtp' );
				}
			}
			$sljmcsmtp_subject = isset( $_POST['sljmcsmtp_subject'] ) ? $_POST['sljmcsmtp_subject'] : '';
			$sljmcsmtp_message = isset( $_POST['sljmcsmtp_message'] ) ? $_POST['sljmcsmtp_message'] : '';
			if( ! empty( $sljmcsmtp_to ) )
				$result = sljmcsmtp_test_mail( $sljmcsmtp_to, $sljmcsmtp_subject, $sljmcsmtp_message );
		} ?>
		<div class="sljmcsmtp-mail wrap" id="sljmcsmtp-mail">
			<div id="icon-options-general" class="icon32 icon32-bws"></div>
			<h2><?php _e( "Easy LJMC SMTP Settings", 'ljmc_smtp' ); ?></h2>
                        <div class="update-nag">Please visit the <a target="_blank" href="https://ljmc-ecommerce.net/easy-ljmc-smtp-send-emails-from-your-ljmc-site-using-a-smtp-server-2197">Easy LJMC SMTP</a> documentation page for usage instructions.</div>
			<div class="updated fade" <?php if( empty( $message ) ) echo "style=\"display:none\""; ?>>
				<p><strong><?php echo $message; ?></strong></p>
			</div>
			<div class="error" <?php if ( empty( $error ) ) echo "style=\"display:none\""; ?>>
				<p><strong><?php echo $error; ?></strong></p>
			</div>
			<div id="sljmcsmtp-settings-notice" class="updated fade" style="display:none">
				<p><strong><?php _e( "Notice:", 'ljmc_smtp' ); ?></strong> <?php _e( "The plugin's settings have been changed. In order to save them please don't forget to click the 'Save Changes' button.", 'ljmc_smtp' ); ?></p>
			</div>
			<h3><?php _e( 'General Settings', 'ljmc_smtp' ); ?></h3>
			<form id="sljmcsmtp_settings_form" method="post" action="">					
				<table class="form-table">
					<tr valign="top">
						<th scope="row"><?php _e( "From Email Address", 'ljmc_smtp' ); ?></th>
						<td>
							<input type="text" name="sljmcsmtp_from_email" value="<?php echo esc_attr( $sljmcsmtp_options['from_email_field'] ); ?>"/><br />
							<span class="sljmcsmtp_info"><?php _e( "This email address will be used in the 'From' field.", 'ljmc_smtp' ); ?></span>
					</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( "From Name", 'ljmc_smtp' ); ?></th>
						<td>
							<input type="text" name="sljmcsmtp_from_name" value="<?php echo esc_attr($sljmcsmtp_options['from_name_field']); ?>"/><br />
							<span  class="sljmcsmtp_info"><?php _e( "This text will be used in the 'FROM' field", 'ljmc_smtp' ); ?></span>
						</td>
					</tr>			
					<tr class="ad_opt sljmcsmtp_smtp_options">
						<th><?php _e( 'SMTP Host', 'ljmc_smtp' ); ?></th>
						<td>
							<input type='text' name='sljmcsmtp_smtp_host' value='<?php echo esc_attr($sljmcsmtp_options['smtp_settings']['host']); ?>' /><br />
							<span class="sljmcsmtp_info"><?php _e( "Your mail server", 'ljmc_smtp' ); ?></span>
						</td>
					</tr>
					<tr class="ad_opt sljmcsmtp_smtp_options">
						<th><?php _e( 'Type of Encription', 'ljmc_smtp' ); ?></th>
						<td>
							<label for="sljmcsmtp_smtp_type_encryption_1"><input type="radio" id="sljmcsmtp_smtp_type_encryption_1" name="sljmcsmtp_smtp_type_encryption" value='none' <?php if( 'none' == $sljmcsmtp_options['smtp_settings']['type_encryption'] ) echo 'checked="checked"'; ?> /> <?php _e( 'None', 'ljmc_smtp' ); ?></label>
							<label for="sljmcsmtp_smtp_type_encryption_2"><input type="radio" id="sljmcsmtp_smtp_type_encryption_2" name="sljmcsmtp_smtp_type_encryption" value='ssl' <?php if( 'ssl' == $sljmcsmtp_options['smtp_settings']['type_encryption'] ) echo 'checked="checked"'; ?> /> <?php _e( 'SSL', 'ljmc_smtp' ); ?></label>
							<label for="sljmcsmtp_smtp_type_encryption_3"><input type="radio" id="sljmcsmtp_smtp_type_encryption_3" name="sljmcsmtp_smtp_type_encryption" value='tls' <?php if( 'tls' == $sljmcsmtp_options['smtp_settings']['type_encryption'] ) echo 'checked="checked"'; ?> /> <?php _e( 'TLS', 'ljmc_smtp' ); ?></label><br />
							<span class="sljmcsmtp_info"><?php _e( "For most servers SSL is the recommended option", 'ljmc_smtp' ); ?></span>
						</td>
					</tr>
					<tr class="ad_opt sljmcsmtp_smtp_options">
						<th><?php _e( 'SMTP Port', 'ljmc_smtp' ); ?></th>
						<td>
							<input type='text' name='sljmcsmtp_smtp_port' value='<?php echo esc_attr($sljmcsmtp_options['smtp_settings']['port']); ?>' /><br />
							<span class="sljmcsmtp_info"><?php _e( "The port to your mail server", 'ljmc_smtp' ); ?></span>
						</td>
					</tr>
					<tr class="ad_opt sljmcsmtp_smtp_options">
						<th><?php _e( 'SMTP Authentication', 'ljmc_smtp' ); ?></th>
						<td>
							<label for="sljmcsmtp_smtp_autentication"><input type="radio" id="sljmcsmtp_smtp_autentication" name="sljmcsmtp_smtp_autentication" value='no' <?php if( 'no' == $sljmcsmtp_options['smtp_settings']['autentication'] ) echo 'checked="checked"'; ?> /> <?php _e( 'No', 'ljmc_smtp' ); ?></label>
							<label for="sljmcsmtp_smtp_autentication"><input type="radio" id="sljmcsmtp_smtp_autentication" name="sljmcsmtp_smtp_autentication" value='yes' <?php if( 'yes' == $sljmcsmtp_options['smtp_settings']['autentication'] ) echo 'checked="checked"'; ?> /> <?php _e( 'Yes', 'ljmc_smtp' ); ?></label><br />
							<span class="sljmcsmtp_info"><?php _e( "This options should always be checked 'Yes'", 'ljmc_smtp' ); ?></span>
						</td>
					</tr>
					<tr class="ad_opt sljmcsmtp_smtp_options">
						<th><?php _e( 'SMTP username', 'ljmc_smtp' ); ?></th>
						<td>
							<input type='text' name='sljmcsmtp_smtp_username' value='<?php echo esc_attr($sljmcsmtp_options['smtp_settings']['username']); ?>' /><br />
							<span class="sljmcsmtp_info"><?php _e( "The username to login to your mail server", 'ljmc_smtp' ); ?></span>
						</td>
					</tr>
					<tr class="ad_opt sljmcsmtp_smtp_options">
						<th><?php _e( 'SMTP Password', 'ljmc_smtp' ); ?></th>
						<td>
							<input type='password' name='sljmcsmtp_smtp_password' value='<?php echo sljmcsmtp_get_password(); ?>' /><br />
							<span class="sljmcsmtp_info"><?php _e( "The password to login to your mail server", 'ljmc_smtp' ); ?></span>
						</td>
					</tr>
				</table>
				<p class="submit">
					<input type="submit" id="settings-form-submit" class="button-primary" value="<?php _e( 'Save Changes', 'ljmc_smtp' ) ?>" />
					<input type="hidden" name="sljmcsmtp_form_submit" value="submit" />
					<?php ljmc_nonce_field( plugin_basename( __FILE__ ), 'sljmcsmtp_nonce_name' ); ?>
				</p>				
			</form>
			
			<div class="updated fade" <?php if( empty( $result ) ) echo "style=\"display:none\""; ?>>
				<p><strong><?php echo $result; ?></strong></p>
			</div>
			<h3><?php _e( 'Testing And Debugging Settings', 'ljmc_smtp' ); ?></h3>
			<form id="sljmcsmtp_settings_form" method="post" action="">					
				<table class="form-table">
					<tr valign="top">
						<th scope="row"><?php _e( "To", 'ljmc_smtp' ); ?>:</th>
						<td>
							<input type="text" name="sljmcsmtp_to" value=""/><br />
							<span class="sljmcsmtp_info"><?php _e( "Enter the email address to recipient", 'ljmc_smtp' ); ?></span>
					</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( "Subject", 'ljmc_smtp' ); ?>:</th>
						<td>
							<input type="text" name="sljmcsmtp_subject" value=""/><br />
							<span  class="sljmcsmtp_info"><?php _e( "Enter a subject for your message", 'ljmc_smtp' ); ?></span>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( "Message", 'ljmc_smtp' ); ?>:</th>
						<td>
							<textarea name="sljmcsmtp_message" id="sljmcsmtp_message" rows="5"></textarea><br />
							<span  class="sljmcsmtp_info"><?php _e( "Write your message", 'ljmc_smtp' ); ?></span>
						</td>
					</tr>				
				</table>
				<p class="submit">
					<input type="submit" id="settings-form-submit" class="button-primary" value="<?php _e( 'Send Test Email', 'ljmc_smtp' ) ?>" />
					<input type="hidden" name="sljmcsmtp_test_submit" value="submit" />
					<?php ljmc_nonce_field( plugin_basename( __FILE__ ), 'sljmcsmtp_nonce_name' ); ?>
				</p>				
			</form>
		</div><!--  #sljmcsmtp-mail .sljmcsmtp-mail -->
	<?php }
}
	
/**
 * Function to test mail sending
 * @return text or errors
 */
if ( ! function_exists( 'sljmcsmtp_test_mail' ) ) {
	function sljmcsmtp_test_mail( $to_email, $subject, $message ) {
		$errors = '';

		$sljmcsmtp_options = get_option( 'sljmcsmtp_options' );

		require_once( ABSPATH . LJMCINC . '/class-phpmailer.php' );
		$mail = new PHPMailer();
                
                $charset = get_bloginfo( 'charset' );
		$mail->CharSet = $charset;
                
		$from_name  = $sljmcsmtp_options['from_name_field'];
		$from_email = $sljmcsmtp_options['from_email_field']; 
		
		$mail->IsSMTP();
		
		/* If using smtp auth, set the username & password */
		if( 'yes' == $sljmcsmtp_options['smtp_settings']['autentication'] ){
			$mail->SMTPAuth = true;
			$mail->Username = $sljmcsmtp_options['smtp_settings']['username'];
			$mail->Password = sljmcsmtp_get_password();
		}
		
		/* Set the SMTPSecure value, if set to none, leave this blank */
		if ( $sljmcsmtp_options['smtp_settings']['type_encryption'] !== 'none' ) {
			$mail->SMTPSecure = $sljmcsmtp_options['smtp_settings']['type_encryption'];
		}
		
		/* Set the other options */
		$mail->Host = $sljmcsmtp_options['smtp_settings']['host'];
		$mail->Port = $sljmcsmtp_options['smtp_settings']['port']; 
		$mail->SetFrom( $from_email, $from_name );
		$mail->isHTML( true );
		$mail->Subject = $subject;
		$mail->MsgHTML( $message );
		$mail->AddAddress( $to_email );
		$mail->SMTPDebug = 0;

		/* Send mail and return result */
		if ( ! $mail->Send() )
			$errors = $mail->ErrorInfo;
		
		$mail->ClearAddresses();
		$mail->ClearAllRecipients();
			
		if ( ! empty( $errors ) ) {
			var_dump($errors);
			return $errors;
		}
		else{
			return 'Test mail was sent';
		}
	}
}

/**
 * Performed at uninstal.
 * @return void
 */
if ( ! function_exists( 'sljmcsmtp_send_uninstall' ) ) {
	function sljmcsmtp_send_uninstall() {
		/* delete plugin options */
		delete_site_option( 'sljmcsmtp_options' );
		delete_option( 'sljmcsmtp_options' );
	}
}

if ( ! function_exists( 'sljmcsmtp_get_password' ) ) {
	function sljmcsmtp_get_password() {
            $sljmcsmtp_options = get_option( 'sljmcsmtp_options' );
            $temp_password = $sljmcsmtp_options['smtp_settings']['password'];
            $password = "";
            $decoded_pass = base64_decode($temp_password);
            /* no additional checks for servers that aren't configured with mbstring enabled */
            if ( ! function_exists( 'mb_detect_encoding' ) ){
                return $decoded_pass;
            }
            /* end of mbstring check */
            if (base64_encode($decoded_pass) === $temp_password) {  //it might be encoded
                if(false === mb_detect_encoding($decoded_pass)){  //could not find character encoding.
                    $password = $temp_password;
                }
                else{
                    $password = base64_decode($temp_password); 
                }               
            }
            else{ //not encoded
                $password = $temp_password;
            }
            return $password;
	}
}


/**
 * Add all hooks
 */

add_filter( 'plugin_action_links', 'sljmcsmtp_plugin_action_links', 10, 2 );
add_filter( 'plugin_row_meta', 'sljmcsmtp_register_plugin_links', 10, 2 );

add_action( 'phpmailer_init','sljmcsmtp_init_smtp');

add_action( 'admin_menu', 'sljmcsmtp_admin_default_setup' );

add_action( 'admin_init', 'sljmcsmtp_admin_init' );
add_action( 'admin_enqueue_scripts', 'sljmcsmtp_admin_head' );

register_uninstall_hook( plugin_basename( __FILE__ ), 'sljmcsmtp_send_uninstall' );