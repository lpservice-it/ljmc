<?php
/*
Plugin Name: LJMC Statistics
Plugin URI: http://statistics.com/
Description: Complete statistics for your LJMC site.
Version: 9.6.5
Author: Mostafa Soufi & Greg Ross
Author URI: http://statistics.com/
Text Domain: ljmc_statistics
Domain Path: /languages/
License: GPL2
*/

	// These defines are used later for various reasons.
	define('LJMC_STATISTICS_VERSION', '9.6.5');
	define('LJMC_STATISTICS_MANUAL', 'manual/LJMC Statistics Admin Manual.');
	define('LJMC_STATISTICS_REQUIRED_PHP_VERSION', '5.3.0');
	define('LJMC_STATISTICS_REQUIRED_GEOIP_PHP_VERSION', LJMC_STATISTICS_REQUIRED_PHP_VERSION);
	define('LJMCS_EXPORT_FILE_NAME', 'statistics');

	// Load the translation code.
	function ljmc_statistics_language() {
		GLOBAL $LJMC_Statistics;
		
		// Users can override loading the default language code, check to see if they have.
		$override = false;
		
		if( is_object( $LJMC_Statistics ) ) {
			if( $LJMC_Statistics->get_option('override_language', false) ) {
				$override = true;
			}
		}
		
		// If not, go ahead and load the translations.
		if( !$override ) {
			load_plugin_textdomain('ljmc_statistics', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/');
			__('LJMC Statistics', 'ljmc_statistics');
			__('Complete statistics for your LJMC site.', 'ljmc_statistics');
		}
	}

	// Add translation action.  We have to load the translation code before the init otherwise the widgets won't get translated properly.
	add_action('plugins_loaded', 'ljmc_statistics_language');
	
	// Load the init code.
	function ljmc_statistics_init() {
		GLOBAL $LJMC_Statistics;
		
		// Check to see if we're exporting data, if so, do so now. 
		// Note this will set the headers to download the export file and then stop running LJMC.
		if( array_key_exists( 'ljmcs_export', $_POST ) ) {
			include_once dirname( __FILE__ ) . '/includes/functions/export.php';
			ljmc_statistics_export_data();
		}

		// Check to see if we're downloading the manual, if so, do so now. 
		// Note this will set the headers to download the manual and then stop running LJMC.
		if( array_key_exists( 'ljmcs_download_manual', $_GET ) ) {
			include_once dirname( __FILE__ ) . '/includes/functions/manual.php';
			ljmc_statistics_download_manual();
		}
	}

	// Add init actions.  For the main init we're going to set our priority to 9 to execute before most plugins so we can export data before and set the headers without 
	// worrying about bugs in other plugins that output text and don't allow us to set the headers.
	add_action('init', 'ljmc_statistics_init', 9);
	
	// This adds a row after LJMC Statistics in the plugin page IF an incompatible version of PHP is running.
	function ljmc_statistics_php_after_plugin_row() {
		echo '<tr><th scope="row" class="check-column"></th><td class="plugin-title" colspan="10"><span style="padding: 3px; color: white; background-color: red; font-weight: bold">&nbsp;&nbsp;' . __('ERROR: LJMC Statistics has detected an unsupported version of PHP, LJMC Statistics will not function without PHP Version ', 'ljmc_statistics') . LJMC_STATISTICS_REQUIRED_PHP_VERSION . __(' or higher!', 'ljmc_statistics') . '  ' . __('Your current PHP version is','ljmc_statistics') . ' ' . phpversion() . '.&nbsp;&nbsp;</span></td></tr>';
	}
	
	// Check the PHP version, if we don't meet the minimum version to run LJMC Statistics return so we don't cause a critical error.
	if( !version_compare( phpversion(), LJMC_STATISTICS_REQUIRED_PHP_VERSION, ">=" ) ) { 
		add_action('after_plugin_row_' . plugin_basename( __FILE__ ), 'ljmc_statistics_php_after_plugin_row', 10, 2);
		return; 
	} 

	// If we've been flagged to remove all of the data, then do so now.
	if( get_option( 'ljmc_statistics_removal' ) == 'true' ) {
		include_once( dirname( __FILE__ ) . '/ljmcs-uninstall.php' );
	}

	// This adds a row after LJMC Statistics in the plugin page IF we've been removed via the settings page.
	function ljmc_statistics_removal_after_plugin_row() {
		echo '<tr><th scope="row" class="check-column"></th><td class="plugin-title" colspan="*"><span style="padding: 3px; color: white; background-color: red; font-weight: bold">&nbsp;&nbsp;' . __('LJMC Statistics has been removed, please disable and delete it.', 'ljmc_statistics') . '&nbsp;&nbsp;</span></td></tr>';
	}
	
	// If we've been removed, return without doing anything else.
	if( get_option( 'ljmc_statistics_removal' ) == 'done' ) {
		add_action('after_plugin_row_' . plugin_basename( __FILE__ ), 'ljmc_statistics_removal_after_plugin_row', 10, 2);
		return;
	}
	
	// Load the user agent parsing code first, the LJMC_Statistics class depends on it.  Then load the LJMC_Statistics class.
	include_once dirname( __FILE__ ) . '/vendor/donatj/phpuseragentparser/Source/UserAgentParser.php';
	include_once dirname( __FILE__ ) . '/includes/classes/statistics.class.php';
	
	// This is our global LJMC_Statitsics class that is used throughout the plugin.
	$LJMC_Statistics = new LJMC_Statistics();

	// Check to see if we're installed and are the current version.
	$LJMCS_Installed = get_option('ljmc_statistics_plugin_version');
	if( $LJMCS_Installed != LJMC_STATISTICS_VERSION ) {	
		include_once( dirname( __FILE__ ) . '/ljmcs-install.php' );
	}

	// Load the update functions for GeoIP and browscap.ini (done in a separate file to avoid a parse error in PHP 5.2 or below)
	include_once dirname( __FILE__ ) . '/ljmcs-updates.php';
	
	// Load the rest of the required files for our global functions, online user tracking and hit tracking.
	include_once dirname( __FILE__ ) . '/includes/functions/functions.php';
	include_once dirname( __FILE__ ) . '/includes/classes/hits.class.php';

	// If GeoIP is enabled and supported, extend the hits class to record the GeoIP information.
	if( $LJMC_Statistics->get_option('geoip') && ljmc_statistics_geoip_supported() ) {
		include_once dirname( __FILE__ ) . '/includes/classes/hits.geoip.class.php';
	}
	
	// Finally load the widget, dashboard, shortcode and scheduled events.
	include_once dirname( __FILE__ ) . '/widget.php';
	include_once dirname( __FILE__ ) . '/dashboard.php';
	include_once dirname( __FILE__ ) . '/editor.php';
	include_once dirname( __FILE__ ) . '/shortcode.php';
	include_once dirname( __FILE__ ) . '/schedule.php';
	include_once dirname( __FILE__ ) . '/ajax.php';
	
	// This function outputs error messages in the admin interface if the primary components of LJMC Statistics are enabled.
	function ljmc_statistics_not_enable() {
		GLOBAL $LJMC_Statistics;

		// If the user had told us to be quite, do so.
		if( !$LJMC_Statistics->get_option('hide_notices') ) {

			// Check to make sure the current user can manage LJMC Statistics, if not there's no point displaying the warnings.
			$manage_cap = ljmc_statistics_validate_capability( $LJMC_Statistics->get_option('manage_capability', 'manage_options') );
			if( ! current_user_can( $manage_cap ) ) { return; }

			$get_bloginfo_url = get_admin_url() . "admin.php?page=statistics/settings";
			
			$itemstoenable = array();
			if( !$LJMC_Statistics->get_option('useronline') ) { $itemstoenable[] = __('online user tracking', 'ljmc_statistics'); }
			if( !$LJMC_Statistics->get_option('visits') ) { $itemstoenable[] = __('hit tracking', 'ljmc_statistics'); }
			if( !$LJMC_Statistics->get_option('visitors') ) { $itemstoenable[] = __('visitor tracking', 'ljmc_statistics'); }
			if( !$LJMC_Statistics->get_option('geoip') && ljmc_statistics_geoip_supported()) { $itemstoenable[] = __('geoip collection', 'ljmc_statistics'); }

			if( count( $itemstoenable ) > 0 )
				echo '<div class="update-nag">'.sprintf(__('The following features are disabled, please go to %s and enable them: %s', 'ljmc_statistics'), '<a href="' . $get_bloginfo_url . '">' . __( 'settings page', 'ljmc_statistics') . '</a>', implode(__(',', 'ljmc_statistics'), $itemstoenable)).'</div>';

			$get_bloginfo_url = get_admin_url() . "admin.php?page=statistics/optimization&tab=database";

			$dbupdatestodo = array();
			
			if(!$LJMC_Statistics->get_option('search_converted')) { $dbupdatestodo[] = __('search table', 'ljmc_statistics'); }

			// Check to see if there are any database changes the user hasn't done yet.
			$dbupdates = $LJMC_Statistics->get_option('pending_db_updates', false);

			// The database updates are stored in an array so loop thorugh it and output some notices.
			if( is_array( $dbupdates ) ) { 
				$dbstrings = array( 'date_ip_agent' => __('countries database index', 'ljmc_statistics'), 'unique_date' => __('visit database index', 'ljmc_statistics') );
			
				foreach( $dbupdates as $key => $update ) {
					if( $update == true ) {
						$dbupdatestodo[] = $dbstrings[$key];
					}
				}
	
			if( count( $dbupdatestodo ) > 0 ) 
				echo '<div class="update-nag">'.sprintf(__('Database updates are required, please go to %s and update the following: %s', 'ljmc_statistics'), '<a href="' . $get_bloginfo_url . '">' . __( 'optimization page', 'ljmc_statistics') . '</a>', implode(__(',', 'ljmc_statistics'), $dbupdatestodo)).'</div>';

			}
		}
	}

	// Display the admin notices if we should.
	if( isset( $pagenow ) && array_key_exists( 'page', $_GET ) ) {
		if( $pagenow == "admin.php" && substr( $_GET['page'], 0, 14) == 'statistics/') {
			add_action('admin_notices', 'ljmc_statistics_not_enable');
		}
	}

	// Add the honey trap code in the footer.
	add_action('ljmc_footer', 'ljmc_statistics_footer_action');
	
	function ljmc_statistics_footer_action() {
		GLOBAL $LJMC_Statistics;

		if( $LJMC_Statistics->get_option( 'use_honeypot' ) && $LJMC_Statistics->get_option( 'honeypot_postid' ) > 0 ) {
			$post_url = get_permalink( $LJMC_Statistics->get_option( 'honeypot_postid' ) );
			echo '<a href="' . $post_url . '" style="display: none;">&nbsp;</a>';
		}
	}

	// If we've been told to exclude the feeds from the statistics add a detection hook when LJMC generates the RSS feed.
	if( $LJMC_Statistics->get_option('exclude_feeds') ) {
		add_filter('the_title_rss', 'ljmc_statistics_check_feed_title' );
	}
	
	function ljmc_statistics_check_feed_title( $title ) {
		GLOBAL $LJMC_Statistics;
		
		$LJMC_Statistics->feed_detected();
		
		return $title;
	}
	
	// We can wait until the very end of the page to process the statistics, that way the page loads and displays
	// quickly.
	add_action('shutdown', 'ljmc_statistics_shutdown_action');
	
	function ljmc_statistics_shutdown_action() {
		GLOBAL $LJMC_Statistics;

		// If something has gone horribly wrong and $LJMC_Statistics isn't an object, bail out.  This seems to happen sometimes with LJMC Cron calls.
		if( !is_object( $LJMC_Statistics ) ) { return; }
		
		// Create a new hit class, if we're GeoIP enabled, use GeoIPHits().
		if( class_exists( 'GeoIPHits' ) ) { 
			$h = new GeoIPHits();
		} else {
			$h = new Hits();
		}
	
		// Call the online users tracking code.
		if( $LJMC_Statistics->get_option('useronline') )
			$h->Check_online();

		// Call the visitor tracking code.
		if( $LJMC_Statistics->get_option('visitors') )
			$h->Visitors();

		// Call the visit tracking code.
		if( $LJMC_Statistics->get_option('visits') )
			$h->Visits();

		// Call the page tracking code.
		if( $LJMC_Statistics->get_option('pages') )
			$h->Pages();

		// Check to see if the GeoIP database needs to be downloaded and do so if required.
		if( $LJMC_Statistics->get_option('update_geoip') )
			ljmc_statistics_download_geoip();
			
		// Check to see if the browscap database needs to be downloaded and do so if required.
		if( $LJMC_Statistics->get_option('update_browscap') )
			ljmc_statistics_download_browscap();
		
		// Check to see if the referrerspam database needs to be downloaded and do so if required.
		if( $LJMC_Statistics->get_option('update_referrerspam') )
			ljmc_statistics_download_referrerspam();
		
		if( $LJMC_Statistics->get_option('send_upgrade_email') ) {
			$LJMC_Statistics->update_option( 'send_upgrade_email', false );
			
			$blogname = get_bloginfo('name');
			$blogemail = get_bloginfo('admin_email');
			
			$headers[] = "From: $blogname <$blogemail>";
			$headers[] = "MIME-Version: 1.0";
			$headers[] = "Content-type: text/html; charset=utf-8";

			if( $LJMC_Statistics->get_option('email_list') == '' ) { $LJMC_Statistics->update_option( 'email_list', $blogemail ); }
			
			ljmc_mail( $LJMC_Statistics->get_option('email_list'), sprintf( __('LJMC Statistics %s installed on', 'ljmc_statistics'),  LJMC_STATISTICS_VERSION ) . ' ' . $blogname, "Installation/upgrade complete!", $headers );
		}

	}

	// Add a settings link to the plugin list.
	function ljmc_statistics_settings_links( $links, $file ) {
		GLOBAL $LJMC_Statistics;

		$manage_cap = ljmc_statistics_validate_capability( $LJMC_Statistics->get_option('manage_capability', 'manage_options') );
		
		if( current_user_can( $manage_cap ) ) {
			array_unshift( $links, '<a href="' . admin_url( 'admin.php?page=statistics/settings' ) . '">' . __( 'Settings', 'ljmc_statistics' ) . '</a>' );
		}
		
		return $links;
	}
	add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'ljmc_statistics_settings_links', 10, 2 );

	// Add a LJMC plugin page and rating links to the meta information to the plugin list.
	function ljmc_statistics_add_meta_links($links, $file) {
		if( $file == plugin_basename(__FILE__) ) {
			$plugin_url = 'http://ljmc.org/plugins/statistics/';
			
			$links[] = '<a href="'. $plugin_url .'" target="_blank" title="'. __('Click here to visit the plugin on LJMC.org', 'ljmc_statistics') .'">'. __('Visit LJMC.org page', 'ljmc_statistics') .'</a>';
			
			$rate_url = 'http://ljmc.org/support/view/plugin-reviews/statistics?rate=5#postform';
			$links[] = '<a href="'. $rate_url .'" target="_blank" title="'. __('Click here to rate and review this plugin on LJMC.org', 'ljmc_statistics') .'">'. __('Rate this plugin', 'ljmc_statistics') .'</a>';
		}
		
		return $links;
	}
	add_filter('plugin_row_meta', 'ljmc_statistics_add_meta_links', 10, 2);
	
	// Add a custom column to post/pages for hit statistics.
	function ljmc_statistics_add_column( $columns ) {
		$columns['statistics'] = __('Skatījumi', 'ljmc_statistics');
		
		return $columns;
	}

	// Render the custom column on the post/pages lists.
	function ljmc_statistics_render_column( $column_name, $post_id ) {
		if( $column_name == 'statistics' ) {
			echo "<a href='" . get_admin_url() . "admin.php?page=ljmcs_pages_menu&page-id={$post_id}'>" . ljmc_statistics_pages( 'total', "", $post_id ) . "</a>";
		}
	}
	
	// Call the add/render functions at the appropriate times.
	function ljmc_statistics_load_edit_init() {
		GLOBAL $LJMC_Statistics;
		
		$read_cap = ljmc_statistics_validate_capability( $LJMC_Statistics->get_option('read_capability', 'manage_options') );
		
		if( current_user_can( $read_cap ) && $LJMC_Statistics->get_option('pages') && !$LJMC_Statistics->get_option('disable_column') ) {
			$post_types = (array)get_post_types( array( 'show_ui' => true ), 'object' );
			
			foreach( $post_types as $type ) {
				add_action( 'manage_' . $type->name . '_posts_columns', 'ljmc_statistics_add_column', 10, 2 );
				add_action( 'manage_' . $type->name . '_posts_custom_column', 'ljmc_statistics_render_column', 10, 2 );
			}
		}
	}
	add_action( 'load-edit.php', 'ljmc_statistics_load_edit_init' );

	// Add the hit count to the publish widget in the post/pages editor.
	function ljmc_statistics_post_init() {
		global $post;
		
		$id = $post->ID;
	
		echo "<div class='misc-pub-section'>" . __( 'Skatījumi', 'ljmc_statistics') . ': <b>' . ljmc_statistics_pages( 'total', '', $id ) . '</b></div>';
	}
	if( $LJMC_Statistics->get_option('pages') && !$LJMC_Statistics->get_option('disable_column') ) {
		add_action( 'post_submitbox_misc_actions', 'ljmc_statistics_post_init' );
	}
	
	// This function will validate that a capability exists, if not it will default to returning the 'manage_options' capability.
	function ljmc_statistics_validate_capability( $capability ) {
	
		global $ljmc_roles;

		$role_list = $ljmc_roles->get_names();

		if( !is_object( $ljmc_roles ) || !is_array( $ljmc_roles->roles) ) { return 'manage_options'; }

		foreach( $ljmc_roles->roles as $role ) {
		
			$cap_list = $role['capabilities'];
			
			foreach( $cap_list as $key => $cap ) {
				if( $capability == $key ) { return $capability; }
			}
		}

		return 'manage_options';
	}
	
	// This function adds the primary menu to LJMC.
	function ljmc_statistics_menu() {
		GLOBAL $LJMC_Statistics;
		
		// Get the read/write capabilities required to view/manage the plugin as set by the user.
		$read_cap = ljmc_statistics_validate_capability( $LJMC_Statistics->get_option('read_capability', 'manage_options') );
		$manage_cap = ljmc_statistics_validate_capability( $LJMC_Statistics->get_option('manage_capability', 'manage_options') );
		
		// Add the top level menu.
		add_menu_page(__('Darbagalds', 'ljmc_statistics'), __('Darbagalds', 'ljmc_statistics'), $read_cap, __FILE__, 'ljmc_statistics_log', 'fa-desktop', 2);
		
		// Add the sub items.
		add_submenu_page(__FILE__, __('Pārskats', 'ljmc_statistics'), __('Pārskats', 'ljmc_statistics'), $read_cap, __FILE__, 'ljmc_statistics_log');
		if( $LJMC_Statistics->get_option('visitors') ) { add_submenu_page(__FILE__, __('Pārlūki', 'ljmc_statistics'), __('Pārlūki', 'ljmc_statistics'), $read_cap, 'ljmcs_browsers_menu', 'ljmc_statistics_log'); }
		if( $LJMC_Statistics->get_option('geoip') && $LJMC_Statistics->get_option('visitors') ) { add_submenu_page(__FILE__, __('Valstis', 'ljmc_statistics'), __('Valstis', 'ljmc_statistics'), $read_cap, 'ljmcs_countries_menu', 'ljmc_statistics_log'); }
		if( $LJMC_Statistics->get_option('record_exclusions') ) { add_submenu_page(__FILE__, __('Izņēmumi', 'ljmc_statistics'), __('Izņēmumi', 'ljmc_statistics'), $read_cap, 'ljmcs_exclusions_menu', 'ljmc_statistics_log'); }
		if( $LJMC_Statistics->get_option('visits') ) { add_submenu_page(__FILE__, __('Skatījumi', 'ljmc_statistics'), __('Skatījumi', 'ljmc_statistics'), $read_cap, 'ljmcs_hits_menu', 'ljmc_statistics_log'); }
		if( $LJMC_Statistics->get_option('useronline') ) { add_submenu_page(__FILE__, __('Tiešsaistē', 'ljmc_statistics'), __('Tiešsaistē', 'ljmc_statistics'), $read_cap, 'ljmcs_online_menu', 'ljmc_statistics_log'); }
		if( $LJMC_Statistics->get_option('pages') ) { add_submenu_page(__FILE__, __('Lapas', 'ljmc_statistics'), __('Lapas', 'ljmc_statistics'), $read_cap, 'ljmcs_pages_menu', 'ljmc_statistics_log'); }
		if( $LJMC_Statistics->get_option('visitors') ) { add_submenu_page(__FILE__, __('Atsauces', 'ljmc_statistics'), __('Atsauces', 'ljmc_statistics'), $read_cap, 'ljmcs_referrers_menu', 'ljmc_statistics_log'); }
		if( $LJMC_Statistics->get_option('visitors') ) { add_submenu_page(__FILE__, __('Meklējumi', 'ljmc_statistics'), __('Meklējumi', 'ljmc_statistics'), $read_cap, 'ljmcs_searches_menu', 'ljmc_statistics_log'); }
		if( $LJMC_Statistics->get_option('visitors') ) { add_submenu_page(__FILE__, __('Atslēgas vārdi', 'ljmc_statistics'), __('Atslēgas vārdi', 'ljmc_statistics'), $read_cap, 'ljmcs_words_menu', 'ljmc_statistics_log'); }
		if( $LJMC_Statistics->get_option('visitors') ) { add_submenu_page(__FILE__, __('Top apmeklētāji', 'ljmc_statistics'), __('Top apmeklētāji', 'ljmc_statistics'), $read_cap, 'ljmcs_top_visitors_menu', 'ljmc_statistics_log'); }
		if( $LJMC_Statistics->get_option('visitors') ) { add_submenu_page(__FILE__, __('Apmeklētāji', 'ljmc_statistics'), __('Apmeklētāji', 'ljmc_statistics'), $read_cap, 'ljmcs_visitors_menu', 'ljmc_statistics_log'); }

		add_submenu_page(__FILE__, '', '', $read_cap, 'ljmcs_break_menu', 'ljmc_statistics_log');

		add_submenu_page(__FILE__, __('Optimization', 'ljmc_statistics'), __('Optimization', 'ljmc_statistics'), $manage_cap, 'statistics/optimization', 'ljmc_statistics_optimization');
		add_submenu_page(__FILE__, __('Uzstādījumi', 'ljmc_statistics'), __('Uzstādījumi', 'ljmc_statistics'), $read_cap, 'statistics/settings', 'ljmc_statistics_settings');
		
		// Only add the manual entry if it hasn't been deleted.
		if( $LJMC_Statistics->get_option('delete_manual') != true ) {
			add_submenu_page(__FILE__, __('Manual', 'ljmc_statistics'), __('Manual', 'ljmc_statistics'), $manage_cap, 'ljmcs_manual_menu', 'ljmc_statistics_manual');
		}
	}
	add_action('admin_menu', 'ljmc_statistics_menu');

	// This function adds the primary menu to LJMC network.
	function ljmc_statistics_networkmenu() {
		GLOBAL $LJMC_Statistics;
		
		// Get the read/write capabilities required to view/manage the plugin as set by the user.
		$read_cap = ljmc_statistics_validate_capability( $LJMC_Statistics->get_option('read_capability', 'manage_options') );
		$manage_cap = ljmc_statistics_validate_capability( $LJMC_Statistics->get_option('manage_capability', 'manage_options') );
		
		// Add the top level menu.
		add_menu_page(__('Statistics', 'ljmc_statistics'), __('Statistics', 'ljmc_statistics'), $read_cap, __FILE__, 'ljmc_statistics_network_overview');
		
		// Add the sub items.
		add_submenu_page(__FILE__, __('Overview', 'ljmc_statistics'), __('Overview', 'ljmc_statistics'), $read_cap, __FILE__, 'ljmc_statistics_network_overview');
		
		$count = 0;
		foreach( ljmc_get_sites() as $blog ) {
			$details = get_blog_details( $blog['blog_id'] );
			add_submenu_page(__FILE__, $details->blogname, $details->blogname, $manage_cap, 'ljmc_statistics_blogid_' . $blog['blog_id'], 'ljmc_statistics_goto_network_blog');
			
			$count++;
			if( $count > 15 ) { break; }
		}
		
		// Only add the manual entry if it hasn't been deleted.
		if( $LJMC_Statistics->get_option('delete_manual') != true ) {
			add_submenu_page(__FILE__, '', '', $read_cap, 'ljmcs_break_menu', 'ljmc_statistics_log_overview');
			add_submenu_page(__FILE__, __('Manual', 'ljmc_statistics'), __('Manual', 'ljmc_statistics'), $manage_cap, 'ljmcs_manual_menu', 'ljmc_statistics_manual');
		}
	}

	if( is_multisite() ) { add_action('network_admin_menu', 'ljmc_statistics_networkmenu'); }
	
	function ljmc_statistics_network_overview() {
		
?>
	<div id="wrap">
		<br>
		
		<table class="widefat ljmc-list-table" style="width: auto;">
			<thead>
				<tr>
					<th style='text-align: left'><?php _e('Site', 'ljmc_statistics'); ?></th>
					<th style='text-align: left'><?php _e('Options', 'ljmc_statistics'); ?></th>
				</tr>
			</thead>
			
			<tbody>
<?php
		$i = 0;
		
		$options = array( 	__('Overview', 'ljmc_statistics') => 'statistics/statistics.php', 
							__('Browsers', 'ljmc_statistics') => 'ljmcs_browsers_menu', 
							__('Countries', 'ljmc_statistics') => 'ljmcs_countries_menu', 
							__('Exclusions', 'ljmc_statistics') => 'ljmcs_exclusions_menu', 
							__('Hits', 'ljmc_statistics') => 'ljmcs_hits_menu', 
							__('Online', 'ljmc_statistics') => 'ljmcs_online_menu', 
							__('Pages', 'ljmc_statistics') => 'ljmcs_pages_menu', 
							__('Referrers', 'ljmc_statistics') => 'ljmcs_referrers_menu', 
							__('Searches', 'ljmc_statistics') => 'ljmcs_searches_menu', 
							__('Search Words', 'ljmc_statistics') => 'ljmcs_words_menu', 
							__('Top Visitors Today', 'ljmc_statistics') => 'ljmcs_top_visitors_menu', 
							__('Visitors', 'ljmc_statistics') => 'ljmcs_visitors_menu', 
							__('Optimization', 'ljmc_statistics') => 'statistics/optimization', 
							__('Settings', 'ljmc_statistics') => 'statistics/settings'
						);
						
		foreach( ljmc_get_sites() as $blog ) {
			$details = get_blog_details( $blog['blog_id'] );
			$url = get_admin_url($blog['blog_id'], '/') . "admin.php?page=";;
			$alternate = "";
			if( $i % 2 == 0 ) { $alternate = ' class="alternate"'; }
?>

				<tr<?php echo $alternate; ?>>
					<td style='text-align: left'>
						<?php echo $details->blogname; ?>
					</td>
					<td style='text-align: left'>
<?php
				$options_len = count( $options );
				$j = 0;
				
				foreach( $options as $key => $value ) {
					echo '<a href="' . $url . $value . '">' . $key . '</a>';
					$j ++;
					if( $j < $options_len ) { echo ' - '; }
				}
?>
					</td>
				</tr>
<?php		
				$i++;
		}
?> 
			</tbody>
		</table>
	</div>
<?php			
	}
	
	function ljmc_statistics_goto_network_blog() {
		global $plugin_page;
		
		$blog_id = str_replace('ljmc_statistics_blogid_', '', $plugin_page );
		
		$details = get_blog_details( $blog_id );

		// Get the admin url for the current site.
		$url = get_admin_url($blog_id) . "/admin.php?page=statistics/statistics.php";

		echo "<script>window.location.href = '$url';</script>";
	}
	
	function ljmc_statistics_donate() {
		$url = get_admin_url() . "/admin.php?page=statistics/settings&tab=about";

		echo "<script>window.open('http://statistics.com/donate','_blank'); window.location.href = '$url';</script>";
	}

	// This function adds the menu icon to the top level menu.  LJMC 3.8 changed the style of the menu a bit and so a different css file is loaded.
	function ljmc_statistics_menu_icon() {
	
		global $ljmc_version;
		
		if( version_compare( $ljmc_version, '3.8-RC', '>=' ) || version_compare( $ljmc_version, '3.8', '>=' ) ) {
			ljmc_enqueue_style('ljmcstatistics-admin-css', plugin_dir_url(__FILE__) . 'assets/css/admin.css', true, '1.0');
		} else {
			ljmc_enqueue_style('ljmcstatistics-admin-css', plugin_dir_url(__FILE__) . 'assets/css/admin-old.css', true, '1.0');
		}
	}
	add_action('admin_head', 'ljmc_statistics_menu_icon');
	
	// This function adds the admin bar menu if the user has selected it.
	function ljmc_statistics_menubar() {
		GLOBAL $ljmc_admin_bar, $ljmc_version, $LJMC_Statistics;

		// Find out if the user can read or manage statistics.
		$read = current_user_can( ljmc_statistics_validate_capability( $LJMC_Statistics->get_option('read_capability', 'manage_options') ) );
		$manage = current_user_can( ljmc_statistics_validate_capability( $LJMC_Statistics->get_option('manage_capability', 'manage_options') ) );
		
		if( is_admin_bar_showing() && ( $read || $manage ) ) {

			$AdminURL = get_admin_url();
		
			if( version_compare( $ljmc_version, '3.8-RC', '>=' ) || version_compare( $ljmc_version, '3.8', '>=' ) ) {
				$ljmc_admin_bar->add_menu( array(
					'id'		=>	'ljmc-statistic-menu',
					'title'		=>	'<span class="ab-icon"></span>',
					'href'		=>	$AdminURL . 'admin.php?page=statistics/statistics.php'
				));
			} else {
				$ljmc_admin_bar->add_menu( array(
					'id'		=>	'ljmc-statistic-menu',
					'title'		=>	'<img src="'.plugin_dir_url(__FILE__).'/assets/images/icon.png"/>',
					'href'		=>	$AdminURL . 'admin.php?page=statistics/statistics.php'
				));
			}
			
			$ljmc_admin_bar->add_menu( array(
				'id'		=> 	'statistics-menu-useronline',
				'parent'	=>	'ljmc-statistic-menu',
				'title'		=>	__('User Online', 'ljmc_statistics') . ": " . ljmc_statistics_useronline(),
				'href'		=>  $AdminURL . 'admin.php?page=ljmcs_online_menu'
			));
			
			$ljmc_admin_bar->add_menu( array(
				'id'		=> 	'statistics-menu-todayvisitor',
				'parent'	=>	'ljmc-statistic-menu',
				'title'		=>	__('Today visitor', 'ljmc_statistics') . ": " . ljmc_statistics_visitor('today')
			));
			
			$ljmc_admin_bar->add_menu( array(
				'id'		=> 	'statistics-menu-todayvisit',
				'parent'	=>	'ljmc-statistic-menu',
				'title'		=>	__('Today visit', 'ljmc_statistics') . ": " . ljmc_statistics_visit('today')
			));
			
			$ljmc_admin_bar->add_menu( array(
				'id'		=> 	'statistics-menu-yesterdayvisitor',
				'parent'	=>	'ljmc-statistic-menu',
				'title'		=>	__('Yesterday visitor', 'ljmc_statistics') . ": " . ljmc_statistics_visitor('yesterday')
			));
			
			$ljmc_admin_bar->add_menu( array(
				'id'		=> 	'statistics-menu-yesterdayvisit',
				'parent'	=>	'ljmc-statistic-menu',
				'title'		=>	__('Yesterday visit', 'ljmc_statistics') . ": " . ljmc_statistics_visit('yesterday')
			));
			
			$ljmc_admin_bar->add_menu( array(
				'id'		=> 	'statistics-menu-viewstats',
				'parent'	=>	'ljmc-statistic-menu',
				'title'		=>	__('View Stats', 'ljmc_statistics'),
				'href'		=>	$AdminURL . 'admin.php?page=statistics/statistics.php'
			));
		}
	}
	
	if( $LJMC_Statistics->get_option('menu_bar') ) {
		add_action('admin_bar_menu', 'ljmc_statistics_menubar', 20);
	}
	
	// This function creates the HTML for the manual page.  The manual is a seperate HTML file that is contained inside of an iframe.
	// There is a bit of JavaScript included to resize the iframe so that the scroll bars can be hidden and it looks like everything
	// is in the same page.
	function ljmc_statistics_manual() {
		if( file_exists(plugin_dir_path(__FILE__) . LJMC_STATISTICS_MANUAL . 'html') ) { 
			echo '<script type="text/javascript">' . "\n";
			echo '    function AdjustiFrameHeight(id,fudge)' . "\n";
			echo '    {' . "\n";
			echo '        var frame = document.getElementById(id);' . "\n";
			echo '        frame.height = frame.contentDocument.body.offsetHeight + fudge;' . "\n";
			echo '    }' . "\n";
			echo '</script>' . "\n";

			echo '<br>';
			echo '<a href="admin.php?page=ljmcs_manual_menu&ljmcs_download_manual=true&type=odt' . '" target="_blank"><img src="' . plugin_dir_url(__FILE__) . 'assets/images/ODT.png' . '" height="32" width="32" alt="' . __('Download ODF file', 'ljmc_statistics') . '"></a>&nbsp;';
			echo '<a href="admin.php?page=ljmcs_manual_menu&ljmcs_download_manual=true&type=html' . '" target="_blank"><img src="' . plugin_dir_url(__FILE__) . 'assets/images/HTML.png' . '" height="32" width="32" alt="' . __('Download HTML file', 'ljmc_statistics') . '"></a><br>';
			
			echo '<iframe src="' .  plugin_dir_url(__FILE__) . LJMC_STATISTICS_MANUAL . 'html' . '" width="100%" frameborder="0" scrolling="no" id="ljmcs_inline_docs" onload="AdjustiFrameHeight(\'ljmcs_inline_docs\', 50);"></iframe>';
		} else {
			echo '<br><br><div class="error"><br>' . __("Manual file not found.", 'ljmc_statistics') . '<br><br></div>';
		}
	}
	
	// This is the main statistics display function.
	function ljmc_statistics_log( $log_type = "" ) {
		GLOBAL $ljmcdb, $LJMC_Statistics, $plugin_page;

		switch( $plugin_page ) {
			case 'ljmcs_browsers_menu':
				$log_type = 'all-browsers';

				break;
			case 'ljmcs_countries_menu':
				$log_type = 'top-countries';

				break;
			case 'ljmcs_exclusions_menu':
				$log_type = 'exclusions';

				break;
			case 'ljmcs_hits_menu':
				$log_type = 'hit-statistics';

				break;
			case 'ljmcs_online_menu':
				$log_type = 'online';

				break;
			case 'ljmcs_pages_menu':
				$log_type = 'top-pages';

				break;
			case 'ljmcs_referrers_menu':
				$log_type = 'top-referring-site';

				break;
			case 'ljmcs_searches_menu':
				$log_type = 'search-statistics';

				break;
			case 'ljmcs_words_menu':
				$log_type = 'last-all-search';

				break;
			case 'ljmcs_top_visitors_menu':
				$log_type = 'top-visitors';

				break;
			case 'ljmcs_visitors_menu':
				$log_type = 'last-all-visitor';

				break;
			default:
				$log_type = "";
		}
		
		// When we create $LJMC_Statistics the user has not been authenticated yet so we cannot load the user preferences
		// during the creation of the class.  Instead load them now that the user exists.
		$LJMC_Statistics->load_user_options();

		// We allow for a get style variable to be passed to define which function to use.
		if( $log_type == "" && array_key_exists('type', $_GET)) 
			$log_type = $_GET['type'];
			
		// Verify the user has the rights to see the statistics.
		if (!current_user_can(ljmc_statistics_validate_capability($LJMC_Statistics->get_option('read_capability', 'manage_option')))) {
			ljmc_die(__('You do not have sufficient permissions to access this page.'));
		}
		
		// We want to make sure the tables actually exist before we blindly start access them.
		$dbname = DB_NAME;
		$result = $ljmcdb->query("SHOW TABLES WHERE `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_visitor' OR `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_visit' OR `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_exclusions' OR `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_historical' OR `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_pages' OR `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_useronline' OR `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_search'" );
	
		if( $result != 7 ) {
			$get_bloginfo_url = get_admin_url() . "admin.php?page=statistics/optimization&tab=database";

			$missing_tables = array();
			
			$result = $ljmcdb->query("SHOW TABLES WHERE `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_visitor'" );
			if( $result != 1 ) { $missing_tables[] = $ljmcdb->prefix . 'statistics_visitor'; }
			$result = $ljmcdb->query("SHOW TABLES WHERE `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_visit'" );
			if( $result != 1 ) { $missing_tables[] = $ljmcdb->prefix . 'statistics_visit'; }
			$result = $ljmcdb->query("SHOW TABLES WHERE `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_exclusions'" );
			if( $result != 1 ) { $missing_tables[] = $ljmcdb->prefix . 'statistics_exclusions'; }
			$result = $ljmcdb->query("SHOW TABLES WHERE `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_historical'" );
			if( $result != 1 ) { $missing_tables[] = $ljmcdb->prefix . 'statistics_historical'; }
			$result = $ljmcdb->query("SHOW TABLES WHERE `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_useronline'" );
			if( $result != 1 ) { $missing_tables[] = $ljmcdb->prefix . 'statistics_useronline'; }
			$result = $ljmcdb->query("SHOW TABLES WHERE `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_pages'" );
			if( $result != 1 ) { $missing_tables[] = $ljmcdb->prefix . 'statistics_pages'; }
			$result = $ljmcdb->query("SHOW TABLES WHERE `Tables_in_{$dbname}` = '{$ljmcdb->prefix}statistics_search'" );
			if( $result != 1 ) { $missing_tables[] = $ljmcdb->prefix . 'statistics_search'; }

			ljmc_die('<div class="error"><p>' . sprintf(__('The following plugin table(s) do not exist in the database, please re-run the %s install routine %s: ', 'ljmc_statistics'),'<a href="' . $get_bloginfo_url . '">','</a>') . implode(', ', $missing_tables) . '</p></div>');
		}
		
		// Load the postbox script that provides the widget style boxes.
		ljmc_enqueue_script('postbox');
		
		// Load the css we use for the statistics pages.
		ljmc_enqueue_style('log-css', plugin_dir_url(__FILE__) . 'assets/css/log.css', true, '1.1');
		ljmc_enqueue_style('pagination-css', plugin_dir_url(__FILE__) . 'assets/css/pagination.css', true, '1.0');
		ljmc_enqueue_style('jqplot-css', plugin_dir_url(__FILE__) . 'assets/css/jquery.jqplot.min.css', true, '1.0.8');
		
		// Don't forget the right to left support.
		if( is_rtl() )
			ljmc_enqueue_style('rtl-css', plugin_dir_url(__FILE__) . 'assets/css/rtl.css', true, '1.1');

		// Load the charts code.
		ljmc_enqueue_script('jqplot', plugin_dir_url(__FILE__) . 'assets/js/jquery.jqplot.min.js', true, '0.8.3');
		ljmc_enqueue_script('jqplot-daterenderer', plugin_dir_url(__FILE__) . 'assets/js/jqplot.dateAxisRenderer.min.js', true, '0.8.3');
		ljmc_enqueue_script('jqplot-tickrenderer', plugin_dir_url(__FILE__) . 'assets/js/jqplot.canvasAxisTickRenderer.min.js', true, '0.8.3');
		ljmc_enqueue_script('jqplot-axisrenderer', plugin_dir_url(__FILE__) . 'assets/js/jqplot.canvasAxisLabelRenderer.min.js', true, '0.8.3');
		ljmc_enqueue_script('jqplot-textrenderer', plugin_dir_url(__FILE__) . 'assets/js/jqplot.canvasTextRenderer.min.js', true, '0.8.3');
		ljmc_enqueue_script('jqplot-tooltip', plugin_dir_url(__FILE__) . 'assets/js/jqplot.highlighter.min.js', true, '0.8.3');
		ljmc_enqueue_script('jqplot-pierenderer', plugin_dir_url(__FILE__) . 'assets/js/jqplot.pieRenderer.min.js', true, '0.8.3');
		ljmc_enqueue_script('jqplot-enhancedlengend', plugin_dir_url(__FILE__) . 'assets/js/jqplot.enhancedLegendRenderer.min.js', true, '0.8.3');

		// Load the pagination code.
		include_once dirname( __FILE__ ) . '/includes/classes/pagination.class.php';

		// The different pages have different files to load.
		if( $log_type == 'last-all-search' ) {
		
			include_once dirname( __FILE__ ) . '/includes/log/last-search.php';
			
		} else if( $log_type == 'last-all-visitor' ) {
		
			include_once dirname( __FILE__ ) . '/includes/log/last-visitor.php';
			
		} else if( $log_type == 'top-referring-site' ) {
		
			include_once dirname( __FILE__ ) . '/includes/log/top-referring.php';
			
		} else if( $log_type == 'all-browsers' ) {

			include_once dirname( __FILE__ ) . '/includes/log/all-browsers.php';
			
		} else if( $log_type == 'top-countries' ) {

			include_once dirname( __FILE__ ) . '/includes/log/top-countries.php';
			
		} else if( $log_type == 'hit-statistics' ) {

			include_once dirname( __FILE__ ) . '/includes/log/hit-statistics.php';
			
		} else if( $log_type == 'search-statistics' ) {

			include_once dirname( __FILE__ ) . '/includes/log/search-statistics.php';
			
		} else if( $log_type == 'exclusions' ) {

			include_once dirname( __FILE__ ) . '/includes/log/exclusions.php';
			
		} else if( $log_type == 'online' ) {

			include_once dirname( __FILE__ ) . '/includes/log/online.php';
			
		} else if( $log_type == 'top-visitors' ) {

			include_once dirname( __FILE__ ) . '/includes/log/top-visitors.php';
			
		} else if( $log_type == 'top-pages' ) {

			// If we've been given a page id or uri to get statistics for, load the page stats, otherwise load the page stats overview page.
			if( array_key_exists( 'page-id', $_GET ) || array_key_exists( 'page-uri', $_GET ) ) {
				include_once dirname( __FILE__ ) . '/includes/log/page-statistics.php';
			} else {
				include_once dirname( __FILE__ ) . '/includes/log/top-pages.php';
			}
			
		} else {
		
			// Load the map code if required.
			if( $LJMC_Statistics->get_option('map_type') == 'jqvmap' ) {
				ljmc_enqueue_style('jqvmap-css', plugin_dir_url(__FILE__) . 'assets/jqvmap/jqvmap.css', true, '1.1');
				ljmc_enqueue_script('jquery-vmap', plugin_dir_url(__FILE__) . 'assets/jqvmap/jquery.vmap.min.js', true, '1.1');
				ljmc_enqueue_script('jquery-vmap-world', plugin_dir_url(__FILE__) . 'assets/jqvmap/maps/jquery.vmap.world.js', true, '1.1');
			}
		
			include_once dirname( __FILE__ ) . '/includes/log/log.php';
		}
	}
	
	// This function loads the optimization page code.
	function ljmc_statistics_optimization() {

		GLOBAL $ljmcdb, $LJMC_Statistics;
		
		// Check the current user has the rights to be here.
		if (!current_user_can(ljmc_statistics_validate_capability($LJMC_Statistics->get_option('manage_capability', 'manage_options')))) {
			ljmc_die(__('You do not have sufficient permissions to access this page.'));
		}

		// When we create $LJMC_Statistics the user has not been authenticated yet so we cannot load the user preferences
		// during the creation of the class.  Instead load them now that the user exists.
		$LJMC_Statistics->load_user_options();

		// Load the jQuery UI code to create the tabs.
		ljmc_register_style("jquery-ui-css", plugin_dir_url(__FILE__) . "assets/css/jquery-ui-1.10.4.custom.css");
		ljmc_enqueue_style("jquery-ui-css");

		ljmc_enqueue_script('jquery-ui-core');
		ljmc_enqueue_script('jquery-ui-tabs');
		
		if( is_rtl() )
			ljmc_enqueue_style('rtl-css', plugin_dir_url(__FILE__) . 'assets/css/rtl.css', true, '1.1');

		// Get the row count for each of the tables, we'll use this later on in the ljmcs_optimization.php file.
		$result['useronline'] = $ljmcdb->get_var("SELECT COUNT(ID) FROM `{$ljmcdb->prefix}statistics_useronline`");
		$result['visit'] = $ljmcdb->get_var("SELECT COUNT(ID) FROM `{$ljmcdb->prefix}statistics_visit`");
		$result['visitor'] = $ljmcdb->get_var("SELECT COUNT(ID) FROM `{$ljmcdb->prefix}statistics_visitor`");
		$result['exclusions'] = $ljmcdb->get_var("SELECT COUNT(ID) FROM `{$ljmcdb->prefix}statistics_exclusions`");
		$result['pages'] = $ljmcdb->get_var("SELECT COUNT(uri) FROM `{$ljmcdb->prefix}statistics_pages`");
		$result['historical'] = $ljmcdb->get_Var("SELECT COUNT(ID) FROM `{$ljmcdb->prefix}statistics_historical`");
		$result['search'] = $ljmcdb->get_Var("SELECT COUNT(ID) FROM `{$ljmcdb->prefix}statistics_search`");
		
		include_once dirname( __FILE__ ) . "/includes/optimization/ljmcs-optimization.php";
	}

	// This function displays the HTML for the settings page.
	function ljmc_statistics_settings() {
		GLOBAL $LJMC_Statistics;
		
		// Check the current user has the rights to be here.
		if (!current_user_can(ljmc_statistics_validate_capability($LJMC_Statistics->get_option('read_capability', 'manage_options')))) {
			ljmc_die(__('You do not have sufficient permissions to access this page.'));
		}
		
		// When we create $LJMC_Statistics the user has not been authenticated yet so we cannot load the user preferences
		// during the creation of the class.  Instead load them now that the user exists.
		$LJMC_Statistics->load_user_options();

		// Load our CSS to be used.
		ljmc_enqueue_style('log-css', plugin_dir_url(__FILE__) . 'assets/css/style.css', true, '1.0');

		// Load the jQuery UI code to create the tabs.
		ljmc_register_style("jquery-ui-css", plugin_dir_url(__FILE__) . "assets/css/jquery-ui-1.10.4.custom.css");
		ljmc_enqueue_style("jquery-ui-css");

		ljmc_enqueue_script('jquery-ui-core');
		ljmc_enqueue_script('jquery-ui-tabs');
		
		if( is_rtl() )
			ljmc_enqueue_style('rtl-css', plugin_dir_url(__FILE__) . 'assets/css/rtl.css', true, '1.1');
		
		// We could let the download happen at the end of the page, but this way we get to give some
		// feedback to the users about the result.
		if( $LJMC_Statistics->get_option('update_geoip') == true ) {
			echo ljmc_statistics_download_geoip();
		}
		
		include_once dirname( __FILE__ ) . "/includes/settings/ljmcs-settings.php";
	}