<?php
	// Get the historical number of visitors to the site
	$historical_visitors = $LJMC_Statistics->Get_Historical_Data( 'visitors' );

	// Get the historical number of visits to the site
	$historical_visits = $LJMC_Statistics->Get_Historical_Data( 'visits' );

?>
	<div class="wrap">
		<form id="ljmcs_historical_form" method="post">
			<?php ljmc_nonce_field('historical_form', 'statistics-nonce');?>
			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" colspan="2"><h3><?php _e('Historical Values', 'ljmc_statistics'); ?></h3></th>
					</tr>
					
					<tr valign="top" id="ljmcs_historical_purge" style="display: none">
						<th scope="row" colspan=2>
							<?php _e('Note: As you have just purged the database you must reload this page for these numbers to be correct.', 'ljmc_statistics'); ?>
						</th>
					</tr>

					<tr valign="top">
						<th scope="row">
							<?php _e('Visitors', 'ljmc_statistics'); ?>:
						</th>
						
						<td>
							<input type="text" size="10" value="<?php echo $historical_visitors; ?>" id="ljmcs_historical_visitors" name="ljmcs_historical_visitors">
							<p class="description"><?php echo sprintf( __('Number of historical number of visitors to the site (current value is %s).', 'ljmc_statistics'), number_format_i18n( $historical_visitors )); ?></p>
						</td>
					</tr>
					
					<tr valign="top">
						<th scope="row">
							<?php _e('Visits', 'ljmc_statistics'); ?>:
						</th>
						
						<td>
							<input type="text" size="10" value="<?php echo $historical_visits; ?>" id="ljmcs_historical_visits" name="ljmcs_historical_visits">
							<p class="description"><?php echo sprintf( __('Number of historical number of visits to the site (current value is %s).', 'ljmc_statistics'), number_format_i18n( $historical_visits )); ?></p>
						</td>
					</tr>
					
					<tr valign="top">
						<td colspan=2>
							<input id="historical-submit" class="button button-primary" type="submit" value="<?php _e('Update now!', 'ljmc_statistics'); ?>" name="historical-submit"/>
						</td>
					</tr>
					
				</tbody>
		
			</table>

		</form>

	</div>
