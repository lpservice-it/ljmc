<?php
	function ljmc_statistics_generate_countries_postbox($ISOCountryCode, $search_engines) {
	
		global $LJMC_Statistics;

		if( $LJMC_Statistics->get_option('geoip') && $LJMC_Statistics->get_option( 'visitors' ) ) { 
?>
				<div class="postbox">
					<div class="handlediv" title="<?php _e('Click to toggle', 'ljmc_statistics'); ?>"><br /></div>
					<h3 class="hndle">
						<span><?php _e('Top 10 Countries', 'ljmc_statistics'); ?> <a href="?page=ljmcs_countries_menu"><?php echo ljmc_statistics_icons('dashicons-visibility', 'visibility'); ?><?php _e('More', 'ljmc_statistics'); ?></a></span>
					</h3>
					<div class="inside">
						<div class="inside">
						<?php ljmc_statistics_generate_countries_postbox_content($ISOCountryCode); ?>
						</div>
					</div>
				</div>
<?php 
		}
	}

	function ljmc_statistics_generate_countries_postbox_content($ISOCountryCode, $count = 10) {
	
		global $ljmcdb, $LJMC_Statistics;

?>
							<table width="100%" class="widefat table-stats" id="last-referrer">
								<tr>
									<td width="10%" style='text-align: left'><?php _e('Vieta', 'ljmc_statistics'); ?></td>
									<td width="40%" style='text-align: left'><?php _e('Apmeklējumi', 'ljmc_statistics'); ?></td>
								</tr>
								
								<?php
									$Countries = array();
									
									$result = $ljmcdb->get_results("SELECT DISTINCT `location` FROM `{$ljmcdb->prefix}statistics_visitor`");
									
									foreach( $result as $item )
										{
										$Countries[$item->location] = $ljmcdb->get_var("SELECT count(location) FROM `{$ljmcdb->prefix}statistics_visitor` WHERE location='" . $item->location . "'" );
										}
										
									arsort($Countries);
									$i = 0;
									
									foreach( $Countries as $item => $value) {
										$i++;
										
										$item = strtoupper($item);
										
										echo "<tr>";
										echo "<td style='text-align: left'>$i</td>";
										echo "<td style='text-align: left'>" . number_format_i18n($value) . "</td>";
										echo "</tr>";
										
										if( $i == $count ) { break; }
									}
								?>
							</table>
<?php 
	}

