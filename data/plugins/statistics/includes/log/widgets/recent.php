<?php
	function ljmc_statistics_generate_recent_postbox($ISOCountryCode, $search_engines) {
		GLOBAL $LJMC_Statistics;
		
		if( $LJMC_Statistics->get_option( 'visitors' ) ) {
?>
				<div class="postbox">
					<div class="handlediv" title="<?php _e('Click to toggle', 'ljmc_statistics'); ?>"><br /></div>
					<h3 class="hndle">
						<span><?php _e('Pēdējie apmeklētāji', 'ljmc_statistics'); ?> <a href="?page=ljmcs_visitors_menu"><?php echo ljmc_statistics_icons('dashicons-visibility', 'visibility'); ?></a></span>
					</h3>
					<div class="inside">
							
					<?php ljmc_statistics_generate_recent_postbox_content($ISOCountryCode); ?>
					</div>
				</div>
<?php		
		}
	}

	function ljmc_statistics_generate_recent_postbox_content($ISOCountryCode, $count = 10) {
	
		global $ljmcdb, $LJMC_Statistics;

		$result = $ljmcdb->get_results("SELECT * FROM `{$ljmcdb->prefix}statistics_visitor` ORDER BY `{$ljmcdb->prefix}statistics_visitor`.`ID` DESC  LIMIT 0, {$count}");
								
		echo "<div class='log-latest'>";

		$dash_icon = ljmc_statistics_icons('dashicons-visibility', 'visibility');
		
		foreach($result as $items) {
			if( substr( $items->ip, 0, 6 ) == '#hash#' ) { 
				$ip_string = __('#hash#', 'ljmc_statistics'); 
				$map_string = "";
			} 
			else { 
				$ip_string = "<a href='?page=statistics/statistics.php&type=last-all-visitor&ip={$items->ip}'>{$dash_icon}{$items->ip}</a>"; 
				$map_string = "<a class='show-map' href='http://www.geoiptool.com/en/?IP={$items->ip}' target='_blank' title='".__('Map', 'ljmc_statistics')."'>".ljmc_statistics_icons('dashicons-location-alt', 'map')."</a>";
			}
			
			echo "<div class='log-item'>";
				echo "<div class='log-referred'>{$ip_string}</div>";
				echo "<div class='log-ip'>" . date(get_option('date_format'), strtotime($items->last_counter)) . "</div>";
				echo "<div class='clear'></div>";
				echo "<div class='log-url'>";
				echo $map_string;
				
				if($LJMC_Statistics->get_option('geoip')) {
					echo "<img src='".plugins_url('statistics/assets/images/flags/' . $items->location . '.png')."' title='{$ISOCountryCode[$items->location]}' class='log-tools'/>";
				}
				
				if( array_search( strtolower( $items->agent ), array( "chrome", "firefox", "msie", "opera", "safari" ) ) !== FALSE ){
					$agent = "<img src='".plugins_url('statistics/assets/images/').$items->agent.".png' class='log-tools' title='{$items->agent}'/>";
				} else {
					$agent = ljmc_statistics_icons('dashicons-editor-help', 'unknown');
				}
				
				echo "<a href='?page=statistics/statistics.php&type=last-all-visitor&agent={$items->agent}'>{$agent}</a>";
				
				echo "<a href='" . htmlentities($items->referred,ENT_QUOTES) . "' title='" . htmlentities($items->referred,ENT_QUOTES) . "'>".ljmc_statistics_icons('dashicons-admin-links', 'link') . " " . htmlentities($items->referred,ENT_QUOTES) . "</a></div>";
			echo "</div>";
		}
		
		echo "</div>";
	}
