<?php
	function ljmc_statistics_generate_summary_postbox($ISOCountryCode, $search_engines) {
	
		global $ljmcdb, $LJMC_Statistics;
?>		
				<div class="postbox">
					<div class="handlediv" title="<?php _e('Click to toggle', 'ljmc_statistics'); ?>"><br /></div>
					<h3 class="hndle"><span><?php _e('Pārskats', 'ljmc_statistics'); ?></span></h3>
					<div class="inside">
					<?php ljmc_statistics_generate_summary_postbox_content($search_engines); ?>
					</div>
				</div>
<?php
	}

	function ljmc_statistics_generate_summary_postbox_content($search_engines, $search = true, $time = true) {
	
		global $ljmcdb, $LJMC_Statistics;
		
		$show_visitors = $LJMC_Statistics->get_option('visitor');
?>		
						<table width="100%" class="widefat table-stats" id="summary-stats">
							<tbody>
<?php if( $LJMC_Statistics->get_option('useronline') ) {?>							
								<tr>
									<th><?php _e('Lietotāji pieslēgušies', 'ljmc_statistics'); ?>:</th>
									<th colspan="2" id="th-colspan">
										<span><a href="admin.php?page=ljmcs_online_menu"><?php echo ljmc_statistics_useronline(); ?></a></span> 
									</th>
								</tr>
<?php }

if( $LJMC_Statistics->get_option('visitors') || $LJMC_Statistics->get_option('visits') ) {
?>								
								<tr>
									<th width="60%"></th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visitors') ) { _e('Apmeklētāji', 'ljmc_statistics'); } else { echo ''; }?></th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visits') ) { _e('Skatījumi', 'ljmc_statistics'); } else { echo ''; }?></th>
								</tr>
								
								<tr>
									<th><?php _e('Šodien', 'ljmc_statistics'); ?>:</th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visitors') ) { echo '<a href="admin.php?page=ljmcs_visitors_menu&hitdays=1"><span>' . number_format_i18n(ljmc_statistics_visitor('today',null,true)) . '</span></a>'; } else { echo ''; }?></th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visits') ) { echo '<a href="admin.php?page=ljmcs_hits_menu&hitdays=1"><span>' . number_format_i18n(ljmc_statistics_visit('today')) . '</span></a>'; } else { echo ''; }?></th>
								</tr>
								
								<tr>
									<th><?php _e('Vakar', 'ljmc_statistics'); ?>:</th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visitors') ) { echo '<a href="admin.php?page=ljmcs_visitors_menu&hitdays=1"><span>' . number_format_i18n(ljmc_statistics_visitor('yesterday',null,true)) . '</span></a>'; } else { echo ''; }?></th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visits') ) { echo '<a href="admin.php?page=ljmcs_hits_menu&hitdays=1"><span>' . number_format_i18n(ljmc_statistics_visit('yesterday')) . '</span></a>'; } else { echo ''; }?></th>
								</tr>
								
								<tr>
									<th><?php _e('Pēdējo nedēļu', 'ljmc_statistics'); ?>:</th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visitors') ) { echo '<a href="admin.php?page=ljmcs_visitors_menu&hitdays=7"><span>' . number_format_i18n(ljmc_statistics_visitor('week',null,true)) . '</span></a>'; } else { echo ''; }?></th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visits') ) { echo '<a href="admin.php?page=ljmcs_hits_menu&hitdays=7"><span>' .  number_format_i18n(ljmc_statistics_visit('week')) . '</span></a>'; } else { echo ''; }?></th>
								</tr>
								
								<tr>
									<th><?php _e('Pēdējo mēnesi', 'ljmc_statistics'); ?>:</th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visitors') ) { echo '<a href="admin.php?page=ljmcs_visitors_menu&hitdays=30"><span>' . number_format_i18n(ljmc_statistics_visitor('month',null,true)) . '</span></a>'; } else { echo ''; }?></th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visits') ) { echo '<a href="admin.php?page=ljmcs_hits_menu&hitdays=30"><span>' . number_format_i18n(ljmc_statistics_visit('month')) . '</span></a>'; } else { echo ''; }?></th>
								</tr>
								
								<tr>
									<th><?php _e('Pēdējo gadu', 'ljmc_statistics'); ?>:</th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visitors') ) { echo '<a href="admin.php?page=ljmcs_visitors_menu&hitdays=365"><span>' . number_format_i18n(ljmc_statistics_visitor('year',null,true)) . '</span></a>'; } else { echo ''; }?></th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visits') ) { echo '<a href="admin.php?page=ljmcs_hits_menu&hitdays=365"><span>' . number_format_i18n(ljmc_statistics_visit('year')) . '</span></a>'; } else { echo ''; }?></th>
								</tr>
								
								<tr>
									<th><?php _e('Kopā', 'ljmc_statistics'); ?>:</th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visitors') ) { echo '<a href="admin.php?page=ljmcs_visitors_menu&hitdays=365"><span>' . number_format_i18n(ljmc_statistics_visitor('total',null,true)) . '</span></a>'; } else { echo ''; }?></th>
									<th class="th-center"><?php if( $LJMC_Statistics->get_option('visits') ) { echo '<a href="admin.php?page=ljmcs_hits_menu&hitdays=365"><span>' . number_format_i18n(ljmc_statistics_visit('total')) . '</span></a>'; } else { echo ''; }?></th>
								</tr>
								
<?php 
	}
	
if( $search == true && $LJMC_Statistics->get_option('visitors' )) {
	
		if( $LJMC_Statistics->get_option('visitors') || $LJMC_Statistics->get_option('visits') || $LJMC_Statistics->get_option('useronline') ) {
?>
								<tr>
									<th colspan="3"><br><hr></th>
								</tr>
<?php		}?>
								<tr>
									<th colspan="3" style="text-align: center;"><?php _e('Meklētājprogrammu atsauces', 'ljmc_statistics'); ?></th>
								</tr>
								
								<tr>
									<th width="60%"></th>
									<th class="th-center"><?php _e('Šodien', 'ljmc_statistics'); ?></th>
									<th class="th-center"><?php _e('Vakar', 'ljmc_statistics'); ?></th>
								</tr>
								
								<?php
								$se_today_total = 0;
								$se_yesterday_total = 0;
								foreach( $search_engines as $se ) {
								?>
								<tr>
									<th><img src='<?php echo plugins_url('statistics/assets/images/' . $se['image'] );?>'> <?php _e($se['name'], 'ljmc_statistics'); ?>:</th>
									<th class="th-center"><span><?php $se_temp = ljmc_statistics_searchengine($se['tag'], 'today'); $se_today_total += $se_temp; echo number_format_i18n($se_temp);?></span></th>
									<th class="th-center"><span><?php $se_temp = ljmc_statistics_searchengine($se['tag'], 'yesterday'); $se_yesterday_total += $se_temp; echo number_format_i18n($se_temp);?></span></th>
								</tr>
								
								<?php
								}
								?>
								<tr>
									<th><?php _e('Dienas kopējais', 'ljmc_statistics'); ?>:</th>
									<td id="th-colspan" class="th-center"><span><?php echo number_format_i18n($se_today_total); ?></span></td>
									<td id="th-colspan" class="th-center"><span><?php echo number_format_i18n($se_yesterday_total); ?></span></td>
								</tr>

								<tr>
									<th><?php _e('Kopā', 'ljmc_statistics'); ?>:</th>
									<th colspan="2" id="th-colspan"><span><?php echo number_format_i18n(ljmc_statistics_searchengine('all')); ?></span></th>
								</tr>
<?php 
	}

if( $time == true ) {
?>
								<tr>
									<th colspan="3"><br><hr></th>
								</tr>

								<tr>
									<th colspan="3" style="text-align: center;"><?php _e('Tagadējais laiks', 'ljmc_statistics'); ?> <span id="time_zone"><a href="<?php echo admin_url('options-general.php'); ?>"><?php _e('(Pielāgot)', 'ljmc_statistics'); ?></a></span></th>
								</tr>

								<tr>
									<th colspan="3"><?php echo sprintf(__('Datums: %s', 'ljmc_statistics'), '<code dir="ltr">' . $LJMC_Statistics->Current_Date_i18n(get_option('date_format')) . '</code>'); ?></th>
								</tr>

								<tr>
									<th colspan="3"><?php echo sprintf(__('Laiks: %s', 'ljmc_statistics'), '<code dir="ltr">' .$LJMC_Statistics->Current_Date_i18n(get_option('time_format')) . '</code>'); ?></th>
								</tr>
<?php }?>
							</tbody>
						</table>
<?php
	}

