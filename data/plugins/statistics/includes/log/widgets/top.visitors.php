<?php
	function ljmc_statistics_generate_top_visitors_postbox($ISOCountryCode, $search_engines) {
	
		global $ljmcdb, $LJMC_Statistics;
		
		if( $LJMC_Statistics->get_option( 'visitors' ) ) {
?>
				<div class="postbox">
					<div class="handlediv" title="<?php _e('Click to toggle', 'ljmc_statistics'); ?>"><br /></div>
					<h3 class="hndle"><span><?php _e('Top 10 Visitors Today', 'ljmc_statistics'); ?> <a href="?page=ljmcs_top_visitors_menu"> <?php echo ljmc_statistics_icons('dashicons-visibility', 'visibility'); ?><?php _e('More', 'ljmc_statistics'); ?></a></span></h3>
					<div class="inside">
<?php								
					ljmc_statistics_generate_top_visitors_postbox_content($ISOCountryCode)
?>						
					</div>
				</div>
<?php		
		}
	}

	function ljmc_statistics_generate_top_visitors_postbox_content($ISOCountryCode, $day='today', $count=10, $compact=false) {
	
		global $ljmcdb, $LJMC_Statistics;
		
		if( $day == 'today' ) { $sql_time = $LJMC_Statistics->Current_Date('Y-m-d'); } else { $sql_time = date( 'Y-m-d', strtotime( $day ) ); } 
		
?>
							<table width="100%" class="widefat table-stats" id="last-referrer">
								<tr>
									<td style='text-align: left'><?php _e('Vieta', 'ljmc_statistics'); ?></td>
									<td style='text-align: left'><?php _e('Skatījumi', 'ljmc_statistics'); ?></td>
									<td style='text-align: left'><?php _e('IP', 'ljmc_statistics'); ?></td>
<?php if( $compact == false ) { ?>
									<td style='text-align: left'><?php _e('Pārlūkprogramma', 'ljmc_statistics'); ?></td>
									<td style='text-align: left'><?php _e('Platforma', 'ljmc_statistics'); ?></td>
									<td style='text-align: left'><?php _e('Versija', 'ljmc_statistics'); ?></td>
<?php } ?>
								</tr>
								
								<?php
									$result = $ljmcdb->get_results("SELECT * FROM `{$ljmcdb->prefix}statistics_visitor` WHERE last_counter = '{$sql_time}' ORDER BY hits DESC");
									
									$i = 0;
									
									foreach( $result as $visitor) {
										$i++;
										
										$item = strtoupper($visitor->location);
										
										echo "<tr>";
										echo "<td style='text-align: left'>$i</td>";
										echo "<td style='text-align: left'>" . (int)$visitor->hits . "</td>";
										echo "<td style='text-align: left'>{$visitor->ip}</td>";
										
										if( $compact == false ) {
											echo "<td style='text-align: left'>{$visitor->agent}</td>";
											echo "<td style='text-align: left'>{$visitor->platform}</td>";
											echo "<td style='text-align: left'>{$visitor->version}</td>";
										}
										echo "</tr>";
										
										if( $i == $count ) { break; }
									}
								?>
							</table>
<?php		
	}
