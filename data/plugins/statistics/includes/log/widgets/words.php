<?php
	function ljmc_statistics_generate_words_postbox($ISOCountryCode, $search_engines) {
	
		global $ljmcdb, $LJMC_Statistics;
		
		if( $LJMC_Statistics->get_option( 'visitors' ) ) {
?>
				<div class="postbox">
					<div class="handlediv" title="<?php _e('Click to toggle', 'ljmc_statistics'); ?>"><br /></div>
					<h3 class="hndle">
						<span><?php _e('Pēdējie atslēgas vārdi', 'ljmc_statistics'); ?> <a href="?page=ljmcs_words_menu"><?php echo ljmc_statistics_icons('dashicons-visibility', 'visibility'); ?></a></span>
					</h3>
					<div class="inside">
					<?php ljmc_statistics_generate_words_postbox_content($ISOCountryCode); ?>
					</div>
				</div>
<?php		
		}
	}

	function ljmc_statistics_generate_words_postbox_content($ISOCountryCode, $count = 10) {
	
		global $ljmcdb, $LJMC_Statistics;

		// Retrieve MySQL data for the search words.
		$search_query = ljmc_statistics_searchword_query('all');

		// Determine if we're using the old or new method of storing search engine info and build the appropriate table name.
		$tablename = $ljmcdb->prefix . 'statistics_';
		
		if( $LJMC_Statistics->get_option('search_converted') ) {
			$tabletwo = $tablename . 'visitor';
			$tablename .= 'search';
			$result = $ljmcdb->get_results("SELECT * FROM `{$tablename}` INNER JOIN `{$tabletwo}` on {$tablename}.`visitor` = {$tabletwo}.`ID` WHERE {$search_query} ORDER BY `{$tablename}`.`ID` DESC  LIMIT 0, {$count}");
		} else {
			$tablename .= 'visitor';
			$result = $ljmcdb->get_results("SELECT * FROM `{$tablename}` WHERE {$search_query} ORDER BY `{$tablename}`.`ID` DESC  LIMIT 0, {$count}");
		}

		if( sizeof($result) > 0 ) {
			echo "<div class='log-latest'>";
			
			foreach($result as $items) {
				if( !$LJMC_Statistics->Search_Engine_QueryString($items->referred) ) continue;
				
				if( substr( $items->ip, 0, 6 ) == '#hash#' ) { $ip_string = __('#hash#', 'ljmc_statistics'); } else { $ip_string = "<a href='http://www.geoiptool.com/en/?IP={$items->ip}' target='_blank'>{$items->ip}</a>"; }

				if( $LJMC_Statistics->get_option('search_converted') ) {
					$this_search_engine = $LJMC_Statistics->Search_Engine_Info_By_Engine($items->engine);
					$words = $items->words;
				} else {
					$this_search_engine = $LJMC_Statistics->Search_Engine_Info($items->referred);
					$words = $LJMC_Statistics->Search_Engine_QueryString($items->referred);
				}
				
				echo "<div class='log-item'>";
				echo "<div class='log-referred'>".$words."</div>";
				echo "<div class='log-ip'>" . date(get_option('date_format'), strtotime($items->last_counter)) . " - {$ip_string}</div>";
				echo "<div class='clear'></div>";
				echo "<div class='log-url'>";
				echo "<a class='show-map' href='http://www.geoiptool.com/en/?IP={$items->ip}' target='_blank' title='".__('Map', 'ljmc_statistics')."'>".ljmc_statistics_icons('dashicons-location-alt', 'map')."</a>";
				
				if($LJMC_Statistics->get_option('geoip')) {
					echo "<img src='".plugins_url('statistics/assets/images/flags/' . $items->location . '.png')."' title='{$ISOCountryCode[$items->location]}' class='log-tools'/>";
				}
				
				$this_search_engine = $LJMC_Statistics->Search_Engine_Info($items->referred);
				echo "<a href='?page=statistics/statistics.php&type=last-all-search&referred={$this_search_engine['tag']}'><img src='".plugins_url('statistics/assets/images/' . $this_search_engine['image'])."' class='log-tools' title='".$this_search_engine['translated']."'/></a>";
				
				if( array_search( strtolower( $items->agent ), array( "chrome", "firefox", "msie", "opera", "safari" ) ) !== FALSE ){
					$agent = "<img src='".plugins_url('statistics/assets/images/').$items->agent.".png' class='log-tools' title='{$items->agent}'/>";
				} else {
					$agent = ljmc_statistics_icons('dashicons-editor-help', 'unknown');
				}
				
				echo "<a href='?page=statistics/statistics.php&type=last-all-visitor&agent={$items->agent}'>{$agent}</a>";
				
				echo "<a href='" . htmlentities($items->referred,ENT_QUOTES) . "' title='" . htmlentities($items->referred,ENT_QUOTES) . "'>" . ljmc_statistics_icons('dashicons-admin-links', 'link') . " " . htmlentities($items->referred,ENT_QUOTES) . "</a></div>";
				echo "</div>";
			}
			
			echo "</div>";
		}
	}

