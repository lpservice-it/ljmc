<?php
	function ljmc_statistics_generate_referring_postbox($ISOCountryCode, $search_engines) {
	
		global $ljmcdb, $LJMC_Statistics;

		if( $LJMC_Statistics->get_option( 'visitors' ) ) {
			?>
			<div class="postbox">
				<div class="handlediv" title="<?php _e('Click to toggle', 'ljmc_statistics'); ?>"><br /></div>
				<h3 class="hndle">
					<span><?php _e('Top atsauču lapas', 'ljmc_statistics'); ?></span> <a href="?page=ljmcs_referrers_menu"><?php echo ljmc_statistics_icons('dashicons-visibility', 'visibility'); ?><?php _e('More', 'ljmc_statistics'); ?></a>
				</h3>
				<div class="inside">
					<div class="inside">
					<?php ljmc_statistics_generate_referring_postbox_content(); ?>
					</div>
				</div>
			</div>
<?php
		}
	}
	
	function ljmc_statistics_generate_referring_postbox_content($count = 10) {
	
		global $ljmcdb, $LJMC_Statistics;
		
		$get_urls = array();
		
		$result = $ljmcdb->get_results( "SELECT referred FROM {$ljmcdb->prefix}statistics_visitor WHERE referred <> ''" );
		
		$urls = array();
		foreach( $result as $item ) {
		
			$url = parse_url($item->referred);
			
			if( empty($url['host']) || stristr(get_bloginfo('url'), $url['host']) )
				continue;
				
			$urls[] = $url['host'];
		}
		
		$get_urls = array_count_values($urls);
	
		arsort( $get_urls );
		$get_urls = array_slice($get_urls, 0, $count);

?>
						<table width="100%" class="widefat table-stats" id="last-referrer">
							<tr>
								<td width="10%"><?php _e('Atsauce', 'ljmc_statistics'); ?></td>
								<td width="90%"><?php _e('Adrese', 'ljmc_statistics'); ?></td>
							</tr>
							
							<?php
							
								foreach( $get_urls as $items => $value) {
								
									echo "<tr>";
									echo "<td><a href='?page=ljmcs_referrers_menu&referr=" . htmlentities($items,ENT_QUOTES) . "'>" . number_format_i18n($value) . "</a></td>";
									echo "<td><a href='http://" . htmlentities($items,ENT_QUOTES) . "' target='_blank'>" . htmlentities($items,ENT_QUOTES) . " " . ljmc_statistics_icons('dashicons-admin-links', 'link') . "</a></td>";
									echo "</tr>";
								}
							?>
						</table>
<?php
	}	