<script type="text/javascript">
	jQuery(document).ready(function(){
		postboxes.add_postbox_toggles(pagenow);
	});
</script>
<?php
	$daysToDisplay = 20; 
	if( array_key_exists('hitdays',$_GET) ) { $daysToDisplay = intval($_GET['hitdays']); }

	if( array_key_exists('rangestart', $_GET ) ) { $rangestart = $_GET['rangestart']; } else { $rangestart = ''; }
	if( array_key_exists('rangeend', $_GET ) ) { $rangeend = $_GET['rangeend']; } else { $rangeend = ''; }

	list( $daysToDisplay, $rangestart_utime, $rangeend_utime ) = ljmc_statistics_date_range_calculator( $daysToDisplay, $rangestart, $rangeend );

?>
<div class="wrap">
	<?php screen_icon('options-general'); ?>
	<h2><?php _e('Top Countries', 'ljmc_statistics'); ?></h2>

	<?php ljmc_statistics_date_range_selector( 'ljmcs_countries_menu', $daysToDisplay ); ?>

	<div class="postbox-container" id="last-log" style="width: 100%;">
		<div class="metabox-holder">
			<div class="meta-box-sortables">
				<div class="postbox">
					<div class="inside">
						<div class="inside">
							<table class="widefat table-stats" id="last-referrer" style="width: 100%;">
								<tr>
									<td><?php _e('Vieta', 'ljmc_statistics'); ?></td>
									<td><?php _e('Apmeklējumi', 'ljmc_statistics'); ?></td>
								</tr>
								
								<?php
									$ISOCountryCode = $LJMC_Statistics->get_country_codes();
									
									$result = $ljmcdb->get_results("SELECT DISTINCT `location` FROM `{$ljmcdb->prefix}statistics_visitor`");
									
									$rangestartdate = $LJMC_Statistics->real_current_date('Y-m-d', '-0', $rangestart_utime );
									$rangeenddate = $LJMC_Statistics->real_current_date('Y-m-d', '-0', $rangeend_utime );
									
									foreach( $result as $item )
										{
										$Countries[$item->location] = $ljmcdb->get_var("SELECT count(location) FROM `{$ljmcdb->prefix}statistics_visitor` WHERE location='{$item->location}' AND `last_counter` BETWEEN '{$rangestartdate}' AND '{$rangeenddate}'" );
										}
										
									arsort($Countries);
									$i = 0;
									
									foreach( $Countries as $item => $value) {
										$i++;

										$item = strtoupper($item);
										
										echo "<tr>";
										echo "<td style='text-align: center;'>$i</td>";
										echo "<td style='text-align: center;'>" . number_format_i18n($value) . "</td>";
										echo "</tr>";
									}
								?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>