<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.show-map').click(function(){
			alert('<?php _e('To be added soon', 'ljmc_statistics'); ?>');
		});

	postboxes.add_postbox_toggles(pagenow);
	});
</script>
<?php
	if( array_key_exists('referr',$_GET) ) {
		$referr = $_GET['referr'];
		$title = $_GET['referr'];
	}
	else {
		$referr = '';
	}
	
	$get_urls = array();
	$total = 0;
		
	if( $referr ) {
		$result = $ljmcdb->get_results($ljmcdb->prepare("SELECT * FROM `{$ljmcdb->prefix}statistics_visitor` WHERE `referred` LIKE %s' AND referred <> '' ORDER BY `{$ljmcdb->prefix}statistics_visitor`.`ID` DESC", '%' . $referr . '%' ) );

		$total = count( $result );
	} else {
		$result = $ljmcdb->get_results( "SELECT referred FROM {$ljmcdb->prefix}statistics_visitor WHERE referred <> ''" );
		
		$urls = array();
		foreach( $result as $item ) {
		
			$url = parse_url($item->referred);
			
			if( empty($url['host']) || stristr(get_bloginfo('url'), $url['host']) )
				continue;
				
			$urls[] = $url['host'];
		}
		
		$get_urls = array_count_values($urls);

		$total = count( $get_urls );
	}

?>
<div class="wrap">
	<?php screen_icon('options-general'); ?>
	<h2><?php _e('Top atsauču saites', 'ljmc_statistics'); ?></h2>
	<ul class="subsubsub">
		<?php if($referr) { ?>
		<li class="all"><a <?php if(!$referr) { echo 'class="current"'; } ?>href="?page=ljmcs_referrers_menu"><?php _e('Visas', 'ljmc_statistics'); ?></a></li>
			| <li><a class="current" href="?page=ljmcs_referrers_menu&referr=<?php echo htmlentities($referr, ENT_QUOTES); ?>"> <?php echo $title; ?> <span class="count">(<?php echo $total; ?>)</span></a></li>
		<?php } else { ?>
		<li class="all"><a <?php if(!$referr) { echo 'class="current"'; } ?>href="?page=ljmcs_referrers_menu"><?php _e('Visas', 'ljmc_statistics'); ?> <span class="count">(<?php echo $total; ?>)</span></a></li>
		<?php }?>
	</ul>
	<div class="postbox-container" id="last-log">
		<div class="metabox-holder">
			<div class="meta-box-sortables">
				<div class="postbox">
					<?php if($referr) { ?>
						<h3 class="hndle"><span><?php _e('Atsauču saites no', 'ljmc_statistics'); ?>: <?php echo htmlentities($referr, ENT_QUOTES); ?></span></h3>
					<?php } else { ?>
						<h3 class="hndle"><span><?php _e('Top atsauču saites', 'ljmc_statistics'); ?></span></h3>
					<?php } ?>
					<div class="inside">
							<?php
								echo "<div class='log-latest'>";

								if( $total > 0 ) {
									// Initiate pagination object with appropriate arguments
									$pagesPerSection = 10;
									$options = array(25, "Visas");
									$stylePageOff = "pageOff";
									$stylePageOn = "pageOn";
									$styleErrors = "paginationErrors";
									$styleSelect = "paginationSelect";

									$Pagination = new LJMC_Statistics_Pagination($total, $pagesPerSection, $options, false, $stylePageOff, $stylePageOn, $styleErrors, $styleSelect);
									
									$start = $Pagination->getEntryStart();
									$end = $Pagination->getEntryEnd();
									
									if( $LJMC_Statistics->get_option('search_converted') ) {
										$result = $ljmcdb->get_results($ljmcdb->prepare("SELECT * FROM `{$ljmcdb->prefix}statistics_search` INNER JOIN `{$ljmcdb->prefix}statistics_visitor` on {$ljmcdb->prefix}statistics_search.`visitor` = {$ljmcdb->prefix}statistics_visitor.`ID` WHERE `host` = %s ORDER BY `{$ljmcdb->prefix}statistics_search`.`ID` DESC LIMIT %d, %d", $referr, $start, $end ) );
									}
									
									if( $referr ) {
										foreach($result as $item) {
									
											echo "<div class='log-item'>";
											echo "<div class='log-referred'><a href='?page=statistics/statistics.php&type=last-all-visitor&ip={$item->ip}'>".ljmc_statistics_icons('dashicons-visibility', 'visibility')."{$item->ip}</a></div>";
											echo "<div class='log-ip'>" . date(get_option('date_format'), strtotime($item->last_counter)) . " - <a href='http://www.geoiptool.com/en/?IP={$item->ip}' target='_blank'>{$item->ip}</a></div>";
											echo "<div class='clear'></div>";
											echo "<a class='show-map' title='".__('Karte', 'ljmc_statistics')."'><div class='dashicons dashicons-location-alt'></div></a>";
											
											if( array_search( strtolower( $item->agent ), array( "chrome", "firefox", "msie", "opera", "safari" ) ) !== FALSE ){
												$agent = "<img src='".plugins_url('statistics/assets/images/').$item->agent.".png' class='log-tools' title='{$item->agent}'/>";
											} else {
												$agent = "<div class='dashicons dashicons-editor-help'></div>";
											}
											
											echo "<div class='log-agent'><a href='?page=statistics/statistics.php&type=last-all-visitor&agent={$item->agent}'>{$agent}</a>";
											
											echo "<a href='" . htmlentities($item->referred,ENT_QUOTES) . "'><div class='dashicons dashicons-admin-links'></div> " . htmlentities(substr($item->referred, 0, 100),ENT_QUOTES) . "[...]</a></div>";
											echo "</div>";
										
										}
									} else {
										arsort( $get_urls );
										$get_urls = array_slice($get_urls, $start, $end);
										
										$i = 0;
										foreach( $get_urls as $items => $value) {
											
											$i++;
											
											echo "<div class='log-item'>";
											echo "<div class='log-referred'>{$i} - <a href='?page=ljmcs_referrers_menu&referr={$items}'>{$items}</a></div>";
											echo "<div class='log-ip'>".__('Atsauces', 'ljmc_statistics').": " . number_format_i18n($value) . "</div>";
											echo "<div class='clear'></div>";
											echo "<div class='log-url'><a href='http://" . htmlentities($items,ENT_QUOTES) . "/' title='" . htmlentities($items,ENT_QUOTES) . "'><div class='dashicons dashicons-admin-links'></div> http://" . htmlentities($items,ENT_QUOTES) . "/</a></div>";
											echo "</div>";
											
										}
									}
								}
								
								echo "</div>";
							?>
					</div>
				</div>
				
				<div class="pagination-log">
					<?php if( $total > 0 ) { echo $Pagination->display(); ?>
					<p id="result-log"><?php echo ' ' . __('Lapa', 'ljmc_statistics') . ' ' . $Pagination->getCurrentPage() . ' ' . __('no', 'ljmc_statistics') . ' ' . $Pagination->getTotalPages(); ?></p>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>