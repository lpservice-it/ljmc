<?php 
if( $ljmcs_nonce_valid ) {

	if( array_key_exists( 'ljmcs_remove_plugin', $_POST ) ) {
		if( is_super_admin() ) {
			update_option('ljmc_statistics_removal', 'true' );
		}
	}
}

?>
<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('LJMC Statisitcs Removal', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2">
				<?php echo __('Uninstalling LJMC Statistics will not remove the data and settings, you can use this option to remove the LJMC Statistics data from your install before uninstalling the plugin.', 'ljmc_statistics'); ?>
				<br>
				<br>
				<?php echo __('Once you submit this form the settings will be deleted during the page load, however LJMC Statistics will still show up in your Admin menu until another page load is executed.', 'ljmc_statistics'); ?>
			</th>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="remove-plugin"><?php _e('Remove data and settings', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="remove-plugin" type="checkbox" name="ljmcs_remove_plugin">
				<label for="remove-plugin"><?php _e('Remove', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Remove data and settings, this action cannot be undone.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

	</tbody>
</table>

<?php submit_button(__('Update', 'ljmc_statistics'), 'primary', 'submit'); ?>