<?php 
global $ljmc_roles;

$role_list = $ljmc_roles->get_names();

if( $ljmcs_nonce_valid ) {

	foreach( $role_list as $role ) {
		$role_post = 'ljmcs_exclude_' . str_replace(" ", "_", strtolower($role) );

		if( array_key_exists( $role_post, $_POST ) ) { $value = $_POST[$role_post]; } else { $value = ''; }

		$new_option = str_replace( "ljmcs_", "", $role_post );
		$LJMC_Statistics->store_option($new_option, $value);

	}

	if( array_key_exists( 'ljmcs_create_honeypot', $_POST ) ) {
		$my_post = array(
			'post_type' 	=> 'page',
			'post_title'    => __('LJMC Statistics Honey Pot Page', 'ljmc_statistics' ) . ' [' . $LJMC_Statistics->Current_Date() . ']',
			'post_content'  => __('This is the honey pot for LJMC Statistics to use, do not delete.', 'ljmc_statistics' ),
			'post_status'   => 'publish',
			'post_author'   => 1,
		);
	
		$_POST['ljmcs_honeypot_postid'] = ljmc_insert_post( $my_post );
	}
	
	$ljmcs_option_list = array_merge( $ljmcs_option_list, array('ljmcs_record_exclusions','ljmcs_robotlist','ljmcs_exclude_ip','ljmcs_exclude_loginpage','ljmcs_exclude_adminpage','ljmcs_force_robot_update','ljmcs_excluded_countries','ljmcs_included_countries','ljmcs_excluded_hosts','ljmcs_robot_threshold','ljmcs_use_honeypot','ljmcs_honeypot_postid','ljmcs_exclude_feeds','ljmcs_excluded_urls','ljmcs_exclude_404s', 'ljmcs_corrupt_browser_info' ) );
	
	foreach( $ljmcs_option_list as $option ) {
		$new_option = str_replace( "ljmcs_", "", $option );

		if( array_key_exists( $option, $_POST ) ) { $value = $_POST[$option]; } else { $value = ''; }
		$LJMC_Statistics->store_option($new_option, $value);
	}
}

?>

<table class="form-table">
	<tbody>
		
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Exclusions', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<th scope="row"><label for="ljmcs-exclusions"><?php _e('Record exclusions', 'ljmc_statistics'); ?></label>:</th>
			<td>
				<input id="ljmcs-exclusions" type="checkbox" value="1" name="ljmcs_record_exclusions" <?php echo $LJMC_Statistics->get_option('record_exclusions')==true? "checked='checked'":'';?>><label for="ljmcs-exclusions"><?php _e('Enable', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('This will record all the excluded hits in a separate table with the reasons why it was excluded but no other information.  This will generate a lot of data but is useful if you want to see the total number of hits your site gets, not just actual user visits.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Exclude User Roles', 'ljmc_statistics'); ?></h3></th>
		</tr>
		<?php
			$role_option_list = '';
			
			foreach( $role_list as $role ) {
				$store_name = 'exclude_' . str_replace(" ", "_", strtolower($role) );
				$option_name = 'ljmcs_' . $store_name;
				$role_option_list .= $option_name . ',';
				
				$translated_role_name = translate_user_role($role);
		?>
		
		<tr valign="top">
			<th scope="row"><label for="<?php echo $option_name;?>"><?php echo $translated_role_name; ?>:</label></th>
			<td>
				<input id="<?php echo $option_name;?>" type="checkbox" value="1" name="<?php echo $option_name;?>" <?php echo $LJMC_Statistics->get_option($store_name)==true? "checked='checked'":'';?>><label for="<?php echo $option_name;?>"><?php _e('Exclude', 'ljmc_statistics'); ?></label>
				<p class="description"><?php echo sprintf(__('Exclude %s role from data collection.', 'ljmc_statistics'), $translated_role_name); ?></p>
			</td>
		</tr>
		<?php } ?>
		
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('IP/Robot Exclusions', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<th scope="row"><?php _e('Robot list', 'ljmc_statistics'); ?>:</th>
			<td>
				<textarea name="ljmcs_robotlist" class="code" dir="ltr" rows="10" cols="60" id="ljmcs_robotlist"><?php 
					$robotlist = $LJMC_Statistics->get_option('robotlist'); 

					include_once(dirname( __FILE__ ) . '/../../../robotslist.php');
					
					if( $robotlist == '' ) { 
						$robotlist = $ljmcs_robotlist; 
						update_option( 'ljmcs_robotlist', $robotlist );
					}

					echo htmlentities( $robotlist, ENT_QUOTES );?></textarea>
				<p class="description"><?php echo __('A list of words (one per line) to match against to detect robots.  Entries must be at least 4 characters long or they will be ignored.', 'ljmc_statistics'); ?></p>
				<a onclick="var ljmcs_robotlist = getElementById('ljmcs_robotlist'); ljmcs_robotlist.value = '<?php echo implode('\n', $ljmcs_robotarray);?>';" class="button"><?php _e('Reset to Default', 'ljmc_statistics');?></a>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><label for="force_robot_update"><?php _e('Force robot list update after upgrades', 'ljmc_statistics'); ?>:</label></th>
			<td>
				<input id="force_robot_update" type="checkbox" value="1" name="ljmcs_force_robot_update" <?php echo $LJMC_Statistics->get_option('force_robot_update')==true? "checked='checked'":'';?>><label for="force_robot_update"><?php _e('Enable', 'ljmc_statistics'); ?></label>
				<p class="description"><?php echo sprintf(__('Force the robot list to be reset to the default after an update to LJMC Statistics takes place.  Note if this option is enabled any custom robots you have added to the list will be lost.', 'ljmc_statistics'), $role); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><label for="ljmcs_robot_threshold"><?php _e('Robot visit threshold', 'ljmc_statistics'); ?>:</label></th>
			<td>
				<input id="ljmcs_robot_threshold" type="text" size="5" name="ljmcs_robot_threshold" value="<?php echo $LJMC_Statistics->get_option('robot_threshold');?>">
				<p class="description"><?php echo __('Treat visitors with more than this number of visits per day as robots.  0 = disabled.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><?php _e('Excluded IP address list', 'ljmc_statistics'); ?>:</th>
			<td>
				<textarea id="ljmcs_exclude_ip" name="ljmcs_exclude_ip" rows="5" cols="60" class="code" dir="ltr"><?php echo htmlentities( $LJMC_Statistics->get_option('exclude_ip'), ENT_QUOTES );?></textarea>
				<p class="description"><?php echo __('A list of IP addresses and subnet masks (one per line) to exclude from statistics collection (both 192.168.0.0/24 and 192.168.0.0/255.255.255.0 formats are accepted).  To specify an IP address only, use a subnet value of 32 or 255.255.255.255.', 'ljmc_statistics'); ?></p>
				<a onclick="var ljmcs_exclude_ip = getElementById('ljmcs_exclude_ip'); if( ljmcs_exclude_ip != null ) { ljmcs_exclude_ip.value = jQuery.trim( ljmcs_exclude_ip.value + '\n10.0.0.0/8' ); }" class="button"><?php _e('Add 10.0.0.0', 'ljmc_statistics');?></a>
				<a onclick="var ljmcs_exclude_ip = getElementById('ljmcs_exclude_ip'); if( ljmcs_exclude_ip != null ) { ljmcs_exclude_ip.value = jQuery.trim( ljmcs_exclude_ip.value + '\n172.16.0.0/12' ); }" class="button"><?php _e('Add 172.16.0.0', 'ljmc_statistics');?></a>
				<a onclick="var ljmcs_exclude_ip = getElementById('ljmcs_exclude_ip'); if( ljmcs_exclude_ip != null ) { ljmcs_exclude_ip.value = jQuery.trim( ljmcs_exclude_ip.value + '\n192.168.0.0/16' ); }" class="button"><?php _e('Add 192.168.0.0', 'ljmc_statistics');?></a>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><?php _e('Use honey pot', 'ljmc_statistics'); ?>:</th>
			<td>
				<input id="use_honeypot" type="checkbox" value="1" name="ljmcs_use_honeypot" <?php echo $LJMC_Statistics->get_option('use_honeypot')==true? "checked='checked'":'';?>><label for="ljmcs_use_honeypot"><?php _e('Enable', 'ljmc_statistics'); ?></label>
				<p class="description"><?php echo __('Use a honey pot page to identify robots.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><label for="honeypot_postid"><?php _e('Honey pot post id', 'ljmc_statistics'); ?>:</label></th>
			<td>
				<input id="honeypot_postid" type="text" value="<?php echo htmlentities( $LJMC_Statistics->get_option('honeypot_postid'), ENT_QUOTES );?>" size="5" name="ljmcs_honeypot_postid">
				<p class="description"><?php echo __('The post id to use for the honeypot page.', 'ljmc_statistics'); ?></p>
				<input id="ljmcs_create_honeypot" type="checkbox" value="1" name="ljmcs_create_honeypot"><label for="ljmcs_create_honeypot"><?php _e('Create a new honey pot page', 'ljmc_statistics'); ?></label>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><label for="corrupt_browser_info"><?php _e('Treat corrupt browser info as a bot', 'ljmc_statistics'); ?>:</label></th>
			<td>
				<input id="corrupt_browser_info" type="checkbox" value="1" name="ljmcs_corrupt_browser_info" <?php echo $LJMC_Statistics->get_option('corrupt_browser_info')==true? "checked='checked'":'';?>><label for="ljmcs_corrupt_browser_info"><?php _e('Enable', 'ljmc_statistics'); ?></label>
				<p class="description"><?php echo __('Treat any visitor with corrupt browser info (missing IP address or empty user agent string) as a robot.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('GeoIP Exclusions', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<th scope="row"><?php _e('Excluded countries list', 'ljmc_statistics'); ?>:</th>
			<td>
				<textarea id="ljmcs_excluded_countries" name="ljmcs_excluded_countries" rows="5" cols="10" class="code" dir="ltr"><?php echo htmlentities( $LJMC_Statistics->get_option('excluded_countries'), ENT_QUOTES );?></textarea>
				<p class="description"><?php echo __('A list of country codes (one per line, two letters each) to exclude from statistics collection.  Use "000" (three zeros) to exclude unknown countries.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row"><?php _e('Included countries list', 'ljmc_statistics'); ?>:</th>
			<td>
				<textarea id="ljmcs_included_countries" name="ljmcs_included_countries" rows="5" cols="10" class="code" dir="ltr"><?php echo htmlentities( $LJMC_Statistics->get_option('included_countries'), ENT_QUOTES );?></textarea>
				<p class="description"><?php echo __('A list of country codes (one per line, two letters each) to include in statistics collection, if this list is not empty, only visitors from the included countries will be recorded.  Use "000" (three zeros) to exclude unknown countries.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Host Exclusions', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<th scope="row"><?php _e('Excluded hosts list', 'ljmc_statistics'); ?>:</th>
			<td>
				<textarea id="ljmcs_excluded_hosts" name="ljmcs_excluded_hosts" rows="5" cols="80" class="code" dir="ltr"><?php echo htmlentities( $LJMC_Statistics->get_option('excluded_hosts'), ENT_QUOTES );?></textarea>
				<p class="description"><?php echo __('A list of fully qualified host names (ie. server.example.com, one per line) to exclude from statistics collection.', 'ljmc_statistics'); ?></p>
				<br>
				<p class="description"><?php echo __('Note: this option will NOT perform a reverse DNS lookup on each page load but instead cache the IP address for the provided hostnames for one hour.  If you are excluding dynamically assigned hosts you may find some degree of overlap when the host changes it\'s IP address and when the cache is updated resulting in some hits recorded.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Site URL Exclusions', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<th scope="row"><?php _e('Excluded login page', 'ljmc_statistics'); ?>:</th>
			<td>
				<input id="ljmcs-exclude-loginpage" type="checkbox" value="1" name="ljmcs_exclude_loginpage" <?php echo $LJMC_Statistics->get_option('exclude_loginpage')==true? "checked='checked'":'';?>><label for="ljmcs-exclude-loginpage"><?php _e('Exclude', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Exclude the login page for registering as a hit.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Excluded admin pages', 'ljmc_statistics'); ?>:</th>
			<td>
				<input id="ljmcs-exclude-adminpage" type="checkbox" value="1" name="ljmcs_exclude_adminpage" <?php echo $LJMC_Statistics->get_option('exclude_adminpage')==true? "checked='checked'":'';?>><label for="ljmcs-exclude-adminpage"><?php _e('Exclude', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Exclude the admin pages for registering as a hit.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Excluded RSS feeds', 'ljmc_statistics'); ?>:</th>
			<td>
				<input id="ljmcs-exclude-feeds" type="checkbox" value="1" name="ljmcs_exclude_feeds" <?php echo $LJMC_Statistics->get_option('exclude_feeds')==true? "checked='checked'":'';?>><label for="ljmcs-exclude-feeds"><?php _e('Exclude', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Exclude the RSS feeds for registering as a hit.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Excluded 404 pages', 'ljmc_statistics'); ?>:</th>
			<td>
				<input id="ljmcs-exclude-404s" type="checkbox" value="1" name="ljmcs_exclude_404s" <?php echo $LJMC_Statistics->get_option('exclude_404s')==true? "checked='checked'":'';?>><label for="ljmcs-exclude-404s"><?php _e('Exclude', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Exclude any URL that returns a "404 - Not Found" message.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><?php _e('Excluded URLs list', 'ljmc_statistics'); ?>:</th>
			<td>
				<textarea id="ljmcs_excluded_urls" name="ljmcs_excluded_urls" rows="5" cols="80" class="code" dir="ltr"><?php echo htmlentities( $LJMC_Statistics->get_option('excluded_urls'), ENT_QUOTES );?></textarea>
				<p class="description"><?php echo __('A list of local urls (ie. /ljmc/about, one per line) to exclude from statistics collection.', 'ljmc_statistics'); ?></p>
				<br>
				<p class="description"><?php echo __('Note: this option will NOT handle url parameters (anything after the ?), only to the script name.  Entries less than two characters will be ignored.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

	</tbody>
</table>

<?php submit_button(__('Update', 'ljmc_statistics'), 'primary', 'submit'); ?>