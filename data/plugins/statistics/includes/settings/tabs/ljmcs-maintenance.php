<?php 
if( $ljmcs_nonce_valid ) {

	$ljmcs_option_list = array('ljmcs_schedule_dbmaint', 'ljmcs_schedule_dbmaint_days', 'ljmcs_schedule_dbmaint_visitor', 'ljmcs_schedule_dbmaint_visitor_hits');
	
	foreach( $ljmcs_option_list as $option ) {
		$new_option = str_replace( "ljmcs_", "", $option );
		if( array_key_exists( $option, $_POST ) ) { $value = $_POST[$option]; } else { $value = ''; }
		$LJMC_Statistics->store_option($new_option, $value);
	}
}

?>
<script type="text/javascript">
	function DBMaintWarning() {
		var checkbox = jQuery('#ljmcs_schedule_dbmaint');
		
		if( checkbox.attr('checked') == 'checked' )
			{
			if(!confirm('<?php _e('This will permanently delete data from the database each day, are you sure you want to enable this option?', 'ljmc_statistics'); ?>'))
				checkbox.attr('checked', false);
			}
		

	}
</script>
<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Purge Old Data Daily', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ljmcs_schedule_dbmaint"><?php _e('Enabled', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="ljmcs_schedule_dbmaint" type="checkbox" name="ljmcs_schedule_dbmaint" <?php echo $LJMC_Statistics->get_option('schedule_dbmaint')==true? "checked='checked'":'';?> onclick='DBMaintWarning();'>
				<label for="ljmcs_schedule_dbmaint"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('A LJMC Cron job will be run daily to purge any data older than a set number of days.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ljmcs_schedule_dbmaint_days"><?php _e('Purge data older than', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input type="text" class="small-text code" id="ljmcs_schedule_dbmaint_days" name="ljmcs_schedule_dbmaint_days" value="<?php echo htmlentities( $LJMC_Statistics->get_option('schedule_dbmaint_days', "365"), ENT_QUOTES ); ?>"/>
				<?php _e('Days', 'ljmc_statistics'); ?>
				<p class="description"><?php echo __('The number of days to keep statistics for.  Minimum value is 30 days.  Invalid values will disable the daily maintenance.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Purge High Hit Count Visitors Daily', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="ljmcs_schedule_dbmaint_visitor"><?php _e('Enabled', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="ljmcs_schedule_dbmaint_visitor" type="checkbox" name="ljmcs_schedule_dbmaint_visitor" <?php echo $LJMC_Statistics->get_option('schedule_dbmaint_visitor')==true? "checked='checked'":'';?> onclick='DBMaintWarning();'>
				<label for="ljmcs_schedule_dbmaint_visitor"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('A LJMC Cron job will be run daily to purge any users statistics data where the user has more than the defined number of hits in a day (aka they are probably a bot).', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ljmcs_schedule_dbmaint_visitor_hits"><?php _e('Purge visitors with more than', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input type="text" class="small-text code" id="ljmcs_schedule_dbmaint_visitor_hits" name="ljmcs_schedule_dbmaint_visitor_hits" value="<?php echo htmlentities( $LJMC_Statistics->get_option('schedule_dbmaint_visitor_hits', '50'), ENT_QUOTES ); ?>"/>
				<?php _e('Hits', 'ljmc_statistics'); ?>
				<p class="description"><?php echo __('The number of hits required to delete the visitor.  Minimum value is 10 hits.  Invalid values will disable the daily maintenance.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

	</tbody>
</table>

<?php submit_button(__('Update', 'ljmc_statistics'), 'primary', 'submit'); ?>