<?php 
$selist = ljmc_statistics_searchengine_list( true );

if( $ljmcs_nonce_valid ) {

	// We need to handle a change in the report schedule manually, so check to see it has been set.
	if( array_key_exists( 'ljmcs_time_report', $_POST ) ) {
		// If the report has been changed, we need to update the schedule.
		if( $LJMC_Statistics->get_option('time_report') != $_POST['ljmcs_time_report'] ) {
			// Remove the old schedule if it exists.
			if( ljmc_next_scheduled('report_hook') ) {
				ljmc_unschedule_event(ljmc_next_scheduled('report_hook'), 'report_hook');
			}

			// Setup the new schedule, we could just let this fall through and let the code in schedule.php deal with it
			// but that would require an extra page load to start the schedule so do it here instead.
			ljmc_schedule_event(time(), $_POST['ljmcs_time_report'], 'report_hook');
		}
	}

	$ljmcs_option_list = array("ljmcs_stats_report","ljmcs_time_report","ljmcs_send_report","ljmcs_content_report","ljmcs_email_list","ljmcs_browscap_report","ljmcs_geoip_report","ljmcs_prune_report","ljmcs_upgrade_report");
	
	foreach( $ljmcs_option_list as $option ) {
		if( array_key_exists( $option, $_POST ) ) { $value = $_POST[$option]; } else { $value = ''; }

		// LJMC escapes form data no matter what the setting of magic quotes is in PHP (http://www.theblog.ca/ljmc-addslashes-magic-quotes).
		$value = stripslashes($value);
			
		$new_option = str_replace( "ljmcs_", "", $option );
		$LJMC_Statistics->store_option($new_option, $value);
	}
}

?>
<script type="text/javascript">
	function ToggleStatOptions() {
		jQuery('[id^="ljmcs_stats_report_option"]').fadeToggle();	
	}
</script>

<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Common Report Options', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<td scope="row" style="vertical-align: top;">
				<label for="email-report"><?php _e('E-mail addresses', 'ljmc_statistics'); ?>:</label>
			</td>
			
			<td>
				<input type="text" id="email_list" name="ljmcs_email_list" size="30" value="<?php if( $LJMC_Statistics->get_option('email_list') == '' ) { $LJMC_Statistics->store_option('email_list', get_bloginfo('admin_email')); } echo htmlentities( $LJMC_Statistics->get_option('email_list'), ENT_QUOTES ); ?>"/>
				<p class="description"><?php _e('A comma separated list of e-mail addresses to send reports to.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Update Reports', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<td scope="row">
				<label for="browscap-report"><?php _e('Browscap', 'ljmc_statistics'); ?>:</label>
			</td>
			
			<td>
				<input id="browscap-report" type="checkbox" value="1" name="ljmcs_browscap_report" <?php echo $LJMC_Statistics->get_option('browscap_report')==true? "checked='checked'":'';?>>
				<label for="browscap-report"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Send a report whenever the browscap.ini is updated.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<td scope="row">
				<label for="geoip-report"><?php _e('GeoIP', 'ljmc_statistics'); ?>:</label>
			</td>
			
			<td>
				<input id="geoip-report" type="checkbox" value="1" name="ljmcs_geoip_report" <?php echo $LJMC_Statistics->get_option('geoip_report')==true? "checked='checked'":'';?>>
				<label for="geoip-report"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Send a report whenever the GeoIP database is updated.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<td scope="row">
				<label for="prune-report"><?php _e('Pruning', 'ljmc_statistics'); ?>:</label>
			</td>
			
			<td>
				<input id="prune-report" type="checkbox" value="1" name="ljmcs_prune_report" <?php echo $LJMC_Statistics->get_option('prune_report')==true? "checked='checked'":'';?>>
				<label for="prune-report"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Send a report whenever the pruning of database is run.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<td scope="row">
				<label for="upgrade-report"><?php _e('Upgrade', 'ljmc_statistics'); ?>:</label>
			</td>
			
			<td>
				<input id="upgrade-report" type="checkbox" value="1" name="ljmcs_upgrade_report" <?php echo $LJMC_Statistics->get_option('upgrade_report')==true? "checked='checked'":'';?>>
				<label for="upgrade-report"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Send a report whenever the plugin is upgraded.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Statistical reporting', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="stats-report"><?php _e('Statistical reporting', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="stats-report" type="checkbox" value="1" name="ljmcs_stats_report" <?php echo $LJMC_Statistics->get_option('stats_report')==true? "checked='checked'":'';?> onClick='ToggleStatOptions();'>
				<label for="stats-report"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Enable or disable this feature', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		
		<?php if( $LJMC_Statistics->get_option('stats_report') ) { $hidden=""; } else { $hidden=" style='display: none;'"; }?>
		<tr valign="top"<?php echo $hidden;?> id='ljmcs_stats_report_option'>
			<td scope="row" style="vertical-align: top;">
				<label for="time-report"><?php _e('Schedule', 'ljmc_statistics'); ?>:</label>
			</td>
			
			<td>
				<select name="ljmcs_time_report" id="time-report">
					<option value="0" <?php selected($LJMC_Statistics->get_option('time_report'), '0'); ?>><?php _e('Please select', 'ljmc_statistics'); ?></option>
<?php
					function ljmc_statistics_schedule_sort( $a, $b ) {
						if ($a['interval'] == $b['interval']) {
							return 0;
							}
							
						return ($a['interval'] < $b['interval']) ? -1 : 1;
					}
					
					$schedules = ljmc_get_schedules();
					
					uasort( $schedules, 'ljmc_statistics_schedule_sort' );
					
					foreach( $schedules as $key => $value ) {
						echo '					<option value="' . $key . '" ' . selected($LJMC_Statistics->get_option('time_report'), $key) . '>' . $value['display'] . '</option>';
					}
?>					
				</select>
				<p class="description"><?php _e('Select how often to receive statistical report.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		
		<tr valign="top"<?php echo $hidden;?> id='ljmcs_stats_report_option'>
			<td scope="row" style="vertical-align: top;">
				<label for="send-report"><?php _e('Send reports via', 'ljmc_statistics'); ?>:</label>
			</td>
			
			<td>
				<select name="ljmcs_send_report" id="send-report">
					<option value="0" <?php selected($LJMC_Statistics->get_option('send_report'), '0'); ?>><?php _e('Please select', 'ljmc_statistics'); ?></option>
					<option value="mail" <?php selected($LJMC_Statistics->get_option('send_report'), 'mail'); ?>><?php _e('Email', 'ljmc_statistics'); ?></option>
				<?php if( is_plugin_active('ljmc-sms/ljmc-sms.php') || is_plugin_active('ljmc-sms-pro/ljmc-sms.php') ) { ?>
					<option value="sms" <?php selected($LJMC_Statistics->get_option('send_report'), 'sms'); ?>><?php _e('SMS', 'ljmc_statistics'); ?></option>
				<?php } ?>
				</select>
				<p class="description"><?php _e('Select delivery method for statistical report.', 'ljmc_statistics'); ?></p>
				
				<?php if( !is_plugin_active('ljmc-sms/ljmc-sms.php') ) { ?>
					<p class="description note"><?php echo sprintf(__('Note: To send SMS text messages please install the %s plugin.', 'ljmc_statistics'), '<a href="http://ljmc.org/extend/plugins/ljmc-sms/" target="_blank">' . __('LJMC SMS', 'ljmc_statistics') . '</a>'); ?></p>
				<?php } ?>
			</td>
		</tr>
		
		<tr valign="top"<?php echo $hidden;?> id='ljmcs_stats_report_option'>
			<td scope="row"  style="vertical-align: top;">
				<label for="content-report"><?php _e('Report body', 'ljmc_statistics'); ?>:</label>
			</td>
			
			<td>
				<?php ljmc_editor( $LJMC_Statistics->get_option('content_report'), 'content-report', array('media_buttons' => false, 'textarea_name' => 'ljmcs_content_report', 'textarea_rows' => 5) ); ?>
				<p class="description"><?php _e('Enter the contents of the report.', 'ljmc_statistics'); ?></p>
				<p class="description data">
					<?php _e('Any shortcode supported by your installation of LJMC, include all shortcodes for LJMC Statistics (see the admin manual for a list of codes available) are supported in the body of the message.  Here are some examples:', 'ljmc_statistics'); ?><br><br>
					&nbsp;&nbsp;&nbsp;&nbsp;<?php _e('User Online', 'ljmc_statistics'); ?>: <code>[ljmcstatistics stat=usersonline]</code><br>
					&nbsp;&nbsp;&nbsp;&nbsp;<?php _e('Today Visitor', 'ljmc_statistics'); ?>: <code>[ljmcstatistics stat=visitors time=today]</code><br>
					&nbsp;&nbsp;&nbsp;&nbsp;<?php _e('Today Visit', 'ljmc_statistics'); ?>: <code>[ljmcstatistics stat=visits time=today]</code><br>
					&nbsp;&nbsp;&nbsp;&nbsp;<?php _e('Yesterday Visitor', 'ljmc_statistics'); ?>: <code>[ljmcstatistics stat=visitors time=yesterday]</code><br>
					&nbsp;&nbsp;&nbsp;&nbsp;<?php _e('Yesterday Visit', 'ljmc_statistics'); ?>: <code>[ljmcstatistics stat=visits time=yesterday]</code><br>
					&nbsp;&nbsp;&nbsp;&nbsp;<?php _e('Total Visitor', 'ljmc_statistics'); ?>: <code>[ljmcstatistics stat=visitors time=total]</code><br>
					&nbsp;&nbsp;&nbsp;&nbsp;<?php _e('Total Visit', 'ljmc_statistics'); ?>: <code>[ljmcstatistics stat=visits time=total]</code><br>
				</p>
			</td>
		</tr>
	</tbody>
</table>

<?php submit_button(__('Update', 'ljmc_statistics'), 'primary', 'submit'); ?>