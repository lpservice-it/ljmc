<?php 
$selist = ljmc_statistics_searchengine_list( true );

$permalink = get_option( 'permalink_structure' );

$disable_strip_uri_parameters = false;
if( $permalink == '' || strpos( $permalink, '?' ) !== false ) {
	$disable_strip_uri_parameters = true;
}

if( $ljmcs_nonce_valid ) {
	foreach( $selist as $se ) {
		$se_post = 'ljmcs_disable_se_' . $se['tag'];
		
		if( array_key_exists( $se_post, $_POST ) ) { $value = $_POST[$se_post]; } else { $value = ''; }
		$new_option = str_replace( "ljmcs_", "", $se_post );
		$LJMC_Statistics->store_option($new_option, $value);
	}

	$ljmcs_option_list = array('ljmcs_useronline','ljmcs_visits','ljmcs_visitors','ljmcs_pages','ljmcs_track_all_pages','ljmcs_disable_column','ljmcs_check_online','ljmcs_menu_bar','ljmcs_coefficient','ljmcs_chart_totals','ljmcs_store_ua','ljmcs_hide_notices','ljmcs_delete_manual','ljmcs_hash_ips', 'ljmcs_all_online', 'ljmcs_strip_uri_parameters', 'ljmcs_override_language','ljmcs_addsearchwords' );
	
	// If the IP hash's are enabled, disable storing the complete user agent.
	if( array_key_exists( 'ljmcs_hash_ips', $_POST ) ) { $_POST['ljmcs_store_ua'] = ''; }
	
	// We need to check the permalink format for the strip_uri_parameters option, if the permalink is the default or contains uri parameters, we can't strip them.
	if( $disable_strip_uri_parameters ) { $_POST['ljmcs_strip_uri_parameters'] = ''; }
	
	foreach( $ljmcs_option_list as $option ) {
		if( array_key_exists( $option, $_POST ) ) { $value = $_POST[$option]; } else { $value = ''; }
		$new_option = str_replace( "ljmcs_", "", $option );
		$LJMC_Statistics->store_option($new_option, $value);
	}

	if( $LJMC_Statistics->get_option('delete_manual') == true ) {
		$filepath = realpath( plugin_dir_path(__FILE__) . "../../../" ) . "/";

		if( file_exists( $filepath . LJMC_STATISTICS_MANUAL . 'html' ) ) { unlink( $filepath . LJMC_STATISTICS_MANUAL . 'html' ); }
		if( file_exists( $filepath . LJMC_STATISTICS_MANUAL . 'odt' ) ) { unlink( $filepath . LJMC_STATISTICS_MANUAL . 'odt' ); }
	}
	
}

?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#delete_manual").click(function(){
			if(!this.checked)
				return;
				
			var agree = confirm('<?php _e('This will delete the manual when you save the settings, are you sure?', 'ljmc_statistics'); ?>');

			if(!agree)
				jQuery("#delete_manual").attr("checked", false);
		
		});
	});
	
	function ToggleStatOptions() {
		jQuery('[id^="ljmcs_stats_report_option"]').fadeToggle();	
	}
</script>

<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('IP Addresses', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="useronline"><?php _e('Hash IP Addresses', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="hash_ips" type="checkbox" value="1" name="ljmcs_hash_ips" <?php echo $LJMC_Statistics->get_option('hash_ips')==true? "checked='checked'":'';?>>
				<label for="hash_ips"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('This feature will not store IP addresses in the database but instead used a unique hash.  The "Store entire user agent string" setting will be disabled if this is selected.  You will not be able to recover the IP addresses in the future to recover location information if this is enabled.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Users Online', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="useronline"><?php _e('User online', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="useronline" type="checkbox" value="1" name="ljmcs_useronline" <?php echo $LJMC_Statistics->get_option('useronline')==true? "checked='checked'":'';?>>
				<label for="useronline"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Enable or disable this feature', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="check_online"><?php _e('Check for online users every', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input type="text" class="small-text code" id="check_online" name="ljmcs_check_online" value="<?php echo htmlentities($LJMC_Statistics->get_option('check_online'), ENT_QUOTES ); ?>"/>
				<?php _e('Second', 'ljmc_statistics'); ?>
				<p class="description"><?php echo sprintf(__('Time for the check accurate online user in the site. Now: %s Second', 'ljmc_statistics'), $LJMC_Statistics->get_option('check_online')); ?></p>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="useronline"><?php _e('Record all user', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="allonline" type="checkbox" value="1" name="ljmcs_all_online" <?php echo $LJMC_Statistics->get_option('all_online')==true? "checked='checked'":'';?>>
				<label for="allonline"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Ignores the exclusion settings and records all users that are online (including self referrals and robots).  Should only be used for troubleshooting.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Visits', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="visits"><?php _e('Visits', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="visits" type="checkbox" value="1" name="ljmcs_visits" <?php echo $LJMC_Statistics->get_option('visits')==true? "checked='checked'":'';?>>
				<label for="visits"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Enable or disable this feature', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Visitors', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="visitors"><?php _e('Visitors', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="visitors" type="checkbox" value="1" name="ljmcs_visitors" <?php echo $LJMC_Statistics->get_option('visitors')==true? "checked='checked'":'';?>>
				<label for="visitors"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Enable or disable this feature', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="visitors"><?php _e('Store entire user agent string', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="store_ua" type="checkbox" value="1" name="ljmcs_store_ua" <?php echo $LJMC_Statistics->get_option('store_ua')==true? "checked='checked'":'';?>>
				<label for="store_ua"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Only enabled for debugging', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="coefficient"><?php _e('Coefficient per visitor', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input type="text" class="small-text code" id="coefficient" name="ljmcs_coefficient" value="<?php echo htmlentities($LJMC_Statistics->get_option('coefficient'), ENT_QUOTES ); ?>"/>
				<p class="description"><?php echo sprintf(__('For each visit to account for several hits. Currently %s.', 'ljmc_statistics'), $LJMC_Statistics->get_option('coefficient')); ?></p>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Pages', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="pages"><?php _e('Pages', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="pages" type="checkbox" value="1" name="ljmcs_pages" <?php echo $LJMC_Statistics->get_option('pages')==true? "checked='checked'":'';?>>
				<label for="pages"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Enable or disable this feature', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="pages"><?php _e('Track all pages', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="all_pages" type="checkbox" value="1" name="ljmcs_track_all_pages" <?php echo $LJMC_Statistics->get_option('track_all_pages')==true? "checked='checked'":'';?>>
				<label for="all_pages"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Enable or disable this feature', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

<?php 
	if( !$disable_strip_uri_parameters ) { 
?>
		<tr valign="top">
			<th scope="row">
				<label for="pages"><?php _e('Strip parameters from URI', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="strip_uri_parameters" type="checkbox" value="1" name="ljmcs_strip_uri_parameters" <?php echo $LJMC_Statistics->get_option('strip_uri_parameters')==true? "checked='checked'":'';?>>
				<label for="strip_uri_parameters"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('This will remove anything after the ? in a URL.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
<?php
	}
?>
		<tr valign="top">
			<th scope="row">
				<label for="pages"><?php _e('Disable hits column in post/pages list', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="disable_column" type="checkbox" value="1" name="ljmcs_disable_column" <?php echo $LJMC_Statistics->get_option('disable_column')==true? "checked='checked'":'';?>>
				<label for="disable_column"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Enable or disable this feature', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Miscellaneous', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="menu-bar"><?php _e('Show stats in menu bar', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<select name="ljmcs_menu_bar" id="menu-bar">
					<option value="0" <?php selected($LJMC_Statistics->get_option('menu_bar'), '0'); ?>><?php _e('No', 'ljmc_statistics'); ?></option>
					<option value="1" <?php selected($LJMC_Statistics->get_option('menu_bar'), '1'); ?>><?php _e('Yes', 'ljmc_statistics'); ?></option>
				</select>
				<p class="description"><?php _e('Show stats in admin menu bar', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="hide_notices"><?php _e('Hide admin notices about non active features', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="hide_notices" type="checkbox" value="1" name="ljmcs_hide_notices" <?php echo $LJMC_Statistics->get_option('hide_notices')==true? "checked='checked'":'';?>>
				<label for="store_ua"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('By default LJMC Statistics displays an alert if any of the core features are disabled on every admin page, this option will disable these notices.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="hide_notices"><?php _e('Delete the manual', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="delete_manual" type="checkbox" value="1" name="ljmcs_delete_manual" <?php echo $LJMC_Statistics->get_option('delete_manual')==true? "checked='checked'":'';?>>
				<label for="delete_manual"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('By default LJMC Statistics stores the admin manual in the plugin directory (~5 meg), if this option is enabled it will be deleted now and during upgrades in the future.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Search Engines', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="hide_notices"><?php _e('Add page title to empty search words', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="addsearchwords" type="checkbox" value="1" name="ljmcs_addsearchwords" <?php echo $LJMC_Statistics->get_option('addsearchwords')==true? "checked='checked'":'';?>>
				<label for="addsearchwords"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('If a search engine is identified as the referrer but it does not include the search query this option will substitute the page title in quotes preceded by "~:" as the search query to help identify what the user may have been searching for.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2">
				<p class="description"><?php _e('Disabling all search engines is not allowed, doing so will result in all search engines being active.', 'ljmc_statistics');?></p>
			</th>
		</tr>
		<?php
			$se_option_list = '';
		
			foreach( $selist as $se ) {
				$option_name = 'ljmcs_disable_se_' . $se['tag'];
				$store_name = 'disable_se_' . $se['tag'];
				$se_option_list .= $option_name . ',';
		?>
		
		<tr valign="top">
			<th scope="row"><label for="<?php echo $option_name;?>"><?php _e($se['name'], 'ljmc_statistics'); ?>:</label></th>
			<td>
				<input id="<?php echo $option_name;?>" type="checkbox" value="1" name="<?php echo $option_name;?>" <?php echo $LJMC_Statistics->get_option($store_name)==true? "checked='checked'":'';?>><label for="<?php echo $option_name;?>"><?php _e('disable', 'ljmc_statistics'); ?></label>
				<p class="description"><?php echo sprintf(__('Disable %s from data collection and reporting.', 'ljmc_statistics'), $se['name']); ?></p>
			</td>
		</tr>
		<?php } ?>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Charts', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="chart-totals"><?php _e('Include totals', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="chart-totals" type="checkbox" value="1" name="ljmcs_chart_totals" <?php echo $LJMC_Statistics->get_option('chart_totals')==true? "checked='checked'":'';?>>
				<label for="chart-totals"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Add a total line to charts with multiple values, like the search engine referrals', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Languages', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="chart-totals"><?php _e('Force English', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="override-language" type="checkbox" value="1" name="ljmcs_override_language" <?php echo $LJMC_Statistics->get_option('override_language')==true? "checked='checked'":'';?>>
				<label for="override-language"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Do not use the translations and instead use the English defaults for LJMC Statistics (requires two page loads)', 'ljmc_statistics'); ?></p>
			</td>
		</tr>
		
	</tbody>
</table>

<?php submit_button(__('Update', 'ljmc_statistics'), 'primary', 'submit'); ?>