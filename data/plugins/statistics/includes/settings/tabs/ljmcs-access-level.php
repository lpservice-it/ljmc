<?php 
global $ljmc_roles;

$role_list = $ljmc_roles->get_names();

if( $ljmcs_nonce_valid ) {

	$ljmcs_option_list = array_merge( $ljmcs_option_list, array('ljmcs_read_capability','ljmcs_manage_capability' ) );
	
	foreach( $ljmcs_option_list as $option ) {
		$new_option = str_replace( "ljmcs_", "", $option );

		if( array_key_exists( $option, $_POST ) ) { $value = $_POST[$option]; } else { $value = ''; }
		$LJMC_Statistics->store_option($new_option, $value);
	}
}

?>

<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Access Levels', 'ljmc_statistics'); ?></h3></th>
		</tr>
		<?php
			global $ljmc_roles;

			$role_list = $ljmc_roles->get_names();

			foreach( $ljmc_roles->roles as $role ) {
			
				$cap_list = $role['capabilities'];
				
				foreach( $cap_list as $key => $cap ) {
					if( substr($key,0,6) != 'level_' ) {
						$all_caps[$key] = 1;
					}
				}
			}
			
			ksort( $all_caps );
			
			$read_cap = $LJMC_Statistics->get_option('read_capability','manage_options');
			$option_list = '';
			
			foreach( $all_caps as $key => $cap ) {
				if( $key == $read_cap ) { $selected = " SELECTED"; } else { $selected = ""; }
				$option_list .= "<option value='{$key}'{$selected}>{$key}</option>";
			}
		?>
		<tr valign="top">
			<th scope="row"><label for="ljmcs_read_capability"><?php _e('Required user level to view LJMC Statistics', 'ljmc_statistics')?>:</label></th>
			<td>
				<select id="ljmcs_read_capability" name="ljmcs_read_capability"><?php echo $option_list;?></select>
			</td>
		</tr>

		<?php
			$manage_cap = $LJMC_Statistics->get_option('manage_capability','manage_options');
			
			foreach( $all_caps as $key => $cap ) {
				if( $key == $manage_cap ) { $selected = " SELECTED"; } else { $selected = ""; }
				$option_list .= "<option value='{$key}'{$selected}>{$key}</option>";
			}
		?>
		<tr valign="top">
			<th scope="row"><label for="ljmcs_manage_capability"><?php _e('Required user level to manage LJMC Statistics', 'ljmc_statistics')?>:</label></th>
			<td>
				<select id="ljmcs_manage_capability" name="ljmcs_manage_capability"><?php echo $option_list;?></select>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row" colspan="2">
				<p class="description"><?php echo sprintf(__('See the %s for details on capability levels.', 'ljmc_statistics'), '<a target=_blank href="http://codex.ljmc.org/Roles_and_Capabilities">' . __('LJMC Roles and Capabilities page', 'ljmc_statistics') . '</a>'); ?></p>
				<p class="description"><?php echo __('Hint: manage_network = Super Admin Network, manage_options = Administrator, edit_others_posts = Editor, publish_posts = Author, edit_posts = Contributor, read = Everyone.', 'ljmc_statistics'); ?></p>
				<p class="description"><?php echo __('Each of the above casscades the rights upwards in the default LJMC configuration.  So for example selecting publish_posts grants the right to Authors, Editors, Admins and Super Admins.', 'ljmc_statistics'); ?></p>
				<p class="description"><?php echo sprintf(__('If you need a more robust solution to delegate access you might want to look at %s in the LJMC plugin directory.', 'ljmc_statistics'), '<a href="http://ljmc.org/plugins/capability-manager-enhanced/" target=_blank>Capability Manager Enhanced</a>'); ?></p>
			</th>
		</tr>
		
	</tbody>
</table>

<?php submit_button(__('Update', 'ljmc_statistics'), 'primary', 'submit'); ?>