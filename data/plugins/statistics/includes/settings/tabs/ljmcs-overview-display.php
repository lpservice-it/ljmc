<?php
	if( !$LJMC_Statistics->isset_user_option('overview_display') ) {
		$LJMC_Statistics->store_user_option('overview_display', array(
			'A' => array(
				1 => 'summary', 
				2 => 'browsers',
				3 => 'referring',
				4 => 'countries',
				5 => 'about' 
			),
			'B' => 	array( 
				1 => 'map',
				2 => 'hits',
				3 => 'search',
				4 => 'words',
				5 => 'pages',
				6 => 'recent' 
			),
		));
	}
	
	$column_a_list = array(
		'none'		=> __('None', 'ljmc_statistics'),
		'summary' 	=> __('Summary Statistics', 'ljmc_statistics'),
		'browsers' 	=> __('Browsers', 'ljmc_statistics'),
		'referring' => __('Top Referring Sites', 'ljmc_statistics'),
		'countries' => __('Top 10 Countries', 'ljmc_statistics'),
		'about' 	=> __('About', 'ljmc_statistics'),
	);
	
	$column_b_list = array(
		'none'			=> __('None', 'ljmc_statistics'),
		'map' 			=> __('Map', 'ljmc_statistics'),
		'hits' 			=> __('Hits Statistical Chart', 'ljmc_statistics'),
		'top.visitors' 	=> __('Top 10 Visitors Today', 'ljmc_statistics'),
		'search' 		=> __('Search Engine Referrers Statistical Chart', 'ljmc_statistics'),
		'words' 		=> __('Latest Search Words', 'ljmc_statistics'),
		'pages' 		=> __('Top Pages Visited', 'ljmc_statistics'),
		'recent' 		=> __('Recent Visitors', 'ljmc_statistics'),
	);
	
	if( $ljmcs_nonce_valid ) {
		$ljmcs_option_list = array('ljmcs_disable_map','ljmcs_google_coordinates','ljmcs_map_type','ljmcs_disable_dashboard','ljmcs_disable_editor');
		
		foreach( $ljmcs_option_list as $option ) {
			$new_option = str_replace( "ljmcs_", "", $option );

			if( array_key_exists( $option, $_POST ) ) { $value = $_POST[$option]; } else { $value = ''; }

			$LJMC_Statistics->store_option($new_option, $value);
		}
		
		for( $i = 1; $i < count( $column_a_list ); $i++ ) {
			$display_array['A'][$i] = '';
			if( array_key_exists( $_POST['ljmcs_display']['A'][$i], $column_a_list) ) { $display_array['A'][$i] = $_POST['ljmcs_display']['A'][$i]; }
		}
		
		for( $i = 1; $i < count( $column_b_list ); $i++) {
			$display_array['B'][$i] = '';
			if( array_key_exists( $_POST['ljmcs_display']['B'][$i], $column_b_list) ) { $display_array['B'][$i] = $_POST['ljmcs_display']['B'][$i]; }
		}
		
		$LJMC_Statistics->store_user_option('overview_display', $display_array );

	}

// Only display the global options if the user is an administrator.
if( $ljmcs_admin ) {
?>
<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Dashboard', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><?php _e('The following items are global to all users.', 'ljmc_statistics');?></td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="disable-map"><?php _e('Disable dashboard widgets', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="disable-dashboard" type="checkbox" value="1" name="ljmcs_disable_dashboard" <?php echo $LJMC_Statistics->get_option('disable_dashboard')==true? "checked='checked'":'';?>>
				<label for="disable-dashboard"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Disable the dashboard widgets.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Page/Post Editor', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><?php _e('The following items are global to all users.', 'ljmc_statistics');?></td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="disable-map"><?php _e('Disable post/page editor widget', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="disable-editor" type="checkbox" value="1" name="ljmcs_disable_editor" <?php echo $LJMC_Statistics->get_option('disable_editor')==true? "checked='checked'":'';?>>
				<label for="disable-editor"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Disable the page/post editor widget.', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Map', 'ljmc_statistics'); ?></h3></th>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><?php _e('The following items are global to all users.', 'ljmc_statistics');?></td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ljmcs_map_type"><?php _e('Map type', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<select name="ljmcs_map_type">
				<?php
					foreach( array( __('Google', 'ljmc_statistics') => 'google', __('JQVMap', 'ljmc_statistics') => 'jqvmap') as $key => $value ) {
						echo "<option value=\"$value\"";
						if( $LJMC_Statistics->get_option('map_type') == $value ) { echo ' SELECTED'; }
						echo ">$key</option>";
					}
				?>
				</select>
				<p class="description"><?php _e('The "Google" option will use Google\'s mapping service to plot the recent visitors (requires access to Google).', 'ljmc_statistics'); ?></p>
				<p class="description"><?php _e('The "JQVMap" option will use JQVMap javascript mapping library to plot the recent visitors (requires no extenral services).', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="disable-map"><?php _e('Disable map', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="disable-map" type="checkbox" value="1" name="ljmcs_disable_map" <?php echo $LJMC_Statistics->get_option('disable_map')==true? "checked='checked'":'';?>>
				<label for="disable-map"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('Disable the map display', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="google-coordinates"><?php _e('Get country location from Google', 'ljmc_statistics'); ?>:</label>
			</th>
			
			<td>
				<input id="google-coordinates" type="checkbox" value="1" name="ljmcs_google_coordinates" <?php echo $LJMC_Statistics->get_option('google_coordinates')==true? "checked='checked'":'';?>>
				<label for="google-coordinates"><?php _e('Active', 'ljmc_statistics'); ?></label>
				<p class="description"><?php _e('This feature may cause a performance degradation when viewing statistics and is only valid if the map type is set to "Google".', 'ljmc_statistics'); ?></p>
			</td>
		</tr>

	</tbody>
</table>	
<?php } ?>

<table class="form-table">
	<tbody>
		<tr valign="top">
			<th scope="row" colspan="2"><h3><?php _e('Overview Widgets to Display', 'ljmc_statistics'); ?></h3></th>
		</tr>

		<tr valign="top">
			<td scope="row" colspan="3"><?php _e('The following items are unique to each user.  If you do not select the \'About\' widget it will automatically be displayed in the last positon of column A.', 'ljmc_statistics');?></td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<?php _e('Slot', 'ljmc_statistics'); ?>
			</th>
			
			<th>
				<?php _e('Column A', 'ljmc_statistics'); ?>
			</th>
			
			<th>
				<?php _e('Column B', 'ljmc_statistics'); ?>
			</th>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<?php _e('Slot 1', 'ljmc_statistics'); ?>
			</th>
			
			<td>
				<select name="ljmcs_display[A][1]">
				<?php
					foreach( $column_a_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['A'][1] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
			
			<td>
				<select name="ljmcs_display[B][1]">
				<?php
					foreach( $column_b_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['B'][1] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<?php _e('Slot 2', 'ljmc_statistics'); ?>
			</th>
			
			<td>
				<select name="ljmcs_display[A][2]">
				<?php
					foreach( $column_a_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['A'][2] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
			
			<td>
				<select name="ljmcs_display[B][2]">
				<?php
					foreach( $column_b_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['B'][2] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<?php _e('Slot 3', 'ljmc_statistics'); ?>
			</th>
			
			<td>
				<select name="ljmcs_display[A][3]">
				<?php
					foreach( $column_a_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['A'][3] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
			
			<td>
				<select name="ljmcs_display[B][3]">
				<?php
					foreach( $column_b_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['B'][3] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<?php _e('Slot 4', 'ljmc_statistics'); ?>
			</th>
			
			<td>
				<select name="ljmcs_display[A][4]">
				<?php
					foreach( $column_a_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['A'][4] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
			
			<td>
				<select name="ljmcs_display[B][4]">
				<?php
					foreach( $column_b_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['B'][4] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<?php _e('Slot 5', 'ljmc_statistics'); ?>
			</th>
			
			<td>
				<select name="ljmcs_display[A][5]">
				<?php
					foreach( $column_a_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['A'][5] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
			
			<td>
				<select name="ljmcs_display[B][5]">
				<?php
					foreach( $column_b_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['B'][5] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<?php _e('Slot 6', 'ljmc_statistics'); ?>
			</th>
			
			<td>
				<?php _e('N/A', 'ljmc_statistics');?>
			</td>
			
			<td>
				<select name="ljmcs_display[B][6]">
				<?php
					foreach( $column_b_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['B'][6] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<?php _e('Slot 7', 'ljmc_statistics'); ?>
			</th>
			
			<td>
				<?php _e('N/A', 'ljmc_statistics');?>
			</td>
			
			<td>
				<select name="ljmcs_display[B][7]">
				<?php
					foreach( $column_b_list as $key => $value ) {
						echo "<option value=\"$key\"";
						if( $LJMC_Statistics->user_options['overview_display']['B'][7] == $key ) { echo ' SELECTED'; }
						echo ">$value</option>";
					}
				?>
				</select>
			</td>
		</tr>
	</tbody>
</table>

<?php submit_button(__('Update', 'ljmc_statistics'), 'primary', 'submit'); ?>