<table class="form-table">
	<tbody>
		<tr valign="top">
			<td scope="row" align="center"><img src="<?php echo plugins_url('statistics/assets/images/logo-250.png'); ?>"></td>
		</tr>

		<tr valign="top">
			<td scope="row" align="center"><h2><?php echo sprintf(__('LJMC Statistics V%s', 'ljmc_statistics'), LJMC_STATISTICS_VERSION); ?></h2></td>
		</tr>

		<tr valign="top">
			<td scope="row" align="center"><?php echo sprintf(__('This product includes GeoLite2 data created by MaxMind, available from %s.', 'ljmc_statistics'), '<a href="http://www.maxmind.com" target=_blank>http://www.maxmind.com</a>'); ?></td>
		</tr>

		<tr valign="top">
			<td scope="row" align="center"><hr /></td>
		</tr>

		<tr valign="top">
			<td scope="row" colspan="2"><h2><?php _e('Donate', 'ljmc_statistics'); ?></h2></td>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><?php echo sprintf( __('Fell like showing us how much you enjoy LJMC Statistics?  Drop by our %s page and show us some love!', 'ljmc_statistics'), '<a href="http://statistics.com/donate" target="_blank">' . __('donation', 'ljmc_statistics') . '</a>');?></td>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><h2><?php _e('Visit Us Online', 'ljmc_statistics'); ?></h2></td>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><?php echo sprintf( __('Come visit our great new %s and keep up to date on the latest news about LJMC Statistics.', 'ljmc_statistics'), '<a href="http://statistics.com" target="_blank">' . __('website', 'ljmc_statistics') . '</a>');?></td>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><h2><?php _e('Rate and Review at LJMC.org', 'ljmc_statistics'); ?></h2></td>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><?php _e('Thanks for installing LJMC Statistics, we encourage you to submit a ', 'ljmc_statistics');?> <a href="http://ljmc.org/support/view/plugin-reviews/statistics" target="_blank"><?php _e('rating and review', 'ljmc_statistics'); ?></a> <?php _e('over at LJMC.org.  Your feedback is greatly appreciated!','ljmc_statistics');?></td>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><h2><?php _e('Translations', 'ljmc_statistics'); ?></h2></td>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><?php echo sprintf( __('LJMC Statistics supports internationalization and we encourage our users to submit translations, please visit our %s to see the current status and %s if you would like to help.', 'ljmc_statistics'), '<a href="http://statistics.com/translations/" target="_blank">' . __('translation collaboration site', 'ljmc_statistics') . '</a>', '<a href="http://statistics.com/contact/" target="_blank">' . __( 'drop us a line', 'ljmc_statistics') . '</a>');?></td>
		</tr>
		
		<tr valign="top">
			<td scope="row" colspan="2"><h2><?php _e('Support', 'ljmc_statistics'); ?></h2></td>
		</tr>

		<tr valign="top">
			<td scope="row" colspan="2">
				<p><?php _e("We're sorry you're having problem with LJMC Statistics and we're happy to help out.  Here are a few things to do before contacting us:", 'ljmc_statistics'); ?></p>

				<ul style="list-style-type: disc; list-style-position: inside; padding-left: 25px;">
					<li><?php echo sprintf( __('Have you read the %s?', 'ljmc_statistics' ), '<a title="' . __('FAQs', 'ljmc_statistics') . '" href="http://statistics.com/?page_id=19" target="_blank">' . __('FAQs', 'ljmc_statistics'). '</a>');?></li>
					<li><?php echo sprintf( __('Have you read the %s?', 'ljmc_statistics' ), '<a title="' . __('manual', 'ljmc_statistics') . '" href="?page=ljmcs_manual_menu">' . __('manual', 'ljmc_statistics') . '</a>');?></li>
					<li><?php echo sprintf( __('Have you search the %s for a similar issue?', 'ljmc_statistics' ), '<a href="http://ljmc.org/support/plugin/statistics" target="_blank">' . __('support forum', 'ljmc_statistics') . '</a>');?></li>
					<li><?php _e('Have you search the Internet for any error messages you are receiving?', 'ljmc_statistics' );?></li>
					<li><?php _e('Make sure you have access to your PHP error logs.', 'ljmc_statistics' );?></li>
				</ul>

				<p><?php _e('And a few things to double-check:', 'ljmc_statistics' );?></p>

				<ul style="list-style-type: disc; list-style-position: inside; padding-left: 25px;">
					<li><?php _e('How\'s your memory_limit in php.ini?', 'ljmc_statistics' );?></li>
					<li><?php _e('Have you tried disabling any other plugins you may have installed?', 'ljmc_statistics' );?></li>
					<li><?php _e('Have you tried using the default LJMC theme?', 'ljmc_statistics' );?></li>
					<li><?php _e('Have you double checked the plugin settings?', 'ljmc_statistics' );?></li>
					<li><?php _e('Do you have all the required PHP extensions installed?', 'ljmc_statistics' );?></li>
					<li><?php _e('Are you getting a blank or incomplete page displayed in your browser?  Did you view the source for the page and check for any fatal errors?', 'ljmc_statistics' );?></li>
					<li><?php _e('Have you checked your PHP and web server error logs?', 'ljmc_statistics' );?></li>
				</ul>

				<p><?php _e('Still not having any luck?', 'ljmc_statistics' );?> <?php echo sprintf(__('Then please open a new thread on the %s and we\'ll respond as soon as possible.', 'ljmc_statistics' ), '<a href="http://ljmc.org/support/plugin/statistics" target="_blank">' . __('LJMC.org support forum', 'ljmc_statistics') . '</a>');?></p>

				<p><br /></p>
				
				<p><?php echo sprintf( __('Alternatively %s support is available as well.', 'ljmc_statistics' ), '<a href="http://forum.ljmc-parsi.com/forum/17-%D9%85%D8%B4%DA%A9%D9%84%D8%A7%D8%AA-%D8%AF%DB%8C%DA%AF%D8%B1/" target="_blank">' . __('Farsi', 'ljmc_statistics' ) .'</a>');?></p>
			</td>
		</tr>

	</tbody>
</table>