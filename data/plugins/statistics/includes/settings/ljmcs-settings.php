<?php 
$ljmcs_nonce_valid = false;

if( array_key_exists( 'statistics-nonce', $_POST ) ) {
	if( ljmc_verify_nonce( $_POST['statistics-nonce'], 'update-options' ) ) { $ljmcs_nonce_valid = true; }
}

$ljmcs_admin = false;

if(current_user_can(ljmc_statistics_validate_capability($LJMC_Statistics->get_option('manage_capability', 'manage_options')))) {
	$ljmcs_admin = true;
}

if( $ljmcs_admin === false ) { $ljmcs_admin = 0; }

$selected_tab = "";
if( array_key_exists( 'tab', $_GET ) ) { $selected_tab = $_GET['tab']; }

switch( $selected_tab )
	{
	case 'notifications':
		if( $ljmcs_admin ) { $current_tab = 1; } else { $current_tab = 0; }
		break;
	case 'overview':
		if( $ljmcs_admin ) { $current_tab = 2; } else { $current_tab = 0; }
		break;
	case 'access':
		if( $ljmcs_admin ) { $current_tab = 3; } else { $current_tab = 0; }
		break;
	case 'exclusions':
		if( $ljmcs_admin ) { $current_tab = 4; } else { $current_tab = 0; }
		break;
	case 'externals':
		if( $ljmcs_admin ) { $current_tab = 5; } else { $current_tab = 0; }
		break;
	case 'maintenance':
		if( $ljmcs_admin ) { $current_tab = 6; } else { $current_tab = 0; }
		break;
	case 'removal':
		if( $ljmcs_admin ) { $current_tab = 7; } else { $current_tab = 0; }
		break;
	case 'about':
		if( $ljmcs_admin ) { $current_tab = 8; } else { $current_tab = 1; }
		break;
	default:
		$current_tab = 0;

	}
?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#tabs").tabs();
		<?php if( $current_tab != 0 ) { echo 'jQuery("#tabs").tabs("option", "active",' . $current_tab. ');' . "\n"; }?>
		jQuery("#ljmcs_update_button").click(function() {
			var ljmcs_admin = <?php echo $ljmcs_admin;?>;
			var tab = '';
			
			switch( jQuery("#tabs").tabs("option", "active") ) {
				case 0:
					if( ljmcs_admin == 1 ) { tab = 'general'; } else { tab = 'overview'; }
					break;
				case 1:
					if( ljmcs_admin == 1 ) { tab = 'notifications'; } else { tab = 'about'; }
					break;
				case 2:
					if( ljmcs_admin == 1 ) { tab = 'overview'; } else { tab = 'about'; }
					break;
				case 3:
					if( ljmcs_admin == 1 ) { tab = 'access'; } else { tab = 'about'; }
					break;
				case 4:
					if( ljmcs_admin == 1 ) { tab = 'exclusions'; } else { tab = 'about'; }
					break;
				case 5:
					if( ljmcs_admin == 1 ) { tab = 'Externals'; } else { tab = 'about'; }
					break;
				case 6:
					if( ljmcs_admin == 1 ) { tab = 'maintenance'; } else { tab = 'about'; }
					break;
				case 7:
					if( ljmcs_admin == 1 ) { tab = 'removal'; } else { tab = 'about'; }
					break;
				case 8:
					tab = 'about';
					break;
			}
			
			var clickurl = jQuery(location).attr('href') + '&tab=' + tab;
			
			jQuery('#ljmcs_settings_form').attr('action', clickurl).submit();
		});
	} );
</script>
<a name="top"></a>
<div class="wrap">
	<form id="ljmcs_settings_form" method="post">
		<?php ljmc_nonce_field('update-options', 'statistics-nonce');?>
		<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
			<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
<?php if( $ljmcs_admin ) { ?>				<li class="ui-state-default ui-corner-top"><a class="ui-tabs-anchor" href="#general-settings"><span><?php _e('General', 'ljmc_statistics'); ?></span></a></li><?php } ?>
<?php if( $ljmcs_admin ) { ?>				<li class="ui-state-default ui-corner-top"><a class="ui-tabs-anchor" href="#notifications-settings"><span><?php _e('Notifications', 'ljmc_statistics'); ?></span></a></li><?php } ?>
				<li class="ui-state-default ui-corner-top"><a class="ui-tabs-anchor" href="#overview-display-settings"><span><?php _e('Dashboard/Overview', 'ljmc_statistics'); ?></span></a></li>
<?php if( $ljmcs_admin ) { ?>				<li class="ui-state-default ui-corner-top"><a class="ui-tabs-anchor" href="#access-settings"><span><?php _e('Access Levels', 'ljmc_statistics'); ?></span></a></li><?php } ?>
<?php if( $ljmcs_admin ) { ?>				<li class="ui-state-default ui-corner-top"><a class="ui-tabs-anchor" href="#exclusions-settings"><span><?php _e('Exclusions', 'ljmc_statistics'); ?></span></a></li><?php } ?>
<?php if( $ljmcs_admin ) { ?>				<li class="ui-state-default ui-corner-top"><a class="ui-tabs-anchor" href="#externals-settings"><span><?php _e('Externals', 'ljmc_statistics'); ?></span></a></li><?php } ?>
<?php if( $ljmcs_admin ) { ?>				<li class="ui-state-default ui-corner-top"><a class="ui-tabs-anchor" href="#maintenance-settings"><span><?php _e('Maintenance', 'ljmc_statistics'); ?></span></a></li><?php } ?>
<?php if( $ljmcs_admin ) { ?>				<li class="ui-state-default ui-corner-top"><a class="ui-tabs-anchor" href="#removal-settings"><span><?php _e('Removal', 'ljmc_statistics'); ?></span></a></li><?php } ?>
				<li class="ui-state-default ui-corner-top"><a class="ui-tabs-anchor" href="#about"><span><?php _e('About', 'ljmc_statistics'); ?></span></a></li>
			</ul>

			<div id="general-settings">
			<?php if( $ljmcs_admin ) { include( dirname( __FILE__ ) . '/tabs/ljmcs-general.php' ); } ?>
			</div>
		
			<div id="notifications-settings">
			<?php if( $ljmcs_admin ) { include( dirname( __FILE__ ) . '/tabs/ljmcs-notifications.php' ); } ?>
			</div>

			<div id="overview-display-settings">
			<?php include( dirname( __FILE__ ) . '/tabs/ljmcs-overview-display.php' ); ?>
			</div>

			<div id="access-settings">
			<?php if( $ljmcs_admin ) { include( dirname( __FILE__ ) . '/tabs/ljmcs-access-level.php' ); } ?>
			</div>

			<div id="exclusions-settings">
			<?php if( $ljmcs_admin ) { include( dirname( __FILE__ ) . '/tabs/ljmcs-exclusions.php' ); } ?>
			</div>

			<div id="externals-settings">
			<?php if( $ljmcs_admin ) { include( dirname( __FILE__ ) . '/tabs/ljmcs-externals.php' ); } ?>
			</div>

			<div id="maintenance-settings">
			<?php if( $ljmcs_admin ) { include( dirname( __FILE__ ) . '/tabs/ljmcs-maintenance.php' ); } ?>
			</div>

			<div id="removal-settings">
			<?php if( $ljmcs_admin ) { include( dirname( __FILE__ ) . '/tabs/ljmcs-removal.php' ); } ?>
			</div>

			<div id="about">
			<?php include( dirname( __FILE__ ) . '/tabs/ljmcs-about.php' ); ?>
			</div>
		</div>
	</form>
</div>

<?php
if( $ljmcs_nonce_valid ) {
	if( $ljmcs_admin ) { $LJMC_Statistics->save_options(); }
	$LJMC_Statistics->save_user_options();
}
