<?php
// if not called from LJMC exit
if ( !defined( 'LJMC_UNINSTALL_PLUGIN' ) ) 
    exit();

// By default, LJMC Statistics leaves all data in the database, however a user can select to
// remove it, in which case the ljmc_statistics_removal option is set and we should remove that
// here in case the user wants to re-install the plugin at some point.
delete_option( 'ljmc_statistics_removal' );
?>
