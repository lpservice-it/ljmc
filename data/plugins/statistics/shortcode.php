<?php
	add_shortcode( 'ljmcstatistics', 'ljmc_statistics_shortcodes' );
	add_filter('widget_text', 'do_shortcode');
	
	function ljmc_statistics_shortcodes($atts) {
		/*
			LJMC Statitics shortcode is in the format of:
			
				[ljmcstatistics stat=xxx time=xxxx provider=xxxx format=xxxxxx id=xxx]
				
			Where:
				stat = the statistic you want.
				time = is the timeframe, strtotime() (http://php.net/manual/en/datetime.formats.php) will be used to calculate it.
				provider = the search provider to get stats on.
				format = i18n, english, none.
				id = the page/post id to get stats on.
		*/
		
		if( ! is_array( $atts ) ) { return; }
		if( !array_key_exists( 'stat', $atts ) ) { return; }
		
		if( !array_key_exists( 'time', $atts ) ) { $atts['time'] = null; }
		if( !array_key_exists( 'provider', $atts ) ) { $atts['provider'] = null; }
		if( !array_key_exists( 'format', $atts ) ) { $atts['format'] = null; }
		if( !array_key_exists( 'id', $atts ) ) { $atts['id'] = -1; }
		
		$formatnumber = array_key_exists( 'format', $atts );
		
		switch( $atts['stat'] ) {
			case 'usersonline':
				$result = ljmc_statistics_useronline();
				break;
				
			case 'visits':
				$result = ljmc_statistics_visit($atts['time']);
				break;
				
			case 'visitors':
				$result = ljmc_statistics_visitor($atts['time'], null, true);
				break;
				
			case 'pagevisits':
				$result = ljmc_statistics_pages($atts['time'], null, $atts['id']);
				break;
				
			case 'searches':
				$result = ljmc_statistics_searchengine($atts['provider']);
				break;
				
			case 'postcount':
				$result = ljmc_statistics_countposts();
				break;
				
			case 'pagecount':
				$result = ljmc_statistics_countpages();
				break;
				
			case 'commentcount':
				$result = ljmc_statistics_countcomment();
				break;
				
			case 'spamcount':
				$result = ljmc_statistics_countspam();
				break;
				
			case 'usercount':
				$result = ljmc_statistics_countusers();
				break;
				
			case 'postaverage':
				$result = ljmc_statistics_average_post();
				break;
				
			case 'commentaverage':
				$result = ljmc_statistics_average_comment();
				break;
				
			case 'useraverage':
				$result = ljmc_statistics_average_registeruser();
				break;
				
			case 'lpd':
				$result = ljmc_statistics_lastpostdate();
				$formatnumber = false;
				break;
			}
			
		if( $formatnumber ) {
			switch( strtolower( $atts['format'] ) )	{
				case 'i18n':
					$result = number_format_i18n( $result );
					
					break;
				case 'english':
					$result = number_format( $result );
					
					break;
			}
		}
			
		return $result;
	}
	
	add_action('admin_init', 'ljmc_statistics_shortcake' );
	
	function ljmc_statistics_shortcake() {
	// ShortCake support if loaded.		
	if( function_exists( 'shortcode_ui_register_for_shortcode' ) ) {
		$se_list = ljmc_statistics_searchengine_list();
		
		$se_options = array( '' => 'None' );
		
		foreach( $se_list as $se ) {
			$se_options[$se['tag']] = $se['translated'];
		}
		
		shortcode_ui_register_for_shortcode(
			'ljmcstatistics',
			array(

				// Display label. String. Required.
				'label' => 'LJMC Statistics',

				// Icon/image for shortcode. Optional. src or dashicons-$icon. Defaults to carrot.
				'listItemImage' => '<img src="' . plugin_dir_url(__FILE__) . 'assets/images/logo-250.png" width="128" height="128">',

				// Available shortcode attributes and default values. Required. Array.
				// Attribute model expects 'attr', 'type' and 'label'
				// Supported field types: text, checkbox, textarea, radio, select, email, url, number, and date.
				'attrs' => array(
					array(
						'label'       => __('Statistic', 'ljmc_statistics'),
						'attr'        => 'stat',
						'type'        => 'select',
						'description' => __('Select the statistic you wish to display.', 'ljmc_statistics'),
						'value'       => 'usersonline',
						'options'	  => array( 
												'usersonline' => __('Users Online', 'ljmc_statistiscs'),
												'visits' => __('Visits', 'ljmc_statistiscs'),
												'visitors' => __('Visitors', 'ljmc_statistiscs'),
												'pagevisits' => __('Page Visits', 'ljmc_statistiscs'),
												'searches' => __('Searches', 'ljmc_statistiscs'),
												'postcount' => __('Post Count', 'ljmc_statistiscs'),
												'pagecount' => __('Page Count', 'ljmc_statistiscs'),
												'commentcount' => __('Comment Count', 'ljmc_statistiscs'),
												'spamcount' => __('Spam Count', 'ljmc_statistiscs'),
												'usercount' => __('User Count', 'ljmc_statistiscs'),
												'postaverage' => __('Post Average', 'ljmc_statistiscs'),
												'commentaverage' => __('Comment Average', 'ljmc_statistiscs'),
												'useraverage' => __('User Average', 'ljmc_statistiscs'),
												'lpd' => __('Last Post Date', 'ljmc_statistiscs'),
												),
					),
					array(
						'label' 	  => __('Time Frame', 'ljmc_statistics'),
						'attr'  	  => 'time',
						'type'  	  => 'url',
						'description' => __('The time frame to get the statistic for, strtotime() (http://php.net/manual/en/datetime.formats.php) will be used to calculate it. Use "total" to get all recorded dates.', 'ljmc_statistics'),
						'meta'  	  => array('size'=>'10'),
					),
					array(
						'label'       => __('Search Provider', 'ljmc_statistics'),
						'attr'        => 'provider',
						'type'        => 'select',
						'description' => __('The search provider to get statistics on.', 'ljmc_statistics'),
						'options'     => $se_options,
					),
					array(
						'label'       => __('Number Format', 'ljmc_statistics'),
						'attr'        => 'format',
						'type'        => 'select',
						'description' => __('The format to display numbers in: i18n, english, none.', 'ljmc_statistics'),
						'value'       => 'none',
						'options'	  => array( 
												'none' => __('None', 'ljmc_statistics'),
												'english' => __('English', 'ljmc_statistics'),
												'i18n' => __('International', 'ljmc_statistics'),
											),
					),
					array(
						'label'       => __('Post/Page ID', 'ljmc_statistics'),
						'attr'        => 'id',
						'type'        => 'number',
						'description' => __('The post/page id to get page statistics on.', 'ljmc_statistics'),
						'meta'  	  => array('size'=>'5'),
					),
				),
			)
		);
	}
		
	}
?>