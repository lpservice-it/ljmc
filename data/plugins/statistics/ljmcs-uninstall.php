<?php
	if( is_admin() ) {
		GLOBAL $ljmcdb;

		// Handle multi site implementations
		if( is_multisite() ) {
			
			// Loop through each of the sites.
			foreach( ljmc_get_sites() as $blog ) {

				switch_to_blog( $blog['blog_id'] );
				ljmc_statistics_site_removal( $ljmcdb->prefix );
			}
			
			restore_current_blog();
		}
		else {

			ljmc_statistics_site_removal( $ljmcdb->prefix );
			
		}

		// Make sure we don't try and remove the data more than once.
		update_option( 'ljmc_statistics_removal', 'done');
	}

	function ljmc_statistics_site_removal( $ljmc_prefix ) {
		GLOBAL $ljmcdb;
					
		// Delete the options from the LJMC options table.
		delete_option('ljmc_statistics');
		delete_option('ljmc_statistics_db_version');
		delete_option('ljmc_statistics_plugin_version');
		
		// Delete the user options.
		$ljmcdb->query("DELETE FROM {$ljmc_prefix}usermeta WHERE meta_key LIKE 'ljmc_statistics%'");
		
		// Drop the tables
		$ljmcdb->query("DROP TABLE IF EXISTS {$ljmc_prefix}statistics_useronline, {$ljmc_prefix}statistics_visit, {$ljmc_prefix}statistics_visitor, {$ljmc_prefix}statistics_exclusions, {$ljmc_prefix}statistics_pages, {$ljmc_prefix}statistics_historical");
	}
	
?>