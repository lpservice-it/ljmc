<?php

require_once LJMCCF7_PLUGIN_DIR . '/includes/functions.php';
require_once LJMCCF7_PLUGIN_DIR . '/includes/formatting.php';
require_once LJMCCF7_PLUGIN_DIR . '/includes/pipe.php';
require_once LJMCCF7_PLUGIN_DIR . '/includes/shortcodes.php';
require_once LJMCCF7_PLUGIN_DIR . '/includes/capabilities.php';
require_once LJMCCF7_PLUGIN_DIR . '/includes/contact-form-template.php';
require_once LJMCCF7_PLUGIN_DIR . '/includes/contact-form.php';
require_once LJMCCF7_PLUGIN_DIR . '/includes/mail.php';
require_once LJMCCF7_PLUGIN_DIR . '/includes/submission.php';
require_once LJMCCF7_PLUGIN_DIR . '/includes/upgrade.php';
require_once LJMCCF7_PLUGIN_DIR . '/includes/integration.php';

if ( is_admin() ) {
	require_once LJMCCF7_PLUGIN_DIR . '/admin/admin.php';
} else {
	require_once LJMCCF7_PLUGIN_DIR . '/includes/controller.php';
}

add_action( 'plugins_loaded', 'ljmccf7' );

function ljmccf7() {
	ljmccf7_load_textdomain();
	ljmccf7_load_modules();

	/* Shortcodes */
	add_shortcode( 'contact-form-7', 'ljmccf7_contact_form_tag_func' );
	add_shortcode( 'contact-form', 'ljmccf7_contact_form_tag_func' );
}

add_action( 'init', 'ljmccf7_init' );

function ljmccf7_init() {
	ljmccf7_get_request_uri();
	ljmccf7_register_post_types();

	do_action( 'ljmccf7_init' );
}

add_action( 'admin_init', 'ljmccf7_upgrade' );

function ljmccf7_upgrade() {
	$opt = get_option( 'ljmccf7' );

	if ( ! is_array( $opt ) )
		$opt = array();

	$old_ver = isset( $opt['version'] ) ? (string) $opt['version'] : '0';
	$new_ver = LJMCCF7_VERSION;

	if ( $old_ver == $new_ver )
		return;

	do_action( 'ljmccf7_upgrade', $new_ver, $old_ver );

	$opt['version'] = $new_ver;

	update_option( 'ljmccf7', $opt );
}

/* Install and default settings */

add_action( 'activate_' . LJMCCF7_PLUGIN_BASENAME, 'ljmccf7_install' );

function ljmccf7_install() {
	if ( $opt = get_option( 'ljmccf7' ) )
		return;

	ljmccf7_load_textdomain();
	ljmccf7_register_post_types();
	ljmccf7_upgrade();

	if ( get_posts( array( 'post_type' => 'ljmccf7_contact_form' ) ) )
		return;

	$contact_form = LJMCCF7_ContactForm::get_template( array(
		'title' => sprintf( __( 'Contact form %d', 'contact-form-7' ), 1 ) ) );

	$contact_form->save();
}
