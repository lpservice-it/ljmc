<?php

require_once LJMCCF7_PLUGIN_DIR . '/admin/includes/admin-functions.php';
require_once LJMCCF7_PLUGIN_DIR . '/admin/includes/help-tabs.php';
require_once LJMCCF7_PLUGIN_DIR . '/admin/includes/tag-generator.php';

add_action( 'admin_menu', 'ljmccf7_admin_menu', 9 );

function ljmccf7_admin_menu() {
	add_object_page( __( 'Contact Form 7', 'contact-form-7' ),
		__( 'Contact Form', 'contact-form-7' ),
		'ljmccf7_read_contact_forms', 'ljmccf7',
		'ljmccf7_admin_management_page', 'fa-envelope-o' );

	$edit = add_submenu_page( 'ljmccf7',
		__( 'Edit Contact Form', 'contact-form-7' ),
		__( 'Contact Forms', 'contact-form-7' ),
		'ljmccf7_read_contact_forms', 'ljmccf7',
		'ljmccf7_admin_management_page' );

	add_action( 'load-' . $edit, 'ljmccf7_load_contact_form_admin' );

	$addnew = add_submenu_page( 'ljmccf7',
		__( 'Add New Contact Form', 'contact-form-7' ),
		__( 'Add New', 'contact-form-7' ),
		'ljmccf7_edit_contact_forms', 'ljmccf7-new',
		'ljmccf7_admin_add_new_page' );

	add_action( 'load-' . $addnew, 'ljmccf7_load_contact_form_admin' );

	$integration = LJMCCF7_Integration::get_instance();

	if ( $integration->service_exists() ) {
		$integration = add_submenu_page( 'ljmccf7',
			__( 'Integration with Other Services', 'contact-form-7' ),
			__( 'Integration', 'contact-form-7' ),
			'ljmccf7_manage_integration', 'ljmccf7-integration',
			'ljmccf7_admin_integration_page' );

		add_action( 'load-' . $integration, 'ljmccf7_load_integration_page' );
	}
}

add_filter( 'set-screen-option', 'ljmccf7_set_screen_options', 10, 3 );

function ljmccf7_set_screen_options( $result, $option, $value ) {
	$ljmccf7_screens = array(
		'cfseven_contact_forms_per_page' );

	if ( in_array( $option, $ljmccf7_screens ) )
		$result = $value;

	return $result;
}

function ljmccf7_load_contact_form_admin() {
	global $plugin_page;

	$action = ljmccf7_current_action();

	if ( 'save' == $action ) {
		$id = $_POST['post_ID'];
		check_admin_referer( 'ljmccf7-save-contact-form_' . $id );

		if ( ! current_user_can( 'ljmccf7_edit_contact_form', $id ) )
			ljmc_die( __( 'You are not allowed to edit this item.', 'contact-form-7' ) );

		$id = ljmccf7_save_contact_form( $id );

		$query = array(
			'message' => ( -1 == $_POST['post_ID'] ) ? 'created' : 'saved',
			'post' => $id,
			'active-tab' => isset( $_POST['active-tab'] ) ? (int) $_POST['active-tab'] : 0 );

		$redirect_to = add_query_arg( $query, menu_page_url( 'ljmccf7', false ) );
		ljmc_safe_redirect( $redirect_to );
		exit();
	}

	if ( 'copy' == $action ) {
		$id = empty( $_POST['post_ID'] )
			? absint( $_REQUEST['post'] )
			: absint( $_POST['post_ID'] );

		check_admin_referer( 'ljmccf7-copy-contact-form_' . $id );

		if ( ! current_user_can( 'ljmccf7_edit_contact_form', $id ) )
			ljmc_die( __( 'You are not allowed to edit this item.', 'contact-form-7' ) );

		$query = array();

		if ( $contact_form = ljmccf7_contact_form( $id ) ) {
			$new_contact_form = $contact_form->copy();
			$new_contact_form->save();

			$query['post'] = $new_contact_form->id();
			$query['message'] = 'created';
		}

		$redirect_to = add_query_arg( $query, menu_page_url( 'ljmccf7', false ) );

		ljmc_safe_redirect( $redirect_to );
		exit();
	}

	if ( 'delete' == $action ) {
		if ( ! empty( $_POST['post_ID'] ) )
			check_admin_referer( 'ljmccf7-delete-contact-form_' . $_POST['post_ID'] );
		elseif ( ! is_array( $_REQUEST['post'] ) )
			check_admin_referer( 'ljmccf7-delete-contact-form_' . $_REQUEST['post'] );
		else
			check_admin_referer( 'bulk-posts' );

		$posts = empty( $_POST['post_ID'] )
			? (array) $_REQUEST['post']
			: (array) $_POST['post_ID'];

		$deleted = 0;

		foreach ( $posts as $post ) {
			$post = LJMCCF7_ContactForm::get_instance( $post );

			if ( empty( $post ) )
				continue;

			if ( ! current_user_can( 'ljmccf7_delete_contact_form', $post->id() ) )
				ljmc_die( __( 'You are not allowed to delete this item.', 'contact-form-7' ) );

			if ( ! $post->delete() )
				ljmc_die( __( 'Error in deleting.', 'contact-form-7' ) );

			$deleted += 1;
		}

		$query = array();

		if ( ! empty( $deleted ) )
			$query['message'] = 'deleted';

		$redirect_to = add_query_arg( $query, menu_page_url( 'ljmccf7', false ) );

		ljmc_safe_redirect( $redirect_to );
		exit();
	}

	$_GET['post'] = isset( $_GET['post'] ) ? $_GET['post'] : '';

	$post = null;

	if ( 'ljmccf7-new' == $plugin_page && isset( $_GET['locale'] ) ) {
		$post = LJMCCF7_ContactForm::get_template( array(
			'locale' => $_GET['locale'] ) );
	} elseif ( ! empty( $_GET['post'] ) ) {
		$post = LJMCCF7_ContactForm::get_instance( $_GET['post'] );
	}

	$current_screen = get_current_screen();

	$help_tabs = new LJMCCF7_Help_Tabs( $current_screen );

	if ( $post && current_user_can( 'ljmccf7_edit_contact_form', $post->id() ) ) {
		$help_tabs->set_help_tabs( 'edit' );

	} else if ( 'ljmccf7-new' == $plugin_page ) {
		$help_tabs->set_help_tabs( 'add_new' );

	} else {
		$help_tabs->set_help_tabs( 'list' );

		if ( ! class_exists( 'LJMCCF7_Contact_Form_List_Table' ) ) {
			require_once LJMCCF7_PLUGIN_DIR . '/admin/includes/class-contact-forms-list-table.php';
		}

		add_filter( 'manage_' . $current_screen->id . '_columns',
			array( 'LJMCCF7_Contact_Form_List_Table', 'define_columns' ) );

		add_screen_option( 'per_page', array(
			'label' => __( 'Contact Forms', 'contact-form-7' ),
			'default' => 20,
			'option' => 'cfseven_contact_forms_per_page' ) );
	}
}

add_action( 'admin_enqueue_scripts', 'ljmccf7_admin_enqueue_scripts' );

function ljmccf7_admin_enqueue_scripts( $hook_suffix ) {
	if ( false === strpos( $hook_suffix, 'ljmccf7' ) ) {
		return;
	}

	ljmc_enqueue_style( 'contact-form-7-admin',
		ljmccf7_plugin_url( 'admin/css/styles.css' ),
		array(), LJMCCF7_VERSION, 'all' );

	if ( ljmccf7_is_rtl() ) {
		ljmc_enqueue_style( 'contact-form-7-admin-rtl',
			ljmccf7_plugin_url( 'admin/css/styles-rtl.css' ),
			array(), LJMCCF7_VERSION, 'all' );
	}

	ljmc_enqueue_script( 'ljmccf7-admin',
		ljmccf7_plugin_url( 'admin/js/scripts.js' ),
		array( 'jquery', 'jquery-ui-tabs' ),
		LJMCCF7_VERSION, true );

	ljmc_localize_script( 'ljmccf7-admin', '_ljmccf7', array(
		'pluginUrl' => ljmccf7_plugin_url(),
		'saveAlert' => __( "The changes you made will be lost if you navigate away from this page.", 'contact-form-7' ),
		'activeTab' => isset( $_GET['active-tab'] ) ? (int) $_GET['active-tab'] : 0 ) );

	add_thickbox();

	ljmc_enqueue_script( 'ljmccf7-admin-taggenerator',
		ljmccf7_plugin_url( 'admin/js/tag-generator.js' ),
		array( 'jquery', 'thickbox', 'ljmccf7-admin' ), LJMCCF7_VERSION, true );
}

function ljmccf7_admin_management_page() {
	if ( $post = ljmccf7_get_current_contact_form() ) {
		$post_id = $post->initial() ? -1 : $post->id();

		require_once LJMCCF7_PLUGIN_DIR . '/admin/includes/editor.php';
		require_once LJMCCF7_PLUGIN_DIR . '/admin/edit-contact-form.php';
		return;
	}

	$list_table = new LJMCCF7_Contact_Form_List_Table();
	$list_table->prepare_items();

?>
<div class="wrap">

<h2><?php
	echo esc_html( __( 'Contact Forms', 'contact-form-7' ) );


	if ( ! empty( $_REQUEST['s'] ) ) {
		echo sprintf( '<span class="subtitle">'
			. __( 'Search results for &#8220;%s&#8221;', 'contact-form-7' )
			. '</span>', esc_html( $_REQUEST['s'] ) );
	}
?></h2>

<?php do_action( 'ljmccf7_admin_notices' ); ?>

<form method="get" action="">
	<input type="hidden" name="page" value="<?php echo esc_attr( $_REQUEST['page'] ); ?>" />
	<?php $list_table->search_box( __( 'Search Contact Forms', 'contact-form-7' ), 'ljmccf7-contact' ); ?>
	<?php $list_table->display(); ?>
</form>

</div>
<?php
}

function ljmccf7_admin_add_new_page() {
	if ( $post = ljmccf7_get_current_contact_form() ) {
		$post_id = -1;

		require_once LJMCCF7_PLUGIN_DIR . '/admin/includes/editor.php';
		require_once LJMCCF7_PLUGIN_DIR . '/admin/edit-contact-form.php';
		return;
	}

	$available_locales = ljmccf7_l10n();
	$default_locale = get_locale();

	if ( ! isset( $available_locales[$default_locale] ) ) {
		$default_locale = 'en_US';
	}

?>
<div class="wrap">

<h2><?php echo esc_html( __( 'Add New Contact Form', 'contact-form-7' ) ); ?></h2>

<?php do_action( 'ljmccf7_admin_notices' ); ?>

<h3><?php echo esc_html( sprintf( __( 'Use the default language (%s)', 'contact-form-7' ), $available_locales[$default_locale] ) ); ?></h3>
<p><a href="<?php echo esc_url( add_query_arg( array( 'locale' => $default_locale ), menu_page_url( 'ljmccf7-new', false ) ) ); ?>" class="button button-primary" /><?php echo esc_html( __( 'Add New', 'contact-form-7' ) ); ?></a></p>

<?php unset( $available_locales[$default_locale] ); ?>
<h3><?php echo esc_html( __( 'Or', 'contact-form-7' ) ); ?></h3>
<form action="" method="get">
<input type="hidden" name="page" value="ljmccf7-new" />
<select name="locale">
<option value="" selected="selected"><?php echo esc_html( __( '(select language)', 'contact-form-7' ) ); ?></option>
<?php foreach ( $available_locales as $code => $locale ) : ?>
<option value="<?php echo esc_attr( $code ); ?>"><?php echo esc_html( $locale ); ?></option>
<?php endforeach; ?>
</select>
<input type="submit" class="button" value="<?php echo esc_attr( __( 'Add New', 'contact-form-7' ) ); ?>" />
</form>
</div>
<?php
}

function ljmccf7_load_integration_page() {
	$integration = LJMCCF7_Integration::get_instance();

	if ( isset( $_REQUEST['service'] )
	&& $integration->service_exists( $_REQUEST['service'] ) ) {
		$service = $integration->get_service( $_REQUEST['service'] );
		$service->load( ljmccf7_current_action() );
	}

	$help_tabs = new LJMCCF7_Help_Tabs( get_current_screen() );
	$help_tabs->set_help_tabs( 'integration' );
}

function ljmccf7_admin_integration_page() {
	$integration = LJMCCF7_Integration::get_instance();

?>
<div class="wrap">

<h2><?php echo esc_html( __( 'Integration with Other Services', 'contact-form-7' ) ); ?></h2>

<?php do_action( 'ljmccf7_admin_notices' ); ?>

<?php
	if ( isset( $_REQUEST['service'] )
	&& $service = $integration->get_service( $_REQUEST['service'] ) ) {
		$message = isset( $_REQUEST['message'] ) ? $_REQUEST['message'] : '';
		$service->admin_notice( $message );
		$integration->list_services( array( 'include' => $_REQUEST['service'] ) );
	} else {
		$integration->list_services();
	}
?>

</div>
<?php
}

/* Misc */

add_action( 'ljmccf7_admin_notices', 'ljmccf7_admin_updated_message' );

function ljmccf7_admin_updated_message() {
	if ( empty( $_REQUEST['message'] ) )
		return;

	if ( 'created' == $_REQUEST['message'] )
		$updated_message = esc_html( __( 'Contact form created.', 'contact-form-7' ) );
	elseif ( 'saved' == $_REQUEST['message'] )
		$updated_message = esc_html( __( 'Contact form saved.', 'contact-form-7' ) );
	elseif ( 'deleted' == $_REQUEST['message'] )
		$updated_message = esc_html( __( 'Contact form deleted.', 'contact-form-7' ) );

	if ( empty( $updated_message ) )
		return;

?>
<div id="message" class="updated"><p><?php echo $updated_message; ?></p></div>
<?php
}

add_filter( 'plugin_action_links', 'ljmccf7_plugin_action_links', 10, 2 );

function ljmccf7_plugin_action_links( $links, $file ) {
	if ( $file != LJMCCF7_PLUGIN_BASENAME )
		return $links;

	$settings_link = '<a href="' . menu_page_url( 'ljmccf7', false ) . '">'
		. esc_html( __( 'Settings', 'contact-form-7' ) ) . '</a>';

	array_unshift( $links, $settings_link );

	return $links;
}

add_action( 'admin_notices', 'ljmccf7_old_ljmc_version_error', 9 );

function ljmccf7_old_ljmc_version_error() {
	global $plugin_page;

	if ( 'ljmccf7' != substr( $plugin_page, 0, 5 ) ) {
		return;
	}

	$ljmc_version = get_bloginfo( 'version' );

	if ( ! version_compare( $ljmc_version, LJMCCF7_REQUIRED_LJMC_VERSION, '<' ) )
		return;

?>
<div class="error">
<p><?php echo sprintf( __( '<strong>Contact Form 7 %1$s requires WordPress %2$s or higher.</strong> Please <a href="%3$s">update WordPress</a> first.', 'contact-form-7' ), LJMCCF7_VERSION, LJMCCF7_REQUIRED_LJMC_VERSION, admin_url( 'update-core.php' ) ); ?></p>
</div>
<?php
}

add_action( 'ljmccf7_admin_notices', 'ljmccf7_welcome_panel', 2 );

function ljmccf7_welcome_panel() {
	global $plugin_page;

	if ( 'ljmccf7' != $plugin_page || ! empty( $_GET['post'] ) ) {
		return;
	}

	$classes = 'welcome-panel';

	$vers = (array) get_user_meta( get_current_user_id(),
		'ljmccf7_hide_welcome_panel_on', true );

	if ( ljmccf7_version_grep( ljmccf7_version( 'only_major=1' ), $vers ) ) {
		$classes .= ' hidden';
	}

?>
<?php
}

add_action( 'ljmc_ajax_ljmccf7-update-welcome-panel', 'ljmccf7_admin_ajax_welcome_panel' );

function ljmccf7_admin_ajax_welcome_panel() {
	check_ajax_referer( 'ljmccf7-welcome-panel-nonce', 'welcomepanelnonce' );

	$vers = get_user_meta( get_current_user_id(),
		'ljmccf7_hide_welcome_panel_on', true );

	if ( empty( $vers ) || ! is_array( $vers ) ) {
		$vers = array();
	}

	if ( empty( $_POST['visible'] ) ) {
		$vers[] = LJMCCF7_VERSION;
	}

	$vers = array_unique( $vers );

	update_user_meta( get_current_user_id(), 'ljmccf7_hide_welcome_panel_on', $vers );

	ljmc_die( 1 );
}

add_action( 'ljmccf7_admin_notices', 'ljmccf7_not_allowed_to_edit' );

function ljmccf7_not_allowed_to_edit() {
	if ( ! $contact_form = ljmccf7_get_current_contact_form() ) {
		return;
	}

	$post_id = $contact_form->id();

	if ( current_user_can( 'ljmccf7_edit_contact_form', $post_id ) ) {
		return;
	}

	$message = __( "You are not allowed to edit this contact form.",
		'contact-form-7' );

	echo sprintf( '<div class="notice notice-warning"><p>%s</p></div>',
		esc_html( $message ) );
}
