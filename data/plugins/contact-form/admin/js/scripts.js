(function($) {

	if (typeof _ljmccf7 == 'undefined' || _ljmccf7 === null) {
		_ljmccf7 = {};
	}

	$(function() {
		var welcomePanel = $('#welcome-panel');
		var updateWelcomePanel;

		updateWelcomePanel = function( visible ) {
			$.post( ajaxurl, {
				action: 'ljmccf7-update-welcome-panel',
				visible: visible,
				welcomepanelnonce: $( '#welcomepanelnonce' ).val()
			});
		};

		$('a.welcome-panel-close', welcomePanel).click(function(event) {
			event.preventDefault();
			welcomePanel.addClass('hidden');
			updateWelcomePanel( 0 );
		});

		$('#contact-form-editor').tabs({
			active: _ljmccf7.activeTab,
			activate: function(event, ui) {
				$('#active-tab').val(ui.newTab.index());
			}
		});

		$('#contact-form-editor-tabs').focusin(function(event) {
			$('#contact-form-editor .keyboard-interaction').css(
				'visibility', 'visible');
		}).focusout(function(event) {
			$('#contact-form-editor .keyboard-interaction').css(
				'visibility', 'hidden');
		});

		$('input:checkbox.toggle-form-table').click(function(event) {
			$(this).ljmccf7ToggleFormTable();
		}).ljmccf7ToggleFormTable();

		if ('' == $('#title').val()) {
			$('#title').focus();
		}

		$.ljmccf7TitleHint();

		$('.contact-form-editor-box-mail span.mailtag').click(function(event) {
			var range = document.createRange();
			range.selectNodeContents(this);
			window.getSelection().addRange(range);
		});

		$(window).on('beforeunload', function(event) {
			var changed = false;

			$('#ljmccf7-admin-form-element :input[type!="hidden"]').each(function() {
				if ($(this).is(':checkbox, :radio')) {
					if (this.defaultChecked != $(this).is(':checked')) {
						changed = true;
					}
				} else {
					if (this.defaultValue != $(this).val()) {
						changed = true;
					}
				}
			});

			if (changed) {
				event.returnValue = _ljmccf7.saveAlert;
				return _ljmccf7.saveAlert;
			}
		});

		$('#ljmccf7-admin-form-element').submit(function() {
			if ('copy' != this.action.value) {
				$(window).off('beforeunload');
			}
		});
	});

	$.fn.ljmccf7ToggleFormTable = function() {
		return this.each(function() {
			var formtable = $(this).closest('.contact-form-editor-box-mail').find('fieldset');

			if ($(this).is(':checked')) {
				formtable.removeClass('hidden');
			} else {
				formtable.addClass('hidden');
			}
		});
	};

	/**
	 * Copied from ljmctitlehint() in ljmc-admin/js/post.js
	 */
	$.ljmccf7TitleHint = function() {
		var title = $('#title');
		var titleprompt = $('#title-prompt-text');

		if ('' == title.val()) {
			titleprompt.removeClass('screen-reader-text');
		}

		titleprompt.click(function() {
			$(this).addClass('screen-reader-text');
			title.focus();
		});

		title.blur(function() {
			if ('' == $(this).val()) {
				titleprompt.removeClass('screen-reader-text');
			}
		}).focus(function() {
			titleprompt.addClass('screen-reader-text');
		}).keydown(function(e) {
			titleprompt.addClass('screen-reader-text');
			$(this).unbind(e);
		});
	};

})(jQuery);
