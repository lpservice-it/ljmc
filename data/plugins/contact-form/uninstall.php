<?php

if ( ! defined( 'LJMC_UNINSTALL_PLUGIN' ) )
	exit();

function ljmccf7_delete_plugin() {
	global $ljmcdb;

	delete_option( 'ljmccf7' );

	$posts = get_posts( array(
		'numberposts' => -1,
		'post_type' => 'ljmccf7_contact_form',
		'post_status' => 'any' ) );

	foreach ( $posts as $post )
		ljmc_delete_post( $post->ID, true );

	$table_name = $ljmcdb->prefix . "contact_form_7";

	$ljmcdb->query( "DROP TABLE IF EXISTS $table_name" );
}

ljmccf7_delete_plugin();

?>