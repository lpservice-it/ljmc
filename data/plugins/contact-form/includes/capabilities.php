<?php

add_filter( 'map_meta_cap', 'ljmccf7_map_meta_cap', 10, 4 );

function ljmccf7_map_meta_cap( $caps, $cap, $user_id, $args ) {
	$meta_caps = array(
		'ljmccf7_edit_contact_form' => LJMCCF7_ADMIN_READ_WRITE_CAPABILITY,
		'ljmccf7_edit_contact_forms' => LJMCCF7_ADMIN_READ_WRITE_CAPABILITY,
		'ljmccf7_read_contact_forms' => LJMCCF7_ADMIN_READ_CAPABILITY,
		'ljmccf7_delete_contact_form' => LJMCCF7_ADMIN_READ_WRITE_CAPABILITY,
		'ljmccf7_manage_integration' => 'manage_options' );

	$meta_caps = apply_filters( 'ljmccf7_map_meta_cap', $meta_caps );

	$caps = array_diff( $caps, array_keys( $meta_caps ) );

	if ( isset( $meta_caps[$cap] ) ) {
		$caps[] = $meta_caps[$cap];
	}

	return $caps;
}
