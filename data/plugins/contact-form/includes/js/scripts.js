(function($) {

	if (typeof _ljmccf7 == 'undefined' || _ljmccf7 === null)
		_ljmccf7 = {};

	_ljmccf7 = $.extend({ cached: 0 }, _ljmccf7);

	$(function() {
		_ljmccf7.supportHtml5 = $.ljmccf7SupportHtml5();
		$('div.ljmccf7 > form').ljmccf7InitForm();
	});

	$.fn.ljmccf7InitForm = function() {
		this.ajaxForm({
			beforeSubmit: function(arr, $form, options) {
				$form.ljmccf7ClearResponseOutput();
				$form.find('[aria-invalid]').attr('aria-invalid', 'false');
				$form.find('img.ajax-loader').css({ visibility: 'visible' });
				return true;
			},
			beforeSerialize: function($form, options) {
				$form.find('[placeholder].placeheld').each(function(i, n) {
					$(n).val('');
				});
				return true;
			},
			data: { '_ljmccf7_is_ajax_call': 1 },
			dataType: 'json',
			success: $.ljmccf7AjaxSuccess,
			error: function(xhr, status, error, $form) {
				var e = $('<div class="ajax-error"></div>').text(error.message);
				$form.after(e);
			}
		});

		if (_ljmccf7.cached)
			this.ljmccf7OnloadRefill();

		this.ljmccf7ToggleSubmit();

		this.find('.ljmccf7-submit').ljmccf7AjaxLoader();

		this.find('.ljmccf7-acceptance').click(function() {
			$(this).closest('form').ljmccf7ToggleSubmit();
		});

		this.find('.ljmccf7-exclusive-checkbox').ljmccf7ExclusiveCheckbox();

		this.find('.ljmccf7-list-item.has-free-text').ljmccf7ToggleCheckboxFreetext();

		this.find('[placeholder]').ljmccf7Placeholder();

		if (_ljmccf7.jqueryUi && ! _ljmccf7.supportHtml5.date) {
			this.find('input.ljmccf7-date[type="date"]').each(function() {
				$(this).datepicker({
					dateFormat: 'yy-mm-dd',
					minDate: new Date($(this).attr('min')),
					maxDate: new Date($(this).attr('max'))
				});
			});
		}

		if (_ljmccf7.jqueryUi && ! _ljmccf7.supportHtml5.number) {
			this.find('input.ljmccf7-number[type="number"]').each(function() {
				$(this).spinner({
					min: $(this).attr('min'),
					max: $(this).attr('max'),
					step: $(this).attr('step')
				});
			});
		}

		this.find('.ljmccf7-character-count').ljmccf7CharacterCount();

		this.find('.ljmccf7-validates-as-url').change(function() {
			$(this).ljmccf7NormalizeUrl();
		});
	};

	$.ljmccf7AjaxSuccess = function(data, status, xhr, $form) {
		if (! $.isPlainObject(data) || $.isEmptyObject(data))
			return;

		var $responseOutput = $form.find('div.ljmccf7-response-output');

		$form.ljmccf7ClearResponseOutput();

		$form.find('.ljmccf7-form-control').removeClass('ljmccf7-not-valid');
		$form.removeClass('invalid spam sent failed');

		if (data.captcha)
			$form.ljmccf7RefillCaptcha(data.captcha);

		if (data.quiz)
			$form.ljmccf7RefillQuiz(data.quiz);

		if (data.invalids) {
			$.each(data.invalids, function(i, n) {
				$form.find(n.into).ljmccf7NotValidTip(n.message);
				$form.find(n.into).find('.ljmccf7-form-control').addClass('ljmccf7-not-valid');
				$form.find(n.into).find('[aria-invalid]').attr('aria-invalid', 'true');
			});

			$('.screen-reader-response').appendTo('.bs-example-modal-sm .modal-body');
			$responseOutput.addClass('ljmccf7-validation-errors');
			$(".bs-example-modal-sm").modal("toggle");
			$form.addClass('invalid');

			$(data.into).trigger('invalid.ljmccf7');

		} else if (1 == data.spam) {
			$('.screen-reader-response').appendTo('.bs-example-modal-sm .modal-body');
			$responseOutput.addClass('ljmccf7-spam-blocked');
			$(".bs-example-modal-sm").modal("toggle");
			$form.addClass('spam');

			$(data.into).trigger('spam.ljmccf7');

		} else if (1 == data.mailSent) {
			$('.screen-reader-response').appendTo('.bs-example-modal-sm .modal-body');
			$responseOutput.addClass('ljmccf7-mail-sent-ok');
			$(".bs-example-modal-sm").modal("toggle");
			$form.addClass('sent');

			if (data.onSentOk)
				$.each(data.onSentOk, function(i, n) { eval(n) });

			$(data.into).trigger('mailsent.ljmccf7');

		} else {
			$('.screen-reader-response').appendTo('.bs-example-modal-sm .modal-body');
			$responseOutput.addClass('ljmccf7-mail-sent-ng');
			$(".bs-example-modal-sm").modal("toggle");
			$form.addClass('failed');

			$(data.into).trigger('mailfailed.ljmccf7');
		}

		if (data.onSubmit)
			$.each(data.onSubmit, function(i, n) { eval(n) });

		$(data.into).trigger('submit.ljmccf7');

		if (1 == data.mailSent)
			$form.resetForm();

		$form.find('[placeholder].placeheld').each(function(i, n) {
			$(n).val($(n).attr('placeholder'));
		});

		$responseOutput.append(data.message).slideDown('fast');
		$responseOutput.attr('role', 'alert');

		$.ljmccf7UpdateScreenReaderResponse($form, data);
	};

	$.fn.ljmccf7ExclusiveCheckbox = function() {
		return this.find('input:checkbox').click(function() {
			var name = $(this).attr('name');
			$(this).closest('form').find('input:checkbox[name="' + name + '"]').not(this).prop('checked', false);
		});
	};

	$.fn.ljmccf7Placeholder = function() {
		if (_ljmccf7.supportHtml5.placeholder)
			return this;

		return this.each(function() {
			$(this).val($(this).attr('placeholder'));
			$(this).addClass('placeheld');

			$(this).focus(function() {
				if ($(this).hasClass('placeheld'))
					$(this).val('').removeClass('placeheld');
			});

			$(this).blur(function() {
				if ('' == $(this).val()) {
					$(this).val($(this).attr('placeholder'));
					$(this).addClass('placeheld');
				}
			});
		});
	};

	$.fn.ljmccf7AjaxLoader = function() {
		return this.each(function() {
			var loader = $('<img class="ajax-loader" />')
				.attr({ src: _ljmccf7.loaderUrl, alt: _ljmccf7.sending })
				.css('visibility', 'hidden');

			$(this).after(loader);
		});
	};

	$.fn.ljmccf7ToggleSubmit = function() {

		return this.each(function() {
			var form = $(this);
			if (this.tagName.toLowerCase() != 'form')
				form = $(this).find('form').first();

			if (form.hasClass('ljmccf7-acceptance-as-validation'))
				return;

			var submit = form.find('input:submit');
			if (! submit.length) return;

			var acceptances = form.find('input:checkbox.ljmccf7-acceptance');
			if (! acceptances.length) return;

			submit.removeAttr('disabled');
			acceptances.each(function(i, n) {
				n = $(n);
				if (n.hasClass('ljmccf7-invert') && n.is(':checked')
				|| ! n.hasClass('ljmccf7-invert') && ! n.is(':checked'))
					submit.attr('disabled', 'disabled');
			});
		});
	};

	$.fn.ljmccf7ToggleCheckboxFreetext = function() {
		return this.each(function() {
			var $wrap = $(this).closest('.ljmccf7-form-control');

			if ($(this).find(':checkbox, :radio').is(':checked')) {
				$(this).find(':input.ljmccf7-free-text').prop('disabled', false);
			} else {
				$(this).find(':input.ljmccf7-free-text').prop('disabled', true);
			}

			$wrap.find(':checkbox, :radio').change(function() {
				var $cb = $('.has-free-text', $wrap).find(':checkbox, :radio');
				var $freetext = $(':input.ljmccf7-free-text', $wrap);

				if ($cb.is(':checked')) {
					$freetext.prop('disabled', false).focus();
				} else {
					$freetext.prop('disabled', true);
				}
			});
		});
	};

	$.fn.ljmccf7CharacterCount = function() {
		return this.each(function() {
			var $count = $(this);
			var name = $count.attr('data-target-name');
			var down = $count.hasClass('down');
			var starting = parseInt($count.attr('data-starting-value'), 10);
			var maximum = parseInt($count.attr('data-maximum-value'), 10);
			var minimum = parseInt($count.attr('data-minimum-value'), 10);

			var updateCount = function($target) {
				var length = $target.val().length;
				var count = down ? starting - length : length;
				$count.attr('data-current-value', count);
				$count.text(count);

				if (maximum && maximum < length) {
					$count.addClass('too-long');
				} else {
					$count.removeClass('too-long');
				}

				if (minimum && length < minimum) {
					$count.addClass('too-short');
				} else {
					$count.removeClass('too-short');
				}
			};

			$count.closest('form').find(':input[name="' + name + '"]').each(function() {
				updateCount($(this));

				$(this).keyup(function() {
					updateCount($(this));
				});
			});
		});
	};

	$.fn.ljmccf7NormalizeUrl = function() {
		return this.each(function() {
			var val = $.trim($(this).val());

			if (val && ! val.match(/^[a-z][a-z0-9.+-]*:/i)) { // check the scheme part
				val = val.replace(/^\/+/, '');
				val = 'http://' + val;
			}

			$(this).val(val);
		});
	};

	$.fn.ljmccf7NotValidTip = function(message) {
		return this.each(function() {
			var $into = $(this);

			$into.find('span.ljmccf7-not-valid-tip').remove();
			$into.append('<span role="alert" class="ljmccf7-not-valid-tip">' + message + '</span>');

			if ($into.is('.use-floating-validation-tip *')) {
				$('.ljmccf7-not-valid-tip', $into).mouseover(function() {
					$(this).ljmccf7FadeOut();
				});

				$(':input', $into).focus(function() {
					$('.ljmccf7-not-valid-tip', $into).not(':hidden').ljmccf7FadeOut();
				});
			}
			$('.ljmccf7-response-output').remove();
		});
	};

	$.fn.ljmccf7FadeOut = function() {
		return this.each(function() {
			$(this).animate({
				opacity: 0
			}, 'fast', function() {
				$(this).css({'z-index': -100});
			});
		});
	};

	$.fn.ljmccf7OnloadRefill = function() {
		return this.each(function() {
			var url = $(this).attr('action');
			if (0 < url.indexOf('#'))
				url = url.substr(0, url.indexOf('#'));

			var id = $(this).find('input[name="_ljmccf7"]').val();
			var unitTag = $(this).find('input[name="_ljmccf7_unit_tag"]').val();

			$.getJSON(url,
				{ _ljmccf7_is_ajax_call: 1, _ljmccf7: id, _ljmccf7_request_ver: $.now() },
				function(data) {
					if (data && data.captcha)
						$('#' + unitTag).ljmccf7RefillCaptcha(data.captcha);

					if (data && data.quiz)
						$('#' + unitTag).ljmccf7RefillQuiz(data.quiz);
				}
			);
		});
	};

	$.fn.ljmccf7RefillCaptcha = function(captcha) {
		return this.each(function() {
			var form = $(this);

			$.each(captcha, function(i, n) {
				form.find(':input[name="' + i + '"]').clearFields();
				form.find('img.ljmccf7-captcha-' + i).attr('src', n);
				var match = /([0-9]+)\.(png|gif|jpeg)$/.exec(n);
				form.find('input:hidden[name="_ljmccf7_captcha_challenge_' + i + '"]').attr('value', match[1]);
			});
		});
	};

	$.fn.ljmccf7RefillQuiz = function(quiz) {
		return this.each(function() {
			var form = $(this);

			$.each(quiz, function(i, n) {
				form.find(':input[name="' + i + '"]').clearFields();
				form.find(':input[name="' + i + '"]').siblings('span.ljmccf7-quiz-label').text(n[0]);
				form.find('input:hidden[name="_ljmccf7_quiz_answer_' + i + '"]').attr('value', n[1]);
			});
		});
	};

	$.fn.ljmccf7ClearResponseOutput = function() {
		return this.each(function() {
			$(this).find('div.ljmccf7-response-output').hide().empty().removeClass('ljmccf7-mail-sent-ok ljmccf7-mail-sent-ng ljmccf7-validation-errors ljmccf7-spam-blocked').removeAttr('role');
			$(this).find('span.ljmccf7-not-valid-tip').remove();
			$(this).find('img.ajax-loader').css({ visibility: 'hidden' });
		});
	};

	$.ljmccf7UpdateScreenReaderResponse = function($form, data) {
		$('.ljmccf7 .screen-reader-response').html('').attr('role', '');

		if (data.message) {
			var $response = $form.siblings('.screen-reader-response').first();
			$response.append(data.message);

			if (data.invalids) {
				var $invalids = $('<ul></ul>');

				$.each(data.invalids, function(i, n) {
					if (n.idref) {
						var $li = $('<li></li>').append($('<a></a>').attr('href', '#' + n.idref).append(n.message));
					} else {
						var $li = $('<li></li>').append(n.message);
					}

					$invalids.append($li);
				});

				$response.append($invalids);
			}

			$response.attr('role', 'alert').focus();
		}
	};

	$.ljmccf7SupportHtml5 = function() {
		var features = {};
		var input = document.createElement('input');

		features.placeholder = 'placeholder' in input;

		var inputTypes = ['email', 'url', 'tel', 'number', 'range', 'date'];

		$.each(inputTypes, function(index, value) {
			input.setAttribute('type', value);
			features[value] = input.type !== 'text';
		});

		return features;
	};

})(jQuery);
