<?php

add_action( 'init', 'ljmccf7_control_init', 11 );

function ljmccf7_control_init() {
	if ( ! isset( $_SERVER['REQUEST_METHOD'] ) ) {
		return;
	}

	if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
		if ( isset( $_GET['_ljmccf7_is_ajax_call'] ) ) {
			ljmccf7_ajax_onload();
		}
	}

	if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
		if ( isset( $_POST['_ljmccf7_is_ajax_call'] ) ) {
			ljmccf7_ajax_json_echo();
		}

		ljmccf7_submit_nonajax();
	}
}

function ljmccf7_ajax_onload() {
	$echo = '';
	$items = array();

	if ( isset( $_GET['_ljmccf7'] )
	&& $contact_form = ljmccf7_contact_form( (int) $_GET['_ljmccf7'] ) ) {
		$items = apply_filters( 'ljmccf7_ajax_onload', $items );
	}

	$echo = json_encode( $items );

	if ( ljmccf7_is_xhr() ) {
		@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
		echo $echo;
	}

	exit();
}

function ljmccf7_ajax_json_echo() {
	$echo = '';

	if ( isset( $_POST['_ljmccf7'] ) ) {
		$id = (int) $_POST['_ljmccf7'];
		$unit_tag = ljmccf7_sanitize_unit_tag( $_POST['_ljmccf7_unit_tag'] );

		if ( $contact_form = ljmccf7_contact_form( $id ) ) {
			$items = array(
				'mailSent' => false,
				'into' => '#' . $unit_tag,
				'captcha' => null );

			$result = $contact_form->submit( true );

			if ( ! empty( $result['message'] ) ) {
				$items['message'] = $result['message'];
			}

			if ( 'mail_sent' == $result['status'] ) {
				$items['mailSent'] = true;
			}

			if ( 'validation_failed' == $result['status'] ) {
				$invalids = array();

				foreach ( $result['invalid_fields'] as $name => $field ) {
					$invalids[] = array(
						'into' => 'span.ljmccf7-form-control-wrap.'
							. sanitize_html_class( $name ),
						'message' => $field['reason'],
						'idref' => $field['idref'] );
				}

				$items['invalids'] = $invalids;
			}

			if ( 'spam' == $result['status'] ) {
				$items['spam'] = true;
			}

			if ( ! empty( $result['scripts_on_sent_ok'] ) ) {
				$items['onSentOk'] = $result['scripts_on_sent_ok'];
			}

			if ( ! empty( $result['scripts_on_submit'] ) ) {
				$items['onSubmit'] = $result['scripts_on_submit'];
			}

			$items = apply_filters( 'ljmccf7_ajax_json_echo', $items, $result );
		}
	}

	$echo = json_encode( $items );

	if ( ljmccf7_is_xhr() ) {
		@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
		echo $echo;
	} else {
		@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
		echo '<textarea>' . $echo . '</textarea>';
	}

	exit();
}

function ljmccf7_is_xhr() {
	if ( ! isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) )
		return false;

	return $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
}

function ljmccf7_submit_nonajax() {
	if ( ! isset( $_POST['_ljmccf7'] ) )
		return;

	if ( $contact_form = ljmccf7_contact_form( (int) $_POST['_ljmccf7'] ) ) {
		$contact_form->submit();
	}
}

add_filter( 'widget_text', 'ljmccf7_widget_text_filter', 9 );

function ljmccf7_widget_text_filter( $content ) {
	if ( ! preg_match( '/\[[\r\n\t ]*contact-form(-7)?[\r\n\t ].*?\]/', $content ) )
		return $content;

	$content = do_shortcode( $content );

	return $content;
}

add_action( 'ljmc_enqueue_scripts', 'ljmccf7_do_enqueue_scripts' );

function ljmccf7_do_enqueue_scripts() {
	if ( ljmccf7_load_js() ) {
		ljmccf7_enqueue_scripts();
	}

	if ( ljmccf7_load_css() ) {
		ljmccf7_enqueue_styles();
	}
}

function ljmccf7_enqueue_scripts() {
	// jquery.form.js originally bundled with WordPress is out of date and deprecated
	// so we need to deregister it and re-register the latest one
	ljmc_deregister_script( 'jquery-form' );
	ljmc_register_script( 'jquery-form',
		ljmccf7_plugin_url( 'includes/js/jquery.form.min.js' ),
		array( 'jquery' ), '3.51.0-2014.06.20', true );

	$in_footer = true;

	if ( 'header' === ljmccf7_load_js() ) {
		$in_footer = false;
	}

	ljmc_enqueue_script( 'contact-form-7',
		ljmccf7_plugin_url( 'includes/js/scripts.js' ),
		array( 'jquery', 'jquery-form' ), LJMCCF7_VERSION, $in_footer );

	$_ljmccf7 = array(
		'loaderUrl' => ljmccf7_ajax_loader(),
		'sending' => __( 'Sending ...', 'contact-form-7' ) );

	if ( defined( 'LJMC_CACHE' ) && LJMC_CACHE )
		$_ljmccf7['cached'] = 1;

	if ( ljmccf7_support_html5_fallback() )
		$_ljmccf7['jqueryUi'] = 1;

	ljmc_localize_script( 'contact-form-7', '_ljmccf7', $_ljmccf7 );

	do_action( 'ljmccf7_enqueue_scripts' );
}

function ljmccf7_script_is() {
	return ljmc_script_is( 'contact-form-7' );
}

function ljmccf7_enqueue_styles() {
	ljmc_enqueue_style( 'contact-form-7',
		ljmccf7_plugin_url( 'includes/css/styles.css' ),
		array(), LJMCCF7_VERSION, 'all' );

	if ( ljmccf7_is_rtl() ) {
		ljmc_enqueue_style( 'contact-form-7-rtl',
			ljmccf7_plugin_url( 'includes/css/styles-rtl.css' ),
			array(), LJMCCF7_VERSION, 'all' );
	}

	do_action( 'ljmccf7_enqueue_styles' );
}

function ljmccf7_style_is() {
	return ljmc_style_is( 'contact-form-7' );
}

/* HTML5 Fallback */

add_action( 'ljmc_enqueue_scripts', 'ljmccf7_html5_fallback', 20 );

function ljmccf7_html5_fallback() {
	if ( ! ljmccf7_support_html5_fallback() ) {
		return;
	}

	if ( ljmccf7_script_is() ) {
		ljmc_enqueue_script( 'jquery-ui-datepicker' );
		ljmc_enqueue_script( 'jquery-ui-spinner' );
	}

	if ( ljmccf7_style_is() ) {
		ljmc_enqueue_style( 'jquery-ui-smoothness',
			ljmccf7_plugin_url( 'includes/js/jquery-ui/themes/smoothness/jquery-ui.min.css' ), array(), '1.10.3', 'screen' );
	}
}
