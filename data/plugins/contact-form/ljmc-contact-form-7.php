<?php
/*
Plugin Name: Contact Form
Plugin URI: http://contactform7.com/
Description: Just another contact form plugin. Simple but flexible.
Author: Takayuki Miyoshi
Author URI: http://ideasilo.ljmc.com/
Text Domain: contact-form-7
Domain Path: /languages/
Version: 4.2.2
*/

/*  Copyright 2007-2015 Takayuki Miyoshi (email: takayukister at gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

define( 'LJMCCF7_VERSION', '4.2.2' );

define( 'LJMCCF7_REQUIRED_LJMC_VERSION', '4.1' );

define( 'LJMCCF7_PLUGIN', __FILE__ );

define( 'LJMCCF7_PLUGIN_BASENAME', plugin_basename( LJMCCF7_PLUGIN ) );

define( 'LJMCCF7_PLUGIN_NAME', trim( dirname( LJMCCF7_PLUGIN_BASENAME ), '/' ) );

define( 'LJMCCF7_PLUGIN_DIR', untrailingslashit( dirname( LJMCCF7_PLUGIN ) ) );

define( 'LJMCCF7_PLUGIN_MODULES_DIR', LJMCCF7_PLUGIN_DIR . '/modules' );

if ( ! defined( 'LJMCCF7_LOAD_JS' ) ) {
	define( 'LJMCCF7_LOAD_JS', true );
}

if ( ! defined( 'LJMCCF7_LOAD_CSS' ) ) {
	define( 'LJMCCF7_LOAD_CSS', true );
}

if ( ! defined( 'LJMCCF7_AUTOP' ) ) {
	define( 'LJMCCF7_AUTOP', true );
}

if ( ! defined( 'LJMCCF7_USE_PIPE' ) ) {
	define( 'LJMCCF7_USE_PIPE', true );
}

if ( ! defined( 'LJMCCF7_ADMIN_READ_CAPABILITY' ) ) {
	define( 'LJMCCF7_ADMIN_READ_CAPABILITY', 'edit_posts' );
}

if ( ! defined( 'LJMCCF7_ADMIN_READ_WRITE_CAPABILITY' ) ) {
	define( 'LJMCCF7_ADMIN_READ_WRITE_CAPABILITY', 'publish_pages' );
}

if ( ! defined( 'LJMCCF7_VERIFY_NONCE' ) ) {
	define( 'LJMCCF7_VERIFY_NONCE', true );
}

// Deprecated, not used in the plugin core. Use ljmccf7_plugin_url() instead.
define( 'LJMCCF7_PLUGIN_URL', untrailingslashit( plugins_url( '', LJMCCF7_PLUGIN ) ) );

require_once LJMCCF7_PLUGIN_DIR . '/settings.php';
