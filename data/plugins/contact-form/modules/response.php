<?php
/**
** A base module for [response]
**/

/* Shortcode handler */

ljmccf7_add_shortcode( 'response', 'ljmccf7_response_shortcode_handler' );

function ljmccf7_response_shortcode_handler( $tag ) {
	if ( $contact_form = ljmccf7_get_current_contact_form() ) {
		return $contact_form->form_response_output();
	}
}

?>