<?php
/**
 * @package LJMCSEO\Main
 */

if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

/**
 * @internal Nobody should be able to overrule the real version number as this can cause serious issues
 * with the options, so no if ( ! defined() )
 */
define( 'LJMCSEO_VERSION', '2.3.4' );

if ( ! defined( 'LJMCSEO_PATH' ) ) {
	define( 'LJMCSEO_PATH', plugin_dir_path( LJMCSEO_FILE ) );
}

if ( ! defined( 'LJMCSEO_BASENAME' ) ) {
	define( 'LJMCSEO_BASENAME', plugin_basename( LJMCSEO_FILE ) );
}

if ( ! defined( 'LJMCSEO_CSSJS_SUFFIX' ) ) {
	define( 'LJMCSEO_CSSJS_SUFFIX', ( ( defined( 'SCRIPT_DEBUG' ) && true === SCRIPT_DEBUG ) ? '' : '.min' ) );
}


/* ***************************** CLASS AUTOLOADING *************************** */

/**
 * Auto load our class files
 *
 * @param string $class Class name.
 *
 * @return void
 */
function ljmcseo_auto_load( $class ) {
	static $classes = null;

	if ( $classes === null ) {
		$classes = array(
			'ljmc_list_table'                      => ABSPATH . 'ljmc-admin/includes/class-ljmc-list-table.php',
			'walker_category'                    => ABSPATH . 'ljmc-includes/category-template.php',
			'pclzip'                             => ABSPATH . 'ljmc-admin/includes/class-pclzip.php',
		);
	}

	$cn = strtolower( $class );

	if ( ! class_exists( $class ) && isset( $classes[ $cn ] ) ) {
		require_once( $classes[ $cn ] );
	}
}

if ( file_exists( LJMCSEO_PATH . '/vendor/autoload_52.php' ) ) {
	require LJMCSEO_PATH . '/vendor/autoload_52.php';
}
elseif ( ! class_exists( 'LJMCSEO_Options' ) ) { // Still checking since might be site-level autoload R.
	add_action( 'admin_init', 'yoast_ljmcseo_missing_autoload', 1 );
	return;
}

if ( function_exists( 'spl_autoload_register' ) ) {
	spl_autoload_register( 'ljmcseo_auto_load' );
}


/* ***************************** PLUGIN (DE-)ACTIVATION *************************** */

/**
 * Run single site / network-wide activation of the plugin.
 *
 * @param bool $networkwide Whether the plugin is being activated network-wide.
 */
function ljmcseo_activate( $networkwide = false ) {
	if ( ! is_multisite() || ! $networkwide ) {
		_ljmcseo_activate();
	}
	else {
		/* Multi-site network activation - activate the plugin for all blogs */
		ljmcseo_network_activate_deactivate( true );
	}
}

/**
 * Run single site / network-wide de-activation of the plugin.
 *
 * @param bool $networkwide Whether the plugin is being de-activated network-wide.
 */
function ljmcseo_deactivate( $networkwide = false ) {
	if ( ! is_multisite() || ! $networkwide ) {
		_ljmcseo_deactivate();
	}
	else {
		/* Multi-site network activation - de-activate the plugin for all blogs */
		ljmcseo_network_activate_deactivate( false );
	}
}

/**
 * Run network-wide (de-)activation of the plugin
 *
 * @param bool $activate True for plugin activation, false for de-activation.
 */
function ljmcseo_network_activate_deactivate( $activate = true ) {
	global $ljmcdb;

	$original_blog_id = get_current_blog_id(); // Alternatively use: $ljmcdb->blogid.
	$all_blogs        = $ljmcdb->get_col( "SELECT blog_id FROM $ljmcdb->blogs" );

	if ( is_array( $all_blogs ) && $all_blogs !== array() ) {
		foreach ( $all_blogs as $blog_id ) {
			switch_to_blog( $blog_id );

			if ( $activate === true ) {
				_ljmcseo_activate();
			}
			else {
				_ljmcseo_deactivate();
			}
		}
		// Restore back to original blog.
		switch_to_blog( $original_blog_id );
	}
}

/**
 * Runs on activation of the plugin.
 */
function _ljmcseo_activate() {
	require_once( LJMCSEO_PATH . 'inc/ljmcseo-functions.php' );

	ljmcseo_load_textdomain(); // Make sure we have our translations available for the defaults.
	LJMCSEO_Options::get_instance();
	if ( ! is_multisite() ) {
		LJMCSEO_Options::initialize();
	}
	else {
		LJMCSEO_Options::maybe_set_multisite_defaults( true );
	}
	LJMCSEO_Options::ensure_options_exist();

	add_action( 'shutdown', 'flush_rewrite_rules' );

	ljmcseo_add_capabilities();

	// Clear cache so the changes are obvious.
	LJMCSEO_Utils::clear_cache();

	do_action( 'ljmcseo_activate' );
}

/**
 * On deactivation, flush the rewrite rules so XML sitemaps stop working.
 */
function _ljmcseo_deactivate() {
	require_once( LJMCSEO_PATH . 'inc/ljmcseo-functions.php' );

	add_action( 'shutdown', 'flush_rewrite_rules' );

	ljmcseo_remove_capabilities();

	// Clear cache so the changes are obvious.
	LJMCSEO_Utils::clear_cache();

	do_action( 'ljmcseo_deactivate' );
}

/**
 * Run ljmcseo activation routine on creation / activation of a multisite blog if LJMCSEO is activated
 * network-wide.
 *
 * Will only be called by multisite actions.
 *
 * @internal Unfortunately will fail if the plugin is in the must-use directory
 * @see      https://core.trac.ljmc.org/ticket/24205
 *
 * @param int $blog_id
 */
function ljmcseo_on_activate_blog( $blog_id ) {
	if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
		require_once( ABSPATH . '/ljmc-admin/includes/plugin.php' );
	}

	if ( is_plugin_active_for_network( plugin_basename( LJMCSEO_FILE ) ) ) {
		switch_to_blog( $blog_id );
		ljmcseo_activate( false );
		restore_current_blog();
	}
}


/* ***************************** PLUGIN LOADING *************************** */

/**
 * Load translations
 */
function ljmcseo_load_textdomain() {
	$ljmcseo_path = str_replace( '\\', '/', LJMCSEO_PATH );
	$mu_path    = str_replace( '\\', '/', LJMCMU_PLUGIN_DIR );

	if ( false !== stripos( $ljmcseo_path, $mu_path ) ) {
		load_muplugin_textdomain( 'ljmc-seo', dirname( LJMCSEO_BASENAME ) . '/languages/' );
	}
	else {
		load_plugin_textdomain( 'ljmc-seo', false, dirname( LJMCSEO_BASENAME ) . '/languages/' );
	}
}

add_action( 'init', 'ljmcseo_load_textdomain', 1 );


/**
 * On plugins_loaded: load the minimum amount of essential files for this plugin
 */
function ljmcseo_init() {
	require_once( LJMCSEO_PATH . 'inc/ljmcseo-functions.php' );

	// Make sure our option and meta value validation routines and default values are always registered and available.
	LJMCSEO_Options::get_instance();
	LJMCSEO_Meta::init();

	$options = LJMCSEO_Options::get_all();
	if ( version_compare( $options['version'], LJMCSEO_VERSION, '<' ) ) {
		new LJMCSEO_Upgrade();
		// Get a cleaned up version of the $options.
		$options = LJMCSEO_Options::get_all();
	}

	if ( $options['stripcategorybase'] === true ) {
		$GLOBALS['ljmcseo_rewrite'] = new LJMCSEO_Rewrite;
	}

	if ( $options['enablexmlsitemap'] === true ) {
		$GLOBALS['ljmcseo_sitemaps'] = new LJMCSEO_Sitemaps;
	}

	if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) {
		require_once( LJMCSEO_PATH . 'inc/ljmcseo-non-ajax-functions.php' );
	}

	// Init it here because the filter must be present on the frontend as well or it won't work in the customizer.
	new LJMCSEO_Customizer();
}

/**
 * Used to load the required files on the plugins_loaded hook, instead of immediately.
 */
function ljmcseo_frontend_init() {
	add_action( 'init', 'initialize_ljmcseo_front' );

	$options = LJMCSEO_Options::get_all();
	if ( $options['breadcrumbs-enable'] === true ) {
		/**
		 * If breadcrumbs are active (which they supposedly are if the users has enabled this settings,
		 * there's no reason to have bbPress breadcrumbs as well.
		 *
		 * @internal The class itself is only loaded when the template tag is encountered via
		 * the template tag function in the ljmcseo-functions.php file
		 */
		add_filter( 'bbp_get_breadcrumb', '__return_false' );
	}

	add_action( 'template_redirect', 'ljmcseo_frontend_head_init', 999 );
}

/**
 * Instantiate the different social classes on the frontend
 */
function ljmcseo_frontend_head_init() {
	$options = LJMCSEO_Options::get_all();
	if ( $options['twitter'] === true ) {
		add_action( 'ljmcseo_head', array( 'LJMCSEO_Twitter', 'get_instance' ), 40 );
	}

	if ( $options['opengraph'] === true ) {
		$GLOBALS['ljmcseo_og'] = new LJMCSEO_OpenGraph;
	}

	if ( $options['googleplus'] === true && is_singular() ) {
		add_action( 'ljmcseo_head', array( 'LJMCSEO_GooglePlus', 'get_instance' ), 35 );
	}
}

/**
 * Used to load the required files on the plugins_loaded hook, instead of immediately.
 */
function ljmcseo_admin_init() {
	new LJMCSEO_Admin_Init();
}


/* ***************************** BOOTSTRAP / HOOK INTO LJMC *************************** */
$spl_autoload_exists = function_exists( 'spl_autoload_register' );
$filter_exists       = function_exists( 'filter_input' );

if ( ! $spl_autoload_exists ) {
	add_action( 'admin_init', 'yoast_ljmcseo_missing_spl', 1 );
}

if ( ! $filter_exists ) {
	add_action( 'admin_init', 'yoast_ljmcseo_missing_filter', 1 );
}

if ( ( ! defined( 'LJMC_INSTALLING' ) || LJMC_INSTALLING === false ) && ( $spl_autoload_exists && $filter_exists ) ) {
	add_action( 'plugins_loaded', 'ljmcseo_init', 14 );

	if ( is_admin() ) {
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			require_once( LJMCSEO_PATH . 'admin/ajax.php' );

			// Crawl Issue Manager AJAX hooks.
			new LJMCSEO_GSC_Ajax;

			// Plugin conflict ajax hooks.
			new _Plugin_Conflict_Ajax();

		}
		else {
			add_action( 'plugins_loaded', 'ljmcseo_admin_init', 15 );
		}
	}
	else {
		add_action( 'plugins_loaded', 'ljmcseo_frontend_init', 15 );
	}

	add_action( 'admin_init', 'load_yoast_notifications' );
}

// Activation and deactivation hook.
register_activation_hook( LJMCSEO_FILE, 'ljmcseo_activate' );
register_activation_hook( LJMCSEO_FILE, array( 'LJMCSEO_Plugin_Conflict', 'hook_check_for_plugin_conflicts' ) );
register_deactivation_hook( LJMCSEO_FILE, 'ljmcseo_deactivate' );
add_action( 'ljmcmu_new_blog', 'ljmcseo_on_activate_blog' );
add_action( 'activate_blog', 'ljmcseo_on_activate_blog' );

/**
 * Wraps for notifications center class.
 */
function load_yoast_notifications() {
	// Init _Notification_Center class.
	_Notification_Center::get();
}


/**
 * Throw an error if the PHP SPL extension is disabled (prevent white screens) and self-deactivate plugin
 *
 * @since 1.5.4
 *
 * @return void
 */
function yoast_ljmcseo_missing_spl() {
	if ( is_admin() ) {
		add_action( 'admin_notices', 'yoast_ljmcseo_missing_spl_notice' );

		yoast_ljmcseo_self_deactivate();
	}
}

/**
 * Returns the notice in case of missing spl extension
 */
function yoast_ljmcseo_missing_spl_notice() {
	$message = esc_html__( 'The Standard PHP Library (SPL) extension seem to be unavailable. Please ask your web host to enable it.', 'ljmc-seo' );
	yoast_ljmcseo_activation_failed_notice( $message );
}

/**
 * Throw an error if the Composer autoload is missing and self-deactivate plugin
 *
 * @return void
 */
function yoast_ljmcseo_missing_autoload() {
	if ( is_admin() ) {
		add_action( 'admin_notices', 'yoast_ljmcseo_missing_autoload_notice' );

		yoast_ljmcseo_self_deactivate();
	}
}

/**
 * Returns the notice in case of missing Composer autoload
 */
function yoast_ljmcseo_missing_autoload_notice() {
	/* translators: %1$s expands to  SEO, %2$s / %3$s: links to the installation manual in the Readme for the  SEO code repository on GitHub */
	$message = esc_html__( 'The %1$s plugin installation is incomplete. Please refer to %2$sinstallation instructions%3$s.', 'ljmc-seo' );
	$message = sprintf( $message, ' SEO', '<a href="https://github.com//ljmc-seo#installation">', '</a>' );
	yoast_ljmcseo_activation_failed_notice( $message );
}

/**
 * Throw an error if the filter extension is disabled (prevent white screens) and self-deactivate plugin
 *
 * @since 2.0
 *
 * @return void
 */
function yoast_ljmcseo_missing_filter() {
	if ( is_admin() ) {
		add_action( 'admin_notices', 'yoast_ljmcseo_missing_filter_notice' );

		yoast_ljmcseo_self_deactivate();
	}
}

/**
 * Returns the notice in case of missing filter extension
 */
function yoast_ljmcseo_missing_filter_notice() {
	$message = esc_html__( 'The filter extension seem to be unavailable. Please ask your web host to enable it.', 'ljmc-seo' );
	yoast_ljmcseo_activation_failed_notice( $message );
}

/**
 * Echo's the Activation failed notice with any given message.
 *
 * @param string $message
 */
function yoast_ljmcseo_activation_failed_notice( $message ) {
	echo '<div class="error"><p>' . __( 'Activation failed:', 'ljmc-seo' ) . ' ' . $message . '</p></div>';
}

/**
 * The method will deactivate the plugin, but only once, done by the static $is_deactivated
 */
function yoast_ljmcseo_self_deactivate() {
	static $is_deactivated;

	if ( $is_deactivated === null ) {
		$is_deactivated = true;
		deactivate_plugins( plugin_basename( LJMCSEO_FILE ) );
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}
}


