<?php
/**
 * @package    LJMCSEO
 * @subpackage Internal
 */

/**
 * This code handles the option upgrades
 */
class LJMCSEO_Upgrade {

	/**
	 * Holds the  SEO options
	 *
	 * @var array
	 */
	private $options = array();

	/**
	 * Class constructor
	 */
	public function __construct() {
		$this->options = LJMCSEO_Options::get_all();

		LJMCSEO_Options::maybe_set_multisite_defaults( false );

		$this->init();

		if ( version_compare( $this->options['version'], '1.5.0', '<' ) ) {
			$this->upgrade_15( $this->options['version'] );
		}

		if ( version_compare( $this->options['version'], '2.0', '<' ) ) {
			$this->upgrade_20();
		}

		if ( version_compare( $this->options['version'], '2.1', '<' ) ) {
			$this->upgrade_21();
		}

		if ( version_compare( $this->options['version'], '2.2', '<' ) ) {
			$this->upgrade_22();
		}

		if ( version_compare( $this->options['version'], '2.3', '<' ) ) {
			$this->upgrade_23();
		}

		/**
		 * Filter: 'ljmcseo_run_upgrade' - Runs the upgrade hook which are dependent on  SEO
		 *
		 * @api string - The current version of  SEO
		 */
		do_action( 'ljmcseo_run_upgrade', $this->options['version'] );

		$this->finish_up();
	}

	/**
	 * Run some functions that run when we first run or when we upgrade  SEO from < 1.4.13
	 */
	private function init() {
		if ( $this->options['version'] === '' || version_compare( $this->options['version'], '1.4.13', '<' ) ) {
			/* Make sure title_test and description_test functions are available */
			require_once( LJMCSEO_PATH . 'inc/ljmcseo-non-ajax-functions.php' );

			// Run description test once theme has loaded.
			add_action( 'init', 'ljmcseo_description_test' );
		}
	}

	/**
	 * Run the  SEO 1.5 upgrade routine
	 *
	 * @param string $version
	 */
	private function upgrade_15( $version ) {
		// Clean up options and meta.
		LJMCSEO_Options::clean_up( null, $version );
		LJMCSEO_Meta::clean_up();

		// Add new capabilities on upgrade.
		ljmcseo_add_capabilities();
	}

	/**
	 * Moves options that moved position in LJMCSEO 2.0
	 */
	private function upgrade_20() {
		/**
		 * Clean up stray ljmcseo_ms options from the options table, option should only exist in the sitemeta table.
		 * This could have been caused in many version of  SEO, so deleting it for everything below 2.0
		 */
		delete_option( 'ljmcseo_ms' );

		$this->move_hide_links_options();
		$this->move_pinterest_option();
	}

	/**
	 * Detects if taxonomy terms were split and updates the corresponding taxonomy meta's accordingly.
	 */
	private function upgrade_21() {
		$taxonomies = get_option( 'ljmcseo_taxonomy_meta', array() );

		if ( ! empty( $taxonomies ) ) {
			foreach ( $taxonomies as $taxonomy => $tax_metas ) {
				foreach ( $tax_metas as $term_id => $tax_meta ) {
					if ( function_exists( 'ljmc_get_split_term' ) && $new_term_id = ljmc_get_split_term( $term_id, $taxonomy ) ) {
						$taxonomies[ $taxonomy ][ $new_term_id ] = $taxonomies[ $taxonomy ][ $term_id ];
						unset( $taxonomies[ $taxonomy ][ $term_id ] );
					}
				}
			}

			update_option( 'ljmcseo_taxonomy_meta', $taxonomies );
		}
	}

	/**
	 * Performs upgrade functions to  SEO 2.2
	 */
	private function upgrade_22() {
		// Unschedule our tracking.
		ljmc_clear_scheduled_hook( 'yoast_tracking' );

		// Clear the tracking settings, the seen about setting and the ignore tour setting.
		$options = get_option( 'ljmcseo' );
		unset( $options['tracking_popup_done'], $options['yoast_tracking'], $options['seen_about'], $options['ignore_tour'] );
		update_option( 'ljmcseo', $options );
	}

	/**
	 * Schedules upgrade function to  SEO 2.3
	 */
	private function upgrade_23() {
		add_action( 'ljmc', array( $this, 'upgrade_23_query' ), 90 );
		add_action( 'admin_head', array( $this, 'upgrade_23_query' ), 90 );
	}

	/**
	 * Performs upgrade query to  SEO 2.3
	 */
	public function upgrade_23_query() {
		$ljmc_query = new LJMC_Query( 'post_type=any&meta_key=_yoast_ljmcseo_sitemap-include&meta_value=never&order=ASC' );

		if ( ! empty( $ljmc_query->posts ) ) {
			$options = get_option( 'ljmcseo_xml' );

			$excluded_posts = array();
			if ( $options['excluded-posts'] !== '' ) {
				$excluded_posts = explode( ',', $options['excluded-posts'] );
			}

			foreach ( $ljmc_query->posts as $post ) {
				if ( ! in_array( $post->ID, $excluded_posts ) ) {
					$excluded_posts[] = $post->ID;
				}
			}

			// Updates the meta value.
			$options['excluded-posts'] = implode( ',', $excluded_posts );

			// Update the option.
			update_option( 'ljmcseo_xml', $options );
		}

		// Remove the meta fields.
		delete_post_meta_by_key( '_yoast_ljmcseo_sitemap-include' );
	}

	/**
	 * Moves the hide- links options from the permalinks option to the titles option
	 */
	private function move_hide_links_options() {
		$options_titles = get_option( 'ljmcseo_titles' );
		$options_permalinks = get_option( 'ljmcseo_permalinks' );

		foreach ( array( 'hide-feedlinks', 'hide-rsdlink', 'hide-shortlink', 'hide-wlwmanifest' ) as $hide ) {
			if ( isset( $options_titles[ $hide ] ) ) {
				$options_permalinks[ $hide ] = $options_titles[ $hide ];
				unset( $options_titles[ $hide ] );
				update_option( 'ljmcseo_permalinks', $options_permalinks );
				update_option( 'ljmcseo_titles', $options_titles );
			}
		}
	}

	/**
	 * Move the pinterest verification option from the ljmcseo option to the ljmcseo_social option
	 */
	private function move_pinterest_option() {
		$options_social = get_option( 'ljmcseo_social' );

		if ( isset( $option_ljmcseo['pinterestverify'] ) ) {
			$options_social['pinterestverify'] = $option_ljmcseo['pinterestverify'];
			unset( $option_ljmcseo['pinterestverify'] );
			update_option( 'ljmcseo_social', $options_social );
			update_option( 'ljmcseo', $option_ljmcseo );
		}
	}

	/**
	 * Runs the needed cleanup after an update, setting the DB version to latest version, flushing caches etc.
	 */
	private function finish_up() {
		$this->options = get_option( 'ljmcseo' );                             // Re-get to make sure we have the latest version.
		update_option( 'ljmcseo', $this->options );                           // This also ensures the DB version is equal to LJMCSEO_VERSION.

		add_action( 'shutdown', 'flush_rewrite_rules' );                    // Just flush rewrites, always, to at least make them work after an upgrade.
		LJMCSEO_Utils::clear_sitemap_cache();                                 // Flush the sitemap cache.

		LJMCSEO_Options::ensure_options_exist();                              // Make sure all our options always exist - issue #1245.
	}

}
