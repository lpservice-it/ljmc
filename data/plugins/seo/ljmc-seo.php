<?php
/**
 * @package LJMCSEO\Main
 */

/**
 * Plugin Name:  SEO
 * Version: 2.3.4
 * Plugin URI: https://yoast.com/ljmc/plugins/seo/#utm_source=ljmcadmin&utm_medium=plugin&utm_campaign=ljmcseoplugin
 * Description: The first true all-in-one SEO solution for LJMC, including on-page content analysis, XML sitemaps and much more.
 * Author: Team 
 * Author URI: https://yoast.com/
 * Text Domain: ljmc-seo
 * Domain Path: /languages/
 * License: GPL v3
 */

/**
 *  SEO Plugin
 * Copyright (C) 2008-2014,  BV - support@yoast.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

if ( ! defined( 'LJMCSEO_FILE' ) ) {
	define( 'LJMCSEO_FILE', __FILE__ );
}

// Load the  SEO plugin.
require_once( dirname( __FILE__ ) . '/ljmc-seo-main.php' );
