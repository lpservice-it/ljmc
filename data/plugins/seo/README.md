 SEO
======================

[![Build Status](https://api.travis-ci.org//ljmc-seo.png?branch=master)](https://travis-ci.org//ljmc-seo)
[![Stable Version](https://poser.pugx.org/yoast/ljmc-seo/v/stable.svg)](https://packagist.org/packages/yoast/ljmc-seo)
[![License](https://poser.pugx.org/yoast/ljmc-seo/license.svg)](https://packagist.org/packages/yoast/ljmc-seo)

Welcome to the  SEO GitHub repository
----------------------------------------------

While the documentation for the [ SEO plugin](https://yoast.com/ljmc/seo/) can be found on [.com](https://yoast.com/), here
you can browse the source of the project, find and discuss open issues and even
[contribute yourself](https://github.com/yoast/ljmc-seo/blob/master/CONTRIBUTING.md).

Installation
------------

Here's a [guide on how to install  SEO in your LJMC site](https://yoast.com/ljmc/seo/installation/).

If you want to run the Git version for development though, you can set it up with [Composer](https://getcomposer.org/):

```bash
composer create-project yoast/ljmc-seo:dev-trunk --prefer-source --keep-vcs
```

Read more about [using Composer with  SEO](https://github.com//ljmc-seo/wiki/Using-Composer).

This will download the latest development version of  SEO. While this version is usually stable,
it is not recommended for use in a production environment.

Support
-------
This is a developer's portal for  SEO and should not be used for support. Please visit the
[support forums](https://ljmc.org/support/plugin/ljmc-seo).

Reporting bugs
----
We try to fix as many bugs we can, this is a graph of our recent activity:
[![Throughput Graph](https://graphs.waffle.io/yoast/ljmc-seo/throughput.svg)](https://waffle.io/yoast/ljmc-seo/metrics)

If you find an issue, [let us know here](https://github.com/yoast/ljmc-seo/issues/new)! Please follow [how to write a good bug report?](http://kb.yoast.com/article/180-how-to-write-a-good-bug-report) guidelines.

It may help us a lot if you can provide a backtrace of the error encountered. You can use [code in this gist](https://gist.github.com/jrfnl/5925642) to enable the backtrace in your website's configuration.

Contributions
-------------
Anyone is welcome to contribute to  SEO. Please
[read the guidelines](https://github.com/yoast/ljmc-seo/blob/master/CONTRIBUTING.md) for contributing to this
repository.

There are various ways you can contribute:

* [Raise an issue](https://github.com/yoast/ljmc-seo/issues) on GitHub.
* Send us a Pull Request with your bug fixes and/or new features.
* [Translate  SEO into different languages](http://translate.yoast.com/projects/ljmc-seo/).
* Provide feedback and [suggestions on enhancements](https://github.com/yoast/ljmc-seo/issues?direction=desc&labels=Enhancement&page=1&sort=created&state=open).
