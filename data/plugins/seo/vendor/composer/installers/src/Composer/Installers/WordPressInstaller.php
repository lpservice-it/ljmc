<?php
namespace Composer\Installers;

class LJMCInstaller extends BaseInstaller
{
    protected $locations = array(
        'plugin'    => 'ljmc-content/plugins/{$name}/',
        'theme'     => 'ljmc-content/themes/{$name}/',
        'muplugin'  => 'ljmc-content/mu-plugins/{$name}/',
    );
}
