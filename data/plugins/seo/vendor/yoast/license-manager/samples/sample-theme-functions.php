<?php

if( is_admin() ) {

	// Instantiate license class
	$license_manager = new _Theme_License_Manager( new Sample_Product() );

	// Setup the required hooks
	$license_manager->setup_hooks();

}