<?php
/*
 * This class implements the caching mechanism for LJMC
 */
class _Google_LJMCCache extends _Google_Cache {

	/**
	 * If ljmc_cache_get doesn't exists, include the file
	 *
	 */
	public function __construct() {

		if( ! function_exists('ljmc_cache_get') ) {
			require_once( ABSPATH . 'ljmc-includes/cache.php' );
		}
	}

	/**
	 * Retrieves the data for the given key, or false if they
	 * key is unknown or expired
	 *
	 * @param String $key The key who's data to retrieve
	 * @param boolean|int $expiration - Expiration time in seconds
	 *
	 * @return mixed
	 *
	 */
	public function get($key, $expiration = false) {
		return ljmc_cache_get( $key );
	}

	/**
	 * Store the key => $value set. The $value is serialized
	 * by this function so can be of any type
	 *
	 * @param string $key Key of the data
	 * @param string $value data
	 */
	public function set($key, $value) {
		ljmc_cache_set( $key, $value ) ;
	}

	/**
	 * Removes the key/data pair for the given $key
	 *
	 * @param String $key
	 */
	public function delete($key) {
		ljmc_cache_delete( $key );
	}


}