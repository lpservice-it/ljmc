<?php
/**
 * @package LJMCSEO\Admin
 */

/**
 * Class LJMCSEO_Admin_Pages
 *
 * Class with functionality for the  SEO admin pages.
 */
class LJMCSEO_Admin_Pages {

	/**
	 * @var string $currentoption The option in use for the current admin page.
	 */
	public $currentoption = 'ljmcseo';

	/**
	 * Class constructor, which basically only hooks the init function on the init hook
	 */
	function __construct() {
		add_action( 'init', array( $this, 'init' ), 20 );
	}

	/**
	 * Make sure the needed scripts are loaded for admin pages
	 */
	function init() {
		if ( filter_input( INPUT_GET, 'ljmcseo_reset_defaults' ) && ljmc_verify_nonce( filter_input( INPUT_GET, 'nonce' ), 'ljmcseo_reset_defaults' ) && current_user_can( 'manage_options' ) ) {
			LJMCSEO_Options::reset();
			ljmc_redirect( admin_url( 'admin.php?page=ljmcseo_dashboard' ) );
		}

		if ( LJMCSEO_Utils::grant_access() ) {
			add_action( 'admin_enqueue_scripts', array( $this, 'config_page_scripts' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'config_page_styles' ) );
		}
	}

	/**
	 * Loads the required styles for the config page.
	 */
	function config_page_styles() {
		ljmc_enqueue_style( 'dashboard' );
		ljmc_enqueue_style( 'thickbox' );
		ljmc_enqueue_style( 'global' );
		ljmc_enqueue_style( 'ljmc-admin' );
		ljmc_enqueue_style( 'yoast-admin-css', plugins_url( 'css/yst_plugin_tools' . LJMCSEO_CSSJS_SUFFIX . '.css', LJMCSEO_FILE ), array(), LJMCSEO_VERSION );

		if ( is_rtl() ) {
			ljmc_enqueue_style( 'ljmcseo-rtl', plugins_url( 'css/ljmcseo-rtl' . LJMCSEO_CSSJS_SUFFIX . '.css', LJMCSEO_FILE ), array(), LJMCSEO_VERSION );
		}
	}

	/**
	 * Loads the required scripts for the config page.
	 */
	function config_page_scripts() {
		ljmc_enqueue_script( 'ljmcseo-admin-script', plugins_url( 'js/ljmc-seo-admin' . LJMCSEO_CSSJS_SUFFIX . '.js', LJMCSEO_FILE ), array(
			'jquery',
			'jquery-ui-core',
		), LJMCSEO_VERSION, true );
		ljmc_localize_script( 'ljmcseo-admin-script', 'ljmcseoAdminL10n', $this->localize_admin_script() );
		ljmc_enqueue_script( 'dashboard' );
		ljmc_enqueue_script( 'thickbox' );

		$page = filter_input( INPUT_GET, 'page' );
		$tool = filter_input( INPUT_GET, 'tool' );

		if ( in_array( $page, array( 'ljmcseo_social', 'ljmcseo_dashboard' ) ) ) {
			ljmc_enqueue_media();
			ljmc_enqueue_script( 'ljmcseo-admin-media', plugins_url( 'js/ljmc-seo-admin-media' . LJMCSEO_CSSJS_SUFFIX . '.js', LJMCSEO_FILE ), array(
				'jquery',
				'jquery-ui-core',
			), LJMCSEO_VERSION, true );
			ljmc_localize_script( 'ljmcseo-admin-media', 'ljmcseoMediaL10n', $this->localize_media_script() );
		}

		if ( 'ljmcseo_tools' === $page && 'bulk-editor' === $tool ) {
			ljmc_enqueue_script( 'ljmcseo-bulk-editor', plugins_url( 'js/ljmc-seo-bulk-editor' . LJMCSEO_CSSJS_SUFFIX . '.js', LJMCSEO_FILE ), array( 'jquery' ), LJMCSEO_VERSION, true );
		}

		if ( 'ljmcseo_tools' === $page && 'import-export' === $tool ) {
			ljmc_enqueue_script( 'ljmcseo-export', plugins_url( 'js/ljmc-seo-export' . LJMCSEO_CSSJS_SUFFIX . '.js', LJMCSEO_FILE ), array( 'jquery' ), LJMCSEO_VERSION, true );
		}
	}

	/**
	 * Pass some variables to js for upload module.
	 *
	 * @return  array
	 */
	public function localize_media_script() {
		return array(
			'choose_image' => __( 'Use Image', 'ljmc-seo' ),
		);
	}

	/**
	 * Pass some variables to js for the admin JS module.
	 *
	 * %s is replaced with <code>%s</code> and replaced again in the javascript with the actual variable.
	 *
	 * @return  array
	 */
	public function localize_admin_script() {
		return array(
			/* translators: %s: '%%term_title%%' variable used in titles and meta's template that's not compatible with the given template */
			'variable_warning' => sprintf( __( 'Warning: the variable %s cannot be used in this template.', 'ljmc-seo' ), '<code>%s</code>' ) . ' ' . __( 'See the help tab for more info.', 'ljmc-seo' ),
		);
	}

	/********************** DEPRECATED METHODS **********************/

	/**
	 * Exports the current site's  SEO settings.
	 *
	 * @param bool $include_taxonomy Whether to include the taxonomy metadata the plugin creates.
	 *
	 * @return bool|string $return False when failed, the URL to the export file when succeeded.
	 */
	public function export_settings( $include_taxonomy ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>LJMCSEO_Export</code> class.' );

		$export = new LJMCSEO_Export( $include_taxonomy );
		if ( $export->success ) {
			return $export->export_zip_url;
		}
		else {
			return false;
		}
	}

	/**
	 * Generates the header for admin pages
	 *
	 * @deprecated 2.0
	 *
	 * @param bool   $form             Whether or not the form start tag should be included.
	 * @param mixed  $option_long_name The long name of the option to use for the current page.
	 * @param string $option           The short name of the option to use for the current page.
	 * @param bool   $contains_files   Whether the form should allow for file uploads.
	 */
	public function admin_header( $form = true, $option_long_name = false, $option = 'ljmcseo', $contains_files = false ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		_Form::get_instance()->admin_header( $form, $option, $contains_files, $option_long_name );
	}

	/**
	 * Generates the footer for admin pages
	 *
	 * @deprecated 2.0
	 *
	 * @param bool $submit       Whether or not a submit button and form end tag should be shown.
	 * @param bool $show_sidebar Whether or not to show the banner sidebar - used by premium plugins to disable it.
	 */
	public function admin_footer( $submit = true, $show_sidebar = true ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		_Form::get_instance()->admin_footer( $submit, $show_sidebar );
	}

	/**
	 * Generates the sidebar for admin pages.
	 *
	 * @deprecated 2.0
	 */
	public function admin_sidebar() {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		_Form::get_instance()->admin_sidebar();
	}

	/**
	 * Create a Checkbox input field.
	 *
	 * @deprecated 2.0
	 *
	 * @param string $var        The variable within the option to create the checkbox for.
	 * @param string $label      The label to show for the variable.
	 * @param bool   $label_left Whether the label should be left (true) or right (false).
	 * @param string $option     The option the variable belongs to.
	 */
	public function checkbox( $var, $label, $label_left = false, $option = '' ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		if ( $option !== '' ) {
			_Form::get_instance()->set_option( $option );
		}

		_Form::get_instance()->checkbox( $var, $label, $label_left );
	}

	/**
	 * Create a Text input field.
	 *
	 * @deprecated 2.0
	 *
	 * @param string $var    The variable within the option to create the text input field for.
	 * @param string $label  The label to show for the variable.
	 * @param string $option The option the variable belongs to.
	 */
	function textinput( $var, $label, $option = '' ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		if ( $option !== '' ) {
			_Form::get_instance()->set_option( $option );
		}
		_Form::get_instance()->textinput( $var, $label );
	}

	/**
	 * Create a textarea.
	 *
	 * @deprecated 2.0
	 *
	 * @param string $var    The variable within the option to create the textarea for.
	 * @param string $label  The label to show for the variable.
	 * @param string $option The option the variable belongs to.
	 * @param array  $attr   The CSS class to assign to the textarea.
	 */
	function textarea( $var, $label, $option = '', $attr = array() ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		if ( $option !== '' ) {
			_Form::get_instance()->set_option( $option );
		}

		_Form::get_instance()->textarea( $var, $label, $attr );
	}

	/**
	 * Create a hidden input field.
	 *
	 * @deprecated 2.0
	 *
	 * @param string $var    The variable within the option to create the hidden input for.
	 * @param string $option The option the variable belongs to.
	 */
	function hidden( $var, $option = '' ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		if ( $option !== '' ) {
			_Form::get_instance()->set_option( $option );
		}

		_Form::get_instance()->hidden( $var );
	}

	/**
	 * Create a Select Box.
	 *
	 * @deprecated 2.0
	 *
	 * @param string $var    The variable within the option to create the select for.
	 * @param string $label  The label to show for the variable.
	 * @param array  $values The select options to choose from.
	 * @param string $option The option the variable belongs to.
	 */
	function select( $var, $label, $values, $option = '' ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		if ( $option !== '' ) {
			_Form::get_instance()->set_option( $option );
		}

		_Form::get_instance()->select( $var, $label, $values );
	}

	/**
	 * Create a File upload field.
	 *
	 * @deprecated 2.0
	 *
	 * @param string $var    The variable within the option to create the file upload field for.
	 * @param string $label  The label to show for the variable.
	 * @param string $option The option the variable belongs to.
	 */
	function file_upload( $var, $label, $option = '' ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		if ( $option !== '' ) {
			_Form::get_instance()->set_option( $option );
		}

		_Form::get_instance()->file_upload( $var, $label );
	}

	/**
	 * Media input
	 *
	 * @deprecated 2.0
	 *
	 * @param string $var
	 * @param string $label
	 * @param string $option
	 */
	function media_input( $var, $label, $option = '' ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		if ( $option !== '' ) {
			_Form::get_instance()->set_option( $option );
		}

		_Form::get_instance()->media_input( $var, $label );
	}

	/**
	 * Create a Radio input field.
	 *
	 * @deprecated 2.0
	 *
	 * @param string $var    The variable within the option to create the file upload field for.
	 * @param array  $values The radio options to choose from.
	 * @param string $label  The label to show for the variable.
	 * @param string $option The option the variable belongs to.
	 */
	function radio( $var, $values, $label, $option = '' ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please use the <code>_Form</code> class.' );

		if ( $option !== '' ) {
			_Form::get_instance()->set_option( $option );
		}

		_Form::get_instance()->radio( $var, $values, $label );
	}

	/**
	 * Create a postbox widget.
	 *
	 * @deprecated 2.0
	 *
	 * @param string $id      ID of the postbox.
	 * @param string $title   Title of the postbox.
	 * @param string $content Content of the postbox.
	 */
	function postbox( $id, $title, $content ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please re-implement the admin pages.' );

		?>
			<div id="<?php echo esc_attr( $id ); ?>" class="yoastbox">
				<h2><?php echo $title; ?></h2>
				<?php echo $content; ?>
			</div>
		<?php
	}

	/**
	 * Create a form table from an array of rows.
	 *
	 * @deprecated 2.0
	 *
	 * @param array $rows Rows to include in the table.
	 *
	 * @return string
	 */
	function form_table( $rows ) {
		_deprecated_function( __METHOD__, 'LJMCSEO 2.0', 'This method is deprecated, please re-implement the admin pages.' );

		if ( ! is_array( $rows ) || $rows === array() ) {
			return '';
		}

		$content = '<table class="form-table">';
		foreach ( $rows as $row ) {
			$content .= '<tr><th scope="row">';
			if ( isset( $row['id'] ) && $row['id'] != '' ) {
				$content .= '<label for="' . esc_attr( $row['id'] ) . '">' . esc_html( $row['label'] ) . ':</label>';
			}
			else {
				$content .= esc_html( $row['label'] );
			}
			if ( isset( $row['desc'] ) && $row['desc'] != '' ) {
				$content .= '<br/><small>' . esc_html( $row['desc'] ) . '</small>';
			}
			$content .= '</th><td>';
			$content .= $row['content'];
			$content .= '</td></tr>';
		}
		$content .= '</table>';

		return $content;
	}

	/**
	 * Resets the site to the default  SEO settings and runs a title test to check
	 * whether force rewrite needs to be on.
	 *
	 * @deprecated 1.5.0
	 * @deprecated use LJMCSEO_Options::reset()
	 * @see        LJMCSEO_Options::reset()
	 */
	function reset_defaults() {
		_deprecated_function( __METHOD__, 'LJMCSEO 1.5.0', 'LJMCSEO_Options::reset()' );
		LJMCSEO_Options::reset();
	}


} /* End of class */
