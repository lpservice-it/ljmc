<?php
/**
 * @package LJMCSEO\Admin
 */

/**
 * Performs the load on admin side.
 */
class LJMCSEO_Admin_Init {

	/**
	 * Holds the  SEO Options
	 *
	 * @var array
	 */
	private $options;

	/**
	 * Holds the global `$pagenow` variable's value.
	 *
	 * @var string
	 */
	private $pagenow;

	/**
	 * Class constructor
	 */
	public function __construct() {
		$this->options = LJMCSEO_Options::get_all();

		$GLOBALS['ljmcseo_admin'] = new LJMCSEO_Admin;

		$this->pagenow = $GLOBALS['pagenow'];

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_dismissible' ) );
		add_action( 'admin_init', array( $this, 'after_update_notice' ), 15 );
		add_action( 'admin_init', array( $this, 'tagline_notice' ), 15 );
		add_action( 'admin_init', array( $this, 'ga_compatibility_notice' ), 15 );

		$this->load_meta_boxes();
		$this->load_taxonomy_class();
		$this->load_admin_page_class();
		$this->load_admin_user_class();
		$this->ignore_tour();
		$this->load_tour();
		$this->load_xml_sitemaps_admin();
	}

	/**
	 * For LJMC versions older than 4.2, this includes styles and a script to make notices dismissible.
	 */
	public function enqueue_dismissible() {
		if ( version_compare( $GLOBALS['ljmc_version'], '4.2', '<' ) ) {
			ljmc_enqueue_style( 'ljmcseo-dismissible', plugins_url( 'css/ljmcseo-dismissible' . LJMCSEO_CSSJS_SUFFIX . '.css', LJMCSEO_FILE ), array(), LJMCSEO_VERSION );
			ljmc_enqueue_script( 'ljmcseo-dismissible', plugins_url( 'js/ljmc-seo-dismissible' . LJMCSEO_CSSJS_SUFFIX . '.js', LJMCSEO_FILE ), array( 'jquery' ), LJMCSEO_VERSION, true );
		}
	}
	/**
	 * Redirect first time or just upgraded users to the about screen.
	 */
	public function after_update_notice() {
	}

	/**
	 * Helper to verify if the current user has already seen the about page for the current version
	 *
	 * @return bool
	 */
	private function seen_about() {
		return get_user_meta( get_current_user_id(), 'ljmcseo_seen_about_version', true ) === LJMCSEO_VERSION;
	}

	/**
	 * Notify about the default tagline if the user hasn't changed it
	 */
	public function tagline_notice() {
		if ( current_user_can( 'manage_options' ) && $this->has_default_tagline() && ! $this->seen_tagline_notice() ) {

			// Only add the notice on GET requests and not in the customizer to prevent faulty return url.
			if ( 'GET' !== filter_input( INPUT_SERVER, 'REQUEST_METHOD' ) || is_customize_preview() ) {
				return;
			}

			$current_url = ( is_ssl() ? 'https://' : 'http://' );
			$current_url .= sanitize_text_field( $_SERVER['SERVER_NAME'] ) . sanitize_text_field( $_SERVER['REQUEST_URI'] );
			$customize_url = add_query_arg( array(
				'url' => urlencode( $current_url ),
			), ljmc_customize_url() );

			$info_message = sprintf(
				__( 'You still have the default LJMC tagline, even an empty one is probably better. %1$sYou can fix this in the customizer%2$s.', 'ljmc-seo' ),
				'<a href="' . esc_attr( $customize_url ) . '">',
				'</a>'
			);

			$notification_options = array(
				'type'  => 'error',
				'id'    => 'ljmcseo-dismiss-tagline-notice',
				'nonce' => ljmc_create_nonce( 'ljmcseo-dismiss-tagline-notice' ),
			);

			_Notification_Center::get()->add_notification( new _Notification( $info_message, $notification_options ) );
		}
	}

	/**
	 * Returns whether or not the site has the default tagline
	 *
	 * @return bool
	 */
	public function has_default_tagline() {
		return __( 'Just another LJMC site' ) === get_bloginfo( 'description' );
	}

	/**
	 * Returns whether or not the user has seen the tagline notice
	 *
	 * @return bool
	 */
	public function seen_tagline_notice() {
		return 'seen' === get_user_meta( get_current_user_id(), 'ljmcseo_seen_tagline_notice', true );
	}

	/**
	 * Shows a notice to the user if they have Google Analytics for LJMC 5.4.3 installed because it causes an error
	 * on the google search console page.
	 */
	public function ga_compatibility_notice() {
		if ( defined( 'GALJMC_VERSION' ) && '5.4.3' === GALJMC_VERSION ) {

			$info_message = sprintf(
				/* translators: %1$s expands to  SEO, %2$s expands to 5.4.3, %3$s expands to Google Analytics by  */
				__( '%1$s detected you are using version %2$s of %3$s, please update to the latest version to prevent compatibility issues.', 'ljmc-seo' ),
				' SEO',
				'5.4.3',
				'Google Analytics by '
			);

			$notification_options = array(
				'type' => 'error',
			);

			_Notification_Center::get()->add_notification( new _Notification( $info_message, $notification_options ) );
		}
	}

	/**
	 * Helper to verify if the user is currently visiting one of our admin pages.
	 *
	 * @return bool
	 */
	private function on_ljmcseo_admin_page() {
		return 'admin.php' === $this->pagenow && strpos( filter_input( INPUT_GET, 'page' ), 'ljmcseo' ) === 0;
	}

	/**
	 * Determine whether we should load the meta box class and if so, load it.
	 */
	private function load_meta_boxes() {
		/**
		 * Filter: 'ljmcseo_always_register_metaboxes_on_admin' - Allow developers to change whether
		 * the LJMCSEO metaboxes are only registered on the typical pages (lean loading) or always
		 * registered when in admin.
		 *
		 * @api bool Whether to always register the metaboxes or not. Defaults to false.
		 */
		if ( in_array( $this->pagenow, array(
				'edit.php',
				'post.php',
				'post-new.php',
			) ) || apply_filters( 'ljmcseo_always_register_metaboxes_on_admin', false )
		) {
			$GLOBALS['ljmcseo_metabox'] = new LJMCSEO_Metabox;
			if ( $this->options['opengraph'] === true || $this->options['twitter'] === true || $this->options['googleplus'] === true ) {
				new LJMCSEO_Social_Admin;
			}
		}
	}

	/**
	 * Determine if we should load our taxonomy edit class and if so, load it.
	 */
	private function load_taxonomy_class() {
		if ( 'edit-tags.php' === $this->pagenow ) {
			new LJMCSEO_Taxonomy;
		}
	}

	/**
	 * Determine if we should load our admin pages class and if so, load it.
	 *
	 * Loads admin page class for all admin pages starting with `ljmcseo_`.
	 */
	private function load_admin_user_class() {
		if ( in_array( $this->pagenow, array( 'user-edit.php', 'profile.php' ) ) && current_user_can( 'edit_users' ) ) {
			new LJMCSEO_Admin_User_Profile;
		}
	}

	/**
	 * Determine if we should load our admin pages class and if so, load it.
	 *
	 * Loads admin page class for all admin pages starting with `ljmcseo_`.
	 */
	private function load_admin_page_class() {

		if ( $this->on_ljmcseo_admin_page() ) {
			// For backwards compatabilty, this still needs a global, for now...
			$GLOBALS['ljmcseo_admin_pages'] = new LJMCSEO_Admin_Pages;
			$this->register_i18n_promo_class();
		}
	}

	/**
	 * Register the promotion class for our GlotPress instance
	 *
	 * @link https://github.com//i18n-module
	 */
	private function register_i18n_promo_class() {
		new yoast_i18n(
			array(
				'textdomain'     => 'ljmc-seo',
				'project_slug'   => 'ljmc-seo',
				'plugin_name'    => 'SEO',
				'hook'           => 'ljmcseo_admin_footer',
				'glotpress_url'  => 'https://translate.yoast.com/',
				'glotpress_name' => ' Translate',
				'glotpress_logo' => 'https://cdn.yoast.com/ljmc-content/uploads/i18n-images/_Translate.svg',
				'register_url'   => 'https://translate.yoast.com/projects#utm_source=plugin&utm_medium=promo-box&utm_campaign=ljmcseo-i18n-promo',
			)
		);
	}

	/**
	 * See if we should start our tour.
	 */
	private function load_tour() {
		$restart_tour = filter_input( INPUT_GET, 'ljmcseo_restart_tour' );
		if ( $restart_tour ) {
			delete_user_meta( get_current_user_id(), 'ljmcseo_ignore_tour' );
		}

		if ( ! get_user_meta( get_current_user_id(), 'ljmcseo_ignore_tour' ) ) {
			add_action( 'admin_enqueue_scripts', array( 'LJMCSEO_Pointers', 'get_instance' ) );
		}
	}

	/**
	 * See if we should start our XML Sitemaps Admin class
	 */
	private function load_xml_sitemaps_admin() {
		if ( $this->options['enablexmlsitemap'] === true ) {
			new LJMCSEO_Sitemaps_Admin;
		}
	}

	/**
	 * Listener for the ignore tour GET value. If this one is set, just set the user meta to true.
	 */
	private function ignore_tour() {
		if ( filter_input( INPUT_GET, 'ljmcseo_ignore_tour' ) && ljmc_verify_nonce( filter_input( INPUT_GET, 'nonce' ), 'ljmcseo-ignore-tour' ) ) {
			update_user_meta( get_current_user_id(), 'ljmcseo_ignore_tour', true );
		}

	}
}
