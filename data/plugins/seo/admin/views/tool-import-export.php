<?php
/**
 * @package LJMCSEO\Admin
 */

if ( ! defined( 'LJMCSEO_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

/**
 * @todo [JRF => testers] Extensively test the export & import of the (new) settings!
 * If that all works fine, getting testers to export before and after upgrade will make testing easier.
 *
 * @todo [] The import for the RSS Footer plugin checks for data already entered via  SEO,
 * the other import routines should do that too.
 */

$yform = _Form::get_instance();

$replace = false;

if ( isset( $_POST['import'] ) || isset( $_GET['import'] ) ) {

	check_admin_referer( 'ljmcseo-import' );

	if ( isset( $_POST['ljmcseo']['deleteolddata'] ) && $_POST['ljmcseo']['deleteolddata'] == 'on' ) {
		$replace = true;
	}

	if ( isset( $_POST['ljmcseo']['importwoo'] ) ) {
		$import = new LJMCSEO_Import_WooThemes_SEO( $replace );
	}

	if ( isset( $_POST['ljmcseo']['importaioseo'] ) || isset( $_GET['importaioseo'] ) ) {
		$import = new LJMCSEO_Import_AIOSEO( $replace );
	}

	if ( isset( $_POST['ljmcseo']['importheadspace'] ) ) {
		$import = new LJMCSEO_Import_External( $replace );
		$import->import_headspace();
	}

	if ( isset( $_POST['ljmcseo']['importrobotsmeta'] ) || isset( $_GET['importrobotsmeta'] ) ) {
		$import = new LJMCSEO_Import_External( $replace );
		$import->import_robots_meta();
	}

	if ( isset( $_POST['ljmcseo']['importrssfooter'] ) ) {
		$import = new LJMCSEO_Import_External( $replace );
		$import->import_rss_footer();
	}

	if ( isset( $_POST['ljmcseo']['importbreadcrumbs'] ) ) {
		$import = new LJMCSEO_Import_External( $replace );
		$import->import_yoast_breadcrumbs();
	}

	// Allow custom import actions.
	do_action( 'ljmcseo_handle_import' );

}

if ( isset( $_FILES['settings_import_file'] ) ) {
	check_admin_referer( 'ljmcseo-import-file' );

	$import = new LJMCSEO_Import();
}

if ( isset( $import ) ) {
	/**
	 * Allow customization of import&export message
	 * @api  string  $msg  The message.
	 */
	$msg = apply_filters( 'ljmcseo_import_message', $import->msg );

	// Check if we've deleted old data and adjust message to match it.
	if ( $replace ) {
		$msg .= ' ' . __( 'The old data of the imported plugin was deleted successfully.', 'ljmc-seo' );
	}

	if ( $msg != '' ) {
		echo '<div id="message" class="message updated" style="width:94%;"><p>', $msg, '</p></div>';
	}
}

?>
<br/><br/>
<h2 class="nav-tab-wrapper" id="ljmcseo-tabs">
	<a class="nav-tab nav-tab-active" id="ljmcseo-import-tab"
	   href="#top#ljmcseo-import"><?php _e( 'Import', 'ljmc-seo' ); ?></a>
	<a class="nav-tab" id="ljmcseo-export-tab" href="#top#ljmcseo-export"><?php _e( 'Export', 'ljmc-seo' ); ?></a>
	<a class="nav-tab" id="import-seo-tab"
	   href="#top#import-seo"><?php _e( 'Import from other SEO plugins', 'ljmc-seo' ); ?></a>
	<a class="nav-tab" id="import-other-tab"
	   href="#top#import-other"><?php _e( 'Import from other plugins', 'ljmc-seo' ); ?></a>
	<?php
	/**
	 * Allow adding a custom import tab header
	 */
	do_action( 'ljmcseo_import_tab_header' );
	?>
</h2>

<div id="ljmcseo-import" class="ljmcseotab">
	<p><?php _e( 'Import settings by locating <em>settings.zip</em> and clicking "Import settings"', 'ljmc-seo' ); ?></p>

	<form
		action="<?php echo esc_attr( admin_url( 'admin.php?page=ljmcseo_tools&tool=import-export#top#ljmcseo-import' ) ); ?>"
		method="post" enctype="multipart/form-data"
		accept-charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>">
		<?php ljmc_nonce_field( 'ljmcseo-import-file', '_ljmcnonce', true, true ); ?>
		<input type="file" name="settings_import_file" accept="application/x-zip,application/x-zip-compressed,application/zip" />
		<input type="hidden" name="action" value="ljmc_handle_upload"/><br/>
		<br/>
		<input type="submit" class="button-primary" value="<?php _e( 'Import settings', 'ljmc-seo' ); ?>"/>
	</form>
</div>

<div id="ljmcseo-export" class="ljmcseotab">
	<p><?php
		/* translators: %1$s expands to  SEO */
		printf( __( 'Export your %1$s settings here, to import them again later or to import them on another site.', 'ljmc-seo' ), ' SEO' );
		?></p>
	<?php $yform->checkbox( 'include_taxonomy_meta', __( 'Include Taxonomy Metadata', 'ljmc-seo' ) ); ?><br/>
	<button class="button-primary" id="export-button"><?php
		/* translators: %1$s expands to  SEO */
		printf( __( 'Export your %1$s settings', 'ljmc-seo' ), ' SEO' );
		?></button>
	<script>
		var ljmcseo_export_nonce = '<?php echo ljmc_create_nonce( 'ljmcseo-export' ); ?>';
	</script>
</div>

<div id="import-seo" class="ljmcseotab">
	<p><?php _e( 'No doubt you\'ve used an SEO plugin before if this site isn\'t new. Let\'s make it easy on you, you can import the data below. If you want, you can import first, check if it was imported correctly, and then import &amp; delete. No duplicate data will be imported.', 'ljmc-seo' ); ?></p>

	<p><?php printf( __( 'If you\'ve used another SEO plugin, try the %sSEO Data Transporter%s plugin to move your data into this plugin, it rocks!', 'ljmc-seo' ), '<a href="https://ljmc.org/plugins/seo-data-transporter/">', '</a>' ); ?></p>

	<form
		action="<?php echo esc_attr( admin_url( 'admin.php?page=ljmcseo_tools&tool=import-export#top#import-seo' ) ); ?>"
		method="post" accept-charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>">
		<?php
		ljmc_nonce_field( 'ljmcseo-import', '_ljmcnonce', true, true );
		$yform->checkbox( 'importheadspace', __( 'Import from HeadSpace2?', 'ljmc-seo' ) );
		$yform->checkbox( 'importaioseo', __( 'Import from All-in-One SEO?', 'ljmc-seo' ) );
		$yform->checkbox( 'importwoo', __( 'Import from WooThemes SEO framework?', 'ljmc-seo' ) );
		?>
		<br/>
		<?php
		$yform->checkbox( 'deleteolddata', __( 'Delete the old data after import? (recommended)', 'ljmc-seo' ) );
		?>
		<br/>
		<input type="submit" class="button-primary" name="import"
		       value="<?php _e( 'Import', 'ljmc-seo' ); ?>"/>
	</form>
	<br/>
	<br/>
</div>

<div id="import-other" class="ljmcseotab">
	<p><?php _e( 'If you want to import data from (by now ancient)  plugins, you can do so here:', 'ljmc-seo' ); ?></p>

	<form
		action="<?php echo esc_attr( admin_url( 'admin.php?page=ljmcseo_tools&tool=import-export#top#import-other' ) ); ?>"
		method="post" accept-charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>">
		<?php
		ljmc_nonce_field( 'ljmcseo-import', '_ljmcnonce', true, true );
		$yform->checkbox( 'importrobotsmeta', __( 'Import from Robots Meta (by )?', 'ljmc-seo' ) );
		$yform->checkbox( 'importrssfooter', __( 'Import from RSS Footer (by )?', 'ljmc-seo' ) );
		$yform->checkbox( 'importbreadcrumbs', __( 'Import from  Breadcrumbs?', 'ljmc-seo' ) );

		/**
		 * Allow option of importing from other 'other' plugins
		 * @api  string  $content  The content containing all import and export methods
		 */
		echo apply_filters( 'ljmcseo_import_other_plugins', '' );

		?>
		<br/>
		<input type="submit" class="button-primary" name="import" value="<?php _e( 'Import', 'ljmc-seo' ); ?>"/>
	</form>
	<br/>
</div>
<?php
/**
 * Allow adding a custom import tab
 */
do_action( 'ljmcseo_import_tab_content' );

