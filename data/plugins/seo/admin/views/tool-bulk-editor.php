<?php
/**
 * @package LJMCSEO\Admin
 * @since      1.5.0
 */

if ( ! defined( 'LJMCSEO_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

$options = get_option( 'ljmcseo' );

$ljmcseo_bulk_titles_table      = new LJMCSEO_Bulk_Title_Editor_List_Table();
$ljmcseo_bulk_description_table = new LJMCSEO_Bulk_Description_List_Table();

// If type is empty, fill it with value of first tab (title).
$_GET['type'] = ( ! empty( $_GET['type'] ) ) ? $_GET['type'] : 'title';

if ( ! empty( $_REQUEST['_ljmc_http_referer'] ) ) {
	ljmc_redirect( remove_query_arg( array( '_ljmc_http_referer', '_ljmcnonce' ), stripslashes( $_SERVER['REQUEST_URI'] ) ) );
	exit;
}
?>
<script>
	var ljmcseo_bulk_editor_nonce = '<?php echo ljmc_create_nonce( 'ljmcseo-bulk-editor' ); ?>';
</script>

<div class="wrap ljmcseo_table_page">

	<h2 class="nav-tab-wrapper" id="ljmcseo-tabs">
		<a class="nav-tab" id="title-tab" href="#top#title"><?php _e( 'Title', 'ljmc-seo' ); ?></a>
		<a class="nav-tab" id="description-tab"
		   href="#top#description"><?php _e( 'Description', 'ljmc-seo' ); ?></a>
	</h2>

	<div class="tabwrapper">
		<div id="title" class="ljmcseotab">
			<?php $ljmcseo_bulk_titles_table->show_page(); ?>
		</div>
		<div id="description" class="ljmcseotab">
			<?php $ljmcseo_bulk_description_table->show_page(); ?>
		</div>

	</div>
</div>
