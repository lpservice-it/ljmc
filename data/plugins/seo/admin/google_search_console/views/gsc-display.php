<?php
/**
 * @package LJMCSEO\Admin|Google_Search_Console
 */

	// Admin header.
	_Form::get_instance()->admin_header( false, 'ljmcseo-gsc', false, 'yoast_ljmcseo_gsc_options' );
?>
	<h2 class="nav-tab-wrapper" id="ljmcseo-tabs">
<?php
if ( defined( 'LJMC_DEBUG' ) && LJMC_DEBUG && LJMCSEO_GSC_Settings::get_profile() !== '' ) {
	?>
		<form action="" method="post">
			<input type='hidden' name='reload-crawl-issues-nonce' value='<?php echo ljmc_create_nonce( 'reload-crawl-issues' ); ?>' />
			<input type="submit" name="reload-crawl-issues" id="reload-crawl-issue" class="button-primary"
				   style="float: right;" value="<?php _e( 'Reload crawl issues', 'ljmc-seo' ); ?>">
		</form>
<?php } ?>
		<?php echo $platform_tabs = new LJMCSEO_GSC_Platform_Tabs; ?>
	</h2>

<?php

switch ( $platform_tabs->current_tab() ) {
	case 'settings' :
		// Check if there is an access token.
		if ( null === $this->service->get_client()->getAccessToken() ) {
			// Print auth screen.
			echo '<p>';
			/* Translators: %1$s: expands to ' SEO', %2$s expands to Google Search Console. */
			echo sprintf( __( 'To allow %1$s to fetch your %2$s information, please enter your Google Authorization Code.', 'ljmc-seo' ), ' SEO', 'Google Search Console' );
			echo "</p>\n";
			echo '<input type="hidden" id="gsc_auth_url" value="', $this->service->get_client()->createAuthUrl() , '" />';
			echo "<button id='gsc_auth_code' class='button-secondary'>" , __( 'Get Google Authorization Code', 'ljmc-seo' ) ,"</button>\n";

			echo '<p>' . __( 'Please enter the Google Authorization Code in the field below and press the Authenticate button.', 'ljmc-seo' ) . "</p>\n";
			echo "<form action='" . admin_url( 'admin.php?page=ljmcseo_search_console&tab=settings' ) . "' method='post'>\n";
			echo "<input type='text' name='gsc[authorization_code]' value='' />";
			echo "<input type='hidden' name='gsc[gsc_nonce]' value='" . ljmc_create_nonce( 'ljmcseo-gsc_nonce' ) . "' />";
			echo "<input type='submit' name='gsc[Submit]' value='" . __( 'Authenticate', 'ljmc-seo' ) . "' class='button-primary' />";
			echo "</form>\n";
		}
		else {
			$reset_button = '<a class="button-secondary" href="' . add_query_arg( 'gsc_reset', 1 ). '">' . __( 'Reauthenticate with Google ', 'ljmc-seo' ) .'</a>';
			echo '<h3>',  __( 'Current profile', 'ljmc-seo' ), '</h3>';
			if ( ($profile = LJMCSEO_GSC_Settings::get_profile() ) !== '' ) {
				echo '<p>';
				echo _Form::get_instance()->label( __( 'Current profile', 'ljmc-seo' ), array() );
				echo $profile;
				echo '</p>';

				echo '<p>';
				echo '<label class="select"></label>';
				echo $reset_button;
				echo '</p>';

			}
			else {
				echo "<form action='" . admin_url( 'options.php' ) . "' method='post'>";

				settings_fields( 'yoast_ljmcseo_gsc_options' );
				_Form::get_instance()->set_option( 'ljmcseo-gsc' );

				echo '<p>';
				if ( $profiles = $this->service->get_sites() ) {
					$show_save = true;
					echo _Form::get_instance()->select( 'profile', __( 'Profile', 'ljmc-seo' ), $profiles );
				}
				else {
					$show_save = false;
					echo '<label class="select" for="profile">', __( 'Profile', 'ljmc-seo' ), '</label>';
					echo __( 'There were no profiles found', 'ljmc-seo' );
				}
				echo '</p>';

				echo '<p>';
				echo '<label class="select"></label>';

				if ( $show_save ) {
					echo '<input type="submit" name="submit" id="submit" class="button button-primary" value="' . __( 'Save Profile', 'ljmc-seo' ) . '" /> ' . __( 'or', 'ljmc-seo' ) , ' ';
				}
				echo $reset_button;
				echo '</p>';
				echo '</form>';
			}
		}
		break;

	default :
		$form_action_url = add_query_arg( 'page', esc_attr( filter_input( INPUT_GET, 'page' ) ) );

		// Open <form>.
		echo "<form id='ljmcseo-crawl-issues-table-form' action='" . $form_action_url . "' method='post'>\n";

		// AJAX nonce.
		echo "<input type='hidden' class='ljmcseo-gsc-ajax-security' value='" . ljmc_create_nonce( 'ljmcseo-gsc-ajax-security' ) . "' />\n";

		$this->display_table();

		// Close <form>.
		echo "</form>\n";

		break;
}
?>
	<br class="clear" />
<?php

// Admin footer.
_Form::get_instance()->admin_footer( false );
