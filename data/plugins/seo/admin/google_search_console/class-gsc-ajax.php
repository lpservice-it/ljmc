<?php
/**
 * @package LJMCSEO\Admin|Google_Search_Console
 */

/**
 * Class LJMCSEO_GSC_Ajax
 */
class LJMCSEO_GSC_Ajax {

	/**
	 * Setting the AJAX hooks for GSC
	 */
	public function __construct() {
		add_action( 'ljmc_ajax_ljmcseo_mark_fixed_crawl_issue',  array( $this, 'ajax_mark_as_fixed' ) );
		add_action( 'ljmc_ajax_ljmcseo_gsc_create_redirect_url', array( $this, 'ajax_create_redirect' ) );
		add_action( 'ljmc_ajax_ljmcseo_dismiss_gsc', array( $this, 'dismiss_notice' ) );
	}

	/**
	 * This method will be access by an AJAX request and will mark an issue as fixed.
	 *
	 * First it will do a request to the Google API
	 */
	public function ajax_mark_as_fixed( ) {
		if ( $this->valid_nonce() ) {
			$marker = new LJMCSEO_GSC_Marker( filter_input( INPUT_POST, 'url' ) );

			ljmc_die( $marker->get_response() );
		}

		ljmc_die( 'false' );
	}

	/**
	 * Handling the request to create a new redirect from the issued URL
	 */
	public function ajax_create_redirect() {
		if ( $this->valid_nonce() && class_exists( 'LJMCSEO_URL_Redirect_Manager' ) && defined( 'LJMCSEO_PREMIUM_PATH' ) ) {
			$redirect_manager = new LJMCSEO_URL_Redirect_Manager();

			$old_url = filter_input( INPUT_POST, 'old_url' );

			// Creates the redirect.
			if ( $redirect_manager->create_redirect( $old_url, filter_input( INPUT_POST, 'new_url' ), filter_input( INPUT_POST, 'type' ) ) ) {
				if ( filter_input( INPUT_POST, 'mark_as_fixed' ) === 'true' ) {
					new LJMCSEO_GSC_Marker( $old_url );
				}

				ljmc_die( 'true' );
			}
		}

		ljmc_die( 'false' );
	}

	/**
	 * Handle the AJAX request and dismiss the GSC notice
	 */
	public function dismiss_notice() {
		check_ajax_referer( 'dismiss-gsc-notice' );

		update_user_meta( get_current_user_id(), 'ljmcseo_dismissed_gsc_notice', true );

		ljmc_die( 'true' );
	}

	/**
	 * Check if posted nonce is valid and return true if it is
	 *
	 * @return mixed
	 */
	private function valid_nonce() {
		return ljmc_verify_nonce( filter_input( INPUT_POST, 'ajax_nonce' ), 'ljmcseo-gsc-ajax-security' );
	}

}
