<?php
/**
 * @package LJMCSEO\Admin\Customizer
 */

/**
 * Class with functionality to support LJMC SEO settings in LJMC Customizer.
 */
class LJMCSEO_Customizer {

	/**
	 * @var LJMC_Customize_Manager
	 */
	protected $ljmc_customize;

	/**
	 * Construct Method.
	 */
	public function __construct() {
		add_action( 'customize_register', array( $this, 'ljmcseo_customize_register' ) );
	}

	/**
	 * Function to support LJMC Customizer
	 *
	 * @param LJMC_Customize_Manager $ljmc_customize
	 */
	public function ljmcseo_customize_register( $ljmc_customize ) {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		$this->ljmc_customize = $ljmc_customize;

		$this->breadcrumbs_section();
		$this->breadcrumbs_blog_remove_setting();
		$this->breadcrumbs_separator_setting();
		$this->breadcrumbs_home_setting();
		$this->breadcrumbs_prefix_setting();
		$this->breadcrumbs_archiveprefix_setting();
		$this->breadcrumbs_searchprefix_setting();
		$this->breadcrumbs_404_setting();
	}

	/**
	 * Add the breadcrumbs section to the customizer
	 */
	private function breadcrumbs_section() {
		$this->ljmc_customize->add_section(
			'ljmcseo_breadcrumbs_customizer_section', array(
				/* translators: %s is the name of the plugin */
				'title'           => sprintf( __( '%s Breadcrumbs', 'ljmc-seo' ), ' SEO' ),
				'priority'        => 999,
				'active_callback' => array( $this, 'breadcrumbs_active_callback' ),
			)
		);

	}

	/**
	 * Returns whether or not the breadcrumbs are active
	 *
	 * @return bool
	 */
	public function breadcrumbs_active_callback() {
		$options = LJMCSEO_Options::get_all();

		return true === ( current_theme_supports( 'yoast-seo-breadcrumbs' ) || $options['breadcrumbs-enable'] );
	}

	/**
	 * Adds the breadcrumbs remove blog checkbox
	 */
	private function breadcrumbs_blog_remove_setting() {
		$this->ljmc_customize->add_setting(
			'ljmcseo_internallinks[breadcrumbs-blog-remove]', array(
				'default'   => '',
				'type'      => 'option',
				'transport' => 'refresh',
			)
		);

		$this->ljmc_customize->add_control(
			new LJMC_Customize_Control(
				$this->ljmc_customize, 'ljmcseo-breadcrumbs-blog-remove', array(
					'label'           => __( 'Remove blog page from breadcrumbs', 'ljmc-seo' ),
					'type'            => 'checkbox',
					'section'         => 'ljmcseo_breadcrumbs_customizer_section',
					'settings'        => 'ljmcseo_internallinks[breadcrumbs-blog-remove]',
					'context'         => '',
					'active_callback' => array( $this, 'breadcrumbs_blog_remove_active_cb' ),
				)
			)
		);
	}

	/**
	 * Returns whether or not to show the breadcrumbs blog remove option
	 *
	 * @return bool
	 */
	public function breadcrumbs_blog_remove_active_cb() {
		return 'page' === get_option( 'show_on_front' );
	}

	/**
	 * Adds the breadcrumbs separator text field
	 */
	private function breadcrumbs_separator_setting() {
		$this->ljmc_customize->add_setting(
			'ljmcseo_internallinks[breadcrumbs-sep]', array(
				'default'   => '',
				'type'      => 'option',
				'transport' => 'refresh',
			)
		);

		$this->ljmc_customize->add_control(
			new LJMC_Customize_Control(
				$this->ljmc_customize, 'ljmcseo-breadcrumbs-separator', array(
					'label'    => __( 'Breadcrumbs separator:', 'ljmc-seo' ),
					'type'     => 'text',
					'section'  => 'ljmcseo_breadcrumbs_customizer_section',
					'settings' => 'ljmcseo_internallinks[breadcrumbs-sep]',
					'context'  => '',
				)
			)
		);
	}

	/**
	 * Adds the breadcrumbs home anchor text field
	 */
	private function breadcrumbs_home_setting() {
		$this->ljmc_customize->add_setting(
			'ljmcseo_internallinks[breadcrumbs-home]', array(
				'default'   => '',
				'type'      => 'option',
				'transport' => 'refresh',
			)
		);

		$this->ljmc_customize->add_control(
			new LJMC_Customize_Control(
				$this->ljmc_customize, 'ljmcseo-breadcrumbs-home', array(
					'label'    => __( 'Anchor text for the homepage:', 'ljmc-seo' ),
					'type'     => 'text',
					'section'  => 'ljmcseo_breadcrumbs_customizer_section',
					'settings' => 'ljmcseo_internallinks[breadcrumbs-home]',
					'context'  => '',
				)
			)
		);
	}

	/**
	 * Adds the breadcrumbs prefix text field
	 */
	private function breadcrumbs_prefix_setting() {
		$this->ljmc_customize->add_setting(
			'ljmcseo_internallinks[breadcrumbs-prefix]', array(
				'default'   => '',
				'type'      => 'option',
				'transport' => 'refresh',
			)
		);

		$this->ljmc_customize->add_control(
			new LJMC_Customize_Control(
				$this->ljmc_customize, 'ljmcseo-breadcrumbs-prefix', array(
					'label'    => __( 'Prefix for breadcrumbs:', 'ljmc-seo' ),
					'type'     => 'text',
					'section'  => 'ljmcseo_breadcrumbs_customizer_section',
					'settings' => 'ljmcseo_internallinks[breadcrumbs-prefix]',
					'context'  => '',
				)
			)
		);
	}

	/**
	 * Adds the breadcrumbs archive prefix text field
	 */
	private function breadcrumbs_archiveprefix_setting() {
		$this->ljmc_customize->add_setting(
			'ljmcseo_internallinks[breadcrumbs-archiveprefix]', array(
				'default'   => '',
				'type'      => 'option',
				'transport' => 'refresh',
			)
		);

		$this->ljmc_customize->add_control(
			new LJMC_Customize_Control(
				$this->ljmc_customize, 'ljmcseo-breadcrumbs-archiveprefix', array(
					'label'    => __( 'Prefix for archive pages:', 'ljmc-seo' ),
					'type'     => 'text',
					'section'  => 'ljmcseo_breadcrumbs_customizer_section',
					'settings' => 'ljmcseo_internallinks[breadcrumbs-archiveprefix]',
					'context'  => '',
				)
			)
		);
	}

	/**
	 * Adds the breadcrumbs search prefix text field
	 */
	private function breadcrumbs_searchprefix_setting() {
		$this->ljmc_customize->add_setting(
			'ljmcseo_internallinks[breadcrumbs-searchprefix]', array(
				'default'   => '',
				'type'      => 'option',
				'transport' => 'refresh',
			)
		);

		$this->ljmc_customize->add_control(
			new LJMC_Customize_Control(
				$this->ljmc_customize, 'ljmcseo-breadcrumbs-searchprefix', array(
					'label'    => __( 'Prefix for search result pages:', 'ljmc-seo' ),
					'type'     => 'text',
					'section'  => 'ljmcseo_breadcrumbs_customizer_section',
					'settings' => 'ljmcseo_internallinks[breadcrumbs-searchprefix]',
					'context'  => '',
				)
			)
		);
	}

	/**
	 * Adds the breadcrumb 404 prefix text field
	 */
	private function breadcrumbs_404_setting() {
		$this->ljmc_customize->add_setting(
			'ljmcseo_internallinks[breadcrumbs-404crumb]', array(
				'default'   => '',
				'type'      => 'option',
				'transport' => 'refresh',
			)
		);

		$this->ljmc_customize->add_control(
			new LJMC_Customize_Control(
				$this->ljmc_customize, 'ljmcseo-breadcrumbs-404crumb', array(
					'label'    => __( 'Breadcrumb for 404 pages:', 'ljmc-seo' ),
					'type'     => 'text',
					'section'  => 'ljmcseo_breadcrumbs_customizer_section',
					'settings' => 'ljmcseo_internallinks[breadcrumbs-404crumb]',
					'context'  => '',
				)
			)
		);
	}
}
