<?php
/**
 * @package LJMCSEO\Admin
 */

if ( ! defined( 'LJMCSEO_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

$tool_page = (string) filter_input( INPUT_GET, 'tool' );

$yform = _Form::get_instance();

$yform->admin_header( false );

if ( '' === $tool_page ) {
	$tools = array(
		'bulk-editor' => array(
			'title' => __( 'Bulk editor', 'ljmc-seo' ),
			'desc' => __( 'This tool allows you to quickly change titles and descriptions of your posts and pages without having to go into the editor for each page.', 'ljmc-seo' ),
		),
		'import-export' => array(
			'title' => __( 'Import and Export', 'ljmc-seo' ),
			'desc' => __( 'Import settings from other SEO plugins and export your settings for re-use on (another) blog.', 'ljmc-seo' ),
		),
	);
	if ( LJMCSEO_Utils::allow_system_file_edit() === true && ! is_multisite() ) {
		$tools['file-editor'] = array(
			'title' => __( 'File editor', 'ljmc-seo' ),
			'desc' => __( 'This tool allows you to quickly change important files for your SEO, like your robots.txt and, if you have one, your .htaccess file.', 'ljmc-seo' ),
		);
	}

	/* translators: %1$s expands to  SEO */
	echo '<p>', sprintf( __( '%1$s comes with some very powerful built-in tools:', 'ljmc-seo' ), ' SEO' ), '</p>';

	asort( $tools );

	echo '<ul class="ul-disc">';
	foreach ( $tools as $slug => $tool ) {
		echo '<li>';
		echo '<strong><a href="', admin_url( 'admin.php?page=ljmcseo_tools&tool=' . $slug ), '">', $tool['title'], '</a></strong><br/>';
		echo $tool['desc'];
		echo '</li>';
	}
	echo '</ul>';

}
else {
	echo '<a href="', admin_url( 'admin.php?page=ljmcseo_tools' ), '">', __( '&laquo; Back to Tools page', 'ljmc-seo' ), '</a>';
	require_once LJMCSEO_PATH . 'admin/views/tool-' . $tool_page . '.php';
}

$yform->admin_footer( false );
