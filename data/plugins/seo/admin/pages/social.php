<?php
/**
 * @package LJMCSEO\Admin
 */

if ( ! defined( 'LJMCSEO_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

$yform = _Form::get_instance();

$social_facebook = new _Social_Facebook( );

$yform->admin_header( true, 'ljmcseo_social' );
?>

	<h2 class="nav-tab-wrapper" id="ljmcseo-tabs">
		<a class="nav-tab" id="accounts-tab" href="#top#accounts"><?php _e( 'Accounts', 'ljmc-seo' ); ?></a>
		<a class="nav-tab" id="facebook-tab" href="#top#facebook"><span class="dashicons dashicons-facebook-alt"></span> <?php _e( 'Facebook', 'ljmc-seo' ); ?></a>
		<a class="nav-tab" id="twitterbox-tab" href="#top#twitterbox"><span class="dashicons dashicons-twitter"></span> <?php _e( 'Twitter', 'ljmc-seo' ); ?></a>
		<a class="nav-tab" id="pinterest-tab" href="#top#pinterest"><?php _e( 'Pinterest', 'ljmc-seo' ); ?></a>
		<a class="nav-tab" id="google-tab" href="#top#google"><span class="dashicons dashicons-googleplus"></span> <?php _e( 'Google+', 'ljmc-seo' ); ?></a>
	</h2>

	<div id="accounts" class="ljmcseotab">
		<p>
			<?php _e( 'To inform Google about your social profiles, we need to know their URLs.', 'ljmc-seo' ); ?>
			<?php _e( 'For each, pick the main account associated with this site and please enter them below:', 'ljmc-seo' ); ?>
		</p>
		<?php
		$yform->textinput( 'facebook_site', __( 'Facebook Page URL', 'ljmc-seo' ) );
		$yform->textinput( 'twitter_site', __( 'Twitter Username', 'ljmc-seo' ) );
		$yform->textinput( 'instagram_url', __( 'Instagram URL', 'ljmc-seo' ) );
		$yform->textinput( 'linkedin_url', __( 'LinkedIn URL', 'ljmc-seo' ) );
		$yform->textinput( 'myspace_url', __( 'MySpace URL', 'ljmc-seo' ) );
		$yform->textinput( 'pinterest_url', __( 'Pinterest URL', 'ljmc-seo' ) );
		$yform->textinput( 'youtube_url', __( 'YouTube URL', 'ljmc-seo' ) );
		$yform->textinput( 'google_plus_url', __( 'Google+ URL', 'ljmc-seo' ) );

		do_action( 'ljmcseo_admin_other_section' );
		?>
	</div>

	<div id="facebook" class="ljmcseotab">
		<p>
			<?php
				/* translators: %s expands to <code>&lt;head&gt;</code> */
				printf( __( 'Add Open Graph meta data to your site\'s %s section, Facebook and other social networks use this data when your pages are shared.', 'ljmc-seo' ), '<code>&lt;head&gt;</code>' );
			?>
		</p>
		<?php $yform->checkbox( 'opengraph', __( 'Add Open Graph meta data', 'ljmc-seo' ) ); ?>

		<?php
		if ( 'posts' == get_option( 'show_on_front' ) ) {
			echo '<p><strong>' . esc_html__( 'Frontpage settings', 'ljmc-seo' ) . '</strong></p>';
			echo '<p>' . esc_html__( 'These are the title, description and image used in the Open Graph meta tags on the front page of your site.', 'ljmc-seo' ) . '</p>';

			$yform->media_input( 'og_frontpage_image', __( 'Image URL', 'ljmc-seo' ) );
			$yform->textinput( 'og_frontpage_title', __( 'Title', 'ljmc-seo' ) );
			$yform->textinput( 'og_frontpage_desc', __( 'Description', 'ljmc-seo' ) );

			// Offer copying of meta description.
			$meta_options = get_option( 'ljmcseo_titles' );
			echo '<input type="hidden" id="meta_description" value="', esc_attr( $meta_options['metadesc-home-ljmcseo'] ), '" />';
			echo '<p class="label desc" style="border:0;"><a href="javascript:;" onclick="ljmcseoCopyHomeMeta();" class="button">', esc_html__( 'Copy home meta description', 'ljmc-seo' ), '</a></p>';

		} ?>

		<p><strong><?php esc_html_e( 'Default settings', 'ljmc-seo' ); ?></strong></p>
		<?php $yform->media_input( 'og_default_image', __( 'Image URL', 'ljmc-seo' ) ); ?>
		<p class="desc label">
			<?php esc_html_e( 'This image is used if the post/page being shared does not contain any images.', 'ljmc-seo' ); ?>
		</p>

		<?php $social_facebook->show_form(); ?>

		<?php do_action( 'ljmcseo_admin_opengraph_section' ); ?>
	</div>

	<div id="twitterbox" class="ljmcseotab">
		<p>
			<?php
			/* translators: %s expands to <code>&lt;head&gt;</code> */
			printf( __( 'Add Twitter card meta data to your site\'s %s section.', 'ljmc-seo' ), '<code>&lt;head&gt;</code>' );
			?>
		</p>

		<?php $yform->checkbox( 'twitter', __( 'Add Twitter card meta data', 'ljmc-seo' ) ); ?>

		<?php
		$yform->select( 'twitter_card_type', __( 'The default card type to use', 'ljmc-seo' ), LJMCSEO_Option_Social::$twitter_card_types );
		do_action( 'ljmcseo_admin_twitter_section' );
		?>
	</div>

	<div id="pinterest" class="ljmcseotab">
		<p>
			<?php _e( 'Pinterest uses Open Graph metadata just like Facebook, so be sure to keep the Open Graph checkbox on the Facebook tab checked if you want to optimize your site for Pinterest.', 'ljmc-seo' ); ?>
		</p>
		<p>
			<?php
				/* translators: %1$s / %2$s expands to a link to pinterest.com's help page. */
				printf( __( 'To %1$sverify your site with Pinterest%2$s, add the meta tag here:', 'ljmc-seo' ), '<a target="_blank" href="https://help.pinterest.com/en/articles/verify-your-website#meta_tag">', '</a>' );
			?>
		</p>

		<?php $yform->textinput( 'pinterestverify', __( 'Pinterest verification', 'ljmc-seo' ) ); ?>

		<?php
		do_action( 'ljmcseo_admin_pinterest_section' );
		?>
	</div>

	<div id="google" class="ljmcseotab">
		<p>
			<?php $yform->checkbox( 'googleplus', __( 'Add Google+ specific post meta data', 'ljmc-seo' ) ); ?>
		</p>

		<p><?php _e( 'If you have a Google+ page for your business, add that URL here and link it on your Google+ page\'s about page.', 'ljmc-seo' ); ?></p>

		<?php $yform->textinput( 'plus-publisher', __( 'Google Publisher Page', 'ljmc-seo' ) ); ?>

		<?php do_action( 'ljmcseo_admin_googleplus_section' ); ?>
	</div>

<?php
$yform->admin_footer();
