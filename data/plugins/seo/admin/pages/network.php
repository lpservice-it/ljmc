<?php
/**
 * @package LJMCSEO\Admin
 */

if ( ! defined( 'LJMCSEO_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

$yform = _Form::get_instance();

$options = get_site_option( 'ljmcseo_ms' );

if ( isset( $_POST['ljmcseo_submit'] ) ) {
	check_admin_referer( 'ljmcseo-network-settings' );

	foreach ( array( 'access', 'defaultblog' ) as $opt ) {
		$options[ $opt ] = $_POST['ljmcseo_ms'][ $opt ];
	}
	unset( $opt );
	LJMCSEO_Options::update_site_option( 'ljmcseo_ms', $options );
	add_settings_error( 'ljmcseo_ms', 'settings_updated', __( 'Settings Updated.', 'ljmc-seo' ), 'updated' );
}

if ( isset( $_POST['ljmcseo_restore_blog'] ) ) {
	check_admin_referer( 'ljmcseo-network-restore' );
	if ( isset( $_POST['ljmcseo_ms']['restoreblog'] ) && is_numeric( $_POST['ljmcseo_ms']['restoreblog'] ) ) {
		$restoreblog = (int) LJMCSEO_Utils::validate_int( $_POST['ljmcseo_ms']['restoreblog'] );
		$blog        = get_blog_details( $restoreblog );

		if ( $blog ) {
			LJMCSEO_Options::reset_ms_blog( $restoreblog );
			add_settings_error( 'ljmcseo_ms', 'settings_updated', sprintf( __( '%s restored to default SEO settings.', 'ljmc-seo' ), esc_html( $blog->blogname ) ), 'updated' );
		}
		else {
			add_settings_error( 'ljmcseo_ms', 'settings_updated', sprintf( __( 'Blog %s not found.', 'ljmc-seo' ), esc_html( $restoreblog ) ), 'error' );
		}
		unset( $restoreblog, $blog );
	}
}

/* Set up selectbox dropdowns for smaller networks (usability) */
$use_dropdown = true;
if ( get_blog_count() > 100 ) {
	$use_dropdown = false;
}
else {
	$sites = ljmc_get_sites( array( 'deleted' => 0 ) );
	if ( is_array( $sites ) && $sites !== array() ) {
		$dropdown_input = array(
			'-' => __( 'None', 'ljmc-seo' ),
		);

		foreach ( $sites as $site ) {
			$dropdown_input[ $site['blog_id'] ] = $site['blog_id'] . ': ' . $site['domain'];

			$blog_states = array();
			if ( $site['public'] === '1' ) {
				$blog_states[] = __( 'public', 'ljmc-seo' );
			}
			if ( $site['archived'] === '1' ) {
				$blog_states[] = __( 'archived', 'ljmc-seo' );
			}
			if ( $site['mature'] === '1' ) {
				$blog_states[] = __( 'mature', 'ljmc-seo' );
			}
			if ( $site['spam'] === '1' ) {
				$blog_states[] = __( 'spam', 'ljmc-seo' );
			}
			if ( $blog_states !== array() ) {
				$dropdown_input[ $site['blog_id'] ] .= ' [' . implode( ', ', $blog_states ) . ']';
			}
		}
		unset( $site, $blog_states );
	}
	else {
		$use_dropdown = false;
	}
	unset( $sites );
}

$yform->admin_header( false, 'ljmcseo_ms' );

echo '<h2>', __( 'MultiSite Settings', 'ljmc-seo' ), '</h2>';
echo '<form method="post" accept-charset="', esc_attr( get_bloginfo( 'charset' ) ), '">';
ljmc_nonce_field( 'ljmcseo-network-settings', '_ljmcnonce', true, true );

/* @internal Important: Make sure the options added to the array here are in line with the options set in the LJMCSEO_Option_MS::$allowed_access_options property */
$yform->select(
	'access',
	/* translators: %1$s expands to  SEO */
	sprintf( __( 'Who should have access to the %1$s settings', 'ljmc-seo' ), ' SEO' ),
	array(
		'admin'      => __( 'Site Admins (default)', 'ljmc-seo' ),
		'superadmin' => __( 'Super Admins only', 'ljmc-seo' ),
	),
	'ljmcseo_ms'
);

if ( $use_dropdown === true ) {
	$yform->select(
		'defaultblog',
		__( 'New sites in the network inherit their SEO settings from this site', 'ljmc-seo' ),
		$dropdown_input,
		'ljmcseo_ms'
	);
	echo '<p>' . __( 'Choose the site whose settings you want to use as default for all sites that are added to your network. If you choose \'None\', the normal plugin defaults will be used.', 'ljmc-seo' ) . '</p>';
}
else {
	$yform->textinput( 'defaultblog', __( 'New sites in the network inherit their SEO settings from this site', 'ljmc-seo' ), 'ljmcseo_ms' );
	echo '<p>' . sprintf( __( 'Enter the %sSite ID%s for the site whose settings you want to use as default for all sites that are added to your network. Leave empty for none (i.e. the normal plugin defaults will be used).', 'ljmc-seo' ), '<a href="' . esc_url( network_admin_url( 'sites.php' ) ) . '">', '</a>' ) . '</p>';
}
	echo '<p><strong>' . __( 'Take note:', 'ljmc-seo' ) . '</strong> ' . __( 'Privacy sensitive (FB admins and such), theme specific (title rewrite) and a few very site specific settings will not be imported to new blogs.', 'ljmc-seo' ) . '</p>';


echo '<input type="submit" name="ljmcseo_submit" class="button-primary" value="' . __( 'Save MultiSite Settings', 'ljmc-seo' ) . '"/>';
echo '</form>';

echo '<h2>' . __( 'Restore site to default settings', 'ljmc-seo' ) . '</h2>';
echo '<form method="post" accept-charset="' . esc_attr( get_bloginfo( 'charset' ) ) . '">';
ljmc_nonce_field( 'ljmcseo-network-restore', '_ljmcnonce', true, true );
echo '<p>' . __( 'Using this form you can reset a site to the default SEO settings.', 'ljmc-seo' ) . '</p>';

if ( $use_dropdown === true ) {
	unset( $dropdown_input['-'] );
	$yform->select(
		'restoreblog',
		__( 'Site ID', 'ljmc-seo' ),
		$dropdown_input,
		'ljmcseo_ms'
	);
}
else {
	$yform->textinput( 'restoreblog', __( 'Blog ID', 'ljmc-seo' ), 'ljmcseo_ms' );
}

echo '<input type="submit" name="ljmcseo_restore_blog" value="' . __( 'Restore site to defaults', 'ljmc-seo' ) . '" class="button"/>';
echo '</form>';


$yform->admin_footer( false );
