<?php
/**
 * @package LJMCSEO\Admin
 */

if ( ! defined( 'LJMCSEO_VERSION' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

$options = LJMCSEO_Options::get_all();

$yform = _Form::get_instance();

$yform->admin_header( true, 'ljmcseo_titles' );
?>

	<h2 class="nav-tab-wrapper" id="ljmcseo-tabs">
		<a class="nav-tab" id="general-tab" href="#top#general"><?php _e( 'General', 'ljmc-seo' ); ?></a>
		<a class="nav-tab" id="home-tab" href="#top#home"><?php _e( 'Homepage', 'ljmc-seo' ); ?></a>
		<a class="nav-tab" id="post_types-tab" href="#top#post_types"><?php _e( 'Post Types', 'ljmc-seo' ); ?></a>
		<a class="nav-tab" id="taxonomies-tab" href="#top#taxonomies"><?php _e( 'Taxonomies', 'ljmc-seo' ); ?></a>
		<a class="nav-tab" id="archives-tab" href="#top#archives"><?php _e( 'Archives', 'ljmc-seo' ); ?></a>
		<a class="nav-tab" id="other-tab" href="#top#other"><?php _e( 'Other', 'ljmc-seo' ); ?></a>
	</h2>

	<div class="tabwrapper">
		<div id="general" class="ljmcseotab">
			<table class="form-table">
				<tr>
					<th>
						<?php _e( 'Force rewrite titles', 'ljmc-seo' ); ?>
					</th>
					<td>
						<?php
						$yform->checkbox( 'forcerewritetitle', __( 'Enable force rewrite titles', 'ljmc-seo' ) );
						/* translators: %1$s expands to  SEO */
						echo '<p class="description">', sprintf( __( '%1$s has auto-detected whether it needs to force rewrite the titles for your pages, if you think it\'s wrong and you know what you\'re doing, you can change the setting here.', 'ljmc-seo' ), ' SEO' ) . '</p>';
						?>
					</td>
				</tr>
				<tr>
					<th>
						<?php _e( 'Title Separator', 'ljmc-seo' ); ?>
					</th>
					<td>
						<?php
						$yform->radio( 'separator', LJMCSEO_Option_Titles::get_instance()->get_separator_options(), '' );
						echo '<p class="description">', __( 'Choose the symbol to use as your title separator. This will display, for instance, between your post title and site name.', 'ljmc-seo' ), ' ', __( 'Symbols are shown in the size they\'ll appear in in search results.', 'ljmc-seo' ), '</p>';
						?>
					</td>
				</tr>
			</table>
		</div>
		<div id="home" class="ljmcseotab">
			<?php
			if ( 'posts' == get_option( 'show_on_front' ) ) {
				echo '<p><strong>', __( 'Homepage', 'ljmc-seo' ), '</strong><br/>';
				$yform->textinput( 'title-home-ljmcseo', __( 'Title template', 'ljmc-seo' ), 'template homepage-template' );
				$yform->textarea( 'metadesc-home-ljmcseo', __( 'Meta description template', 'ljmc-seo' ), array( 'class' => 'template homepage-template' ) );
				if ( $options['usemetakeywords'] === true ) {
					$yform->textinput( 'metakey-home-ljmcseo', __( 'Meta keywords template', 'ljmc-seo' ) );
				}
				echo '</p>';
			}
			else {
				echo '<p><strong>', __( 'Homepage &amp; Front page', 'ljmc-seo' ), '</strong><br/>';
				printf( __( 'You can determine the title and description for the front page by %sediting the front page itself &raquo;%s', 'ljmc-seo' ), '<a href="' . esc_url( get_edit_post_link( get_option( 'page_on_front' ) ) ) . '">', '</a>' );
				echo '</p>';
				if ( get_option( 'page_for_posts' ) > 0 ) {
					echo '<p>', sprintf( __( 'You can determine the title and description for the blog page by %sediting the blog page itself &raquo;%s', 'ljmc-seo' ), '<a href="' . esc_url( get_edit_post_link( get_option( 'page_for_posts' ) ) ) . '">', '</a>' ), '</p>';
				}
			}
			?>
		</div>
		<div id="post_types" class="ljmcseotab">
			<?php
			$post_types = get_post_types( array( 'public' => true ), 'objects' );
			if ( is_array( $post_types ) && $post_types !== array() ) {
				foreach ( $post_types as $pt ) {
					$warn = false;
					if ( $options['redirectattachment'] === true && $pt->name == 'attachment' ) {
						echo '<div class="ljmcseo-warning">';
						$warn = true;
					}

					$name = $pt->name;
					echo '<strong id="' . esc_attr( $name ) . '">' . esc_html( ucfirst( $pt->labels->name ) ) . '</strong><br/>';
					if ( $warn === true ) {
						echo '<h4 class="error-message">' . __( 'Take note:', 'ljmc-seo' ) . '</h4>';

						echo '<p class="error-message">' . __( 'As you are redirecting attachment URLs to parent post URLs, these settings will currently only have an effect on <strong>unattached</strong> media items!', 'ljmc-seo' ) . '</p>';
						echo '<p class="error-message">' . sprintf( __( 'So remember: If you change the %sattachment redirection setting%s in the future, the below settings will take effect for *all* media items.', 'ljmc-seo' ), '<a href="' . esc_url( admin_url( 'admin.php?page=ljmcseo_advanced&tab=permalinks' ) ) . '">', '</a>' ) . '</p>';
					}

					$yform->textinput( 'title-' . $name, __( 'Title template', 'ljmc-seo' ), 'template posttype-template' );
					$yform->textarea( 'metadesc-' . $name, __( 'Meta description template', 'ljmc-seo' ), array( 'class' => 'template posttype-template' ) );
					if ( $options['usemetakeywords'] === true ) {
						$yform->textinput( 'metakey-' . $name, __( 'Meta keywords template', 'ljmc-seo' ) );
					}
					$yform->checkbox( 'noindex-' . $name, '<code>noindex, follow</code>', __( 'Meta Robots', 'ljmc-seo' ) );
					$yform->checkbox( 'showdate-' . $name, __( 'Show date in snippet preview?', 'ljmc-seo' ), __( 'Date in Snippet Preview', 'ljmc-seo' ) );
					/* translators: %1$s expands to  SEO */
					$yform->checkbox( 'hideeditbox-' . $name, __( 'Hide', 'ljmc-seo' ), sprintf( __( '%1$s Meta Box', 'ljmc-seo' ), ' SEO' ) );

					/**
					 * Allow adding a custom checkboxes to the admin meta page - Post Types tab
					 * @api  LJMCSEO_Admin_Pages  $yform  The LJMCSEO_Admin_Pages object
					 * @api  String  $name  The post type name
					 */
					do_action( 'ljmcseo_admin_page_meta_post_types', $yform, $name );

					echo '<br/><br/>';
					if ( $warn === true ) {
						echo '</div>';
					}
					unset( $warn );
				}
				unset( $pt );
			}
			unset( $post_types );


			$post_types = get_post_types( array( '_builtin' => false, 'has_archive' => true ), 'objects' );
			if ( is_array( $post_types ) && $post_types !== array() ) {
				echo '<h2>' . __( 'Custom Post Type Archives', 'ljmc-seo' ) . '</h2>';
				echo '<p>' . __( 'Note: instead of templates these are the actual titles and meta descriptions for these custom post type archive pages.', 'ljmc-seo' ) . '</p>';

				foreach ( $post_types as $pt ) {
					$name = $pt->name;

					echo '<strong>' . esc_html( ucfirst( $pt->labels->name ) ) . '</strong><br/>';
					$yform->textinput( 'title-ptarchive-' . $name, __( 'Title', 'ljmc-seo' ), 'template posttype-template' );
					$yform->textarea( 'metadesc-ptarchive-' . $name, __( 'Meta description', 'ljmc-seo' ), array( 'class' => 'template posttype-template' ) );
					if ( $options['usemetakeywords'] === true ) {
						$yform->textinput( 'metakey-ptarchive-' . $name, __( 'Meta keywords', 'ljmc-seo' ) );
					}
					if ( $options['breadcrumbs-enable'] === true ) {
						$yform->textinput( 'bctitle-ptarchive-' . $name, __( 'Breadcrumbs title', 'ljmc-seo' ) );
					}
					$yform->checkbox( 'noindex-ptarchive-' . $name, '<code>noindex, follow</code>', __( 'Meta Robots', 'ljmc-seo' ) );

					echo '<br/><br/>';
				}
				unset( $pt );
			}
			unset( $post_types );

			?>
		</div>
		<div id="taxonomies" class="ljmcseotab">
			<?php
			$taxonomies = get_taxonomies( array( 'public' => true ), 'objects' );
			if ( is_array( $taxonomies ) && $taxonomies !== array() ) {
				foreach ( $taxonomies as $tax ) {
					echo '<strong>' . esc_html( ucfirst( $tax->labels->name ) ) . '</strong><br/>';
					$yform->textinput( 'title-tax-' . $tax->name, __( 'Title template', 'ljmc-seo' ), 'template taxonomy-template' );
					$yform->textarea( 'metadesc-tax-' . $tax->name, __( 'Meta description template', 'ljmc-seo' ), array( 'class' => 'template taxonomy-template' ) );
					if ( $options['usemetakeywords'] === true ) {
						$yform->textinput( 'metakey-tax-' . $tax->name, __( 'Meta keywords template', 'ljmc-seo' ) );
					}
					$yform->checkbox( 'noindex-tax-' . $tax->name, '<code>noindex, follow</code>', __( 'Meta Robots', 'ljmc-seo' ) );
					/* translators: %1$s expands to  SEO */
					$yform->checkbox( 'hideeditbox-tax-' . $tax->name, __( 'Hide', 'ljmc-seo' ), sprintf( __( '%1$s Meta Box', 'ljmc-seo' ), ' SEO' ) );
					echo '<br/><br/>';
				}
				unset( $tax );
			}
			unset( $taxonomies );

			?>
		</div>
		<div id="archives" class="ljmcseotab">
			<?php
			echo '<h3>' . __( 'Author Archives', 'ljmc-seo' ) . '</h3>';
			$yform->textinput( 'title-author-ljmcseo', __( 'Title template', 'ljmc-seo' ), 'template author-template' );
			$yform->textarea( 'metadesc-author-ljmcseo', __( 'Meta description template', 'ljmc-seo' ), array( 'class' => 'template author-template' ) );
			if ( $options['usemetakeywords'] === true ) {
				$yform->textinput( 'metakey-author-ljmcseo', __( 'Meta keywords template', 'ljmc-seo' ) );
			}

			echo '<h3>' . __( 'Date Archives', 'ljmc-seo' ) . '</h3>';
			$yform->textinput( 'title-archive-ljmcseo', __( 'Title template', 'ljmc-seo' ), 'template date-template' );
			$yform->textarea( 'metadesc-archive-ljmcseo', __( 'Meta description template', 'ljmc-seo' ), array( 'class' => 'template date-template' ) );
			echo '<br/>';

			echo '<h3>' . __( 'Duplicate content prevention', 'ljmc-seo' ) . '</h3>';
			echo '<p>';
			/* translators: %1$s / %2$s: links to an article about duplicate content on yoast.com */
			printf( __( 'If you\'re running a one author blog, the author archive will be exactly the same as your homepage. This is what\'s called a %1$sduplicate content problem%2$s.', 'ljmc-seo' ), '<a href="https://yoast.com/articles/duplicate-content/">', '</a>' );
			echo '<br />';
			/* translators: %s expands to <code>noindex, follow</code> */
			echo sprintf( __( 'If this is the case on your site, you can choose to either disable it (which makes it redirect to the homepage), or to add %s to it so it doesn\'t show up in the search results.', 'ljmc-seo' ), '<code>noindex,follow</code>' );
			echo '</p>';
			/* translators: %s expands to <code>noindex, follow</code> */
			$yform->checkbox( 'noindex-author-ljmcseo', sprintf( __( 'Add %s to the author archives', 'ljmc-seo' ), '<code>noindex, follow</code>' ) );
			$yform->checkbox( 'disable-author', __( 'Disable the author archives', 'ljmc-seo' ) );
			echo '<p>';
			_e( 'Date-based archives could in some cases also be seen as duplicate content.', 'ljmc-seo' );
			echo '</p>';
			/* translators: %s expands to <code>noindex, follow</code> */
			$yform->checkbox( 'noindex-archive-ljmcseo', sprintf( __( 'Add %s to the date-based archives', 'ljmc-seo' ), '<code>noindex, follow</code>' ) );
			$yform->checkbox( 'disable-date', __( 'Disable the date-based archives', 'ljmc-seo' ) );

			echo '<br/>';

			echo '<h2>' . __( 'Special Pages', 'ljmc-seo' ) . '</h2>';
			/* translators: %s expands to <code>noindex, follow</code> */
			echo '<p>' . sprintf( __( 'These pages will be %s by default, so they will never show up in search results.', 'ljmc-seo' ), '<code>noindex, follow</code>' ) . '</p>';
			echo '<p><strong>' . __( 'Search pages', 'ljmc-seo' ) . '</strong><br/>';
			$yform->textinput( 'title-search-ljmcseo', __( 'Title template', 'ljmc-seo' ), 'template search-template' );
			echo '</p>';
			echo '<p><strong>' . __( '404 pages', 'ljmc-seo' ) . '</strong><br/>';
			$yform->textinput( 'title-404-ljmcseo', __( 'Title template', 'ljmc-seo' ), 'template error404-template' );
			echo '</p>';
			echo '<br class="clear"/>';
			?>
		</div>
		<div id="other" class="ljmcseotab">
			<strong><?php _e( 'Sitewide meta settings', 'ljmc-seo' ); ?></strong><br/>
			<br/>
			<?php
			echo '<p>', __( 'If you want to prevent /page/2/ and further of any archive to show up in the search results, enable this.', 'ljmc-seo' ), '</p>';
			$yform->checkbox( 'noindex-subpages-ljmcseo', __( 'Noindex subpages of archives', 'ljmc-seo' ) );

			echo '<p>', __( 'I don\'t know why you\'d want to use meta keywords, but if you want to, check this box.', 'ljmc-seo' ), '</p>';
			$yform->checkbox( 'usemetakeywords', __( 'Use meta keywords tag?', 'ljmc-seo' ) );

			echo '<p>', __( 'Prevents search engines from using the DMOZ description for pages from this site in the search results.', 'ljmc-seo' ), '</p>';
			/* translators: %s expands to <code>noodp</code> */
			$yform->checkbox( 'noodp', sprintf( __( 'Add %s meta robots tag sitewide', 'ljmc-seo' ), '<code>noodp</code>' ) );

			echo '<p>', __( 'Prevents search engines from using the Yahoo! directory description for pages from this site in the search results.', 'ljmc-seo' ), '</p>';
			/* translators: %s expands to <code>noydir</code> */
			$yform->checkbox( 'noydir', sprintf( __( 'Add %s meta robots tag sitewide', 'ljmc-seo' ), '<code>noydir</code>' ) );

			?>
		</div>

	</div>
<?php
$yform->admin_footer();
