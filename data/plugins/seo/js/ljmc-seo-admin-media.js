/* global ljmcseoMediaL10n */
/* global ajaxurl */
/* global ljmc */
/* jshint -W097 */
/* jshint -W003 */
/* jshint unused:false */
'use strict';
// Taken and adapted from http://www.webmaster-source.com/2013/02/06/using-the-ljmc-3-5-media-uploader-in-your-plugin-or-theme/
jQuery( document ).ready( function( $ ) {
		var ljmcseo_custom_uploader;
		$( '.ljmcseo_image_upload_button' ).click( function( e ) {
				var ljmcseo_target_id = $( this ).attr( 'id' ).replace( /_button$/, '' );
				e.preventDefault();
				if ( ljmcseo_custom_uploader ) {
					ljmcseo_custom_uploader.open();
					return;
				}
				ljmcseo_custom_uploader = ljmc.media.frames.file_frame = ljmc.media( {
						title: ljmcseoMediaL10n.choose_image,
						button: { text: ljmcseoMediaL10n.choose_image },
						multiple: false
					}
				);
				ljmcseo_custom_uploader.on( 'select', function() {
						var attachment = ljmcseo_custom_uploader.state().get( 'selection' ).first().toJSON();
						$( '#' + ljmcseo_target_id ).val( attachment.url );
					}
				);
				ljmcseo_custom_uploader.open();
			}
		);
	}
);
