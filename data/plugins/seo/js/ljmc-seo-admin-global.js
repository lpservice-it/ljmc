/* global ajaxurl */
/* jshint -W097 */
/* jshint unused:false */
'use strict';
/**
 * Used to dismiss the after-update admin notice for a specific user until the next update.
 *
 * @param {string} nonce
 */
function ljmcseoDismissAbout( nonce ) {
	jQuery.post( ajaxurl, {
			action: 'ljmcseo_dismiss_about',
			_ljmcnonce: nonce
		}
	);
}

/**
 * Used to dismiss the tagline notice for a specific user.
 *
 * @param {string} nonce
 */
function ljmcseoDismissTaglineNotice( nonce ) {
	jQuery.post( ajaxurl, {
			action: 'ljmcseo_dismiss_tagline_notice',
			_ljmcnonce: nonce
		}
	);
}

/**
 * Used to remove the admin notices for several purposes, dies on exit.
 *
 * @param {string} option
 * @param {string} hide
 * @param {string} nonce
 */
function ljmcseoSetIgnore( option, hide, nonce ) {
	jQuery.post( ajaxurl, {
			action: 'ljmcseo_set_ignore',
			option: option,
			_ljmcnonce: nonce
		}, function( data ) {
			if ( data ) {
				jQuery( '#' + hide ).hide();
				jQuery( '#hidden_ignore_' + option ).val( 'ignore' );
			}
		}
	);
}

/**
 * Make the notices dismissible (again)
 */
function ljmcseoMakeDismissible() {
	jQuery( '.notice.is-dismissible' ).each( function() {
		var $notice = jQuery( this );
		if ( $notice.find( '.notice-dismiss').empty() ) {
			var	$button = jQuery( '<button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>' );

			$notice.append( $button );

			$button.on( 'click.ljmc-dismiss-notice', function( event ) {
				event.preventDefault();
				$notice.fadeTo( 100 , 0, function() {
					jQuery(this).slideUp( 100, function() {
						jQuery(this).remove();
					});
				});
			});
		}
	});
}

jQuery( document ).ready( function() {
	jQuery( '#ljmcseo-dismiss-about > .notice-dismiss' ).click( function() {
		ljmcseoDismissAbout( jQuery( '#ljmcseo-dismiss-about' ).data( 'nonce' ) );
	});

	jQuery( '#ljmcseo-dismiss-tagline-notice > .notice-dismiss').click( function() {
		ljmcseoDismissTaglineNotice( jQuery( '#ljmcseo-dismiss-tagline-notice').data( 'nonce' ) );
	});

	jQuery( '.yoast-dismissible > .notice-dismiss').click( function() {
		var parent_div = jQuery( this ).parent('.yoast-dismissible');

		jQuery.post(
			ajaxurl,
			{
				action: parent_div.attr( 'id').replace( /-/g, '_' ),
				_ljmcnonce: parent_div.data( 'nonce' ),
				data: parent_div.data( 'json' )
			}
		);
	});
});
