<div class="wrap">
    <div id="icon-ljmcml" class="icon32"><br /></div>
    <h2><?php echo __('Translation options', 'sitepress') ?></h2>
    <br />
    <?php include dirname(__FILE__) . '/_posts_sync_options.php'; ?>

    <?php if(defined('Languages_ST_VERSION')): ?>
    <?php  include Languages_ST_PATH . '/menu/_slug-translation-options.php'; ?>
    <?php endif; ?>

    <br clear="all" />
    <?php include dirname(__FILE__) . '/_custom_types_translation.php'; ?>

    <?php do_action('icl_menu_footer'); ?>
</div>