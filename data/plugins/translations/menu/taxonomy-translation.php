<div class="wrap">
    <div id="icon-ljmcml" class="icon32" style="clear:both" ><br /></div>    
    <h2><?php _e('Taxonomy Translation', 'sitepress') ?></h2>
    
    <br />
    <?php 
    $Languages_Translate_Taxonomy = new Languages_Taxonomy_Translation();
    $Languages_Translate_Taxonomy->render();
    ?>    
    
    <?php do_action('icl_menu_footer'); ?>
    
</div>
