<?php
if(defined('LJMC_ADMIN')) return;

add_action('template_redirect', 'icl_language_canonical_redirects', 1);

function icl_language_canonical_redirects () {
    global $ljmc_query, $sitepress_settings;
    if(3 == $sitepress_settings['language_negotiation_type'] && is_singular() && empty($ljmc_query->posts)){
        global $ljmcdb;
        $pid = get_query_var('p');
        $permalink = html_entity_decode(get_permalink($pid));
        if($permalink){
            ljmc_redirect($permalink, 301);
            exit;
        }
    } 
    
}  

?>
