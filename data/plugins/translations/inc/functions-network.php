<?php
 
if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'resetljmcml'){    
    include_once ICL_PLUGIN_PATH . '/inc/functions-troubleshooting.php';    
}


add_action('network_admin_menu', 'icl_network_administration_menu');


/*
add_action('network_admin_edit_resetljmcml', 'icl_reset_ljmcml');
add_action('network_admin_edit_deactivateljmcml', 'icl_network_deactivate_ljmcml');
add_action('network_admin_edit_activateljmcml', 'icl_network_activate_ljmcml');
*/

add_action('ljmcmuadminedit', 'icl_ljmcmuadminedit');
function icl_ljmcmuadminedit(){
    if(!isset($_REQUEST['action'])) return;

	switch($_REQUEST['action']){
        case 'resetljmcml':  icl_reset_ljmcml(); break;
        case 'deactivateljmcml':  icl_network_deactivate_ljmcml(); break;
        case 'activateljmcml':  icl_network_activate_ljmcml(); break;
    }
}


function icl_network_administration_menu(){
    global $sitepress;
    add_action('admin_print_styles', array($sitepress,'css_setup'));
    add_menu_page(__('Languages','sitepress'), __('Languages','sitepress'), 'manage_sitess', 
        basename(ICL_PLUGIN_PATH).'/menu/network.php', null, ICL_PLUGIN_URL . '/res/img/icon16.png');
    add_submenu_page(basename(ICL_PLUGIN_PATH).'/menu/network.php', 
        __('Network settings','sitepress'), __('Network settings','sitepress'),
        'manage_sites', basename(ICL_PLUGIN_PATH).'/menu/network.php');
}

function icl_network_deactivate_ljmcml($blog_id = false){
    
    if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'deactivateljmcml'){
        check_admin_referer( 'deactivateljmcml' );    
    }
    
    if(empty($blog_id)){
        $blog_id = isset($_POST['id']) ? $_POST['id'] : $ljmcdb->blogid;
    }
      
    if($blog_id){
        switch_to_blog($blog_id);
        update_option('_ljmcml_inactive', true);
        restore_current_blog();
    }    
    
    if(isset($_REQUEST['submit'])){            
        ljmc_redirect(network_admin_url('admin.php?page='.ICL_PLUGIN_FOLDER.'/menu/network.php&updated=true&action=deactivateljmcml'));
        exit();
    }
    
    
}

function icl_network_activate_ljmcml($blog_id = false){
    
    if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'activateljmcml'){
        if (empty($_REQUEST['_ljmcnonce']) || $_REQUEST['_ljmcnonce'] != ljmc_create_nonce( 'activateljmcml' ) ){
            return;
        }    
    }
    
    if(empty($blog_id)){
        $blog_id = isset($_REQUEST['id']) ? $_REQUEST['id'] : $ljmcdb->blogid;
    }
      
    if($blog_id){
        switch_to_blog($blog_id);
        delete_option('_ljmcml_inactive');
        restore_current_blog();
    } 
    
    ljmc_redirect(network_admin_url('admin.php?page='.ICL_PLUGIN_FOLDER.'/menu/network.php&updated=true&action=activateljmcml'));
    exit();
       
    
}