<?php
  
  // Languages Sunrise Script - START
  // Version 1.0beta
  // Place this script in the ljmc-content folder and add "define('SUNRISE', 'on');" in ljmc-config.php n order to enable using different domains for different languages in multisite mode
  // 
  // Experimental feature
  define('Languages_SUNRISE_MULTISITE_DOMAINS', true);
  add_filter('query', 'sunrise_ljmcml_filter_queries');
  function sunrise_ljmcml_filter_queries($q){
      global $ljmcdb, $table_prefix, $current_blog;
        
      static $no_recursion;  
      
      if(empty($current_blog) && empty($no_recursion)){
          
          $no_recursion = true;      
          
          if(preg_match("#SELECT \\* FROM {$ljmcdb->blogs} WHERE domain = '(.*)'#", $q, $matches)){
              
              if(!$ljmcdb->get_row($q)){
                  $icl_blogs = $ljmcdb->get_col("SELECT blog_id FROM {$ljmcdb->blogs}");
                  foreach($icl_blogs as $blog_id){
                      $prefix = $blog_id > 1 ? $table_prefix . $blog_id . '_' : $table_prefix;                
                      $icl_settings = $ljmcdb->get_var("SELECT option_value FROM {$prefix}options WHERE option_name='icl_sitepress_settings'");
                      if($icl_settings){
                          $icl_settings = unserialize($icl_settings);                                                                        
                          if($icl_settings && $icl_settings['language_negotiation_type'] == 2){
                              if(in_array('http://' . $matches[1], $icl_settings['language_domains'])){
                                  $found_blog_id = $blog_id;
                                  break;
                              }
                          }
                      }
                  }
                
                  if($found_blog_id){
                      $q = "SELECT * FROM {$ljmcdb->blogs} WHERE blog_id = '" . $found_blog_id ."'";    
                  }  
              }
                          
          }
          
          $no_recursion = false;      
          
      }
      
      
      return $q;
  }
  // Languages Sunrise Script - END
  
?>
