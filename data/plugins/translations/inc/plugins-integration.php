<?php

add_action('plugins_loaded', 'ljmcml_plugins_integration_setup', 10);


function ljmcml_plugins_integration_setup(){
    // LJMCSEO XML Sitemaps integration
    if ( defined( 'LJMCSEO_VERSION' ) && version_compare( LJMCSEO_VERSION, '1.0.3', '>=' ) ){
        require_once ICL_PLUGIN_PATH . '/inc/ljmcseo-sitemaps-filter.php';
    }
    // NextGen Gallery
    if ( defined( 'NEXTGEN_GALLERY_PLUGIN_VERSION' ) ){
        require_once ICL_PLUGIN_PATH . '/inc/plugin-integration-nextgen.php';
    }
}