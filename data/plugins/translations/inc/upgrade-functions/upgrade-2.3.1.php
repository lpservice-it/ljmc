<?php
  
$iclsettings = get_option('icl_sitepress_settings');

if($iclsettings['theme_localization_type'] == 2 && !empty($iclsettings['gettext_theme_domain_name'])){
    $iclsettings['theme_localization_load_textdomain'] = 1;
    update_option('icl_sitepress_settings', $iclsettings);
}elseif(empty($iclsettings['theme_localization_type'])){
    $iclsettings['theme_localization_type'] = 2;
    $iclsettings['theme_localization_load_textdomain'] = 0;
    update_option('icl_sitepress_settings', $iclsettings);
}

$sql = "ALTER TABLE {$ljmcdb->prefix}icl_locale_map CHANGE locale VARCHAR(32) NOT NULL";
$ljmcdb->query($sql);


$ljmcdb->query("DELETE m FROM {$ljmcdb->postmeta} m JOIN {$ljmcdb->posts} p ON p.ID = m.post_id WHERE m.meta_key='_alp_processed' AND p.post_type='nav_menu_item'");
  
?>
