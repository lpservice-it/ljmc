<?php
  
  $keys = $ljmcdb->get_results("SHOW KEYS FROM `{$ljmcdb->prefix}icl_translations` WHERE `Column_name`='trid' AND `Key_name`<>'trid_lang'");
  if(empty($keys)){
      $sql = "ALTER TABLE `{$ljmcdb->prefix}icl_translations` ADD KEY `trid` (`trid`)";
      $ljmcdb->query($sql);
  }
  
  $sql = "ALTER TABLE `{$ljmcdb->prefix}icl_languages` ADD tag VARCHAR(8)";
  $ljmcdb->query($sql);
  $sql = "UPDATE `{$ljmcdb->prefix}icl_languages` SET tag = REPLACE(default_locale, '_', '-')";
  $ljmcdb->query($sql);
  icl_cache_clear();
  
?>
