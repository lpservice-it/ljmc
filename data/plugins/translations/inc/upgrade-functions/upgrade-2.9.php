<?php
  
  $ms = array(
        
        'code'          => 'ms',
        'english_name'  => 'Malay',
        'major'         => 0,
        'active'        => 0,
        'default_locale'=> 'ms_MY',
        'tag'           => 'ms-MY',
        'encode_url'    => 0        
  );
  
  $ljmcdb->insert($ljmcdb->prefix . 'icl_languages', $ms);
  
  
  $ljmcdb->insert($ljmcdb->prefix . 'icl_languages_translations', array('language_code' => 'ms', 'display_language_code' => 'en', 'name' => 'Malay'));
  $ljmcdb->insert($ljmcdb->prefix . 'icl_languages_translations', array('language_code' => 'ms', 'display_language_code' => 'es', 'name' => 'Malayo'));
  $ljmcdb->insert($ljmcdb->prefix . 'icl_languages_translations', array('language_code' => 'ms', 'display_language_code' => 'de', 'name' => 'Malay'));
  $ljmcdb->insert($ljmcdb->prefix . 'icl_languages_translations', array('language_code' => 'ms', 'display_language_code' => 'fr', 'name' => 'Malay'));
  $ljmcdb->insert($ljmcdb->prefix . 'icl_languages_translations', array('language_code' => 'ms', 'display_language_code' => 'ms', 'name' => 'Melayu'));
  
  $ljmcdb->insert($ljmcdb->prefix . 'icl_flags', array('lang_code' => 'ms', 'flag' => 'ms.png', 'from_template' => 0));
  
  
?>
