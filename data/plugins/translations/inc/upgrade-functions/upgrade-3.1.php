<?php

// Add a new `language_context` index to icl_strings table
$sql = "ALTER TABLE `{$ljmcdb->prefix}icl_translate` CHANGE field_data field_data longtext NOT NULL";
$ljmcdb->query($sql);
$sql = "ALTER TABLE `{$ljmcdb->prefix}icl_translate` CHANGE field_data_translated field_data_translated longtext NOT NULL";
$ljmcdb->query($sql);
