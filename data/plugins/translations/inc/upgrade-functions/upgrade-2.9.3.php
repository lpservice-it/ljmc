<?php

$widget_strings = $ljmcdb->get_results("SELECT * FROM {$ljmcdb->prefix}icl_strings WHERE context = 'Widgets' AND name LIKE 'widget body - %'");
foreach($widget_strings as $string){
    $ljmcdb->update($ljmcdb->prefix . 'icl_strings', array('name' => 'widget body - ' . md5($string->value)), array('id' => $string->id));
}  

$widget_strings = $ljmcdb->get_results("SELECT * FROM {$ljmcdb->prefix}icl_strings WHERE context = 'Widgets' AND name LIKE 'widget title - %'");
foreach($widget_strings as $string){
    $ljmcdb->update($ljmcdb->prefix . 'icl_strings', array('name' => 'widget title - ' . md5($string->value)), array('id' => $string->id));
}  


// Add a new `language_context` index to icl_strings table
$sql = "ALTER TABLE `{$ljmcdb->prefix}icl_strings` ADD INDEX `language_context` ( `context` , `language` )";
$ljmcdb->query($sql);

?>
