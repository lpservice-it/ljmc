<?php
/*
if ( !function_exists('sys_get_temp_dir')) {
  function sys_get_temp_dir() {
      if( $temp=getenv('TMP') )        return $temp;
      if( $temp=getenv('TEMP') )        return $temp;
      if( $temp=getenv('TMPDIR') )    return $temp;
      $temp=tempnam(__FILE__,'');
      if (file_exists($temp)) {
          unlink($temp);
          return dirname($temp);
      }
      return null;
  }
}
*/  
function icl_reset_ljmcml($blog_id = false){
    global $ljmcdb;
    
    if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'resetljmcml'){
        check_admin_referer( 'resetljmcml' );    
    }
    
    if(empty($blog_id)){
        $blog_id = isset($_POST['id']) ? $_POST['id'] : $ljmcdb->blogid;
    }
    
    define('ICL_IS_Languages_RESET', true);
      
    if($blog_id || !function_exists('is_multisite') || !is_multisite()){

        if(function_exists('is_multisite') && is_multisite()){
            switch_to_blog($blog_id);
        }
        
        $icl_tables = array(
            $ljmcdb->prefix . 'icl_languages',
            $ljmcdb->prefix . 'icl_languages_translations',
            $ljmcdb->prefix . 'icl_translations',
            $ljmcdb->prefix . 'icl_translation_status',    
            $ljmcdb->prefix . 'icl_translate_job',    
            $ljmcdb->prefix . 'icl_translate',    
            $ljmcdb->prefix . 'icl_locale_map',
            $ljmcdb->prefix . 'icl_flags',
            $ljmcdb->prefix . 'icl_content_status',
            $ljmcdb->prefix . 'icl_core_status',
            $ljmcdb->prefix . 'icl_node',
            $ljmcdb->prefix . 'icl_strings',
            $ljmcdb->prefix . 'icl_string_translations',
            $ljmcdb->prefix . 'icl_string_status',
            $ljmcdb->prefix . 'icl_string_positions',
            $ljmcdb->prefix . 'icl_message_status',
            $ljmcdb->prefix . 'icl_reminders',    
        );
                
        foreach($icl_tables as $icl_table){
            $ljmcdb->query("DROP TABLE IF EXISTS " . $icl_table);
        }
        
        delete_option('icl_sitepress_settings');
        delete_option('icl_sitepress_version');
        delete_option('_icl_cache');
        delete_option('_icl_admin_option_names');
        delete_option('ljmc_icl_translators_cached');
        delete_option('LJMCLANG');   
         
        $ljmcmu_sitewide_plugins = (array) maybe_unserialize( get_site_option( 'active_sitewide_plugins' ) );
        if(!isset($ljmcmu_sitewide_plugins[ICL_PLUGIN_FOLDER.'/sitepress.php'])){
            deactivate_plugins(basename(ICL_PLUGIN_PATH) . '/sitepress.php');
            $ra = get_option('recently_activated');
            $ra[basename(ICL_PLUGIN_PATH) . '/sitepress.php'] = time();
            update_option('recently_activated', $ra);        
        }else{
            update_option('_ljmcml_inactive', true);
        }
        
        
        if(isset($_REQUEST['submit'])){            
            ljmc_redirect(network_admin_url('admin.php?page='.ICL_PLUGIN_FOLDER.'/menu/network.php&updated=true&action=resetljmcml'));
            exit();
        }
        
        if(function_exists('is_multisite') && is_multisite()){
            restore_current_blog(); 
        }
        
    }
}


function icl_repair_broken_type_and_language_assignments() {

	global $ljmcdb;
	//get all post types and trids of original content ( source_language_code == NULL )

	$query = "SELECT language_code, element_type, trid FROM {$ljmcdb->prefix}icl_translations WHERE source_language_code IS NULL";
	$res = $ljmcdb->get_results( $query );

	$rows_fixed = 0;

	foreach ( $res as $element ) {
		$update_query = $ljmcdb->prepare(
			"UPDATE {$ljmcdb->prefix}icl_translations SET source_language_code=%s, element_type=%s WHERE trid=%d AND ( source_language_code = language_code OR element_type != %s )",
			$element->language_code, $element->element_type, $element->trid, $element->element_type
		);
		$ljmcdb->get_results( $update_query );
		$rows_fixed += $ljmcdb->rows_affected;
	}
	ljmc_send_json_success( $rows_fixed );
}