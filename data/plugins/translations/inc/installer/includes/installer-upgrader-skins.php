<?php
  class Installer_Upgrader_Skins extends LJMC_Upgrader_Skin{
      
      function __construct($args = array()){
          $defaults = array( 'url' => '', 'nonce' => '', 'title' => '', 'context' => false );
          $this->options = ljmc_parse_args($args, $defaults);
      }
      
      function header(){
          
      }
      
      function footer(){
          
      }
      
      function add_strings(){
          
      }
      
      function feedback($string){
          
      }
      
      function before(){
          
      }
      
      function after(){
          
      }
      
  }