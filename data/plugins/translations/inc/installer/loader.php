<?php
/*
Plugin Name: Installer
Plugin URI: http://ljmc-compatibility.com/installer-plugin/
Description: Need help buying, installing and upgrading commercial themes and plugins? **Installer** handles all this for you, right from the LJMC admin. Installer lets you find themes and plugins from different sources, then, buy them from within the LJMC admin. Instead of manually uploading and unpacking, you'll see those themes and plugins available, just like any other plugin you're getting from LJMC.org.
Version: 1.2
Author: OnTheGoSystems Inc.     
Author URI: http://www.onthegosystems.com/
*/

  
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


$ljmc_installer_instance = dirname(__FILE__) . '/installer.php';

// Global stack of instances
$ljmc_installer_instances[$ljmc_installer_instance] = array(
    'bootfile'  => $ljmc_installer_instance,
    'version'   => 1.2
);

// Only one of these in the end
remove_action('after_setup_theme', 'ljmcml_installer_instance_delegator', 1);
add_action('after_setup_theme', 'ljmcml_installer_instance_delegator', 1);

// When all plugins load pick the newest version
if(!function_exists('ljmcml_installer_instance_delegator')){
    function ljmcml_installer_instance_delegator(){
        global $ljmc_installer_instances;
        
        foreach($ljmc_installer_instances as $instance){
            
            if(!isset($delegate)){
                $delegate = $instance;
                continue;
            }
            
            if(version_compare($instance['version'], $delegate['version'], '>')){
                $delegate = $instance;    
            }
        }
        
        include_once $delegate['bootfile'];
        
        // set configuration
        if(strpos(realpath($delegate['bootfile']), realpath(TEMPLATEPATH)) === 0){
            $delegate['args']['in_theme_folder'] = dirname(ltrim(str_replace(realpath(TEMPLATEPATH), '', realpath($delegate['bootfile'])), '\\/'));            
        }        
        if(isset($delegate['args']) && is_array($delegate['args'])){
            foreach($delegate['args'] as $key => $value){                
                LJMC_Installer()->set_config($key, $value);                
            }
        }
        
    }
}  

if(!function_exists('LJMC_Installer_Setup')){
    
    // $args:
    // plugins_install_tab = true|false (default: true) 
    // repositories_include = array() (default: all)
    // repositories_exclude = array() (default: none)
    // template = name (default: default)            
    // 
    // Ext function 
    function LJMC_Installer_Setup($ljmc_installer_instance, $args = array()){
        global $ljmc_installer_instances;
        
        //if(isset($ljmc_installer_instances[$ljmc_installer_instance])){
            $ljmc_installer_instances[$ljmc_installer_instance]['args'] = $args;
        //}

    }
    
}

