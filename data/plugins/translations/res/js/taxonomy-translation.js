var Languages_Translate_taxonomy = {
    
    init: function(){
        
        jQuery(document).delegate('#icl_tt_tax_switch', 'change', Languages_Translate_taxonomy.switch_taxonomy);
        
        
        jQuery(document).delegate('#ljmcml_tt_filters', 'submit', Languages_Translate_taxonomy.filter_taxonomies);
        /*jQuery(document).delegate('#ljmcml_tt_search', 'submit', Languages_Translate_taxonomy.search_taxonomies);*/
        jQuery(document).delegate('#ljmcml_tt_clear_search', 'click', Languages_Translate_taxonomy.clear_search_taxonomies);
        
        
        jQuery(document).delegate('.icl_tt_form', 'submit', Languages_Translate_taxonomy.save_term_translation);        
        
        jQuery(document).delegate('.icl_tt_form .cancel', 'click', Languages_Translate_taxonomy.hide_form);
        jQuery(document).delegate('.icl_tt_labels_form .cancel', 'click', Languages_Translate_taxonomy.hide_labels_form);
        
        jQuery(document).delegate('#ljmcml_tt_taxonomy_translation_wrap .tablenav-pages a.first-page', 'click', Languages_Translate_taxonomy.navigate_first);
        jQuery(document).delegate('#ljmcml_tt_taxonomy_translation_wrap .tablenav-pages a.prev-page', 'click', Languages_Translate_taxonomy.navigate_prev);
        jQuery(document).delegate('#ljmcml_tt_taxonomy_translation_wrap .tablenav-pages a.next-page', 'click', Languages_Translate_taxonomy.navigate_next);
        jQuery(document).delegate('#ljmcml_tt_taxonomy_translation_wrap .tablenav-pages a.last-page', 'click', Languages_Translate_taxonomy.navigate_last);
        jQuery(document).delegate('#ljmcml_tt_taxonomy_translation_wrap .tablenav-pages input.current-page', 'change', Languages_Translate_taxonomy.navigate_to);
        
        jQuery(document).delegate('.icl_tt_labels_form', 'submit', Languages_Translate_taxonomy.save_labels_translation);
        
        jQuery(document).delegate('#icl_tt_sync_assignment', 'click', Languages_Translate_taxonomy.sync_taxonomies_in_content);
        
        jQuery(document).delegate('form.icl_tt_do_sync a.submit', 'click', Languages_Translate_taxonomy.sync_taxonomies_do_sync);
        
        
    },
    
    switch_taxonomy: function(){

        jQuery('#ljmcml_tt_filters').find('input[name=taxonomy]').val(jQuery(this).val());
        
        Languages_Translate_taxonomy.filter_taxonomies();
        
    },
    
    show_terms: function(parameters){
        
        Languages_Translate_taxonomy.working_start();
        
        jQuery.ajax({
            type:       "POST", 
            url:        ajaxurl, 
            data:       'action=ljmcml_tt_show_terms&' + parameters,
            success:    
                function(ret){                
                    jQuery('#ljmcml_tt_taxonomy_translation_wrap').html(ret);
	                  Languages_Translate_taxonomy.working_end();
                }   
                
            });    
        
        return false;
        
        
    },
    
    filter_taxonomies: function(){
        
        var parameters = jQuery('#ljmcml_tt_filters').serialize();
        
        Languages_Translate_taxonomy.show_terms(parameters);
        
        return false;
        
    },
    
    /*
    search_taxonomies: function(){
        
        var parameters = jQuery('#ljmcml_tt_search').serialize();
        
        Languages_Translate_taxonomy.show_terms(parameters);

        return false;
        
    },
    */
    
    clear_search_taxonomies: function(){
        jQuery('#ljmcml_tt_filters').find("input[name=search]").val('');
        Languages_Translate_taxonomy.show_terms(jQuery('#ljmcml_tt_filters').serialize());
        return false;    
    },
    
    working_start: function(){
        jQuery('.icl_tt_tools .ljmcml_tt_spinner').fadeIn();
        jQuery('#ljmcml_tt_taxonomy_translation_wrap input, #ljmcml_tt_taxonomy_translation_wrap select, #ljmcml_tt_taxonomy_translation_wrap textarea').attr('disabled', 'disabled');
    },
    
    working_end: function(){
        jQuery('.icl_tt_tools .ljmcml_tt_spinner').fadeOut();
        jQuery('#ljmcml_tt_taxonomy_translation_wrap input, #ljmcml_tt_taxonomy_translation_wrap select, #ljmcml_tt_taxonomy_translation_wrap textarea').removeAttr('disabled');
    },
    
    show_form: function(tt_id, language){
        
        jQuery('.icl_tt_form').hide();
        jQuery('.icl_tt_form').prev().show();
        
        var form = jQuery('#icl_tt_form_' + tt_id+'_'+language);
        
        if(!form.is(':visible')){
            form.prev().hide();
            form.show();                
        }else{
            Languages_Translate_taxonomy.hide_form(form);    
        }
        
        return false;
        
    },
    
    hide_form: function(form){
        
        if(!form || form.type == 'click'){
            var form = jQuery(this).closest('.icl_tt_form');
        }
        
        form.hide(100, function(){
            form.find('.errors').html('');
        });
        form.prev().show();
        
    },
    
    show_labels_form: function(taxonomy, language){
        
        jQuery('.icl_tt_labels_form').hide();
        
        var form = jQuery('#icl_tt_labels_form_' + taxonomy+'_'+language);
        
        if(!form.is(':visible')){
            form.prev().hide();
            form.show();                
        }else{
            Languages_Translate_taxonomy.hide_labels_form(form);    
        }
        
        
        return false;
        
    },
    
    hide_labels_form: function(form){
        
        if(!form || form.type == 'click'){
            var form = jQuery(this).closest('.icl_tt_labels_form');
        }
                
        form.hide(100, function(){
            form.find('.errors').html('');
        });        
        form.prev().show();
        
    },
    
    callbacks: jQuery.Callbacks(),
    
    save_term_translation: function(){
    
        this_form = jQuery(this);
        var parameters = jQuery(this).serialize();
        
        this_form.find('.errors').html('');        
        this_form.find('.ljmcml_tt_spinner').fadeIn();
        this_form.find('textarea,input').attr('disabled', 'disabled');
        
        jQuery.ajax({
            type:       "POST", 
            dataType:   'json',
            url:        ajaxurl, 
            data:       'action=ljmcml_tt_save_term_translation&' + parameters,
            success:    
                function(ret){                
                    this_form.find('.ljmcml_tt_spinner').fadeOut();
                    this_form.find('textarea,input').removeAttr('disabled');                                            
                    if(ret.errors){
                        
                        this_form.find('.errors').html(ret.errors);    
                        
                    }else{                        
                        this_form.find('input[name=slug]').val(ret.slug);
                        this_form.find('input[name=submit]').val('Update');
                        Languages_Translate_taxonomy.hide_form(this_form);
//                        this_form.prev().html(this_form.find('input[name=name]').val()).removeClass('lowlight');
						this_form.prev().html(this_form.find('input[name=term_leveled]').val() + this_form.find('input[name=name]').val()).removeClass('lowlight');

                        Languages_Translate_taxonomy.callbacks.fire('ljmcml_tt_save_term_translation', this_form.find('input[name=taxonomy]').val());
                        
                    }
                }   
                
            });    
        
        return false;
        
    },
    
    navigate: function(page){

        var parameters = jQuery('#ljmcml_tt_filters').serialize() + '&' + jQuery('#ljmcml_tt_search').serialize() + '&page=' + page;
        
        Languages_Translate_taxonomy.show_terms(parameters);

        return false;
        
        
    },
    
    navigate_first: function(){
        if(!jQuery(this).hasClass('disabled')){
            Languages_Translate_taxonomy.navigate(1);    
        }        
        return false;
    },
    
    navigate_prev: function(){
        if(!jQuery(this).hasClass('disabled')){
            var current_page = jQuery('#ljmcml_tt_taxonomy_translation_wrap .tablenav-pages input.current-page').val();
            Languages_Translate_taxonomy.navigate(current_page - 1);    
        }        
        return false;
    },
    
    navigate_next: function(){        
        if(!jQuery(this).hasClass('disabled')){
            var current_page = jQuery('#ljmcml_tt_taxonomy_translation_wrap .tablenav-pages input.current-page').val();
            Languages_Translate_taxonomy.navigate(parseInt(current_page) + 1);    
        }        
        return false;
    },
    
    navigate_last: function(){
        if(!jQuery(this).hasClass('disabled')){
            var total_pages = jQuery('#ljmcml_tt_taxonomy_translation_wrap .tablenav-pages .total-pages').html();
            Languages_Translate_taxonomy.navigate(total_pages);    
        }       
        return false; 
    },
    
    navigate_to: function(){        
        var total_pages = jQuery('#ljmcml_tt_taxonomy_translation_wrap .tablenav-pages .total-pages').html();
        
        if(jQuery(this).val() > total_pages){
            jQuery(this).val(total_pages);
        }
        
        Languages_Translate_taxonomy.navigate(jQuery(this).val());                   
        return false; 
    },
    
    save_labels_translation: function(){
    
        var this_form = jQuery(this);
        var parameters = jQuery(this).serialize();
        
        this_form.find('.errors').html('');
        
        this_form.find('.ljmcml_tt_spinner').fadeIn();
        this_form.find('textarea,input').attr('disabled', 'disabled');
        
        jQuery.ajax({
            type:       "POST", 
            dataType:   'json',
            url:        ajaxurl, 
            data:       'action=ljmcml_tt_save_labels_translation&' + parameters,
            success:    
                function(ret){                
                    
                    this_form.find('.ljmcml_tt_spinner').fadeOut();
                    this_form.find('textarea,input').removeAttr('disabled');                                            
                    
                    if(ret.errors){
                        this_form.find('.errors').html(ret.errors);    
                    }else{
                        Languages_Translate_taxonomy.hide_form(this_form);
                        jQuery('.icl_tt_labels_' + this_form.find('input[name=taxonomy]').val() + '_' + 
                            this_form.find('input[name=language]').val()).html(this_form.find('input[name=singular]').val() + 
                            ' / ' + this_form.find('input[name=general]').val()).removeClass('lowlight');
                    }
                    
                }   
                
            });    
        
        return false;
        
    },
        
    sync_taxonomies_in_content: function(){
        
        var this_form = jQuery(this);
        var parameters = jQuery(this).serialize();
        
        this_form.find('.ljmcml_tt_spinner').fadeIn();
        this_form.find('input').attr('disabled', 'disabled');
        
        jQuery('.icl_tt_sync_row').remove();
        
        jQuery.ajax({
            type:       "POST", 
            dataType:   'json',
            url:        ajaxurl, 
            data:       'action=ljmcml_tt_sync_taxonomies_in_content_preview&' + parameters,
            success:    
                function(ret){                
                    
                    this_form.find('.ljmcml_tt_spinner').fadeOut();
                    this_form.find('input').removeAttr('disabled');                                            
                    
                    if(ret.errors){
                        this_form.find('.errors').html(ret.errors);    
                    }else{
                        jQuery('#icl_tt_sync_preview').html(ret.html);    
                    }
                    
                }   
                
            });         
                
        return false;
        
    },
    
    sync_taxonomies_do_sync: function (){
        var this_form = jQuery(this).closest('form');
        
        var parameters = this_form.serialize();
        
        this_form.find('.ljmcml_tt_spinner').fadeIn();
        this_form.find('input').attr('disabled', 'disabled');
        
        jQuery.ajax({
            type:       "POST", 
            dataType:   'json',
            url:        ajaxurl, 
            data:       'action=ljmcml_tt_sync_taxonomies_in_content&' + parameters,
            success:    
                function(ret){                
                    
                    this_form.find('.ljmcml_tt_spinner').fadeOut();
                    this_form.find('input').removeAttr('disabled');                                            
                    
                    if(ret.errors){
                        this_form.find('.errors').html(ret.errors);    
                    }else{                        
                        this_form.closest('.icl_tt_sync_row').html(ret.html);    
                    }
                    
                }   
                
            });         
                
        return false;
        
        
        
    }
    
    
    
    
    
    
}

jQuery(document).ready(Languages_Translate_taxonomy.init);