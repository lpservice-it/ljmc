<?php
if ( !isset($ljmc_did_header) ) :

$ljmc_did_header = true;


define( 'ABSPATH', dirname(__FILE__) . '/' );

error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR );

/*
	 * If configuration.php exists in the system root, or if it exists in the root and system-setup.php
	 * doesn't, load configuration.php. The secondary check for system-setup.php has the added benefit
	 * of avoiding cases where the current directory is a nested installation, e.g. / is system(a)
	 * and /blog/ is system(b).
	 *
	 * If neither set of conditions is true, initiate loading the setup process.
	 */
if ( file_exists( ABSPATH . 'configuration.php') ) {


require_once( ABSPATH . 'configuration.php' );

} elseif ( file_exists( dirname(ABSPATH) . '/configuration.php' ) && ! file_exists( dirname(ABSPATH) . '/system-setup.php' ) ) {


require_once( dirname(ABSPATH) . '/configuration.php' );

} else {

// A config file doesn't exist

define( 'LJMCINC', 'inc' );
require_once( ABSPATH . LJMCINC . '/load.php' );

// Standardize $_SERVER variables across setups.
 ljmc_fix_server_vars();

require_once( ABSPATH . LJMCINC . '/functions.php' );

$path = ljmc_guess_url() . '/ljmc-admin/setup-config.php';

/*
		 * We're going to redirect to setup-config.php. While this shouldn't result
		 * in an infinite loop, that's a silly thing to assume, don't you think? If
		 * we're traveling in circles, our last-ditch effort is "Need more help?"
		 */
if ( false === strpos( $_SERVER['REQUEST_URI'], 'setup-config' ) ) {
header( 'Location: ' . $path );
exit;
}

define( 'LJMC_CONTENT_DIR', ABSPATH . 'data' );
require_once( ABSPATH . LJMCINC . '/version.php' );

ljmc_check_php_mysql_versions();
ljmc_load_translations_early();

// Die with an error message
 $die = __( "There doesn't seem to be a <code>configuration.php</code> file. I need this before we can get started." ) . '</p>';
$die .= '<p>' . __( "Need more help? <a href='localhost/Editing_configuration.php'>We got it</a>." ) . '</p>';
$die .= '<p>' . __( "You can create a <code>configuration.php</code> file through a web interface, but this doesn't work for all server setups. The safest way is to manually create the file." ) . '</p>';
$die .= '<p><a href="' . $path . '" class="button button-large">' . __( "Create a Configuration File" ) . '</a>';

ljmc_die( $die, __( 'system &rsaquo; Error' ) );
}

ljmc();


if ( defined('LJMC_USE_THEMES') && LJMC_USE_THEMES )

do_action( 'template_redirect' );


if ( 'HEAD' === $_SERVER['REQUEST_METHOD'] && apply_filters( 'exit_on_http_head', true ) )
exit();

// Process feeds and trackbacks even if not using themes.
 if ( is_robots() ) :

do_action( 'do_robots' );
return;
elseif ( is_feed() ) :
do_feed();
return;
elseif ( is_trackback() ) :
include( ABSPATH . 'trackping.php' );
return;
endif;

if ( defined('LJMC_USE_THEMES') && LJMC_USE_THEMES ) :
$template = false;
if ( is_404() && $template = get_404_template() ) :
elseif ( is_search() && $template = get_search_template() ) :
elseif ( is_front_page() && $template = get_front_page_template() ) :
elseif ( is_home() && $template = get_home_template() ) :
elseif ( is_post_type_archive() && $template = get_post_type_archive_template() ) :
elseif ( is_tax() && $template = get_taxonomy_template() ) :
elseif ( is_attachment() && $template = get_attachment_template() ) :
remove_filter('the_content', 'prepend_attachment');
elseif ( is_single() && $template = get_single_template() ) :
elseif ( is_page() && $template = get_page_template() ) :
elseif ( is_category() && $template = get_category_template() ) :
elseif ( is_tag() && $template = get_tag_template() ) :
elseif ( is_author() && $template = get_author_template() ) :
elseif ( is_date() && $template = get_date_template() ) :
elseif ( is_archive() && $template = get_archive_template() ) :
elseif ( is_comments_popup() && $template = get_comments_popup_template() ) :
elseif ( is_paged() && $template = get_paged_template() ) :
else :
$template = get_index_template();
endif;

if ( $template = apply_filters( 'template_include', $template ) )
include( $template );
return;
endif;
endif;
?>